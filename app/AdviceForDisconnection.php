<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdviceForDisconnection extends Model
{
    protected $table = "tab_advice74";
    protected $fillable = [
      'sysId',
      'month',
      'inputCode',
      'sheetNo',
      'pageNo',
      'billingCycle',
      'billingGroup',
      'subdivisionCode',
      'ledgerGroup',
      'accountNo',
      'checkDigit',
      'dateOfAffecting',
      'meterReadingAffecting',
      'connectionStatusCode',
      'remarks',
      'cUser',
      'cDate',
      'cTime',
      'uUser',
      'uDate',
      'uTime',
      'uReason',
      'dUser',
      'dReason',
      'eFlag',
      'eDate',
      'eTime',
      'eUser',
    ];
}
