<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdviceForPosting extends Model
{
  protected $fillable = [
    'sysId',
    'month',
    'inputCode',
    'sheetNo',
    'pageNo',
    'numberOfEnties',
    'subdivisionCode',
    'ledgerGroup',
    'accountNo',
    'checkDigit',
    'sundryAllowanceAmount',
    'registerNo',
    'itemNo',
    'cashDeposit',
    'dateOfPayment',
    'ccrParticular',
    'remarks',
  ];
}
