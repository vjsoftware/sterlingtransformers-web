<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BatchDataForm extends Model
{
  protected $fillable = [
     '_token',
     'sysId',
     'month',
     'inputCode',
     'trType',
     'subdivisionCode',
     'octraiCode',
     'ledgerGroup',
     'billingGroup',
     'billingSubGroup',
     'villageName',
     'accountNo',
     'consumerName',
     'address',
     'feederCode',
     'tariffType',
     'employeeConsessionCode',
     'voltClassCode',
     'connectedLoad',
     'meterRatio',
     'connectionDate',
     'lineCtRatio',
     'meterMultiplier',
     'overallMultiply',
     'phaseCode',
     'meterSerialNo',
     'amps',
     'mfsCode',
     'mcbRent',
     'meterReading',
     'ruralUrbenConsumer',
     'meterLocationCode',
     'meterSecurity',
     'acd',
     'nrs',
     'cUser',
     'cDate',
     'cTime',
     'uUser',
     'uDate',
     'uTime',
     'uReason',
     'dUser',
     'dReason',
     'eFlag',
     'eDate',
     'eTime',
     'eUser',
     // add all other fields
 ];
}
