<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChangeInLedgerMaster extends Model
{
  protected $table = "tab_advice87";
  protected $fillable = [
    'sysId',
    'month',
    'inputCode',
    'sheetNo',
    'numberOfEnties',
    'pageNo',
    'billingCycle',
    'billingGroup',
    'subdivisionCode',
    'ledgerGroup',
    'villageName',
    'bGroup',
    'bSub',
    'octroiCode',
    'admCode',
    'remarks',
    'cUser',
    'cDate',
    'cTime',
    'uUser',
    'uDate',
    'uTime',
    'uReason',
    'dUser',
    'dReason',
    'eFlag',
    'eDate',
    'eTime',
    'eUser',
  ];
}
