<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChangeOfAcd extends Model
{
  protected $table = "tab_advice80";
  protected $fillable = [
    'sysId',
    'month',
    'inputCode',
    'sheetNo',
    'pageNo',
    'billingCycle',
    'billingGroup',
    'subdivisionCode',
    'ledgerGroup',
    'accountNoFrom',
    'accountNoTo',
    'villageName',
    'meterSecurity',
    'oldAcd',
    'additionalAcd',
    'dateOfDeposite',
    'meterLocation',
    'remarks',
    'cUser',
    'cDate',
    'cTime',
    'uUser',
    'uDate',
    'uTime',
    'uReason',
    'dUser',
    'dReason',
    'eFlag',
    'eDate',
    'eTime',
    'eUser',
  ];
}
