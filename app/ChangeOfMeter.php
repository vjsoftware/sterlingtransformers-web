<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChangeOfMeter extends Model
{
  protected $table = "tab_advice79";
  protected $fillable = [
    'month',
    'sysId',
    'inputCode',
    'sheetNo',
    'numberOfEnties',
    'pageNo',
    'billingCycle',
    'billingGroup',
    'subdivisionCode',
    'ledgerGroup',
    'accountNo',
    'checkDigit',
    'meterSerialNo',
    'phaseCode',
    'amps',
    'dateOfInstalling',
    'initialReading',
    'currentReading',
    'dateOfCurrentReading',
    'codeOfMco',
    'meterMultiplier',
    'meterCtRatio',
    'lineCtRatio',
    'overAllMultiplyingFactor',
    'meterMfrsCode',
    'numberOfDigits',
    'unitBilledAgainstOldMeter',
    'remarks',
    'cUser',
    'cDate',
    'cTime',
    'uUser',
    'uDate',
    'uTime',
    'uReason',
    'dUser',
    'dReason',
    'eFlag',
    'eDate',
    'eTime',
    'eUser',
  ];
}
