<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChangeOfTariff extends Model
{
    protected $table = "tab_advice71";
    protected $fillable = [
      'sysId',
      'month',
      'inputCode',
      'sheetNo',
      'pageNo',
      'numberOfEnties',
      'billingCycle',
      'billingGroup',
      'subdivisionCode',
      'ledgerGroup',
      'accountNo',
      'checkDigit',
      'tariffType',
      'bE1st',
      'bE2nd',
      'bE3rd',
      'bE4th',
      'bE5th',
      'numberOfBEs',
      'voltClass',
      'govtConnectionCode',
      'typesOfNRS',
      'remarks',
      'cUser',
      'cDate',
      'cTime',
      'uUser',
      'uDate',
      'uTime',
      'uReason',
      'dUser',
      'dReason',
      'eFlag',
      'eDate',
      'eTime',
      'eUser',
    ];
}
