<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CorrectionOfMeterParticular extends Model
{
    protected $table = "tab_advice73";
    protected $fillable = [
      'sysId',
      'month',
      'inputCode',
      'sheetNo',
      'pageNo',
      'numberOfEnties',
      'billingCycle',
      'billingGroup',
      'subdivisionCode',
      'ledgerGroup',
      'accountNo',
      'checkDigit',
      'phaseCode',
      'meterNo',
      'ampere',
      'meterMultipiler',
      'meterCtRatio',
      'lineCtRatio',
      'overAllMultiplyingFactor',
      'additionalMeterRentals',
      'numberOfDigits',
      'mfrsCode',
      'admCode',
      'remarks',
      'cUser',
      'cDate',
      'cTime',
      'uUser',
      'uDate',
      'uTime',
      'uReason',
      'dUser',
      'dReason',
      'eFlag',
      'eDate',
      'eTime',
      'eUser',
    ];
}
