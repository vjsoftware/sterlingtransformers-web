<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CorrectionOfName extends Model
{
    protected $table = "tab_advice75";
    protected $fillable = [
      'sysId',
      'month',
      'inputCode',
      'sheetNo',
      'pageNo',
      'billingCycle',
      'billingGroup',
      'subdivisionCode',
      'ledgerGroup',
      'accountNo',
      'checkDigit',
      'nameOfConsumer',
      'address',
      'connectedLoad',
      'additionalServiceRentals',
      'remarks',
      'cUser',
      'cDate',
      'cTime',
      'uUser',
      'uDate',
      'uTime',
      'uReason',
      'dUser',
      'dReason',
      'eFlag',
      'eDate',
      'eTime',
      'eUser',
    ];
}
