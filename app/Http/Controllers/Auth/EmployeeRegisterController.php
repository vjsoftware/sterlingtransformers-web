<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Role;
use App\userId;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Auth;

class EmployeeRegisterController extends Controller
{

  public function __construct()
  {
      $this->middleware('auth');
  }

  public function register(Request $request)
  {
    $this->validate($request,[
      'name' => 'required|string|max:255',
      'email' => 'required|string|max:255|unique:users',
      'password' => 'required|string|min:6|confirmed',
      'role' => 'required',
      ]);

      if ($request->hasFile('photo')) {
        // return 'img3';

          // $file1 = $request->photo;
          // $fullPhotoNameWithExt1 = $file1->getClientOriginalName();
          // $fileName1 = pathinfo($fullPhotoNameWithExt1, PATHINFO_FILENAME);
          // $fileExt1 = $file1->getClientOriginalExtension();
          // $photoToSave1 = $fileName1.'_'.time().'.'. $fileExt1;
          // $path1 = $file1->move('profilepic', $photoToSave1);


          // S3
          //get filename with extension
        $filenamewithextension = $request->file('photo')->getClientOriginalName();

        //get filename without extension
        $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);

        //get file extension
        $extension = $request->file('photo')->getClientOriginalExtension();

        //filename to store
        $filenametostore = $filename.'_'.time().'.'.$extension;

        //Upload File to s3
        Storage::disk('s3')->put('/profilepic/'.$filenametostore, fopen($request->file('photo'), 'r+'), 'public');

        //Store $filenametostore in the database

        }else {
          $filenametostore = ' ';
        }
      if ($request->hasFile('aadhaarPhoto')) {

          // $file2 = $request->aadhaarPhoto;
          // $fullPhotoNameWithExt2 = $file2->getClientOriginalName();
          // $fileName2 = pathinfo($fullPhotoNameWithExt2, PATHINFO_FILENAME);
          // $fileExt2 = $file2->getClientOriginalExtension();
          // $photoToSave2 = $fileName2.'_'.time().'.'. $fileExt2;
          // $path2 = $file2->move('aadharpic', $photoToSave2);


          // S3
          //get filename with extension
        $filenamewithextension = $request->file('aadhaarPhoto')->getClientOriginalName();

        //get filename without extension
        $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);

        //get file extension
        $extension = $request->file('aadhaarPhoto')->getClientOriginalExtension();

        //filename to store
        $filenametostore2 = $filename.'_'.time().'.'.$extension;

        //Upload File to s3
        Storage::disk('s3')->put('/aadhaarpics/'.$filenametostore2, fopen($request->file('aadhaarPhoto'), 'r+'), 'public');

        //Store $filenametostore in the database
        }else {
          $filenametostore2 = ' ';
        }

      $data = new User();
      // $data->name = $request->name;
      // $data->email = $request->email;
      // $data->password = Hash::make($request['password']);
      // $data->role = $request->role;
      // $data->save();
      // $data->roles()->attach($request['role']);
      $created_by = Auth::User()->email;

      $data->name = $request->name;
      $data->fatherName = $request->fatherName;
      $data->emailId = $request->emailId;
      $data->address = $request->address;
      $data->email = $request->email;
      $data->mobile = $request->mobile;
      $data->aadhaarNo = $request->aadhaarNo;
      $data->photo = $filenametostore;
      $data->aadhaarPhoto = $filenametostore2;
      $data->password = Hash::make($request['password']);
      $data->role = $request->role;
      $data->userGroup = $request->group;
      $data->created_by = $created_by;
      if ($request->email >= 30060 && $request->email < 40050) {
        $data->userGroup = 'sterling';
        $UserIdCount = userId::find(3);
        $updateUserIdCount = userId::find(3)->update(['current_count' => $UserIdCount->current_count+1]);
        // return $updateUserIdCount;
      } elseif ($request->email >= 40050) {
        $data->userGroup = 'pspcl';
        $UserIdCount = userId::find(4);
        $updateUserIdCount = userId::find(4)->update(['current_count' => $UserIdCount->current_count+1]);
      }
      $data->save();
      $data->roles()->attach($request['role']);

      return redirect('/userSuccess');
  }
  public function sbaregister(Request $request)
  {
    //   return $request;
    $this->validate($request,[
      'name' => 'required|string|max:255',
      'fatherName' => 'required|string|max:255',
      'address' => 'required',
      'email' => 'required|string|max:255|unique:users',
      'mobile'=> 'required|min:10|max:10',
      'aadhaarNo'=> 'required|max:16',
      'photo' => 'required|mimes:jpeg,jpg,png|max:2500',
      'aadhaarPhoto' => 'required|mimes:jpeg,jpg,png|max:2500',

      ]);



      // if ($request->hasFile('photo')) {
      //
      //     $file1 = $request->photo;
      //     $fullPhotoNameWithExt1 = $file1->getClientOriginalName();
      //     $fileName1 = pathinfo($fullPhotoNameWithExt1, PATHINFO_FILENAME);
      //     $fileExt1 = $file1->getClientOriginalExtension();
      //     $photoToSave1 = $fileName1.'_'.time().'.'. $fileExt1;
      //     $path1 = $file1->move('profilepic', $photoToSave1);
      //   }else {
      //     $photoToSave1 = ' ';
      //   }
      // if ($request->hasFile('aadhaarPhoto')) {
      //
      //     $file2 = $request->aadhaarPhoto;
      //     $fullPhotoNameWithExt2 = $file2->getClientOriginalName();
      //     $fileName2 = pathinfo($fullPhotoNameWithExt2, PATHINFO_FILENAME);
      //     $fileExt2 = $file2->getClientOriginalExtension();
      //     $photoToSave2 = $fileName2.'_'.time().'.'. $fileExt2;
      //     $path2 = $file2->move('aadharpic', $photoToSave2);
      //   }else {
      //     $photoToSave2 = ' ';
      //   }

      if ($request->hasFile('photo')) {
        // return 'img3';

          // $file1 = $request->photo;
          // $fullPhotoNameWithExt1 = $file1->getClientOriginalName();
          // $fileName1 = pathinfo($fullPhotoNameWithExt1, PATHINFO_FILENAME);
          // $fileExt1 = $file1->getClientOriginalExtension();
          // $photoToSave1 = $fileName1.'_'.time().'.'. $fileExt1;
          // $path1 = $file1->move('profilepic', $photoToSave1);


          // S3
          //get filename with extension
        $filenamewithextension = $request->file('photo')->getClientOriginalName();

        //get filename without extension
        $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);

        //get file extension
        $extension = $request->file('photo')->getClientOriginalExtension();

        //filename to store
        $filenametostore = $filename.'_'.time().'.'.$extension;

        //Upload File to s3
        Storage::disk('s3')->put('/profilepic/'.$filenametostore, fopen($request->file('photo'), 'r+'), 'public');

        //Store $filenametostore in the database

        }else {
          $filenametostore = ' ';
        }
      if ($request->hasFile('aadhaarPhoto')) {

          // $file2 = $request->aadhaarPhoto;
          // $fullPhotoNameWithExt2 = $file2->getClientOriginalName();
          // $fileName2 = pathinfo($fullPhotoNameWithExt2, PATHINFO_FILENAME);
          // $fileExt2 = $file2->getClientOriginalExtension();
          // $photoToSave2 = $fileName2.'_'.time().'.'. $fileExt2;
          // $path2 = $file2->move('aadharpic', $photoToSave2);


          // S3
          //get filename with extension
        $filenamewithextension = $request->file('aadhaarPhoto')->getClientOriginalName();

        //get filename without extension
        $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);

        //get file extension
        $extension = $request->file('aadhaarPhoto')->getClientOriginalExtension();

        //filename to store
        $filenametostore2 = $filename.'_'.time().'.'.$extension;

        //Upload File to s3
        Storage::disk('s3')->put('/aadhaarpics/'.$filenametostore2, fopen($request->file('aadhaarPhoto'), 'r+'), 'public');

        //Store $filenametostore in the database
        }else {
          $filenametostore2 = ' ';
        }

        $created_by = Auth::User()->email;

      $data = new User();
      $data->name = $request->name;
      $data->fatherName = $request->fatherName;
      $data->emailId = $request->emailId;
      $data->address = $request->address;
      $data->email = $request->email;
      $data->mobile = $request->mobile;
      $data->aadhaarNo = $request->aadhaarNo;
      $data->photo = $filenametostore;
      $data->aadhaarPhoto = $filenametostore2;
      $data->password = Hash::make($request['mobile']);
      $data->role = 2;
      $data->created_by = $created_by;

      if ($request->email > 20000) {
        $data->userGroup = 'sap';
        $UserIdCount = userId::find(1);
        $updateUserIdCount = userId::find(1)->update(['current_count' => $UserIdCount->current_count+1]);
        // return $updateUserIdCount;
      } else {
        $data->userGroup = 'non sap';
        $UserIdCount = userId::find(2);
        $updateUserIdCount = userId::find(2)->update(['current_count' => $UserIdCount->current_count+1]);
        // return dd($updateUserIdCount);
      }
      $data->save();

      $data->roles()->attach($request['role']);

      return redirect('/sbaSuccess');
  }
  public function changepassword(Request $request)
  {
    $this->validate($request,[
      'old_password' => 'required|string|min:6',
      'password' => 'required|string|min:6|confirmed',
      ]);
      $userId = Auth::user()->id;
      $data = User::find($userId);
      if (Hash::check($request['old_password'], $data->password)) {
        $data->password = Hash::make($request['password']);
        $data->save();
          // $updateUser = User::find($userDetails->id)->update(['password' => Hash::make($request->newPassword)]);
          if ($data->count()) {
        return redirect('/passwordSuccess');
      } else {
        // return 'oooo';
        return redirect('/passwordfailed');
      }
      } else {
          return redirect('/passwordfailed');
      }



  }

  public function changepasswordForce(Request $request)
  {
    $this->validate($request,[
      'userId' => 'required',
      'password' => 'required|string|min:6|confirmed',
      ]);
      $userId = $request->userId;
      // return $userId;
      $data = User::where('email', $userId)->get();
      // return $data->first()->id;
      $userId = $data->first()->id;
      $data = User::find($userId);

        $data->password = Hash::make($request['password']);
        $data->save();
          // $updateUser = User::find($userDetails->id)->update(['password' => Hash::make($request->newPassword)]);
          if ($data->count()) {
        return redirect('/passwordSuccess');
      } else {
        // return 'oooo';
        return redirect('/passwordfailed');
      }



  }

  public function edit($id)
  {
    // return $id;
    $meterReader = User::find($id);
    return view('auth/sbaregisteredit')->with('data', $meterReader);
  }

  public function sbaRegisterUpdate(Request $request)
  {

    // $created_by = Auth::User()->email;
    $activated_by = Auth::User()->email;
    // return $request;
    $meterReader = User::find($request->id);
    $meterReader->name = $request->name;
    $meterReader->fatherName = $request->fatherName;
    $meterReader->emailId = $request->emailId;
    $meterReader->address = $request->address;
    $meterReader->permanentAddress = $request->permanentAddress;
    $meterReader->communicationAddress = $request->communicationAddress;
    $meterReader->mobile = $request->mobile;
    $meterReader->aadhaarNo = $request->aadhaarNo;
    $meterReader->simNumber = $request->simNumber;
    $meterReader->imeiNumber = $request->imeiNumber;
    $meterReader->status = $request->status;

    if ($meterReader->status == 1) {
      $meterReader->activated_by = $activated_by;
      $meterReader->activated_at = Date('Y-m-d H:i:s');
    } elseif ($meterReader->status == 2) {
      $meterReader->deactivated_by = $activated_by;
      $meterReader->deactivated_at = Date('Y-m-d H:i:s');
    }
    if ($request->password != null) {
      $meterReader->password = Hash::make($request->password);
    }
    $meterReader->save();
    return redirect('/meter-readers');
  }
}
