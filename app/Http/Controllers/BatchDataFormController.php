<?php

namespace App\Http\Controllers;

use App\BatchDataForm;
use App\tabAdvice11;
use Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class BatchDataFormController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */



    public function batch()
    {
        return view('batchDataForm');
    }

    public function batchData(Request $request)
    {
      $this->validate($request,[
        'accountNo' => 'required',
        'connectionDate' => 'required',
        ]);

        $connectionDate = Carbon\Carbon::createFromFormat('d/m/Y', $request->connectionDate)->format('Y-m-d');
        // return $request->connectionDate;



        // $data = new BatchDataForm();
        $data = BatchDataForm::create($request->except('_token'));
        return redirect('/batchDataForm');
        // $data->accountNo = $request->accountNo;
        // $data->consumerName = $request->consumerName;
        // $data->address = $request->address;
        // $data->feederCode = $request->feederCode;
        // $data->tariffType = $request->tariffType;
        // $data->employeeConsessionCode = $request->employeeConsessionCode;
        // $data->voltClassCode = $request->voltClassCode;
        // $data->connectedLoad = $request->connectedLoad;
        // $data->meterRatio = $request->meterRatio;
        // $data->connectionDate = $connectionDate;
        // $data->lineCtRatio = $request->lineCtRatio;
        // $data->meterMultiplier = $request->meterMultiplier;
        // $data->overallMultiply = $request->overallMultiply;
        // $data->phaseCode = $request->phaseCode;
        // $data->meterSerialNo = $request->meterSerialNo;
        // $data->amps = $request->amps;
        // $data->mfsCode = $request->mfsCode;
        // $data->mcbRent = $request->mcbRent;
        // $data->meterReading = $request->meterReading;
        // $data->ruralUrbenConsumer = $request->ruralUrbenConsumer;
        // $data->meterLocationCode = $request->meterLocationCode;
        // $data->meterSecurity = $request->meterSecurity;
        // $data->acd = $request->acd;
        // $data->nrs = $request->nrs;
        $data->save();

        return redirect('/batchDataForm');
      }

      public function batchDataAjax(Request $request)
      {
        // return $request;
        $data = tabAdvice11::create($request->except('_token'));

        if ($data) {
          $status = [
            'status' => 'success'
          ];
        } else {
          $status = [
            'status' => 'error'
          ];
        }

        return $data;
      }

      public function report()
      {
        // $report = tabAdvice11::all();
        return view('advice11-report');

        // return $data;
      }
      public function batchDataedit()
      {
        $edit = tabAdvice11::whereNull('dUser')->get();

        return view('batchDataFormEdit')->with('data', $edit);
      }

      public function batchDataGenerateReport(Request $request)
      {
        $bills = tabAdvice11::where('month', $request['month'])->whereNull('dUser')->get();
        // return $bills;

        // return view('batchDataFormEdit')->with('data', $edit);
        $txt = '';
        foreach ($bills as $bill) {
          $txt .= $this->addSpace($bill->sysId, 3);
          $txt .= $this->addSpace($bill->inputCode, 2);
          $txt .= $this->addSpace(trim($bill->trType), 1);
          $txt .= $this->addSpace($bill->subDivisonCode, 3);
          $txt .= $this->addSpace($bill->octraiCode, 1);
          $txt .= $this->addSpace($bill->ledgerGroup, 4);
          $txt .= $this->addSpace($bill->billingGroup, 1);
          $txt .= $this->addSpace($bill->billingSubGroup, 2);
          $txt .= $this->addSpace($bill->billingSubGroup, 2);
          $txt .= $this->addSpace($bill->accountNumber, 4);
          $txt .= $this->addSpace($bill->consumerName, 20);
          $txt .= $this->addSpace($bill->feederCode, 12);
          $txt .= $this->addSpace($bill->tariffType, 1);
          $txt .= $this->addSpace($bill->voltClassCode, 1);
          $txt .= "\r\n";
        }

        $sub = 'ADVICE-11';

        return response($txt)
                  ->withHeaders([
                      'Content-Type' => 'text/plain',
                      'Cache-Control' => 'no-store, no-cache',
                      'Content-Disposition' => "attachment; filename=$sub.txt",
                  ]);
        return $bills;
      }

      public function editable($id)
      {
        $editview = tabAdvice11::find($id);

        return view('batchDataFormEditView')->with('data', $editview);
      }
      public function deletable($id)

      {
        $editview = tabAdvice11::find($id);

        return view('batchDataFormDeleteView')->with('data', $editview);
      }
      public function delete(Request $request)
      {
        // return $request;
        $dUser = Auth::user()->id;

        $dDate = Date('d-m-Y');
        $dTime = Date('H:i:s');
        // return $uUser;
        // $connectionDate = Carbon\Carbon::createFromFormat('d/m/Y', $request->connectionDate)->format('Y-m-d');
        // return $request->connectionDate;



        // $data = new BatchDataForm();
        $data = tabAdvice11::find($request->id);
        $data->dReason = $request->dReason;
        $data->dUser = $dUser;
        $data->uDate = $dDate;
        $data->uTime = $dTime;
        $data->save();


        return redirect('/advice-11-report');
      }

      public function update(Request $request)
      {
        // return $request;

        $this->validate($request,[
          'accountNo' => 'required',
          'connectionDate' => 'required',
          ]);
          $uUser = Auth::user()->id;

          $uDate = Date('d-m-Y');
          $uTime = Date('H:i:s');
          // return $uUser;
          // $connectionDate = Carbon\Carbon::createFromFormat('d/m/Y', $request->connectionDate)->format('Y-m-d');
          // return $request->connectionDate;



          // $data = new BatchDataForm();
          $data = tabAdvice11::find($request->id);
          $data->sysId = $request->sysId;
          $data->month = $request->month;
          $data->inputCode = $request->inputCode;
          $data->trType = $request->trType;
          $data->subdivisionCode = $request->subdivisionCode;
          $data->octraiCode = $request->octraiCode;
          $data->ledgerGroup = $request->ledgerGroup;
          $data->billingGroup = $request->billingGroup;
          $data->billingSubGroup = $request->billingSubGroup;
          $data->villageName = $request->villageName;
          $data->accountNo = $request->accountNo;
          $data->consumerName = $request->consumerName;
          $data->address = $request->address;
          $data->feederCode = $request->feederCode;
          $data->tariffType = $request->tariffType;
          $data->employeeConsessionCode = $request->employeeConsessionCode;
          $data->voltClassCode = $request->voltClassCode;
          $data->connectedLoad = $request->connectedLoad;
          $data->meterRatio = $request->meterRatio;
          $data->connectionDate = $request->connectionDate;
          $data->lineCtRatio = $request->lineCtRatio;
          $data->meterMultiplier = $request->meterMultiplier;
          $data->overallMultiply = $request->overallMultiply;
          $data->phaseCode = $request->phaseCode;
          $data->meterSerialNo = $request->meterSerialNo;
          $data->amps = $request->amps;
          $data->mfsCode = $request->mfsCode;
          $data->mcbRent = $request->mcbRent;
          $data->meterReading = $request->meterReading;
          $data->ruralUrbenConsumer = $request->ruralUrbenConsumer;
          $data->meterLocationCode = $request->meterLocationCode;
          $data->meterSecurity = $request->meterSecurity;
          $data->acd = $request->acd;
          $data->nrs = $request->nrs;
          $data->uReason = $request->uReason;
          $data->uUser = $uUser;
          $data->uDate = $uDate;
          $data->uTime = $uTime;
          $data->save();

          return redirect('/advice-11-report');
      }



      public function reportPost(Request $request)
      {
        // return $request;
        $sub = $request->subdivisionCode;
        $report = tabAdvice11::where('subdivisionCode', $sub)->get();
        return view('advice11-report')->with('data', $report);

        // return $data;
      }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\BatchDataForm  $batchDataForm
     * @return \Illuminate\Http\Response
     */
    public function show(BatchDataForm $batchDataForm)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\BatchDataForm  $batchDataForm
     * @return \Illuminate\Http\Response
     */
    public function edit(BatchDataForm $batchDataForm)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BatchDataForm  $batchDataForm
     * @return \Illuminate\Http\Response
     */


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BatchDataForm  $batchDataForm
     * @return \Illuminate\Http\Response
     */
    public function destroy(BatchDataForm $batchDataForm)
    {
        //
    }


        public function vle($value = '', $count)
        {
          $countMain = $count;
          $count = $count - 1;
          if ($value > 0) {
            $abs = abs($value);
            $abs = sprintf("%.2f", $abs);
            $name = '+'.$this->addZeros($abs, $count);
          } elseif ($value == 0) {
            $abs = 0;
            $abs = sprintf("%.2f", $abs);
            $name = '+'.$this->addZeros($abs, $count);
          } else {
            $abs = abs($value);
            $abs = sprintf("%.2f", $abs);
            // $abs = number_format($abs, 2);
            // return $abs = number_format($abs, 2);
            $name = '-'.$this->addZeros($abs, $count);
            // $blNetSop = $abs;
          }
          return $name;
        }

        public function vlen($value = '', $count)
        {
          $countMain = $count;
          $count = $count - 1;
          if ($value > 0) {
            $name = '+'.$this->addZeros((int) $value, $count);
          } elseif ($value == 0) {
            $name = '+'.$this->addZeros('', $count);
          } else {
            $abs = abs((int) $value);
            // $abs = number_format($abs, 2);
            $name = '-'.$this->addZeros($abs, $count);
            // $blNetSop = $abs;
          }
          return $name;
        }

        public function addZerosSign($value='', $length)
        {
          $count = strlen($value);
          if ($value >= 1 || $value == 0 || $value == 0.00) {
            $sign = "+";
          } else {
            $sign = "-";
          }
          $numberOfZeros = $length - $count;
          $zeros = '';
          for ($i=1; $i < $numberOfZeros; $i++) {
            $zeros .= 0;
          }

          $newValue = $sign.$zeros.$value;
          return $newValue;
        }


        public function addZerosSignN($value='', $length)
        {
          $count = strlen($value);
          if ($value >= 1 || $value == 0 || $value == 0.00) {
            $sign = "+";
          } else {
            $sign = "-";
          }
          $numberOfZeros = $length - $count;
          $zeros = '';
          for ($i=0; $i < $numberOfZeros; $i++) {
            $zeros .= 0;
          }

          $newValue = $sign.$zeros.$value;
          return $newValue;
        }

        public function addZeros($value='', $length)
        {
          $count = strlen($value);
          $numberOfZeros = $length - $count;
          $zeros = '';
          for ($i=0; $i < $numberOfZeros; $i++) {
            $zeros .= 0;
          }

          $newValue = $zeros.$value;
          return $newValue;
        }

        public function addZerosBack($value='', $length)
        {
          $count = strlen($value);
          $numberOfZeros = $length - $count;
          $zeros = '';
          for ($i=0; $i < $numberOfZeros; $i++) {
            $zeros .= 0;
          }

          $newValue = $value.$zeros;
          return $newValue;
        }

        public function addSpace($value='', $length)
        {

          $count = strlen(trim($value));
          $numberOfZeros = $length - $count;
          $zeros = '';
          for ($i=0; $i < $numberOfZeros; $i++) {
            $zeros .= ' ';
          }

          $newValue = $value.$zeros;
          $newValue = substr($newValue, 0, $length);
          return $newValue;
        }

        public function addZerosFloat($value = '', $count)
        {
          $countMain = $count;
          $count = $count - 2;
          if ($value > 0) {
            $abs = sprintf("%.2f", $value);
            $name = $this->addZeros($abs, $countMain);
            // $name = $this->addZeros($value, $count);
          } elseif ($value == 0) {
            $abs = 0;
            $abs = sprintf("%.2f", $abs);
            $name = $this->addZeros($abs, $countMain);
          } else {
            $abs = abs($value);
            $abs = sprintf("%.2f", $abs);
            // $abs = number_format($abs, 2);
            // return $abs = number_format($abs, 2);
            $name = $this->addZeros($abs, $countMain);
            // $blNetSop = $abs;
          }
          return $name;
        }

        public function addSpaceFront($value='', $length)
        {
          $count = strlen($value);
          $numberOfZeros = $length - $count;
          $zeros = '';
          for ($i=0; $i < $numberOfZeros; $i++) {
            $zeros .= ' ';
          }

          $newValue = $zeros.$value;
          return $newValue;
        }
}
