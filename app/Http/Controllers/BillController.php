<?php

namespace App\Http\Controllers;
use App\sapInput;
use App\sapOutput;
use App\Billed;
// Delete Bills backUp Table `sap_deleted_bills`

use Illuminate\Http\Request;

class BillController extends Controller
{
  public function sapDeleteFormView()
  {
    return View('sap-delete-form-view');
  }
  public function nonsapDeleteFormView()
  {
    // return 'pp';
    return View('non-sap-delete-form-view');
  }

  public function consumerEnquirySap(Request $request)
  {
    if ($request->legacyNumber != null) {
      return '1';
    } elseif ($request->acNumber != '') {
      $search = sapInput::where('contractAcNumber', $request->acNumber)->get();
      if ($search->count()) {
        $data = [
          'data' => $search,
          'status' => 'Y'
        ];
      } else {
        $data = [
          'status' => 'N'
        ];
      }
      // return $data;
      return view('sap-delete-form-view')->with('data', $data);
      // return $search;
    } elseif ($request->meterNumber != '') {
      return '3';
    }
  }

  public function consumerEnquiryNonSap(Request $request)
  {
    if ($request->legacyNumber != null) {
      return '1';
    } elseif ($request->acNumber != '') {
      $search = Billed::where('blMasterKey', $request->acNumber)->get();
      if ($search->count()) {
        $data = [
          'data' => $search,
          'status' => 'Y'
        ];
      } else {
        $data = [
          'status' => 'N'
        ];
      }
      // return $data;
      return view('non-sap-delete-form-view')->with('data', $data);
      // return $search;
    } elseif ($request->meterNumber != '') {
      return '3';
    }
  }
}
