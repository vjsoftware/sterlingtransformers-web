<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Bind;
use App\User;
use App\Bill;
use App\sapInput;

use Auth;
use DB;

class BindMeterReaderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     // Non SAP
    public function index()
    {
        $meterReaders = User::where('role', 2)->get();
        return view('bind')->with('data', $meterReaders);
    }

    // SAP
    public function sapView()
    {
        $meterReaders = User::where('role', 2)->get();
        return view('bind-sap')->with('data', $meterReaders);
    }

    public function bindSuccess()
    {
        return view('bindSuccess');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

     // Non SAP
    public function store(Request $request)
    {
      $this->validate($request,[
          'meterReader' => 'required',
          'accountNoG' => 'required',
          'accountNoL' => 'required',
          'subDivisionCode' => 'required|min:3|max:3',
          'billingCycle' => 'required',
          'billingGroup' => 'required',
          'ledgerCode' => 'required|min:3|max:5',
        ]);

        $data  = [];
        $log = new Bind();
        $log->meterReader = $request->meterReader;
        $log->accountNoG = $request->accountNoG;
        $log->accountNoL = $request->accountNoL;
        $log->subDivisionCode = $request->subDivisionCode;
        $log->billingCycle = $request->billingCycle;
        $log->billingGroup = $request->billingGroup;
        $log->ledgerCode = $request->ledgerCode;
        $log->user_id = Auth::user()->id;
        $log->save();

        $assigned_by = Auth::user()->email;
        $assigned_at = Date('Y-m-d H:i:s');
        $meterReader = User::find($request->meterReader);

        if ($log) {
          $searchBills = Bill::where('subDivisionCode', $request->subDivisionCode)
                             ->where('billingCycle', '>=', $request->billingCycle)
                             ->where('billingGroup', '>=', $request->billingGroup)
                             ->where('accountNumber', '>=', $request->accountNoG)
                             ->where('accountNumber', '<=', $request->accountNoL)
                             ->update(['meterReader' => $meterReader->email, 'binding_status' => 'Y', 'assigned_by' => $assigned_by, 'assigned_at' => $assigned_at, 'a_count' => 1]);
        }

        // return $searchBills->success();

        if ($searchBills) {
          $data['status'] = 'Bind Meter Reader Success';
          return view('message')->with('data', $data);
        } else {
          $data['status'] = 'Binding Failed';
          return view('message')->with('data', $data);
        }

        // return $searchBills;
    }

     // SAP
    public function storeSap(Request $request)
    {
      $this->validate($request,[
          'meterReader' => 'required',
          'accountNoG' => 'required',
          'accountNoL' => 'required',
          'subDivisionCode' => 'required',
          'billingCycle' => 'required',
          'billingGroup' => 'required',
          'ledgerCode' => 'required',
        ]);

        $data  = [];
        $log = new Bind();
        $log->meterReader = $request->meterReader;
        $log->accountNoG = $request->accountNoG;
        $log->accountNoL = $request->accountNoL;
        $log->subDivisionCode = $request->subDivisionCode;
        $log->billingCycle = $request->billingCycle;
        $log->billingGroup = $request->billingGroup;
        $log->ledgerCode = $request->ledgerCode;
        $log->user_id = Auth::user()->id;
        $log->save();

        // return $request;

        $meterReader = User::find($request->meterReader);
        $assigned_by = Auth::User()->email;
        $assigned_at = Date('Y-m-d H:i:s');

        if ($log) {
          $searchBills = sapInput::where('subDivisionCode', $request->subDivisionCode)
                             ->where('billingCycle', $request->billingCycle)
                             ->where('billingGroup', $request->billingGroup)
                             ->where('contractAcNumber', '>=', $request->accountNoG)
                             ->where('contractAcNumber', '<=', $request->accountNoL)
                             ->where('bindingStatus', 'N')
                             ->update(['assigned_by' => $assigned_by, 'assigned_at' => $assigned_at, 'meterReader' => $meterReader->email, 'bindingStatus' => 'Y', 'a_count' => 1]);
        }

        // return $searchBills->count();

        if ($searchBills) {
          $data['status'] = 'Bind Meter Reader Success';
          return view('message')->with('data', $data);
        } else {
          $data['status'] = 'Binding Failed';
          return view('message')->with('data', $data);
        }

        // return $searchBills;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function ledgerView()
    {
      // $sub = DB::select("SELECT SUBSTRING(masterKey, 1, 3) subDivCode FROM bills group by SUBSTRING(masterKey, 1, 3)");
      // return $sub[0]->subDivCode;
        $meterReaders = User::where('role', 2)->where('userGroup', 'non sap')->get();
        return view('bind-ledger')->with('data', $meterReaders);
    }

    public function ledgerViewNew()
    {
      // $sub = DB::select("SELECT SUBSTRING(masterKey, 1, 3) subDivCode FROM bills group by SUBSTRING(masterKey, 1, 3)");
      // return $sub[0]->subDivCode;
        $meterReaders = User::where('role', 2)->where('userGroup', 'non sap')->get();
        return view('bind-ledger-new')->with('data', $meterReaders);
    }

    public function storeLedger(Request $request)
    {
      $this->validate($request,[
          'meterReader' => 'required',
          'subDivisionCode' => 'required',
          'billingCycle' => 'required',
          'billingGroup' => 'required',
          'ledgerCode' => 'required',
        ]);

        $data  = [];
        $log = new Bind();
        $log->meterReader = $request->meterReader;
        $log->accountNoG = '';
        $log->accountNoL = '';
        $log->subDivisionCode = $request->subDivisionCode;
        $log->billingCycle = $request->billingCycle;
        $log->billingGroup = $request->billingGroup;
        $log->ledgerCode = $request->ledgerCode;
        $log->user_id = Auth::user()->id;
        $log->save();

        $assigned_by = Auth::User()->email;
        $assigned_at = Date('Y-m-d H:i:s');

        if ($log) {

          // return $searchBills->result();
          $meterReader = User::find($request->meterReader);
          $meterReaderFound = $meterReader->email;
          $meterReaderNew = $meterReader->email . ',';
          // $getMeterReaders = sapInput::where(SUBSTRING(masterKey, 1, 3), $request->subDivisionCode)
          //                             ->where('billingGroup', $request->billingGroup)
          //                             ->where('billingCycle', $request->billingCycle)
          //                             ->where(SUBSTRING(masterKey, 4, 4), $request->ledgerCode)->get();
          $getMeterReaders = DB::select("SELECT meterReader FROM bills where SUBSTRING(masterKey, 1, 3) = '$request->subDivisionCode' and billingGroup = '$request->billingGroup' and billingCycle = '$request->billingCycle' and SUBSTRING(masterKey, 4, 4) = '$request->ledgerCode'");
          // return $getMeterReaders[0]->meterReader;
          $meterReader = $getMeterReaders[0]->meterReader;
          // return $meterReader;
          // $search = DB::select("SELECT * FROM bills where SUBSTRING(masterKey, 1, 3) = '$request->subDivisionCode' and billingGroup = '$request->billingGroup' and billingCycle = '$request->billingCycle' and SUBSTRING(masterKey, 4, 4) = '$request->ledgerCode'");
          // return $search;
          if ($getMeterReaders[0]->meterReader == '') {
            $searchBills = DB::update("UPDATE bills SET meterReader = '$meterReaderNew', assigned_by = '$assigned_by', assigned_at = '$assigned_at', a_count = a_count + 1, binding_status = 'Y' where SUBSTRING(masterKey, 1, 3) = '$request->subDivisionCode' and billingGroup = '$request->billingGroup' and billingCycle = '$request->billingCycle' and SUBSTRING(masterKey, 4, 4) = '$request->ledgerCode'");

          } else {
            // code...
            $searchBills = DB::update("UPDATE bills SET meterReader = CONCAT('$meterReader', '$meterReaderNew'), assigned_by = '$assigned_by', assigned_at = '$assigned_at', a_count = a_count + 1, binding_status = 'Y' where SUBSTRING(masterKey, 1, 3) = '$request->subDivisionCode' and billingGroup = '$request->billingGroup' and billingCycle = '$request->billingCycle' and SUBSTRING(masterKey, 4, 4) = '$request->ledgerCode' and not find_in_set('$meterReaderFound', meterReader) <> 0");
          }
           // return $searchBills;
        }

        // return $searchBills->success();

        if ($searchBills) {
          $data['status'] = 'Bind Meter Reader Success';
          return view('message')->with('data', $data);
        } else {
          $data['status'] = 'Binding failed';
          return view('message')->with('data', $data);
        }

        // return $searchBills;
    }

    // Sap

    public function ledgerViewSap()
    {
      // $sub = DB::select("SELECT distinct subDivisionCode FROM sap_inputs group by subDivisionCode");
      // $sub = sapInput::select('subDivisionCode')->groupBy('subDivisionCode')->take(20)->get();
      // $subDivision = $sub;
      // return $subDivision;
        $meterReaders = User::where('role', 2)->where('userGroup', 'sap')->get();
        return view('bind-ledger-sap')->with('data', $meterReaders);
    }

    public function ledgerViewSapNew()
    {
        $meterReaders = User::where('role', 2)->where('userGroup', 'sap')->get();
        return view('bind-ledger-sap-new')->with('data', $meterReaders);
    }


    public function storeLedgerSap(Request $request)
    {
      $this->validate($request,[
          'meterReader' => 'required',
          'subDivisionCode' => 'required',
          'billingCycle' => 'required',
          'billingGroup' => 'required',
          'ledgerCode' => 'required',
        ]);

        $data  = [];
        $log = new Bind();
        $log->meterReader = $request->meterReader;
        $log->accountNoG = '';
        $log->accountNoL = '';
        $log->subDivisionCode = $request->subDivisionCode;
        $log->billingCycle = $request->billingCycle;
        $log->billingGroup = $request->billingGroup;
        $log->ledgerCode = $request->ledgerCode;
        $log->user_id = Auth::user()->id;
        $log->save();

        $assigned_by = Auth::User()->email;
        $assigned_at = Date('Y-m-d H:i:s');

        $meterReader = User::find($request->meterReader);
        $meterReaderFound = $meterReader->email;

        if ($log) {
          // return $request;
          $meterReaderNew = $meterReader->email . ',';
          $getMeterReaders = sapInput::where('mru', $request->ledgerCode)
                                      ->where('billingGroup', $request->billingGroup)
                                      ->where('billingCycle', $request->billingCycle)
                                      ->where('subDivisionCode', $request->subDivisionCode)->get();
                                      // return $getMeterReaders;
           $meterReader = $getMeterReaders->first()->meterReader;
          // $searchBills = DB::update("UPDATE sap_inputs SET meterReader = '$meterReader->email', assigned_by = '$assigned_by', assigned_at = '$assigned_at', bindingStatus = 'Y', a_count = a_count + 1 where mru = '$request->ledgerCode' and billingGroup = '$request->billingGroup' and billingCycle = '$request->billingCycle' and subDivisionCode = '$request->subDivisionCode' AND bindingStatus = 'N'");
          $searchBills = DB::update("UPDATE sap_inputs SET meterReader = CONCAT('$meterReader', '$meterReaderNew'), assigned_by = '$assigned_by', assigned_at = '$assigned_at', bindingStatus = 'Y', a_count = a_count + 1 where mru = '$request->ledgerCode' and billingGroup = '$request->billingGroup' and billingCycle = '$request->billingCycle' and subDivisionCode = '$request->subDivisionCode' and not find_in_set('$meterReaderFound', meterReader) <> 0");
          // return $searchBills;
          // $affectedRows = DB::select("SELECT id FROM sap_inputs where mru = '$request->ledgerCode' and billingGroup = '$request->billingGroup' and billingCycle = '$request->billingCycle' and subDivisionCode = '$request->subDivisionCode'");

          // return $searchBills->result();
        }

        // return $searchBills->success();

        if ($searchBills) {
          $data['status'] = 'Bind Meter Reader Success';
          return view('message')->with('data', $data);
        } else {
          $data['status'] = 'Binding failed';
          return view('message')->with('data', $data);
        }

        // return $searchBills;
    }

    public function storeLedgerSapNew(Request $request)
    {
      return $request;
      $this->validate($request,[
          'meterReader' => 'required',
          'subDivisionCode' => 'required',
          'billingCycle' => 'required',
          'billingGroup' => 'required',
          'ledgerCode' => 'required',
        ]);

        $data  = [];
        $log = new Bind();
        $log->meterReader = $request->meterReader;
        $log->accountNoG = '';
        $log->accountNoL = '';
        $log->subDivisionCode = $request->subDivisionCode;
        $log->billingCycle = $request->billingCycle;
        $log->billingGroup = $request->billingGroup;
        $log->ledgerCode = $request->ledgerCode;
        $log->user_id = Auth::user()->id;
        $log->save();

        $assigned_by = Auth::User()->email;
        $assigned_at = Date('Y-m-d H:i:s');

        $meterReader = User::find($request->meterReader);
        $meterReaderFound = $meterReader->email;

        if ($log) {
          // return $request;
          $meterReaderNew = $meterReader->email . ',';
          $getMeterReaders = sapInput::where('mru', $request->ledgerCode)
                                      ->where('billingGroup', $request->billingGroup)
                                      ->where('billingCycle', $request->billingCycle)
                                      ->where('subDivisionCode', $request->subDivisionCode)->get();
                                      // return $getMeterReaders;
           $meterReader = $getMeterReaders->first()->meterReader;
          // $searchBills = DB::update("UPDATE sap_inputs SET meterReader = '$meterReader->email', assigned_by = '$assigned_by', assigned_at = '$assigned_at', bindingStatus = 'Y', a_count = a_count + 1 where mru = '$request->ledgerCode' and billingGroup = '$request->billingGroup' and billingCycle = '$request->billingCycle' and subDivisionCode = '$request->subDivisionCode' AND bindingStatus = 'N'");
          $searchBills = DB::update("UPDATE sap_inputs SET meterReader = CONCAT('$meterReader', '$meterReaderNew'), assigned_by = '$assigned_by', assigned_at = '$assigned_at', bindingStatus = 'Y', a_count = a_count + 1 where mru = '$request->ledgerCode' and billingGroup = '$request->billingGroup' and billingCycle = '$request->billingCycle' and subDivisionCode = '$request->subDivisionCode' and not find_in_set('$meterReaderFound', meterReader) <> 0");
          // return $searchBills;
          // $affectedRows = DB::select("SELECT id FROM sap_inputs where mru = '$request->ledgerCode' and billingGroup = '$request->billingGroup' and billingCycle = '$request->billingCycle' and subDivisionCode = '$request->subDivisionCode'");

          // return $searchBills->result();
        }

        // return $searchBills->success();

        if ($searchBills) {
          $data['status'] = 'Bind Meter Reader Success';
          return view('message')->with('data', $data);
        } else {
          $data['status'] = 'Binding failed';
          return view('message')->with('data', $data);
        }

        // return $searchBills;
    }

    public function dataResetView()
    {
      return view('data_reset');
    }

    public function dataReset(Request $request)
    {
      $userIdMain = $request->userId;
      $userId = $request->userId.',';
      // return $userId;
      if ($request->type == "non sap") {
        $releaseData = DB::update("UPDATE bills SET received_by = REPLACE(received_by, '$userId', '') WHERE find_in_set('$userIdMain', meterReader) and bill_status = 'N'");
        // return $userIdMain;
      } elseif ($request->type == "sap") {
        $releaseData = DB::update("UPDATE sap_inputs SET received_by = REPLACE(received_by, '$userId', '') WHERE find_in_set('$userIdMain', meterReader) and bill_status = 'N'");
      }
      // $user = User::where('email', $request->userId)->update(['imeiNumber' => '', 'simNumber' => '']);
      // return $user;
      if ($releaseData) {
        $data['status'] = 'Successfully Released Data Mobile';
        return view('message')->with('data', $data);
      } else {
        $data['status'] = 'Release Data Failed';
        return view('message')->with('data', $data);
      }
    }

    public function mobileResetView()
    {
      return view('mobile_reset');
    }

    public function mobileReset(Request $request)
    {
      // return $request;
      $user = User::where('email', $request->userId)->update(['imeiNumber' => '', 'simNumber' => '']);
      // return $user;
      if ($user) {
        $data['status'] = 'Successfully Reset Mobile';
        return view('message')->with('data', $data);
      } else {
        $data['status'] = 'Reset Failed';
        return view('message')->with('data', $data);
      }

    }

}
