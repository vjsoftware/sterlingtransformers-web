<?php

namespace App\Http\Controllers;

use App\ChangeOfAcd;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class ChangeOfAcdController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function acd()
    {
        return view('changeOfAcd');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $data = ChangeOfAcd::create($request->except('_token'));
      return redirect('changeOfAcd');
    }

    public function storeAjax(Request $request)
    {
      $data = ChangeOfAcd::create($request->except('_token'));
      if ($data) {
        $status = [
          'status' => 'success'
        ];
      } else {
        $status = [
          'status' => 'error'
        ];
      }

      return $data;
    }

    public function changeOfAcdEdit()
    {

      $edit = ChangeOfAcd::whereNull('dUser')->get();

      return view('changeOfAcdEdit')->with('data', $edit);

    }
    public function editable($id)
    {
      $editview = ChangeOfAcd::find($id);

      return view('changeOfAcdEditView')->with('data', $editview);
    }

    public function update(Request $request)
    {
      // return $request;

      $this->validate($request,[
        'ledgerGroup' => 'required',
        ]);

        $uUser = Auth::user()->id;

        $uDate = Date('d-m-Y');
        $uTime = Date('H:i:s');
        // return $uUser;
        // $connectionDate = Carbon\Carbon::createFromFormat('d/m/Y', $request->connectionDate)->format('Y-m-d');
        // return $request->connectionDate;



        // $data = new BatchDataForm();
        $data = ChangeOfAcd::find($request->id);
        $data->sysId = $request->sysId;
        $data->month = $request->month;
        $data->inputCode = $request->inputCode;
        $data->sheetNo = $request->sheetNo;
        $data->pageNo = $request->pageNo;
        $data->billingCycle = $request->billingCycle;
        $data->billingGroup = $request->billingGroup;
        $data->subdivisionCode = $request->subdivisionCode;
        $data->ledgerGroup = $request->ledgerGroup;
        $data->accountNoFrom = $request->accountNoFrom;
        $data->accountNoTo = $request->accountNoTo;
        $data->villageName = $request->villageName;
        $data->meterSecurity = $request->meterSecurity;
        $data->oldAcd = $request->oldAcd;
        $data->additionalAcd = $request->additionalAcd;
        $data->dateOfDeposite = $request->dateOfDeposite;
        $data->meterLocation = $request->meterLocation;
        $data->remarks = $request->remarks;
        $data->uReason = $request->uReason;
        $data->uUser = $uUser;
        $data->uDate = $uDate;
        $data->uTime = $uTime;
        $data->save();

        return redirect('/changeOfAcdEdit');
    }

    public function deletable($id)

    {
      $editview = ChangeOfAcd::find($id);

      return view('changeOfAcdDeleteView')->with('data', $editview);
    }

    public function delete(Request $request)
    {
      // return $request;
      $dUser = Auth::user()->id;

      $dDate = Date('d-m-Y');
      $dTime = Date('H:i:s');
      // return $uUser;
      // $connectionDate = Carbon\Carbon::createFromFormat('d/m/Y', $request->connectionDate)->format('Y-m-d');
      // return $request->connectionDate;



      // $data = new BatchDataForm();
      $data = ChangeOfAcd::find($request->id);
      $data->dReason = $request->dReason;
      $data->dUser = $dUser;
      $data->uDate = $dDate;
      $data->uTime = $dTime;
      $data->save();


      return redirect('/changeOfAcdEdit');
    }
    public function batchDataGenerateReport(Request $request)
    {
      $bills = ChangeOfAcd::where('month', $request['month'])->whereNull('dUser')->get();
      // return $bills;

      // return view('batchDataFormEdit')->with('data', $edit);
      $txt = '';
      foreach ($bills as $bill) {
        $txt .= $this->addSpace($bill->sysId, 3);
        $txt .= $this->addSpace($bill->inputCode, 2);
        $txt .= $this->addZeros($bill->sheetNo, 2);
        $txt .= $this->addZeros($bill->pageNo, 3);
        $txt .= $this->addZeros($bill->numberOfEnties, 2);
        $txt .= $this->addSpace($bill->billingCycle, 2);
        $txt .= $this->addSpace($bill->billingGroup, 1);
        $txt .= $this->addSpace($bill->subdivisionCode, 3);
        $txt .= $this->addSpace($bill->ledgerGroup, 4);
        $txt .= $this->addSpace($bill->accountNoFrom, 4);
        $txt .= $this->addSpace($bill->accountNoTo, 4);
        $txt .= $this->addZeros($bill->additionalAcd, 7);
        $txt .= "\r\n";
      }

      $sub = 'ADVICE-80';

      return response($txt)
                ->withHeaders([
                    'Content-Type' => 'text/plain',
                    'Cache-Control' => 'no-store, no-cache',
                    'Content-Disposition' => "attachment; filename=$sub.txt",
                ]);
      return $bills;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ChangeOfAcd  $changeOfAcd
     * @return \Illuminate\Http\Response
     */
    public function show(ChangeOfAcd $changeOfAcd)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ChangeOfAcd  $changeOfAcd
     * @return \Illuminate\Http\Response
     */
    public function edit(ChangeOfAcd $changeOfAcd)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ChangeOfAcd  $changeOfAcd
     * @return \Illuminate\Http\Response
     */

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ChangeOfAcd  $changeOfAcd
     * @return \Illuminate\Http\Response
     */
    public function destroy(ChangeOfAcd $changeOfAcd)
    {
        //
    }
    public function vle($value = '', $count)
    {
      $countMain = $count;
      $count = $count - 1;
      if ($value > 0) {
        $abs = abs($value);
        $abs = sprintf("%.2f", $abs);
        $name = '+'.$this->addZeros($abs, $count);
      } elseif ($value == 0) {
        $abs = 0;
        $abs = sprintf("%.2f", $abs);
        $name = '+'.$this->addZeros($abs, $count);
      } else {
        $abs = abs($value);
        $abs = sprintf("%.2f", $abs);
        // $abs = number_format($abs, 2);
        // return $abs = number_format($abs, 2);
        $name = '-'.$this->addZeros($abs, $count);
        // $blNetSop = $abs;
      }
      return $name;
    }

    public function vlen($value = '', $count)
    {
      $countMain = $count;
      $count = $count - 1;
      if ($value > 0) {
        $name = '+'.$this->addZeros((int) $value, $count);
      } elseif ($value == 0) {
        $name = '+'.$this->addZeros('', $count);
      } else {
        $abs = abs((int) $value);
        // $abs = number_format($abs, 2);
        $name = '-'.$this->addZeros($abs, $count);
        // $blNetSop = $abs;
      }
      return $name;
    }

    public function addZerosSign($value='', $length)
    {
      $count = strlen($value);
      if ($value >= 1 || $value == 0 || $value == 0.00) {
        $sign = "+";
      } else {
        $sign = "-";
      }
      $numberOfZeros = $length - $count;
      $zeros = '';
      for ($i=1; $i < $numberOfZeros; $i++) {
        $zeros .= 0;
      }

      $newValue = $sign.$zeros.$value;
      return $newValue;
    }


    public function addZerosSignN($value='', $length)
    {
      $count = strlen($value);
      if ($value >= 1 || $value == 0 || $value == 0.00) {
        $sign = "+";
      } else {
        $sign = "-";
      }
      $numberOfZeros = $length - $count;
      $zeros = '';
      for ($i=0; $i < $numberOfZeros; $i++) {
        $zeros .= 0;
      }

      $newValue = $sign.$zeros.$value;
      return $newValue;
    }

    public function addZeros($value='', $length)
    {
      $count = strlen($value);
      $numberOfZeros = $length - $count;
      $zeros = '';
      for ($i=0; $i < $numberOfZeros; $i++) {
        $zeros .= 0;
      }

      $newValue = $zeros.$value;
      return $newValue;
    }

    public function addZerosBack($value='', $length)
    {
      $count = strlen($value);
      $numberOfZeros = $length - $count;
      $zeros = '';
      for ($i=0; $i < $numberOfZeros; $i++) {
        $zeros .= 0;
      }

      $newValue = $value.$zeros;
      return $newValue;
    }

    public function addSpace($value='', $length)
    {

      $count = strlen(trim($value));
      $numberOfZeros = $length - $count;
      $zeros = '';
      for ($i=0; $i < $numberOfZeros; $i++) {
        $zeros .= ' ';
      }

      $newValue = $value.$zeros;
      $newValue = substr($newValue, 0, $length);
      return $newValue;
    }

    public function addZerosFloat($value = '', $count)
    {
      $countMain = $count;
      $count = $count - 2;
      if ($value > 0) {
        $abs = sprintf("%.2f", $value);
        $name = $this->addZeros($abs, $countMain);
        // $name = $this->addZeros($value, $count);
      } elseif ($value == 0) {
        $abs = 0;
        $abs = sprintf("%.2f", $abs);
        $name = $this->addZeros($abs, $countMain);
      } else {
        $abs = abs($value);
        $abs = sprintf("%.2f", $abs);
        // $abs = number_format($abs, 2);
        // return $abs = number_format($abs, 2);
        $name = $this->addZeros($abs, $countMain);
        // $blNetSop = $abs;
      }
      return $name;
    }

    public function addSpaceFront($value='', $length)
    {
      $count = strlen($value);
      $numberOfZeros = $length - $count;
      $zeros = '';
      for ($i=0; $i < $numberOfZeros; $i++) {
        $zeros .= ' ';
      }

      $newValue = $zeros.$value;
      return $newValue;
    }
}
