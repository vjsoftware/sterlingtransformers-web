<?php

namespace App\Http\Controllers;

use App\ChangeOfTariff;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class ChangeOfTariffController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function tariff()
    {
        return view('ChangeOfTariff');
    }

    public function tariffChange(Request $request)
    {
      $this->validate($request,[
        'ledgerGroup' => 'required',
        ]);


        // $data = new ChangeOfTariff();
        $data = ChangeOfTariff::create($request->except('_token'));
        // $data->ledgerGroup = $request->ledgerGroup;
        // $data->accountNo = $request->accountNo;
        // $data->checkDigit = $request->checkDigit;
        // $data->tariffType = $request->tariffType;
        // $data->bE1st = $request->bE1st;
        // $data->bE2nd = $request->bE2nd;
        // $data->bE3rd = $request->bE3rd;
        // $data->bE4th = $request->bE4th;
        // $data->bE5th = $request->bE5th;
        // $data->numberOfBEs = $request->numberOfBEs;
        // $data->voltClass = $request->voltClass;
        // $data->govtConnectionCode = $request->govtConnectionCode;
        // $data->typesOfNRS = $request->typesOfNRS;
        // $data->remarks = $request->remarks;
        //
        // $data->save();

        return redirect('/changeOfTariff');

    }

    public function tariffChangeAjax(Request $request)
    {
      // $this->validate($request,[
      //   'ledgerGroup' => 'required',
      //   ]);
      //
      // return $request['sysId'];
        $data = ChangeOfTariff::create($request->except('_token'));

        if ($data) {
          $status = [
            'status' => 'success'
          ];
        } else {
          $status = [
            'status' => 'error'
          ];
        }

        return $data;

    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function report()
     {
       // $report = tabAdvice11::all();
       return view('changeOfTariff');

       // return $data;
     }
     public function tarrifEdit()
     {
       $edit = ChangeOfTariff::whereNull('dUser')->get();
       return view('changeOfTariffEdit')->with('data', $edit);
     }

     public function editable($id)
     {
       $editview = ChangeOfTariff::find($id);

       return view('changeOfTariffEditView')->with('data', $editview);
     }

     public function update(Request $request)
     {
       // return $request;

       $this->validate($request,[
         'accountNo' => 'required',
         ]);

         $uUser = Auth::user()->id;

         $uDate = Date('d-m-Y');
         $uTime = Date('H:i:s');
         // return $uUser;
         // $connectionDate = Carbon\Carbon::createFromFormat('d/m/Y', $request->connectionDate)->format('Y-m-d');
         // return $request->connectionDate;



         // $data = new BatchDataForm();
         $data = ChangeOfTariff::find($request->id);
         $data->sysId = $request->sysId;
         $data->month = $request->month;
         $data->inputCode = $request->inputCode;
         $data->sheetNo = $request->sheetNo;
         $data->pageNo = $request->pageNo;
         $data->numberOfEnties = $request->numberOfEnties;
         $data->billingCycle = $request->billingCycle;
         $data->billingGroup = $request->billingGroup;
         $data->subdivisionCode = $request->subdivisionCode;
         $data->ledgerGroup = $request->ledgerGroup;
         $data->accountNo = $request->accountNo;
         $data->checkDigit = $request->checkDigit;
         $data->tariffType = $request->tariffType;
         $data->bE1st = $request->bE1st;
         $data->bE2nd = $request->bE2nd;
         $data->bE3rd = $request->bE3rd;
         $data->bE4th = $request->bE4th;
         $data->bE5th = $request->bE5th;
         $data->numberOfBEs = $request->numberOfBEs;
         $data->voltClass = $request->voltClass;
         $data->govtConnectionCode = $request->govtConnectionCode;
         $data->typesOfNRS = $request->typesOfNRS;
         $data->remarks = $request->remarks;
         $data->uReason = $request->uReason;
         $data->uUser = $uUser;
         $data->uDate = $uDate;
         $data->uTime = $uTime;
         $data->save();

         return redirect('/changeOfTariffEdit');
     }
     public function deletable($id)

     {
       $editview = ChangeOfTariff::find($id);

       return view('changeOfTarriffDeleteView')->with('data', $editview);
     }

     public function delete(Request $request)
     {
       // return $request;
       $dUser = Auth::user()->id;

       $dDate = Date('d-m-Y');
       $dTime = Date('H:i:s');
       // return $uUser;
       // $connectionDate = Carbon\Carbon::createFromFormat('d/m/Y', $request->connectionDate)->format('Y-m-d');
       // return $request->connectionDate;



       // $data = new BatchDataForm();
       $data = ChangeOfTariff::find($request->id);
       $data->dReason = $request->dReason;
       $data->dUser = $dUser;
       $data->uDate = $dDate;
       $data->uTime = $dTime;
       $data->save();


       return redirect('/changeOfTariffEdit');
     }

     public function batchDataGenerateReport(Request $request)
     {
       $bills = ChangeOfTariff::where('month', $request['month'])->whereNull('dUser')->get();
       // return $bills;

       // return view('batchDataFormEdit')->with('data', $edit);
       $txt = '';
       foreach ($bills as $bill) {
         $txt .= $this->addSpace($bill->sysId, 3);
         $txt .= $this->addSpace($bill->inputCode, 2);
         $txt .= $this->addSpace($bill->sheetNo, 2);
         $txt .= $this->addSpace($bill->pageNo, 3);
         $txt .= $this->addSpace($bill->numberOfEnties, 2);
         $txt .= $this->addSpace($bill->billingCycle, 2);
         $txt .= $this->addSpace($bill->billingGroup, 1);
         $txt .= $this->addSpace($bill->subdivisionCode, 3);
         $txt .= $this->addSpace($bill->ledgerGroup, 4);
         $txt .= $this->addSpace($bill->accountNumber, 4);
         $txt .= $this->addSpace($bill->checkDigit, 1);
         $txt .= $this->addSpace($bill->tariffType, 1);
         $txt .= "\r\n";
       }

       $sub = 'ADVICE-71';

       return response($txt)
                 ->withHeaders([
                     'Content-Type' => 'text/plain',
                     'Cache-Control' => 'no-store, no-cache',
                     'Content-Disposition' => "attachment; filename=$sub.txt",
                 ]);
       return $bills;
     }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ChangeOfTariff  $changeOfTariff
     * @return \Illuminate\Http\Response
     */
    public function show(ChangeOfTariff $changeOfTariff)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ChangeOfTariff  $changeOfTariff
     * @return \Illuminate\Http\Response
     */
    public function edit(ChangeOfTariff $changeOfTariff)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ChangeOfTariff  $changeOfTariff
     * @return \Illuminate\Http\Response
     */

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ChangeOfTariff  $changeOfTariff
     * @return \Illuminate\Http\Response
     */
    public function destroy(ChangeOfTariff $changeOfTariff)
    {
        //
    }
    public function vle($value = '', $count)
    {
      $countMain = $count;
      $count = $count - 1;
      if ($value > 0) {
        $abs = abs($value);
        $abs = sprintf("%.2f", $abs);
        $name = '+'.$this->addZeros($abs, $count);
      } elseif ($value == 0) {
        $abs = 0;
        $abs = sprintf("%.2f", $abs);
        $name = '+'.$this->addZeros($abs, $count);
      } else {
        $abs = abs($value);
        $abs = sprintf("%.2f", $abs);
        // $abs = number_format($abs, 2);
        // return $abs = number_format($abs, 2);
        $name = '-'.$this->addZeros($abs, $count);
        // $blNetSop = $abs;
      }
      return $name;
    }

    public function vlen($value = '', $count)
    {
      $countMain = $count;
      $count = $count - 1;
      if ($value > 0) {
        $name = '+'.$this->addZeros((int) $value, $count);
      } elseif ($value == 0) {
        $name = '+'.$this->addZeros('', $count);
      } else {
        $abs = abs((int) $value);
        // $abs = number_format($abs, 2);
        $name = '-'.$this->addZeros($abs, $count);
        // $blNetSop = $abs;
      }
      return $name;
    }

    public function addZerosSign($value='', $length)
    {
      $count = strlen($value);
      if ($value >= 1 || $value == 0 || $value == 0.00) {
        $sign = "+";
      } else {
        $sign = "-";
      }
      $numberOfZeros = $length - $count;
      $zeros = '';
      for ($i=1; $i < $numberOfZeros; $i++) {
        $zeros .= 0;
      }

      $newValue = $sign.$zeros.$value;
      return $newValue;
    }


    public function addZerosSignN($value='', $length)
    {
      $count = strlen($value);
      if ($value >= 1 || $value == 0 || $value == 0.00) {
        $sign = "+";
      } else {
        $sign = "-";
      }
      $numberOfZeros = $length - $count;
      $zeros = '';
      for ($i=0; $i < $numberOfZeros; $i++) {
        $zeros .= 0;
      }

      $newValue = $sign.$zeros.$value;
      return $newValue;
    }

    public function addZeros($value='', $length)
    {
      $count = strlen($value);
      $numberOfZeros = $length - $count;
      $zeros = '';
      for ($i=0; $i < $numberOfZeros; $i++) {
        $zeros .= 0;
      }

      $newValue = $zeros.$value;
      return $newValue;
    }

    public function addZerosBack($value='', $length)
    {
      $count = strlen($value);
      $numberOfZeros = $length - $count;
      $zeros = '';
      for ($i=0; $i < $numberOfZeros; $i++) {
        $zeros .= 0;
      }

      $newValue = $value.$zeros;
      return $newValue;
    }

    public function addSpace($value='', $length)
    {

      $count = strlen(trim($value));
      $numberOfZeros = $length - $count;
      $zeros = '';
      for ($i=0; $i < $numberOfZeros; $i++) {
        $zeros .= ' ';
      }

      $newValue = $value.$zeros;
      $newValue = substr($newValue, 0, $length);
      return $newValue;
    }

    public function addZerosFloat($value = '', $count)
    {
      $countMain = $count;
      $count = $count - 2;
      if ($value > 0) {
        $abs = sprintf("%.2f", $value);
        $name = $this->addZeros($abs, $countMain);
        // $name = $this->addZeros($value, $count);
      } elseif ($value == 0) {
        $abs = 0;
        $abs = sprintf("%.2f", $abs);
        $name = $this->addZeros($abs, $countMain);
      } else {
        $abs = abs($value);
        $abs = sprintf("%.2f", $abs);
        // $abs = number_format($abs, 2);
        // return $abs = number_format($abs, 2);
        $name = $this->addZeros($abs, $countMain);
        // $blNetSop = $abs;
      }
      return $name;
    }

    public function addSpaceFront($value='', $length)
    {
      $count = strlen($value);
      $numberOfZeros = $length - $count;
      $zeros = '';
      for ($i=0; $i < $numberOfZeros; $i++) {
        $zeros .= ' ';
      }

      $newValue = $zeros.$value;
      return $newValue;
    }
}
