<?php

namespace App\Http\Controllers;

use App\CorrectionOfMeterParticular;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class CorrectionOfMeterParticularController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function particular()
    {
        return view('correctionOfMeterParticulars');
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = CorrectionOfMeterParticular::create($request->except('_token'));
        return redirect('/correctionOfMeterParticulars');
    }


    public function particularDataAjax(Request $request)
    {

      // return $request;

      $data = CorrectionOfMeterParticular::create($request->except('_token'));

      if ($data) {
        $status = [
          'status' => 'success'
        ];
      } else {
        $status = [
          'status' => 'error'
        ];
      }

      return $data;
    }

    // public function previousrReadingEdit()
    // {
    //   $edit = CorrectionOfMeterParticular::whereNull('dUser')->get();
    //   return view('previousReadingCorrectionEdit')->with('data', $edit);
    //
    // }

    /**
     * Display the specified resource.
     *
     * @param  \App\CorrectionOfMeterParticular  $correctionOfMeterParticular
     * @return \Illuminate\Http\Response
     */
     public function correctionOfMeterEdit()
     {

       $edit = CorrectionOfMeterParticular::whereNull('dUser')->get();

       return view('correctionOfMeterParticularsEdit')->with('data', $edit);

     }

     public function editable($id)
     {
       $editview = CorrectionOfMeterParticular::find($id);

       return view('correctionOfMeterParticularsEditView')->with('data', $editview);
     }

     public function update(Request $request)
     {
       // return $request;

       $this->validate($request,[
         'ledgerGroup' => 'required',
         ]);

         $uUser = Auth::user()->id;

         $uDate = Date('d-m-Y');
         $uTime = Date('H:i:s');
         // return $uUser;
         // $connectionDate = Carbon\Carbon::createFromFormat('d/m/Y', $request->connectionDate)->format('Y-m-d');
         // return $request->connectionDate;



         // $data = new BatchDataForm();
         $data = CorrectionOfMeterParticular::find($request->id);
         $data->sysId = $request->sysId;
         $data->month = $request->month;
         $data->inputCode = $request->inputCode;
         $data->sheetNo = $request->sheetNo;
         $data->numberOfEnties = $request->numberOfEnties;
         $data->pageNo = $request->pageNo;
         $data->billingCycle = $request->billingCycle;
         $data->billingGroup = $request->billingGroup;
         $data->subdivisionCode = $request->subdivisionCode;
         $data->ledgerGroup = $request->ledgerGroup;
         $data->accountNo = $request->accountNo;
         $data->checkDigit = $request->checkDigit;
         $data->meterNo = $request->meterNo;
         $data->phaseCode = $request->phaseCode;
         $data->ampere = $request->ampere;
         $data->meterMultipiler = $request->meterMultipiler;
         $data->meterCtRatio = $request->meterCtRatio;
         $data->lineCtRatio = $request->lineCtRatio;
         $data->overAllMultiplyingFactor = $request->overAllMultiplyingFactor;
         $data->additionalMeterRentals = $request->additionalMeterRentals;
         $data->numberOfDigits = $request->numberOfDigits;
         $data->mfrsCode = $request->mfrsCode;
         $data->admCode = $request->admCode;
         $data->remarks = $request->remarks;
         $data->uReason = $request->uReason;
         $data->uUser = $uUser;
         $data->uDate = $uDate;
         $data->uTime = $uTime;
         $data->save();

         return redirect('/correctionOfMeterParticularsEdit');
     }

     public function deletable($id)

     {
       $deleteview = CorrectionOfMeterParticular::find($id);

       return view('correctionOfMeterParticularsDeleteView')->with('data', $deleteview);
     }

     public function delete(Request $request)
     {
       // return $request;
       $dUser = Auth::user()->id;

       $dDate = Date('d-m-Y');
       $dTime = Date('H:i:s');
       // return $uUser;
       // $connectionDate = Carbon\Carbon::createFromFormat('d/m/Y', $request->connectionDate)->format('Y-m-d');
       // return $request->connectionDate;



       // $data = new BatchDataForm();
       $data = CorrectionOfMeterParticular::find($request->id);
       $data->dReason = $request->dReason;
       $data->dUser = $dUser;
       $data->uDate = $dDate;
       $data->uTime = $dTime;
       $data->save();


       return redirect('/correctionOfMeterParticularsEdit');
     }

     public function batchDataGenerateReport(Request $request)
     {
       $bills = CorrectionOfMeterParticular::where('month', $request['month'])->whereNull('dUser')->get();
       // return $bills;

       // return view('batchDataFormEdit')->with('data', $edit);
       $txt = '';
       foreach ($bills as $bill) {
         $txt .= $this->addSpace($bill->sysId, 3);
         $txt .= $this->addSpace($bill->inputCode, 2);
         $txt .= $this->addZeros($bill->sheetNo, 2);
         $txt .= $this->addZeros($bill->pageNo, 3);
         $txt .= $this->addZeros($bill->numberOfEnties, 2);
         $txt .= $this->addSpace($bill->billingCycle, 2);
         $txt .= $this->addSpace($bill->billingGroup, 1);
         $txt .= $this->addSpace($bill->subdivisionCode, 3);
         $txt .= $this->addSpace($bill->ledgerGroup, 4);
         $txt .= $this->addSpace($bill->accountNoFrom, 4);
         $txt .= $this->addSpace($bill->accountNoTo, 4);
         $txt .= $this->addZeros($bill->additionalAcd, 7);
         $txt .= "\r\n";
       }

       $sub = 'ADVICE-73';

       return response($txt)
                 ->withHeaders([
                     'Content-Type' => 'text/plain',
                     'Cache-Control' => 'no-store, no-cache',
                     'Content-Disposition' => "attachment; filename=$sub.txt",
                 ]);
       return $bills;
     }

     public function vle($value = '', $count)
     {
       $countMain = $count;
       $count = $count - 1;
       if ($value > 0) {
         $abs = abs($value);
         $abs = sprintf("%.2f", $abs);
         $name = '+'.$this->addZeros($abs, $count);
       } elseif ($value == 0) {
         $abs = 0;
         $abs = sprintf("%.2f", $abs);
         $name = '+'.$this->addZeros($abs, $count);
       } else {
         $abs = abs($value);
         $abs = sprintf("%.2f", $abs);
         // $abs = number_format($abs, 2);
         // return $abs = number_format($abs, 2);
         $name = '-'.$this->addZeros($abs, $count);
         // $blNetSop = $abs;
       }
       return $name;
     }

     public function vlen($value = '', $count)
     {
       $countMain = $count;
       $count = $count - 1;
       if ($value > 0) {
         $name = '+'.$this->addZeros((int) $value, $count);
       } elseif ($value == 0) {
         $name = '+'.$this->addZeros('', $count);
       } else {
         $abs = abs((int) $value);
         // $abs = number_format($abs, 2);
         $name = '-'.$this->addZeros($abs, $count);
         // $blNetSop = $abs;
       }
       return $name;
     }

     public function addZerosSign($value='', $length)
     {
       $count = strlen($value);
       if ($value >= 1 || $value == 0 || $value == 0.00) {
         $sign = "+";
       } else {
         $sign = "-";
       }
       $numberOfZeros = $length - $count;
       $zeros = '';
       for ($i=1; $i < $numberOfZeros; $i++) {
         $zeros .= 0;
       }

       $newValue = $sign.$zeros.$value;
       return $newValue;
     }


     public function addZerosSignN($value='', $length)
     {
       $count = strlen($value);
       if ($value >= 1 || $value == 0 || $value == 0.00) {
         $sign = "+";
       } else {
         $sign = "-";
       }
       $numberOfZeros = $length - $count;
       $zeros = '';
       for ($i=0; $i < $numberOfZeros; $i++) {
         $zeros .= 0;
       }

       $newValue = $sign.$zeros.$value;
       return $newValue;
     }

     public function addZeros($value='', $length)
     {
       $count = strlen($value);
       $numberOfZeros = $length - $count;
       $zeros = '';
       for ($i=0; $i < $numberOfZeros; $i++) {
         $zeros .= 0;
       }

       $newValue = $zeros.$value;
       return $newValue;
     }

     public function addZerosBack($value='', $length)
     {
       $count = strlen($value);
       $numberOfZeros = $length - $count;
       $zeros = '';
       for ($i=0; $i < $numberOfZeros; $i++) {
         $zeros .= 0;
       }

       $newValue = $value.$zeros;
       return $newValue;
     }

     public function addSpace($value='', $length)
     {

       $count = strlen(trim($value));
       $numberOfZeros = $length - $count;
       $zeros = '';
       for ($i=0; $i < $numberOfZeros; $i++) {
         $zeros .= ' ';
       }

       $newValue = $value.$zeros;
       $newValue = substr($newValue, 0, $length);
       return $newValue;
     }

     public function addZerosFloat($value = '', $count)
     {
       $countMain = $count;
       $count = $count - 2;
       if ($value > 0) {
         $abs = sprintf("%.2f", $value);
         $name = $this->addZeros($abs, $countMain);
         // $name = $this->addZeros($value, $count);
       } elseif ($value == 0) {
         $abs = 0;
         $abs = sprintf("%.2f", $abs);
         $name = $this->addZeros($abs, $countMain);
       } else {
         $abs = abs($value);
         $abs = sprintf("%.2f", $abs);
         // $abs = number_format($abs, 2);
         // return $abs = number_format($abs, 2);
         $name = $this->addZeros($abs, $countMain);
         // $blNetSop = $abs;
       }
       return $name;
     }

     public function addSpaceFront($value='', $length)
     {
       $count = strlen($value);
       $numberOfZeros = $length - $count;
       $zeros = '';
       for ($i=0; $i < $numberOfZeros; $i++) {
         $zeros .= ' ';
       }

       $newValue = $zeros.$value;
       return $newValue;
     }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CorrectionOfMeterParticular  $correctionOfMeterParticular
     * @return \Illuminate\Http\Response
     */
    public function edit(CorrectionOfMeterParticular $correctionOfMeterParticular)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CorrectionOfMeterParticular  $correctionOfMeterParticular
     * @return \Illuminate\Http\Response
     */


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CorrectionOfMeterParticular  $correctionOfMeterParticular
     * @return \Illuminate\Http\Response
     */
    public function destroy(CorrectionOfMeterParticular $correctionOfMeterParticular)
    {
        //
    }
}
