<?php

namespace App\Http\Controllers;

use App\CorrectionOfName;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CorrectionOfNameController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function name()
    {
        return view('correctionOfName');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      // return $request;
        $data = CorrectionOfName::create($request->except('_token'));
        return redirect('correctionOfName');
    }

    public function storeAjax(Request $request)
    {
      // return $request;
        $data = CorrectionOfName::create($request->except('_token'));
        if ($data) {
          $status = [
            'status' => 'success'
          ];
        } else {
          $status = [
            'status' => 'error'
          ];
        }

        return $data;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CorrectionOfName  $correctionOfName
     * @return \Illuminate\Http\Response
     */
     public function correctionOfNameEdit()
     {

       $edit = CorrectionOfName::whereNull('dUser')->get();

       return view('correctionOfNameEdit')->with('data', $edit);

     }

     public function batchDataGenerateReport(Request $request)
     {
       $bills = CorrectionOfName::where('month', $request['month'])->whereNull('dUser')->get();
       // return $bills;

       // return view('batchDataFormEdit')->with('data', $edit);
       $txt = '';
       foreach ($bills as $bill) {
         $txt .= $this->addSpace($bill->sysId, 3);
         $txt .= $this->addSpace($bill->inputCode, 2);
         $txt .= $this->addZeros($bill->sheetNo, 2);
         $txt .= $this->addZeros($bill->pageNo, 3);
         $txt .= $this->addSpace($bill->billingCycle, 1);
         $txt .= $this->addSpace($bill->billingGroup, 1);
         $txt .= $this->addSpace($bill->subdivisionCode, 3);
         $txt .= $this->addSpace($bill->ledgerGroup, 4);
         $txt .= $this->addSpace($bill->accountNo, 4);
         $txt .= $this->addSpace($bill->checkDigit, 1);
         $txt .= $this->addSpace($bill->nameOfConsumer, 15);
         $txt .= $this->addSpace($bill->address, 20);
         $txt .= "\r\n";
       }

       $sub = 'ADVICE-75';

       return response($txt)
                 ->withHeaders([
                     'Content-Type' => 'text/plain',
                     'Cache-Control' => 'no-store, no-cache',
                     'Content-Disposition' => "attachment; filename=$sub.txt",
                 ]);
       return $bills;
     }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CorrectionOfName  $correctionOfName
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CorrectionOfName $correctionOfName)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CorrectionOfName  $correctionOfName
     * @return \Illuminate\Http\Response
     */
    public function destroy(CorrectionOfName $correctionOfName)
    {
        //
    }
}
