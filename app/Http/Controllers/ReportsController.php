<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\Bill;
use App\Billed;
use App\sapOutput;
use App\sapInput;
use Carbon;
use DB;

use Mapper;

class ReportsController extends Controller
{
    public function pendingBills()
    {
      $meterReaders = User::where('role', 2)->get();
      return view('pendingBills')->with('data', $meterReaders);
    }

    public function pendingBillsSearch(Request $request)
    {
      $this->validate($request,[
        'divisionCode' => 'required',
        'meterReader' => 'required',
        ]);

      if ($request->divisionCode == "all") {
        $pendingBills = Bill::where('status', 1)
                              ->where('meterReader', $request->meterReader)
                              ->get();
      } else {
        $pendingBills = Bill::where('divisionCode', $request->divisionCode)
                              ->where('status', 1)
                              ->where('meterReader', $request->meterReader)
                              ->get();
      }
      // return $request;
      if (count($pendingBills)) {
        $message = "Success";
      } else {
        $message = "No Data Found";
      }
      $data = [
        'pendingList' => $pendingBills,
        'message' => $message
      ];
      return view('pendingList')->with('data', $data);
    }

    public function trackMeterReader()
    {
      // $meterReaders = User::where('userGroup', 'sap')->get();
      // $data = [
      //   'meterReaders' => $meterReaders
      // ];
      return view('trackMeterReader');
    }

    public function trackMeterReaderNonSAP()
    {
      $meterReaders = User::where('userGroup', 'non sap')->get();
      $data = [
        'meterReaders' => $meterReaders
      ];
      return view('trackMeterReaderNonSAP')->with('data', $data);
    }

    public function trackMeterReaderSearch(Request $request)
    {
      $this->validate($request,[
        'from' => 'required',
        'to' => 'required',
        'subDivisionCode' => 'required',
        'meterReader' => 'required',
        ]);
        // return $request['server'];
        if ($request['server'] == "nonsap") {
        // return $request->meterReader;
        $meterReader = User::find($request->meterReader);
        // return $meterReader->email;
        $from = Carbon\Carbon::createFromFormat('d-m-Y', $request->from)->format('Y-m-d');
        $to = Carbon\Carbon::createFromFormat('d-m-Y', $request->to)->format('Y-m-d');
        // return $request->subDivisionCode;
          // $billsSearch = Billed::whereBetween('blPresentReadingDateFormated', [$from, $to])
          // ->where('blSubDivisionCode', $request->subDivisionCode)
          // ->where('meterReader', $meterReader->email)
          // ->where('blLat', '!=', '0.0')
          // ->where('blLong', '!=', '0.0')
          // ->get();
          // return $billsSearch;
        }
        if($request['server'] == "sap") {
          // return $request;
          $from = Carbon\Carbon::createFromFormat('d-m-Y', $request->from)->format('d-m-Y');
          $to = Carbon\Carbon::createFromFormat('d-m-Y', $request->to)->format('d-m-Y');
          // $getMeterReaders = User::find($request->meterReader);
          // $billsSearch = sapOutput::whereBetween('currentMeterReadingDate', [$request->from, $request->to])
          // ->where('subDivisonCode', $request->subDivisionCode)
          // ->where('meterReader', $getMeterReaders->email)
          // ->where('blLat', '!=', '0.0')
          // ->where('blLong', '!=', '0.0')
          // ->get();
          // return $billsSearch;
        }

      // $meterReaders = User::where('role', 2)->get();
      $data = [
        'from' => "$from",
        'to' => $to,
        'sub' => $request->subDivisionCode,
        'reader' => $request->meterReader
      ];

      // return $data;

      // echo count($data['bills']);
      // return $data['bills'];

      // Mapper::map(20.5937, 78.9629, ['center' => false, 'type' => 'ROADMAP', 'marker' => false]);
      // if ($request['server'] == "nonsap") {
      //   foreach ($billsSearch as $geo) {
      //     Mapper::marker($geo->blLat, $geo->blLong, ['title' => $geo->blConsumerName . ' - ' . $geo->blMasterKey]);
      //   }
      // } else {
      //   foreach ($billsSearch as $geo) {
      //     Mapper::marker($geo->blLat, $geo->blLong, ['title' => 'Name: '.$geo->consumerName . ' - Account No: ' .$geo->consumerLegacyNumber]);
      //   }
      // }
      return view('trackMeterReader')->with('data', $data);
    }

    public function miniLedger()
    {

      return view('miniLedger');
    }

    public function miniLedgerSearch(Request $request)
    {
      $this->validate($request,[
        'from' => 'required',
        'to' => 'required',
        'subDivisionCode' => 'required',
        'billingGroup' => 'required',
        'billingCycle' => 'required',
        ]);
      // return $request;
      $billsSearch = Billed::whereBetween('created_at', [$request->from, $request->to])
      ->where('blSubDivisionCode', $request->subDivisionCode)
      ->where('blBillingGroup', $request->billingGroup)
      ->where('blBillingCycle', $request->billingCycle)
      ->get();
      // return $billsSearch;
      return view('miniLedgerSearch')->with('data', $billsSearch);
    }

    public function meterReaders()
    {
      $meterReaders = User::where('role', 2)->where('userGroup', 'non sap')->get();
      return view('meterReaders')->with('data', $meterReaders);
    }

    public function meterReadersSap()
    {
      $meterReaders = User::where('role', 2)->where('userGroup', 'sap')->get();
      return view('meterReadersSap')->with('data', $meterReaders);
    }

    public function meterReaderPerformance()
    {
      return view('meterReaderPerformance');
    }

    public function meterReaderPerformanceSearch(Request $request)
    {
      $this->validate($request,[
        'from' => 'required',
        'to' => 'required',
        'subDivisionCode' => 'required',
        'billingGroup' => 'required',
        'billingCycle' => 'required'
        ]);

      $meterReaders = User::where('role', 2)->get();
      $data = [
        'meterReaders' => $meterReaders,
        'from' => $request->from,
        'to' => $request->to,
        'subDivisionCode' => $request->subDivisionCode,
        'billingGroup' => $request->billingGroup,
        'billingCycle' => $request->billingCycle,
      ];

      // return $data['from'];

      return view('meterReaderPerformance')->with('data', $data);
    }

    public function exceptionalReportsPost(Request $request)
    {
      $this->validate($request,[
        'from' => 'required',
        'to' => 'required',
        'subDivisionCode' => 'required',
        'billingGroup' => 'required',
        'billingCycle' => 'required',
        ]);
      // return $request;
      $billsSearch = sapOutput::whereBetween('currentMeterReadingDate', [$request->from, $request->to])
      ->where('subDivisonCode', $request->subDivisionCode)
      ->where('billingGroup', $request->billingGroup)
      ->where('billingCycle', $request->billingCycle)
      ->get();
      // return $billsSearch;
      return view('exceptionReportView')->with('data', $billsSearch);
    }

    public function pendingBillsView()
    {
      return view('pending-bills-sap');
    }

    public function pendingBillsSAP(Request $request)
    {
      // return $request;
      $subDivisionCode = $request->subDivisionCode;
      $billingGroup = $request->billingGroup;
      $billingCycle = $request->billingCycle;
      $MRU = $request->ledgerCode;
      $report = DB::select("select subDivisionCode, billingGroup, billingCycle, MRU, contractAcNumber, consumerLegacyNumber, consumerName, address, meterMnfSerialNumber, bill_status from sap_inputs where subDivisionCode = '$subDivisionCode' and billingGroup = '$billingGroup' and billingCycle = '$billingCycle' and MRU = '$MRU' and bill_status = 'N'");
      return view('pending-bills-sap-view')->with('data', $report);
    }

    public function pendingBillsNonSapView()
    {
      return view('pending-bills-non-sap');
    }

    public function pendingBillsNonSAP(Request $request)
    {
      // return $request;
      $subDivisionCode = $request->subDivisionCode;
      $billingGroup = $request->billingGroup;
      $billingCycle = $request->billingCycle;
      $MRU = $request->ledgerCode;
      $report = DB::select("select subDivisionCode, billingGroup, billingCycle, SUBSTRING(masterKey, 4, 4) ledgerCode, masterKey, meterNo, consumerName, consumerAddress, bill_status from bills where subDivisionCode = '$subDivisionCode' and billingGroup = '$billingGroup' and billingCycle = '$billingCycle' and SUBSTRING(masterKey, 4, 4) = '$MRU' and bill_status = 'N'");
      return view('pending-bills-non-sap-view')->with('data', $report);
    }

    public function consumerEnquiryForm()
    {
      return view('consumer-enquiry-from');
    }

    public function consumerEnquiry(Request $request)
    {
      // return $request;
      if ($request->legacyNumber != null) {
        return '1';
      } elseif ($request->acNumber != '') {
        $search = sapInput::where('contractAcNumber', $request->acNumber)->get();
        if ($search->count()) {
          $data = [
            'data' => $search,
            'status' => 'Y'
          ];
        } else {
          $data = [
            'status' => 'N'
          ];
        }
        // return $data;
        return view('consumer-enquiry-from')->with('data', $data);
        // return $search;
      } elseif ($request->meterNumber != '') {
        return '3';
      }
    }

    public function mrCheck()
    {
      // $search = sapOutput::where('MR_DOCNumber', 'null')->get();
      $search = DB::select("SELECT contractAcNumber FROM sap_outputs WHERE MR_DOCNumber is null");
      // return $search;
      foreach ($search as $result) {
        $getMr = DB::select("SELECT mr_docnumber FROM sap_inputs WHERE contractAcNumber = '$result->contractAcNumber'");
        // return $getMr[0]->mr_docnumber;
        $mr = $getMr[0]->mr_docnumber;
        $updateMr = DB::update("UPDATE sap_outputs SET MR_DOCNumber = '$mr' WHERE contractAcNumber = '$result->contractAcNumber'");
        echo $mr;
        echo "<br>";
      }
    }

    public function billingSummaryView()
    {

      return view('billingSummaryView');
    }

    public function billingSummary(Request $request)
    {
      // return $request;
      $subDivisionCode = $request->subDivisionCode;
      $billingGroup = $request->billingGroup;
      $billingCycle = $request->billingCycle;
      $records = DB::select("select subDivisionCode, subDivisionName, billingGroup, billingCycle, MRU, count(*) totalCons from sap_inputs where subDivisionCode = '$subDivisionCode' and billingGroup = '$billingGroup' and billingCycle = '$billingCycle' group by subDivisionCode, billingGroup, billingCycle, MRU order by subDivisionCode, billingGroup, billingCycle, MRU");

      // return $records;
      $data = [
        'data' => $records,
        'sub' => $subDivisionCode,
        'bg' => $billingGroup,
        'bc' => $billingCycle,
      ];
      return view('billingSummary')->with('data', $data);
    }

    public function billingSummaryNonSapView()
    {
      return view('billingSummaryNonSapView');
    }

    public function billingSummaryNonSap(Request $request)
    {
      $subDivisionCode = $request->subDivisionCode;
      $billingGroup = $request->billingGroup;
      $billingCycle = $request->billingCycle;
      $records = DB::select("select subDivisionCode, subDivisionName, billingGroup, billingCycle, SUBSTRING(masterKey, 4, 4) MRU, count(*) totalCons from bills where subDivisionCode = '$subDivisionCode' and billingGroup = '$billingGroup' and billingCycle = '$billingCycle' group by subDivisionCode, billingGroup, billingCycle, SUBSTRING(masterKey, 4, 4) order by subDivisionCode, billingGroup, billingCycle, SUBSTRING(masterKey, 4, 4)");

      // return $records;
      $data = [
        'data' => $records,
        'sub' => $subDivisionCode,
        'bg' => $billingGroup,
        'bc' => $billingCycle
      ];
      return view('billingSummaryNonSap')->with('data', $data);
    }

    // Mini Ledger
    public function miniLedgerView()
    {
      // return 'llll';
      return view('mini-ledger-view');
    }

    public function miniLedgerNonSapView()
    {
      // return 'llll';
      return view('mini-ledger-non-sap-view');
    }

    // Mini Ledger SAP
    public function miniLedgerReport(Request $request)
    {
      // return $request;
      $table = $request->table;
      $subDivisionCode = $request->subDivisionCode;
      $billingGroup = $request->billingGroup;
      $billingCycle = $request->billingCycle;
      // $meterReader = $request->meterReader;
      // $from = $request->from;
      // $to = $request->to;
      // $report = DB::select("select * from sap_outputs where subDivisonCode = '$subDivisionCode' and billingGroup ='$billingGroup' and billingCycle = '$billingCycle' and (STR_TO_DATE(currentMeterReadingDate,'%d-%m-%Y') between '$from' and '$to')");
      $report = DB::select("select contractAcNumber, consumerName, address, manufacturerSRNo, previoustNoteFromMeterReader, previousMeterReadingKWH, currentMeterReadingKWH, currentNoteFromMeterReader, currentMeterReadingDate, consumptionKWH, noOfDaysBilledFor, SOP, ED, OCTRAI, InfradevCess, amountOfCowCess, sundryChargesSOP, sundryChargesED, sundryChargesOCTRAI, sundryAllowancesSOP, sundryAllowancesED, sundryAllowancesOCTRAI, otherCharges, roundAdjustmentAmount, adjustmentAmountSOP, adjustmentAmountED, adjustmentAmountOCTRAI, provisionalAdjustmentAmountSOP, provisionalAdjustmentAmountED, provisionalAdjustmentAmountOCTRAI, arrearSOPCurrentYear, arrearEDCurrentYear, arrearOCTRAICurrentYear, arrearSOPPreviousYear, arrearEDPreviousYear, arrearOCTRAIPreviousYear, dueDate, DuedateCheq, totalAmountAfterDueDate, totBillAmount, meterReader, meterReader, currentMeterReadingDate, TotalInfraDevCess from $table where subDivisonCode = '$subDivisionCode' and billingGroup ='$billingGroup' and billingCycle = '$billingCycle'");
      $data = [
        'data' => $report,
        'table' => $table,
        'sub' => $subDivisionCode,
        'bg' => $billingGroup,
        'bc' => $billingCycle
      ];
      // return $data;
      return view('mini-ledger-view')->with('data', $data);
    }

    public function miniLedgerNonSapReport(Request $request)
    {
      // return $request;
      $table = $request->table;
      $subDivisionCode = $request->subDivisionCode;
      $billingGroup = $request->billingGroup;
      $billingCycle = $request->billingCycle;
      // $meterReader = $request->meterReader;
      $from = $request->from;
      $to = $request->to;
      $report = DB::select("select * from $table where blSubDivisionCode = '$subDivisionCode' and blBillingGroup ='$billingGroup' and blBillingCycle = '$billingCycle'");
      // return $report;
      $data = [
        'table' => $table,
        'data' => $report,
        'sub' => $subDivisionCode,
        'bg' => $billingGroup,
        'bc' => $billingCycle,
      ];
      return view('mini-ledger-non-sap-view')->with('data', $data);
    }

    public function meterReaderPerformanceReportView()
    {
      return view('meter-reader-preformance-report-view');
    }

    public function sapDetailsRequiredForm()
    {

      return view('sap-details-required-form');
    }

    // %
    public function percentageReportSapForm()
    {

      return view('precentage-report-sap-form');
    }
    public function percentageReportNonSapForm()
    {

      return view('precentage-report-non-sap-form');
    }

    public function ledgerPerformanceReportView()
    {
      return view('ledger-preformance-report-view');
    }

    public function ledgerPerformanceReportNonSapView()
    {
      // return 'lll';
      return view('ledger-preformance-report-non-sap-view');
    }


    public function meterReaderPerformanceReport(Request $request)
    {
      // return $request;
      $subDivisionCode = $request->subDivisionCode;
      $billingGroup = $request->billingGroup;
      $billingCycle = $request->billingCycle;
      // $meterReader = $request->meterReader;
      $from = $request->from;
      $to = $request->to;
      $report = DB::select("
      select meterReader as reader,
count( meterReader ) connections
from sap_outputs where subDivisonCode = '$subDivisionCode' and billingGroup ='$billingGroup' and billingCycle = '$billingCycle' and (currentMeterReadingDate between '$from' and '$to') group by meterReader
      ");
      $data = [
        'data' => $report,
        'sub' => $subDivisionCode,
        'bg' => $billingGroup,
        'bc' => $billingCycle,
        'from' => $from,
        'to' => $to
      ];
      // return $data;
      return view('meter-reader-preformance-report-view')->with('data', $data);
    }

    public function meterReaderPerformanceNonSapReport(Request $request)
    {
      // return $request;
      $subDivisionCode = $request->subDivisionCode;
      $billingGroup = $request->billingGroup;
      $billingCycle = $request->billingCycle;
      // $meterReader = $request->meterReader;
      $from = $request->from;
      $to = $request->to;
      $report = DB::select("
      select meterReader as reader,
count( meterReader ) connections
from billeds where blSubDivisionCode = '$subDivisionCode' and blBillingGroup ='$billingGroup' and blBillingCycle = '$billingCycle' and (blPresentReadingDate between '$from' and '$to') group by meterReader
      ");
      // return $report;
      $data = [
        'data' => $report,
        'sub' => $subDivisionCode,
        'bg' => $billingGroup,
        'bc' => $billingCycle,
        'from' => $from,
        'to' => $to
      ];
      return view('meter-reader-preformance-report-non-sap-view')->with('data', $data);
    }

    // Ledger Wise performance Report
    public function ledgerPerformanceReport(Request $request)
    {
      // return $request;
      $subDivisionCode = $request->subDivisionCode;
      $billingGroup = $request->billingGroup;
      $billingCycle = $request->billingCycle;
      // $meterReader = $request->meterReader;
      $from = $request->from;
      $to = $request->to;
      $report = DB::select("select MRU as reader, count( meterReader ) connections from sap_outputs where subDivisonCode = '$subDivisionCode' and billingGroup ='$billingGroup' and billingCycle = '$billingCycle' group by MRU");
      $data = [
        'data' => $report,
        'sub' => $subDivisionCode,
        'bg' => $billingGroup,
        'bc' => $billingCycle
      ];
      return view('ledger-preformance-report-view')->with('data', $data);
    }

    public function ledgerPerformanceReportNonSap(Request $request)
    {
      // return $request;
      $subDivisionCode = $request->subDivisionCode;
      $billingGroup = $request->billingGroup;
      $billingCycle = $request->billingCycle;
      // $meterReader = $request->meterReader;
      $report = DB::select("select SUBSTRING(blMasterKey, 4, 4) as reader, count( meterReader ) connections from billeds where blsubDivisionCode = '$subDivisionCode' and blBillingGroup ='$billingGroup' and blBillingCycle = '$billingCycle' group by SUBSTRING(blMasterKey, 4, 4)");
      $data = [
        'data' => $report,
        'sub' => $subDivisionCode,
        'bg' => $billingGroup,
        'bc' => $billingCycle
      ];
      return view('ledger-preformance-report-non-sap-view')->with('data', $data);
    }

    public function meterReaderPerformanceReportNonSapView()
    {
      return view('meter-reader-preformance-report-non-sap-view');
    }

    public function exceptional()
    {
      return view('exceptionalReports');
    }
    public function summary()
    {
      return view('summaryReports');
    }
    public function category()
    {
      return view('categoryWiseReports');
    }
    public function division()
    {
      return view('divisionWisePerfomanceReports');
    }
    public function multiple()
    {
      return view('multipleBillsSameLocation');
    }
    public function arrears()
    {
      return view('viewArrears');
    }
}
