<?php

namespace App\Http\Controllers;

use App\sapInput;
use Illuminate\Http\Request;

class SapInputController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\sapInput  $sapInput
     * @return \Illuminate\Http\Response
     */
    public function show(sapInput $sapInput)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\sapInput  $sapInput
     * @return \Illuminate\Http\Response
     */
    public function edit(sapInput $sapInput)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\sapInput  $sapInput
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, sapInput $sapInput)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\sapInput  $sapInput
     * @return \Illuminate\Http\Response
     */
    public function destroy(sapInput $sapInput)
    {
        //
    }
}
