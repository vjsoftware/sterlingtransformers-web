<?php

namespace App\Http\Controllers;

use App\tab_advice63;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class TabAdvice63Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function posting()
     {
         return view('advice63');
     }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(Request $request)
     {
       $data = tab_advice63::create($request->except('_token'));
       return redirect('advice63');
     }

     public function posthDataAjax(Request $request)
     {
       // return $request;
       $data = tab_advice63::create($request->except('_token'));

       if ($data) {
         $status = [
           'status' => 'success'
         ];
       } else {
         $status = [
           'status' => 'error'
         ];
       }

       return $data;
     }

    /**
     * Display the specified resource.
     *
     * @param  \App\tab_advice63  $tab_advice63
     * @return \Illuminate\Http\Response
     */
     public function adviceForPostingEdit()
     {

       $edit = tab_advice63::where('dUser', '')->get();

       return view('advice63-edit')->with('data', $edit);


     }
     public function editable($id)
     {
       $editview = tab_advice63::find($id);

       return view('advice63-EditView')->with('data', $editview);
     }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\tab_advice63  $tab_advice63
     * @return \Illuminate\Http\Response
     */
    public function edit(tab_advice63 $tab_advice63)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\tab_advice63  $tab_advice63
     * @return \Illuminate\Http\Response
     */
     public function update(Request $request)
     {
       // return $request;

       $this->validate($request,[
         'ledgerGroup' => 'required',
         ]);

         $uUser = Auth::user()->id;
         $uDate = Date('d-m-Y');
         $uTime = Date('H:i:s');
         // return $uUser;
         // $connectionDate = Carbon\Carbon::createFromFormat('d/m/Y', $request->connectionDate)->format('Y-m-d');
         // return $request->connectionDate;



         // $data = new BatchDataForm();
         $data = tab_advice63::find($request->id);
         $data->sysId = $request->sysId;
         $data->month = $request->month;
         $data->inputCode = $request->inputCode;
         $data->numberOfEnties = $request->numberOfEnties;
         $data->sheetNo = $request->sheetNo;
         $data->pageNo = $request->pageNo;
         $data->subdivisionCode = $request->subdivisionCode;
         $data->ledgerGroup = $request->ledgerGroup;
         $data->checkDigit = $request->checkDigit;
         $data->accountNo = $request->accountNo;
         $data->sundryAllowanceAmount = $request->sundryAllowanceAmount;
         $data->registerNo = $request->registerNo;
         $data->itemNo = $request->itemNo;
         $data->cashDeposit = $request->cashDeposit;
         $data->dateOfPayment = $request->dateOfPayment;
         $data->ccrParticular = $request->ccrParticular;
         $data->remarks = $request->remarks;
         $data->uReason = $request->uReason;
         $data->uUser = $uUser;
         $data->uDate = $uDate;
         $data->uTime = $uTime;
         $data->save();

         return redirect('/advice63-edit');
     }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\tab_advice63  $tab_advice63
     * @return \Illuminate\Http\Response
     */
     public function deletable($id)

     {
       $editview = tab_advice63::find($id);

       return view('advice63-DeleteView')->with('data', $editview);
     }
     public function delete(Request $request)
     {
       // return $request;
       $dUser = Auth::user()->id;

       $dDate = Date('d-m-Y');
       $dTime = Date('H:i:s');
       // return $uUser;
       // $connectionDate = Carbon\Carbon::createFromFormat('d/m/Y', $request->connectionDate)->format('Y-m-d');
       // return $request->connectionDate;



       // $data = new BatchDataForm();
       $data = tab_advice63::find($request->id);
       $data->dReason = $request->dReason;
       $data->dUser = $dUser;
       $data->uDate = $dDate;
       $data->uTime = $dTime;
       $data->save();


       return redirect('/advice63-edit');
     }

     public function batchDataGenerateReport(Request $request)
     {
       $bills = tab_advice63::where('month', $request['month'])->whereNull('dUser')->get();
       // return $bills;

       // return view('batchDataFormEdit')->with('data', $edit);
       $txt = '';
       foreach ($bills as $bill) {
         $txt .= $this->addSpace($bill->sysId, 3);
         $txt .= $this->addSpace($bill->inputCode, 2);
         $txt .= $this->addZeros($bill->sheetNo, 2);
         $txt .= $this->addZeros($bill->pageNo, 3);
         $txt .= $this->addZeros($bill->numberOfEnties, 3);
         $txt .= $this->addSpace($bill->subdivisionCode, 3);
         $txt .= $this->addSpace($bill->ledgerGroup, 4);
         $txt .= $this->addSpace($bill->accountNo, 4);
         $txt .= $this->addSpace($bill->checkDigit, 4);
         $txt .= $this->addZeros($bill->sundryAllowanceAmount, 7);
         $txt .= $this->addZeros($bill->registerNo, 7);
         $txt .= $this->addZeros($bill->itemNo, 7);
         $txt .= $this->addZeros($bill->cashDeposit, 7);
         $txt .= $this->addZeros($bill->dateOfPayment, 7);
         $txt .= "\r\n";
       }

       $sub = 'ADVICE-63';

       return response($txt)
                 ->withHeaders([
                     'Content-Type' => 'text/plain',
                     'Cache-Control' => 'no-store, no-cache',
                     'Content-Disposition' => "attachment; filename=$sub.txt",
                 ]);
       return $bills;
     }

     public function vle($value = '', $count)
     {
       $countMain = $count;
       $count = $count - 1;
       if ($value > 0) {
         $abs = abs($value);
         $abs = sprintf("%.2f", $abs);
         $name = '+'.$this->addZeros($abs, $count);
       } elseif ($value == 0) {
         $abs = 0;
         $abs = sprintf("%.2f", $abs);
         $name = '+'.$this->addZeros($abs, $count);
       } else {
         $abs = abs($value);
         $abs = sprintf("%.2f", $abs);
         // $abs = number_format($abs, 2);
         // return $abs = number_format($abs, 2);
         $name = '-'.$this->addZeros($abs, $count);
         // $blNetSop = $abs;
       }
       return $name;
     }

     public function vlen($value = '', $count)
     {
       $countMain = $count;
       $count = $count - 1;
       if ($value > 0) {
         $name = '+'.$this->addZeros((int) $value, $count);
       } elseif ($value == 0) {
         $name = '+'.$this->addZeros('', $count);
       } else {
         $abs = abs((int) $value);
         // $abs = number_format($abs, 2);
         $name = '-'.$this->addZeros($abs, $count);
         // $blNetSop = $abs;
       }
       return $name;
     }

     public function addZerosSign($value='', $length)
     {
       $count = strlen($value);
       if ($value >= 1 || $value == 0 || $value == 0.00) {
         $sign = "+";
       } else {
         $sign = "-";
       }
       $numberOfZeros = $length - $count;
       $zeros = '';
       for ($i=1; $i < $numberOfZeros; $i++) {
         $zeros .= 0;
       }

       $newValue = $sign.$zeros.$value;
       return $newValue;
     }


     public function addZerosSignN($value='', $length)
     {
       $count = strlen($value);
       if ($value >= 1 || $value == 0 || $value == 0.00) {
         $sign = "+";
       } else {
         $sign = "-";
       }
       $numberOfZeros = $length - $count;
       $zeros = '';
       for ($i=0; $i < $numberOfZeros; $i++) {
         $zeros .= 0;
       }

       $newValue = $sign.$zeros.$value;
       return $newValue;
     }

     public function addZeros($value='', $length)
     {
       $count = strlen($value);
       $numberOfZeros = $length - $count;
       $zeros = '';
       for ($i=0; $i < $numberOfZeros; $i++) {
         $zeros .= 0;
       }

       $newValue = $zeros.$value;
       return $newValue;
     }

     public function addZerosBack($value='', $length)
     {
       $count = strlen($value);
       $numberOfZeros = $length - $count;
       $zeros = '';
       for ($i=0; $i < $numberOfZeros; $i++) {
         $zeros .= 0;
       }

       $newValue = $value.$zeros;
       return $newValue;
     }

     public function addSpace($value='', $length)
     {

       $count = strlen(trim($value));
       $numberOfZeros = $length - $count;
       $zeros = '';
       for ($i=0; $i < $numberOfZeros; $i++) {
         $zeros .= ' ';
       }

       $newValue = $value.$zeros;
       $newValue = substr($newValue, 0, $length);
       return $newValue;
     }

     public function addZerosFloat($value = '', $count)
     {
       $countMain = $count;
       $count = $count - 2;
       if ($value > 0) {
         $abs = sprintf("%.2f", $value);
         $name = $this->addZeros($abs, $countMain);
         // $name = $this->addZeros($value, $count);
       } elseif ($value == 0) {
         $abs = 0;
         $abs = sprintf("%.2f", $abs);
         $name = $this->addZeros($abs, $countMain);
       } else {
         $abs = abs($value);
         $abs = sprintf("%.2f", $abs);
         // $abs = number_format($abs, 2);
         // return $abs = number_format($abs, 2);
         $name = $this->addZeros($abs, $countMain);
         // $blNetSop = $abs;
       }
       return $name;
     }

     public function addSpaceFront($value='', $length)
     {
       $count = strlen($value);
       $numberOfZeros = $length - $count;
       $zeros = '';
       for ($i=0; $i < $numberOfZeros; $i++) {
         $zeros .= ' ';
       }

       $newValue = $zeros.$value;
       return $newValue;
     }
}
