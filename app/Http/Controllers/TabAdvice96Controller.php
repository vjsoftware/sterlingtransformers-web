<?php

namespace App\Http\Controllers;

use App\tab_advice96;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class TabAdvice96Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function charging()
    {
        return view('advice96');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(Request $request)
     {
       $data = tab_advice96::create($request->except('_token'));
       return redirect('advice96');
     }

     public function chargeDataAjax(Request $request)
     {
       // return $request;
       $data = tab_advice96::create($request->except('_token'));

       if ($data) {
         $status = [
           'status' => 'success'
         ];
       } else {
         $status = [
           'status' => 'error'
         ];
       }

       return $data;
     }

     public function adviceForChargingEdit()
     {

       $edit = tab_advice96::whereNull('dUser')->get();

       return view('advice96-edit')->with('data', $edit);

     }
     public function editable($id)
     {
       $editview = tab_advice96::find($id);

       return view('advice96-EditView')->with('data', $editview);
     }

    /**
     * Display the specified resource.
     *
     * @param  \App\tab_advice96  $tab_advice96
     * @return \Illuminate\Http\Response
     */
     public function update(Request $request)
     {
       // return $request;

       $this->validate($request,[
         'ledgerGroup' => 'required',
         ]);

         $uUser = Auth::user()->id;
         $uDate = Date('d-m-Y');
         $uTime = Date('H:i:s');
         // return $uUser;
         // $connectionDate = Carbon\Carbon::createFromFormat('d/m/Y', $request->connectionDate)->format('Y-m-d');
         // return $request->connectionDate;



         // $data = new BatchDataForm();
         $data = tab_advice96::find($request->id);
         $data->month = $request->month;
         $data->sysId = $request->sysId;
         $data->inputCode = $request->inputCode;
         $data->pSub = $request->pSub;
         $data->pSer = $request->pSer;
         $data->subdivisionCode = $request->subdivisionCode;
         $data->ledgerGroup = $request->ledgerGroup;
         $data->billingCycle = $request->billingCycle;
         $data->billingGroup = $request->billingGroup;
         $data->registerNo = $request->registerNo;
         $data->pageNo = $request->pageNo;
         $data->rcoFee = $request->rcoFee;
         $data->from = $request->from;
         $data->to = $request->to;
         $data->accountNo = $request->accountNo;
         $data->checkDigit = $request->checkDigit;
         $data->remarks = $request->remarks;
         $data->uReason = $request->uReason;
         $data->uUser = $uUser;
         $data->uDate = $uDate;
         $data->uTime = $uTime;
         $data->save();

         return redirect('/advice96-edit');
     }

     public function deletable($id)

     {
       $editview = tab_advice96::find($id);

       return view('advice96-DeleteView')->with('data', $editview);
     }

     public function delete(Request $request)
     {
       // return $request;
       $dUser = Auth::user()->id;

       $dDate = Date('d-m-Y');
       $dTime = Date('H:i:s');
       // return $uUser;
       // $connectionDate = Carbon\Carbon::createFromFormat('d/m/Y', $request->connectionDate)->format('Y-m-d');
       // return $request->connectionDate;



       // $data = new BatchDataForm();
       $data = tab_advice96::find($request->id);
       $data->dReason = $request->dReason;
       $data->dUser = $dUser;
       $data->dDate = $dDate;
       $data->dTime = $dTime;
       $data->save();


       return redirect('/advice96-edit');
     }

     public function batchDataGenerateReport(Request $request)
     {
       $bills = tab_advice96::where('month', $request['month'])->whereNull('dUser')->get();
       // return $bills;

       // return view('batchDataFormEdit')->with('data', $edit);
       $txt = '';
       foreach ($bills as $bill) {
         $txt .= $this->addSpace($bill->sysId, 3);
         $txt .= $this->addSpace($bill->inputCode, 2);
         $txt .= $this->addZeros($bill->pSub, 2);
         $txt .= $this->addZeros($bill->pSer, 3);
         $txt .= $this->addSpace($bill->billingCycle, 2);
         $txt .= $this->addSpace($bill->billingGroup, 1);
         $txt .= $this->addSpace($bill->subdivisionCode, 3);
         $txt .= $this->addSpace($bill->registerNo, 3);
         $txt .= $this->addSpace($bill->ledgerGroup, 4);
         $txt .= $this->addSpace($bill->accountNo, 4);
         $txt .= $this->addSpace($bill->from, 4);
         $txt .= $this->addZeros($bill->to, 7);
         $txt .= "\r\n";
       }

       $sub = 'ADVICE-96';

       return response($txt)
                 ->withHeaders([
                     'Content-Type' => 'text/plain',
                     'Cache-Control' => 'no-store, no-cache',
                     'Content-Disposition' => "attachment; filename=$sub.txt",
                 ]);
       return $bills;
     }

     public function vle($value = '', $count)
     {
       $countMain = $count;
       $count = $count - 1;
       if ($value > 0) {
         $abs = abs($value);
         $abs = sprintf("%.2f", $abs);
         $name = '+'.$this->addZeros($abs, $count);
       } elseif ($value == 0) {
         $abs = 0;
         $abs = sprintf("%.2f", $abs);
         $name = '+'.$this->addZeros($abs, $count);
       } else {
         $abs = abs($value);
         $abs = sprintf("%.2f", $abs);
         // $abs = number_format($abs, 2);
         // return $abs = number_format($abs, 2);
         $name = '-'.$this->addZeros($abs, $count);
         // $blNetSop = $abs;
       }
       return $name;
     }

     public function vlen($value = '', $count)
     {
       $countMain = $count;
       $count = $count - 1;
       if ($value > 0) {
         $name = '+'.$this->addZeros((int) $value, $count);
       } elseif ($value == 0) {
         $name = '+'.$this->addZeros('', $count);
       } else {
         $abs = abs((int) $value);
         // $abs = number_format($abs, 2);
         $name = '-'.$this->addZeros($abs, $count);
         // $blNetSop = $abs;
       }
       return $name;
     }

     public function addZerosSign($value='', $length)
     {
       $count = strlen($value);
       if ($value >= 1 || $value == 0 || $value == 0.00) {
         $sign = "+";
       } else {
         $sign = "-";
       }
       $numberOfZeros = $length - $count;
       $zeros = '';
       for ($i=1; $i < $numberOfZeros; $i++) {
         $zeros .= 0;
       }

       $newValue = $sign.$zeros.$value;
       return $newValue;
     }


     public function addZerosSignN($value='', $length)
     {
       $count = strlen($value);
       if ($value >= 1 || $value == 0 || $value == 0.00) {
         $sign = "+";
       } else {
         $sign = "-";
       }
       $numberOfZeros = $length - $count;
       $zeros = '';
       for ($i=0; $i < $numberOfZeros; $i++) {
         $zeros .= 0;
       }

       $newValue = $sign.$zeros.$value;
       return $newValue;
     }

     public function addZeros($value='', $length)
     {
       $count = strlen($value);
       $numberOfZeros = $length - $count;
       $zeros = '';
       for ($i=0; $i < $numberOfZeros; $i++) {
         $zeros .= 0;
       }

       $newValue = $zeros.$value;
       return $newValue;
     }

     public function addZerosBack($value='', $length)
     {
       $count = strlen($value);
       $numberOfZeros = $length - $count;
       $zeros = '';
       for ($i=0; $i < $numberOfZeros; $i++) {
         $zeros .= 0;
       }

       $newValue = $value.$zeros;
       return $newValue;
     }

     public function addSpace($value='', $length)
     {

       $count = strlen(trim($value));
       $numberOfZeros = $length - $count;
       $zeros = '';
       for ($i=0; $i < $numberOfZeros; $i++) {
         $zeros .= ' ';
       }

       $newValue = $value.$zeros;
       $newValue = substr($newValue, 0, $length);
       return $newValue;
     }

     public function addZerosFloat($value = '', $count)
     {
       $countMain = $count;
       $count = $count - 2;
       if ($value > 0) {
         $abs = sprintf("%.2f", $value);
         $name = $this->addZeros($abs, $countMain);
         // $name = $this->addZeros($value, $count);
       } elseif ($value == 0) {
         $abs = 0;
         $abs = sprintf("%.2f", $abs);
         $name = $this->addZeros($abs, $countMain);
       } else {
         $abs = abs($value);
         $abs = sprintf("%.2f", $abs);
         // $abs = number_format($abs, 2);
         // return $abs = number_format($abs, 2);
         $name = $this->addZeros($abs, $countMain);
         // $blNetSop = $abs;
       }
       return $name;
     }

     public function addSpaceFront($value='', $length)
     {
       $count = strlen($value);
       $numberOfZeros = $length - $count;
       $zeros = '';
       for ($i=0; $i < $numberOfZeros; $i++) {
         $zeros .= ' ';
       }

       $newValue = $zeros.$value;
       return $newValue;
     }

}
