<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\sapInput;

use DB;
use Auth;

class dataTransferController extends Controller
{
    public function sapTransferView()
    {
      return view('sap_transfer');
    }

    public function sapDataTransfer()
    {

      $newData = DB::select("select * from sap_server where transfer_status = 'N'");
      // return $newData[0]->newData;
      foreach ($newData as $data) {
        $rowSearch = DB::select("select * from sap_server where transfer_status = 'N' and contractAcNumber = $data->contractAcNumber");
        if ($rowSearch->count()) {
          // code...
        } else {
          // return $data;
          $updateId = $data->id;
          $newInput = new sapInput();
          $newInput->subDivisionCode = $data->subDivisionCode;
          $newInput->subDivisionName = $data->subDivisionName;
          $newInput->MRU = $data->MRU;
          $newInput->MR_DOCNumber = $data->MR_DOCNumber;
          $newInput->neighbourMeterId = $data->neighbourMeterId;
          $newInput->connectedPoleNINNumber = $data->connectedPoleNINNumber;
          // return strlen(trim($data->consumerLegacyNumber));
          if (strlen(trim($data->consumerLegacyNumber)) < 10) {
            $consLno1 = '00000000000';
          } else {
            $consLno1 = $data->consumerLegacyNumber;
          }
          $newInput->consumerLegacyNumber = $data->consumerLegacyNumber;
          $newInput->consLno1 = $consLno1;
          $newInput->contractAcNumber = $data->contractAcNumber;
          $newInput->consumerName = $data->consumerName;
          $newInput->houseNumber = $data->houseNumber;
          $newInput->streetNumber = $data->streetNumber;
          $newInput->address = $data->address;
          $newInput->category = $data->category;
          $newInput->subCategory = $data->subCategory;
          $newInput->moveInFlag = $data->moveInFlag;
          $newInput->categoryChangeFlag = $data->categoryChangeFlag;
          $newInput->categoryChangeDate = $data->categoryChangeDate;
          $newInput->oldCategory = $data->oldCategory;
          $newInput->oldSubCategory = $data->oldSubCategory;
          $newInput->industryCode = $data->industryCode;
          $newInput->billingCycle = $data->billingCycle;
          $newInput->billingCycleType = $data->billingCycleType;
          $newInput->billingGroup = $data->billingGroup;
          $newInput->triVectorMeterFlag = $data->triVectorMeterFlag;
          $newInput->installation = $data->installation;
          $newInput->meterMnfSerialNumber = $data->meterMnfSerialNumber;
          $newInput->meterManufacturerName = $data->meterManufacturerName;
          $newInput->meterSAPSerialNumber = $data->meterSAPSerialNumber;
          $newInput->multiplicationFactor = $data->multiplicationFactor;
          $newInput->overallMf = $data->overallMf;
          $newInput->oldMf = $data->oldMf;
          $newInput->numberOfDigits = $data->numberOfDigits;
          $newInput->lineCtRatio = $data->lineCtRatio;
          $newInput->externalPtRatio = $data->externalPtRatio;
          $newInput->phaseCode = $data->phaseCode;
          $newInput->sanctionedLoadKW = $data->sanctionedLoadKW;
          $newInput->contractedDemandKVA = $data->contractedDemandKVA;
          $newInput->sanctionedLoadChangeFlag = $data->sanctionedLoadChangeFlag;
          $newInput->sanctionedLoadChangeDate = $data->sanctionedLoadChangeDate;
          $newInput->oldSanctionedLoadKW = $data->oldSanctionedLoadKW;
          $newInput->admissibleVoltage = $data->admissibleVoltage;
          $newInput->supplyVoltage = $data->supplyVoltage;
          $newInput->noOfBOardEmployees6 = $data->noOfBOardEmployees6;
          $newInput->noOfBOardEmployees7 = $data->noOfBOardEmployees7;
          $newInput->noOfBOardEmployees8 = $data->noOfBOardEmployees8;
          $newInput->scheduleMeterReadDate = $data->scheduleMeterReadDate;
          $newInput->previousReadingDate = $data->previousReadingDate;
          $newInput->previousBillDate = $data->previousBillDate;
          $newInput->previousReadingKWH = $data->previousReadingKWH;
          $newInput->previousReadingKVA = $data->previousReadingKVA;
          $newInput->previousReadingKVAH = $data->previousReadingKVAH;
          $newInput->previousMeterStatus = $data->previousMeterStatus;
          $newInput->previousBillType = $data->previousBillType;
          $newInput->meterChangeFlag = $data->meterChangeFlag;
          $newInput->meterChangeDate = $data->meterChangeDate;
          $newInput->totalOldMeterConsumptionUnitsKWH = $data->totalOldMeterConsumptionUnitsKWH;
          $newInput->totalOldMeterConsumptionUnitsKVAH = $data->totalOldMeterConsumptionUnitsKVAH;
          $newInput->oldMeterTotalRentAmount = $data->oldMeterTotalRentAmount;
          $newInput->oldMeterTotalServiceRentAmount = $data->oldMeterTotalServiceRentAmount;
          $newInput->oldMeterTotalServiceChargeAmount = $data->oldMeterTotalServiceChargeAmount;
          $newInput->oldMeterTotalMBCRentAmount = $data->oldMeterTotalMBCRentAmount;
          $newInput->newMeterInitialReadingKWH = $data->newMeterInitialReadingKWH;
          $newInput->newMeterInitialReadingKVAH = $data->newMeterInitialReadingKVAH;
          $newInput->reconnectionFlag = $data->reconnectionFlag;
          $newInput->reconnectionDate = $data->reconnectionDate;
          $newInput->consumptionBeforeDisConnectionKWH = $data->consumptionBeforeDisConnectionKWH;
          $newInput->consumptionBeforeDisConnectionKVAH = $data->consumptionBeforeDisConnectionKVAH;
          $newInput->periodBeforeDisConnection = $data->periodBeforeDisConnection;
          $newInput->edExemptedFlag = $data->edExemptedFlag;
          $newInput->edExemptionExpiry = $data->edExemptionExpiry;
          $newInput->octraiChargesApplicableFlag = $data->octraiChargesApplicableFlag;
          $newInput->meterRentRate = $data->meterRentRate;
          $newInput->serviceRentRate = $data->serviceRentRate;
          if (trim($data->peakLoadExemptionCharges) == '') {
            $peakLoadExemptionCharges = 0.00;
          } else {
            $peakLoadExemptionCharges = $data->peakLoadExemptionCharges;
          }
          $newInput->peakLoadExemptionCharges = $peakLoadExemptionCharges;
          $newInput->peakLoadExemptionChargesExpiry = $data->peakLoadExemptionChargesExpiry;
          $newInput->MMTSCorrectionFactor = $data->MMTSCorrectionFactor;
          $newInput->shuntCapacitorChargesApplicableFlag = $data->shuntCapacitorChargesApplicableFlag;
          $newInput->capacityOfCapacitor = $data->capacityOfCapacitor;
          $newInput->MBCRentRate = $data->MBCRentRate;
          $newInput->serviceChargeRate = $data->serviceChargeRate;
          $newInput->seasonStartDate = $data->seasonStartDate;
          $newInput->seasonEndDate = $data->seasonEndDate;
          $newInput->sundryChargesSOP = $data->sundryChargesSOP;
          $newInput->sundryChargesED = $data->sundryChargesED;
          $newInput->sundryChargesOCTRAI = $data->sundryChargesOCTRAI;
          $newInput->sundryAllowancesSOP = $data->sundryAllowancesSOP;
          $newInput->sundryAllowancesED = $data->sundryAllowancesED;
          $newInput->sundryAllowancesOCTRAI = $data->sundryAllowancesOCTRAI;
          $newInput->waterChargesProvisionalAmount = $data->waterChargesProvisionalAmount;
          $newInput->otherCharges = $data->otherCharges;
          $newInput->roundAdjustmentAmount = $data->roundAdjustmentAmount;
          $newInput->adjustmentAmountSOP = $data->adjustmentAmountSOP;
          $newInput->adjustmentAmountED = $data->adjustmentAmountED;
          $newInput->adjustmentAmountOCTRAI = $data->adjustmentAmountOCTRAI;
          $newInput->provisionalAdjustmentAmountSOP = $data->provisionalAdjustmentAmountSOP;
          $newInput->provisionalAdjustmentAmountED = $data->provisionalAdjustmentAmountED;
          $newInput->provisionalAdjustmentAmountOCTRAI = $data->provisionalAdjustmentAmountOCTRAI;
          $newInput->arrearSOPCurrentYear = $data->arrearSOPCurrentYear;
          $newInput->arrearEDCurrentYear = $data->arrearEDCurrentYear;
          $newInput->arrearOCTRAICurrentYear = $data->arrearOCTRAICurrentYear;
          $newInput->arrearSOPPreviousYear = $data->arrearSOPPreviousYear;
          $newInput->arrearEDPreviousYear = $data->arrearEDPreviousYear;
          $newInput->arrearOCTRAIPreviousYear = $data->arrearOCTRAIPreviousYear;
          if (strpos($data->advanceConsumptionDeposit, '.00-')) {
            $advanceConsumptionDeposit = "-".str_replace(".00-", ".00", $data->advanceConsumptionDeposit);
          } else {
            $advanceConsumptionDeposit = $data->advanceConsumptionDeposit;
          }
          $newInput->advanceConsumptionDeposit = $advanceConsumptionDeposit;
          $newInput->courtCaseAmount = $data->courtCaseAmount;
          $newInput->previousKWHCycle1 = $data->previousKWHCycle1;
          $newInput->previousKWHCycle2 = $data->previousKWHCycle2;
          $newInput->previousKWHCycle3 = $data->previousKWHCycle3;
          $newInput->previousKWHCycle4 = $data->previousKWHCycle4;
          $newInput->previousKWHCycle5 = $data->previousKWHCycle5;
          $newInput->previousKWHCycle6 = $data->previousKWHCycle6;
          $newInput->averageKWHofAbove6Cycles = $data->averageKWHofAbove6Cycles;
          $newInput->samePeriodLastYearConsumptionKWH = $data->samePeriodLastYearConsumptionKWH;
          $newInput->PF1 = $data->PF1;
          $newInput->PF2 = $data->PF2;
          $newInput->PF3 = $data->PF3;
          $newInput->averageofAboveThreePF = $data->averageofAboveThreePF;
          $newInput->standardPFOfConsumerCategory = $data->standardPFOfConsumerCategory;
          $newInput->MD1 = $data->MD1;
          $newInput->MD2 = $data->MD2;
          $newInput->gaushalaFlag = $data->gaushalaFlag;
          $newInput->arrearWaterChargesCurrentYear = $data->arrearWaterChargesCurrentYear;
          $newInput->arrearWaterChargesPreviousYear = $data->arrearWaterChargesPreviousYear;
          $newInput->dataExpiryDate = $data->dataExpiryDate;
          $newInput->maxOfAboveSixMDI = $data->maxOfAboveSixMDI;
          $newInput->factorForCustomerCategory = $data->factorDay;
          $newInput->DValueInLDHFNumberOfWorkingDaysPerMonthForCustomerCategory = $data->dValueDays;
          $newInput->HValueInLDHFSupplyHoursPerDayForCustomerCategory = $data->hValueDay;
          $newInput->previousTotalEstimatedConsumptionKWH = $data->previousTotalEstimatedConsumptionKWH;
          $newInput->previousTotalEstimatedConsumptionKVAH = $data->previousTotalEstimatedConsumptionKVAH;
          $newInput->previousEstimatedMDI = $data->previousEstimatedMDI;
          $newInput->nearestCashCounter = $data->nearestCashCounter;
          $newInput->complaintCenterPhoneNumber = $data->complaintCenterPhoneNumber;
          $newInput->previousPaymentStatus = $data->previousPaymentStatus;
          $newInput->reasonForAdjustment = $data->reasonForAdjustment;
          $newInput->miscExpensesDetails = $data->miscExpensesDetails;
          $newInput->miscExpenses = $data->miscExpenses;
          $newInput->meterSecurityAmount = $data->meterSecurityAmount;
          $newInput->waterChargesApplicableFlag = $data->waterChargesApplicableFlag;
          $newInput->transformerCode = $data->transformerCode;
          $newInput->oldMeterRent = $data->oldMeterRent;
          $newInput->expirationDateOfOldMeterRent = $data->expirationDateOfOldMeterRent;
          $newInput->oldMBCRent = $data->oldMBCRent;
          $newInput->expirationDateOfOldMBCRent = $data->expirationDateOfOldMBCRent;
          $newInput->oldServiceCharges = $data->oldServiceCharges;
          $newInput->expirationDateOfOldServiceCharges = $data->expirationDateOfOldServiceCharges;
          $newInput->meterLocation = $data->meterLocation;
          $newInput->meterType = $data->meterType;
          $newInput->version = $data->version;
          $newInput->ML = $data->ML;
          $newInput->MT = $data->MT;
          $newInput->mobileNo = $data->mobileNo;
          $newInput->feederCode = $data->feederCode;
          $newInput->SCWSDAmountWithHeld = $data->SCWSDAmountWithHeld;
          if (trim($data->previousFyCons) == '') {
            $previousFyCons = 0;
          } else {
            $previousFyCons = $data->previousFyCons;
          }
          $newInput->previousFyCons = $previousFyCons;
          $newInput->muncipalTaxApplicableFlag = $data->muncipalTaxApplicableFlag;
          $newInput->PLogicApplicableFlag = $data->PLogicApplicableFlag;
          $newInput->limitForPNore = $data->limitForPNore;
          $newInput->receivedAs = 'A';
          $newInput->created_by = '';
          $newInput->save();

          if ($newInput) {
            $transfered_by = Auth::user()->email;
            $transfered_at = Date('Y-m-d H:i:s');
            $updateServer = DB::update("update sap_server set transfer_status = 'Y', transfered_by = '$transfered_by', transfered_at = '$transfered_at' where id = $updateId");
          }
        }




        // return 'llll';

        // return $newInput;
      }

        $dataa['status'] = 'Data Transfer Success';
        return view('message')->with('data', $dataa);
    }
}
