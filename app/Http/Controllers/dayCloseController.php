<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class dayCloseController extends Controller
{
    public function sap()
    {
      $user_log = DB::SELECT("SELECT t1.id, t1.userId, t1.loginDate, t1.loginTime, t2.name FROM user_logs as t1 inner join users as t2 on t1.userId = t2.email WHERE t1.status = 'O' and t2.userGroup = 'sap'");

      // return $user_log;
      return view('day-close-sap')->with('data', $user_log);
    }
    public function nonSap()
    {
      $user_log = DB::SELECT("SELECT t1.id, t1.userId, t1.loginDate, t1.loginTime, t2.name FROM user_logs as t1 inner join users as t2 on t1.userId = t2.email WHERE t1.status = 'O' and t2.userGroup = 'non sap'");

      // return $user_log;
      return view('day-close-non-sap')->with('data', $user_log);
    }
}
