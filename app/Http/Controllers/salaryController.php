<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\User;
use DB;

class salaryController extends Controller
{
    public function sapForm()
    {
      // $meterReaders = User::where('role', 2)->where('userGroup', 'sap')->get();
      return view('salary-report-sap-form-view');
    }

    public function sap(Request $request)
    {
      $tableName = $request['table'];
      $divisionCode = $request['divisionCode'];
      $group = $request['group'];
      $report = DB::SELECT("select subDivisonCode, billingGroup, billingCycle, count(*) count, users.name name, users.email email, users.mobile mobile, users.aadhaarNo aadhaar, users.address address from $tableName inner join users on $tableName.meterReader = users.email WHERE subDivisonCode = '$divisionCode' AND billingGroup = '$group' group by $tableName.subDivisonCode, $tableName.billingGroup, $tableName.billingCycle, $tableName.meterReader order by $tableName.subDivisonCode, $tableName.billingGroup, $tableName.billingCycle, $tableName.meterReader");
      // return $report;
      return view('salary-report-sap')->with('data', $report);
    }

    // Form View
    public function nonSapForm()
    {
      return view('salary-report-non-sap-form-view');
    }

    // Post
    public function nonSap(Request $request)
    {
      // return $request;
      $tableName = $request['table'];
      $divisionCode = $request['divisionCode'];
      $group = $request['group'];
      $report = DB::SELECT("select blSubDivisionCode, blBillingGroup, blBillingCycle, count(*) count, users.name name, users.email email, users.mobile mobile, users.aadhaarNo aadhaar, users.address address from $tableName inner join users on $tableName.meterReader = users.email WHERE substr(blMasterKey, 1, 2) = '$divisionCode' AND blBillingGroup = '$group' group by $tableName.blSubDivisionCode, $tableName.blBillingGroup, $tableName.blBillingCycle, $tableName.meterReader order by $tableName.blSubDivisionCode, $tableName.blBillingGroup, $tableName.blBillingCycle, $tableName.meterReader");
      // return count($report);
      return view('salary-report-non-sap')->with('data', $report);
    }
}
