<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class scheduleReport extends Controller
{
    public function sap()
    {
      return view('schedule-details-sap');
    }

    public function sapInfo($meterReader, $ledgerCode)
    {
      $data = [
        'meterReader' => $meterReader,
        'ledgerCode' => $ledgerCode,
      ];
      return view('schedule-details-sap-info')->with('data', $data);
    }

    public function nonsap()
    {
      return view('schedule-details-non-sap');
    }

    public function nonsapInfo($meterReader, $ledgerCode)
    {
      $data = [
        'meterReader' => $meterReader,
        'ledgerCode' => $ledgerCode,
      ];
      return view('schedule-details-non-sap-info')->with('data', $data);
    }
}
