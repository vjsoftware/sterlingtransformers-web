<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\sapOutput;
use App\Billed;
use App\Bill;

use SoapClient;
use DB;

use Carbon\Carbon;


class soapDataController extends Controller
{
    public function transfer(Request $request)
    {

      $sub = $request->subDivisionCode;
      $from = $request->from;
      // $data = sapOutput::where('soapStatus', '')
      //                     ->orderBy('id', 'DESC')
      //                     ->take(100)->get();

      $data = DB::select("select id from sap_outputs where subDivisonCode = '$sub' and currentMeterReadingDate = '$from' and soapStatus is null or soapStatus = '' or soapStatus = 'F'");
      // $data = DB::select("select id from sap_outputs where soapStatus = 'F' or soapStatus = '' or soapStatus is null limit 2000");
      // return $data;
//             $data = sapOutput::select('id')
//                           ->where('soapStatus', 'F')
//                           ->where('soapMessage', 'Invalid IP address(13.126.168.177)')
//                           ->where('soapMessage', 'Invalid IP address(13.126.181.30)')
//                           ->where('soapMessage', 'Invalid IP address(13.126.44.24)')
//                           ->where('soapMessage', 'Invalid IP address(13.127.113.66)')
// ->take(1500)->get();
      // return $data;
      // $data = DB::select("
      // SELECT id FROM sap_outputs WHERE soapStatus = 'F'
      // and soapMessage = 'Invalid IP address(13.126.70.254)'
      // or soapMessage = 'Invalid IP address(13.232.18.34)'
      // or soapMessage = 'Invalid IP address(13.232.81.213)'
      //  order by id DESC limit 300");
      // $data = DB::select("SELECT id FROM sap_outputs WHERE subDivisonCode in (3211, 3212, 3213, 3215) and soapStatus = 'F'");
      // return $data;
      $count = count($data);

      foreach ($data as $output) {
        // $sapOutput = sapOutput::find($output->id);
        $sapOutput = DB::select("SELECT * FROM sap_outputs WHERE id = '$output->id'");
        // $arr = [$sapOutput];
        // return $sapOutput[0]->id;

        $scheduledMRDate = Carbon::createFromFormat('d/m/Y', $sapOutput[0]->scheduledMRDate)->format('Y-m-d');
        $scheduledMRTime = 'T'.'00:00:00';
        $scheduledMRDate = $scheduledMRDate.$scheduledMRTime;
        $currentMeterReadingDate = Carbon::createFromFormat('d-m-Y', $sapOutput[0]->currentMeterReadingDate)->format('Y-m-d');
        $currentMeterReadingTime = 'T'.'00:00:00';
        $currentMeterReadingDate = $currentMeterReadingDate.$currentMeterReadingTime;
        $previousMeterReadingDate = Carbon::createFromFormat('d/m/Y', $sapOutput[0]->previousMeterReadingDate)->format('Y-m-d');
        $previousMeterReadingTime = 'T'.'00:00:00';
        $previousMeterReadingDate = $previousMeterReadingDate.$previousMeterReadingTime;
        $billDate = Carbon::createFromFormat('d/m/Y', $sapOutput[0]->billDate)->format('Y-m-d');
        $billTime = 'T'.'00:00:00';
        $billDate = $billDate.$billTime;
        $dueDate = Carbon::createFromFormat('d-m-Y', $sapOutput[0]->dueDate)->format('Y-m-d');
        $dueTime = 'T'.'00:00:00';
        $dueDate = $dueDate.$dueTime;
        $billDate2 = Carbon::createFromFormat('d-m-Y', $sapOutput[0]->dueDate)->format('Y-m-d');
        $billTime2 = 'T'.'00:00:00';
        $billDate2 = $billDate2.$billTime2;
        $dueDate2 = Carbon::createFromFormat('d-m-Y', $sapOutput[0]->DuedateCheq)->format('Y-m-d');
        $dueTime2 = 'T'.'00:00:00';
        $dueDate2 = $dueDate2.$dueTime2;
        $currentMeterReadingTime = Carbon::createFromFormat('H:i:s', $sapOutput[0]->currentMeterReadingTime)->format('His');
        // return $currentMeterReadingTime;
        // return $sapOutput[0]->subDivisonCode'];
        try {
            // $client = new SoapClient ( "some.aspx?WSDL" );
            $client = new SoapClient("https://billingdatareceiver.pspcl.in/SAP_SBM_DATA.ASMX?wsdl");
            // return $sapOutput[0]->scheduledMRDate'];
            // $scheduledMRDate = 2018-06-11T21:32:52;
            if ($sapOutput[0]->ml == '-' || $sapOutput[0]->ml == '' || $sapOutput[0]->ml == null) {
              $sapOutput[0]->ml = 0;
            }
            if ($sapOutput[0]->mtFlag == '-' || $sapOutput[0]->mtFlag == '' || $sapOutput[0]->mtFlag == null) {
              $sapOutput[0]->mtFlag = 0;
            }
            if ($sapOutput[0]->mobileNo == null || $sapOutput[0]->mobileNo == '' || $sapOutput[0]->mobileNo == '-') {
              $sapOutput[0]->mobileNo = 0;
            }
            if ($sapOutput[0]->feederCode == '-' || $sapOutput[0]->feederCode == '' || $sapOutput[0]->feederCode == null) {
              $sapOutput[0]->feederCode = 0;
            }
            if ($sapOutput[0]->fixedCharges == '' || $sapOutput[0]->fixedCharges == null || $sapOutput[0]->fixedCharges == '-') {
              $sapOutput[0]->fixedCharges = 0;
            }
            if ($sapOutput[0]->mt == '' || $sapOutput[0]->mt == null || $sapOutput[0]->mt == '-') {
              $sapOutput[0]->mt = 0;
            }
            if ($sapOutput[0]->previousFyCons == '-' || $sapOutput[0]->previousFyCons == '' || $sapOutput[0]->previousFyCons == null) {
              $sapOutput[0]->previousFyCons = 0;
            }
            if ($sapOutput[0]->meterReadingHigh == '-' || $sapOutput[0]->meterReadingHigh == '' || $sapOutput[0]->meterReadingHigh == null) {
              $sapOutput[0]->meterReadingHigh = 0;
            }
            if ($sapOutput[0]->meterReadingNote == '-' || $sapOutput[0]->meterReadingNote == '' || $sapOutput[0]->meterReadingNote == null) {
              $sapOutput[0]->meterReadingNote = 0;
            }
            if ($sapOutput[0]->amountOfCowCess == '-' || $sapOutput[0]->amountOfCowCess == '' || $sapOutput[0]->amountOfCowCess == null) {
              $sapOutput[0]->amountOfCowCess = 0;
            }

            $octroi = $sapOutput[0]->OCTRAI;

            $params = array(
              'reqArr' => [
                'SAP_RequestParams' => [
                  'SUB_DIVISION_CODE' => $sapOutput[0]->subDivisonCode,
                  'MRU' => $sapOutput[0]->MRU,
                  'CONNECTED_POLE_INI_NUMBER' => $sapOutput[0]->connectedPoleNINNumber,
                  'NEIGHBOR_METER_NO' => $sapOutput[0]->neighbourMeterNo,
                  'STREET_NAME' => $sapOutput[0]->streetName,
                  'INSTALLATION' => $sapOutput[0]->installation,
                  'MR_DOC_NO' => $sapOutput[0]->MR_DOCNumber,
                  'SCHEDULED_MRDATE' => $scheduledMRDate,
                  'METER_NUMBER' => $sapOutput[0]->SAPMeterNumber,
                  'MANUFACTURER_SR_NO' => $sapOutput[0]->manufacturerSRNo,
                  'MANUFACTURER_NAME' => $sapOutput[0]->manufacturerName,
                  'CONTRACT_ACCOUNT_NUMBER' => $sapOutput[0]->contractAcNumber,
                  'CONSUMPTION_KWH' => $sapOutput[0]->consumptionKWH,
                  'CONSUMPTION_KWAH' => $sapOutput[0]->consumptionKVAH,
                  'CONSUMPTION_KVA' => $sapOutput[0]->consumptionKVA,
                  'CUR_METER_READING_KWH' => $sapOutput[0]->currentMeterReadingKWH,
                  'CUR_METER_READING_KVA' => $sapOutput[0]->currentMeterReadingKVA,
                  'CUR_METER_READING_KVAH' => $sapOutput[0]->currentMeterReadingKVAH,
                  'CUR_METER_READING_DATE' => $currentMeterReadingDate,
                  'CUR_METER_READING_TIME' => $currentMeterReadingTime,
                  'CUR_METER_READER_NOTE' => $sapOutput[0]->currentNoteFromMeterReader,
                  'PRV_METER_READING_KWH' => $sapOutput[0]->previousMeterReadingKWH,
                  'PRV_METER_READING_KVA' => $sapOutput[0]->previousMeterReadingKVA,
                  'PRV_METER_READING_KWAH' => $sapOutput[0]->previousMeterReadingKVAH,
                  'PRV_METER_READING_DATE' => $previousMeterReadingDate,
                  'PRV_METER_READING_TIME' => $sapOutput[0]->previousMeterReadingTime,
                  'PRV_METER_READER_NOTE' => $sapOutput[0]->previoustNoteFromMeterReader,
                  'OCTROI_FLAG' => $sapOutput[0]->octraiFlag,
                  'SOP' => $sapOutput[0]->SOP,
                  'ED' => $sapOutput[0]->ED,
                  'OCTROI' => $octroi,
                  'DSSF' => $sapOutput[0]->TotalInfraDevCess,
                  'SURCHARGE_LEIVED' => $sapOutput[0]->surchargeLevied,
                  'SERVICE_RENT' => $sapOutput[0]->serviceRent,
                  'METER_RENT' => $sapOutput[0]->meterRent,
                  'SERVICE_CHARGE' => $sapOutput[0]->serviceCharge,
                  'MONTHLY_MIN_CHARGES' => $sapOutput[0]->monthlyMinimumCharges,
                  'PF_SURCHARGE' => $sapOutput[0]->powerFactorSurcharges,
                  'PF_INCENTIVE' => $sapOutput[0]->powerFactorIncentive,
                  'DEMAND_CHARGES' => $sapOutput[0]->demandCharges,
                  'FIXEDCHARGES' => $sapOutput[0]->fixedCharges,
                  'VOLTAGE_SURCHARGE' => $sapOutput[0]->voltageCharges,
                  'PEAKLOAD_EXEMPTION_CHARGES' => $sapOutput[0]->peakLoadExemptionCharges,
                  'SUNDRY_CHARGES' => $sapOutput[0]->sundryCharges,
                  'MISCELLANEOUS_CHARGES' => $sapOutput[0]->miscellaneousCharges,
                  'FUEL_ADJUSTMENT' => $sapOutput[0]->fuelAdjustment,
                  'BILL_NUMBER' => $sapOutput[0]->billNumber,
                  'NO_OF_DAYS_BILLED' => $sapOutput[0]->noOfDaysBilledFor,
                  'BILL_CYCLE' => $sapOutput[0]->billCycle,
                  'BILL_DATE' => $billDate2,
                  'DUE_DATE' => $dueDate2,
                  'BILL_TYPE' => $sapOutput[0]->billType,
                  'PAYMENT_AMOUNT' => $sapOutput[0]->paymantAmount,
                  'PAYMENT_MODE' => $sapOutput[0]->paymantMode,
                  'CHECK_NO' => $sapOutput[0]->checkNo,
                  'BANK_NAME' => $sapOutput[0]->bankName,
                  'PAYMENT_ID' => $sapOutput[0]->paymentID,
                  'IFSC_CODE' => $sapOutput[0]->IFSCcode,
                  'MICRCODE' => $sapOutput[0]->MICRcode,
                  'PAYMENT_DATE' => $sapOutput[0]->paymentDate,
                  'PAYMENT_REMARK' => '#'.$sapOutput[0]->ml.'#'.$sapOutput[0]->mtFlag.'#'.$sapOutput[0]->mobileNo.'#'.$sapOutput[0]->feederCode.'#'.$sapOutput[0]->scWsdAmountWithHeld.'#'.$sapOutput[0]->fixedCharges.'#'.$sapOutput[0]->mt.'#'.$sapOutput[0]->previousFyCons.'#'.$sapOutput[0]->meterReadingHigh.'#'.$sapOutput[0]->meterReadingNote.'#'.$sapOutput[0]->amountOfCowCess,
                  'TOT_BILLAMOUNT' => $sapOutput[0]->totBillAmount,
                  'SBM_NUMBER' => $sapOutput[0]->SBMnumber,
                  'METER_READER_NAME' => $sapOutput[0]->meterReaderName,
                  'INHOUSE_OUTSOURCED_SBM' => $sapOutput[0]->inHouseOutsourcedSBM,
                  'TRANSFROMERCODE' => $sapOutput[0]->transformerCode,
                  'MCB_RENT' => $sapOutput[0]->mcbRent,
                  'LPSC' => $sapOutput[0]->LPSC,
                  'TOT_AMT_DUE_DATE' => $sapOutput[0]->totalAmountAfterDueDate,
                  'TOT_SOP_ED_OCT' => $sapOutput[0]->totalOfNetSopNetEdNetOctrai,
                  'SUBDIVISIONNAME' => $sapOutput[0]->subDivisionName,
                  'CONSUMERLEGACYNUMBER' => $sapOutput[0]->consumerLegacyNumber,
                  'CONSUMERNAME' => $sapOutput[0]->consumerName,
                  'HOUSENUMBER' => $sapOutput[0]->houseNumber,
                  'ADDRESS' => $sapOutput[0]->address,
                  'CATEGORY' => $sapOutput[0]->category,
                  'SUBCATEGORY' => $sapOutput[0]->subCategory,
                  'BILLINGCYCLE' => $sapOutput[0]->billingCycle,
                  'BILLINGGROUP' => $sapOutput[0]->billingGroup,
                  'PHASECODE' => $sapOutput[0]->phaseCode,
                  'SANCTIONEDLOAD_KW' => $sapOutput[0]->sanctionedLoadKW,
                  'TOTALOLDMETER_CONS_UNIT_KWH' => $sapOutput[0]->totalOldMeterConsumptionUnitsKWH,
                  'TOTALOLDMETER_CONS_UNIT_KVAH' => $sapOutput[0]->totalOldMeterConsumptionUnitsKVAH,
                  'NEWMETERINITIALREADING_KWH' => $sapOutput[0]->newMeterInitialReadingKWH,
                  'NEWMETERINITIALREADING_KVAH' => $sapOutput[0]->newMeterInitialReadingKVAH,
                  'SUNDRYCHRS_SOP' => $sapOutput[0]->sundryChargesSOP,
                  'SUNDRYCHRS_ED' => $sapOutput[0]->sundryChargesED,
                  'SUNDRYCHRS_OCTRAI' => $sapOutput[0]->sundryChargesOCTRAI,
                  'SUNDRYALLOWANCES_SOP' => $sapOutput[0]->sundryAllowancesSOP,
                  'SUNDRYALLOWANCES_ED' => $sapOutput[0]->sundryAllowancesED,
                  'SUNDRYALLOWANCES_OCTRAI' => $sapOutput[0]->sundryAllowancesOCTRAI,
                  'OTHERCHRS' => $sapOutput[0]->otherCharges,
                  'ROUNDADJUSTMENTAMNT' => $sapOutput[0]->roundAdjustmentAmount,
                  'ADJUSTMENTAMT_SOP' => $sapOutput[0]->adjustmentAmountSOP,
                  'ADJUSTMENTAMT_ED' => $sapOutput[0]->adjustmentAmountED,
                  'ADJUSTMENTAMT_OCTRAI' => $sapOutput[0]->adjustmentAmountOCTRAI,
                  'PROVISIONALADJUSTMENTAMT_SOP' => $sapOutput[0]->provisionalAdjustmentAmountSOP,
                  'PROVISIONALADJUSTMENTAMT_ED' => $sapOutput[0]->provisionalAdjustmentAmountED,
                  'PROVADJUSTMENTAMT_OCTRAI' => $sapOutput[0]->provisionalAdjustmentAmountOCTRAI,
                  'ARREARSOPCURRENTYEAR' => $sapOutput[0]->arrearSOPCurrentYear,
                  'ARREAREDCURRENTYEAR' => $sapOutput[0]->arrearEDCurrentYear,
                  'ARREAROCTRAICURRENTYEAR' => $sapOutput[0]->arrearOCTRAICurrentYear,
                  'ARREARSOPPREVIOUSYEAR' => $sapOutput[0]->arrearSOPPreviousYear,
                  'ARREAREDPREVIOUSYEAR' => $sapOutput[0]->arrearEDPreviousYear,
                  'ARREAROCTRAIPREVIOUSYEAR' => $sapOutput[0]->arrearOCTRAIPreviousYear,
                  'ADVANCECONS_DEPOSIT' => $sapOutput[0]->advanceConsumptionDeposit,
                  'COMPLAINTCENTERPHONENUMBER' => $sapOutput[0]->complaintCenterPhoneNumber,
                  'METERSECURITYAMT' => $sapOutput[0]->meterSecurityAmount,
                  'NEARESTCASHCOUNTER' => $sapOutput[0]->nearestCashCounter,
                  'MULTIPLICATIONFACTOR' => $sapOutput[0]->multiplicationFactor,
                  'OVERALLMF' => $sapOutput[0]->overallMf,
                  'CONTRACTEDLOAD_KVA' => $sapOutput[0]->contractedDemandKVA,
                  'MISCEXPENSESDETAILS' => $sapOutput[0]->miscExpensesDetails,
                  'PREVIOUSKWHCYCLE_1' => (int) $sapOutput[0]->previousKWHCycle1,
                  'PREVIOUSKWHCYCLE_2' => (int) $sapOutput[0]->previousKWHCycle2,
                  'PREVIOUSKWHCYCLE_3' => (int) $sapOutput[0]->previousKWHCycle3,
                  'PREVIOUSKWHCYCLE_4' => (int) $sapOutput[0]->previousKWHCycle4,
                  'PREVIOUSKWHCYCLE_5' => (int) $sapOutput[0]->previousKWHCycle5,
                  'PREVIOUSKWHCYCLE_6' => (int) $sapOutput[0]->previousKWHCycle6,
                  'ROUNDING_PRESENT' => $sapOutput[0]->roundingPresent,
                  'ESTIMATION_TYPE' => $sapOutput[0]->estimationType,
                  'RECTYPE' => "1",
                  'USERID' => "STRTECH",
                  'PASSCODE' => "STR@1PSPCL",
                  'VID' => "1",
                  ]
                ]

                );

            $resultt = $client->PushSAP_Data($params);
          //   return $result;

            $result['soap'] = $resultt;
            // return 'ppppp';
            // return dd($result['soap']->PushSAP_DataResult->SAP_ResponseParams);
            $updateSoap = sapOutput::find($sapOutput[0]->id);
            // return $result['soap'];
            if ($result['soap']->PushSAP_DataResult->SAP_ResponseParams->MSG == 'Exception: ORA-00001: unique constraint (ONLINEBILL.SAP_SBM_GSC_NEW_PK) violated
ORA-06512: at line 12') {
                $updateSoap->soapStatus = 'D';
            } elseif ($result['soap']->PushSAP_DataResult->SAP_ResponseParams->MSG == 'Duplicate record found, primary key violated') {
              $updateSoap->soapStatus = 'D';
            } else {
              $updateSoap->soapStatus = $result['soap']->PushSAP_DataResult->SAP_ResponseParams->STATUS;
            }
            $updateSoap->soapMessage = $result['soap']->PushSAP_DataResult->SAP_ResponseParams->MSG;
            $updateSoap->soapCount = $updateSoap->soapCount + 1;
            $updateSoap->save();
            // sleep(1);
            // return $updateSoap;
            // return $result;
            // $res = $xml->STATUS;
          //   return $xml;
          //   echo "<pre>" ;


        } catch ( SoapFault $fault ) {

          //   trigger_error ( "SOAP Fault: (faultcode: {$fault->faultcode}, faultstring: {$fault->faultstring})", E_USER_ERROR );
        }
      }
      $result['status'] = 'Transfered Data To SOAP server Success';
      $result['count'] = $count;
      return view('message')->with('data', $result);
    }

    public function transfernonSAP(Request $request)
    {
      $sub = $request->subDivisionCode;
      $from = $request->from;
      // return $request;
      // return $request;
      // $data = DB::select("SELECT * FROM billeds WHERE soapStatus = '' order by id DESC limit 1500");
      // $data = DB::select("select * from billeds where meterReader = 1234 and blSubDivisionCode = 'A11'");
      // return $data;
//       $data = Billed::select('id', 'blMasterKey')
//                     ->where('soapStatus', 'F')
//                     ->where('soapMessage', '!=', 'Exception: ORA-00001: unique constraint (ONLINEBILL.DSBELOW10KW_PK) violated
// ORA-06512: at line 12')
//                     ->take(1500)->get();
     $data = Billed::select('id', 'blMasterKey')
                   ->where('blSubDivisionCode', $sub)
                   ->where('blPresentReadingDate', $from)
                   ->WhereNull('soapStatus')
                   ->orWhere('soapStatus', 'F')
                   ->orWhere('soapStatus', '')->get();
                   // return count($data);
// $data = Billed::select('id', 'blMasterKey')
//                     ->where('soapStatus', 'F')
//                     ->orWhere('soapStatus', '')
//                     ->orWhereNull('soapStatus')->limit('5000')->get();
                    // return $data;
      // $data = Billed::select('id', 'blMasterKey')
      //               ->where('blMasterKey', 'J64GT110230K')
      //               ->take(1500)->get();

      // return $data;
      // return $data;
      // $data = Billed::whereIn('blmasterKey', ['A11GT110002N', 'A11GT110003W', 'A11GT110004Y', 'A11GT110005F', 'A11GT110006K', 'A11GT110007M', 'A11GT110008P', 'A11GT110009X', 'A11GT110010M', 'A11GT110011P'])
      //                     ->orderBy('id', 'DESC')->get();
      $count = count($data);

      foreach ($data as $newBilll) {
        // return $newBilll->id;
        $newBill = Billed::find($newBilll->id);
        // return $newBill['blPresentReadingDate'];
        // $mtkey = $newBill[0]->$newBill;
        // $billDetails = Bill::find($newBill->id);
        $bill = Bill::where('masterKey', $newBill->blMasterKey)->take(1)->get();
        // return $bill;
        $searchId = $bill[0]->id;
        // return $searchId;
        $billDetails = Bill::find($searchId);
        // return $billDetails;
        $fnYearY = Date('Y');
        // return $fnYearY;
        $fnYear = Date('y')+1;
        $fnYear = $fnYearY.'-'.$fnYear;
        // return $newBill['blPresentReadingDate'];
        if($billDetails['connectedLoadInKW'] < 10){

          // return $newBill['blPresentReadingDate'];
          try {
            // $client = new SoapClient ( "some.aspx?WSDL" );
            $client = new SoapClient("https://billingdatareceiver.pspcl.in/NONSAP_SBM_DATA.ASMX?wsdl");

            $blPresentReadingDate = Carbon::createFromFormat('d/m/Y', $newBill['blPresentReadingDate'])->format('Y-m-d');
            $blPresentReadingTime = 'T'.'00:00:00';
            $time = $newBill['blTime'];
            $timeS1 = substr($time, 0, 2);
            $timeS2 = substr($time, 2, 2);
            $timeS3 = substr($time, 4, 2);
            $blTime = $timeS1.':'.$timeS2.':'.$timeS3;
            $blPresentReadingDate = $blPresentReadingDate.'T'.$blTime;

            $blDueDateByCash = Carbon::createFromFormat('d-m-Y', $newBill['blDueDateByCash'])->format('Y-m-d');
            $blDueTimeByCash = 'T'.'00:00:00';
            $blDueDateByCash = $blDueDateByCash.$blDueTimeByCash;

            $blDueDateByCheque = Carbon::createFromFormat('d-m-Y', $newBill['blDueDateByCheque'])->format('Y-m-d');
            $blDueTimeByCheque = 'T'.'00:00:00';
            $blDueDateByCheque = $blDueDateByCheque.$blDueTimeByCheque;

            $blcurrentReadingDate = Carbon::createFromFormat('d/m/Y', $newBill['blPresentReadingDate'])->format('d-m-Y');
            $blpreviousReadingDate = Carbon::createFromFormat('d/m/Y', $billDetails['previousReadingDate'])->format('d-m-Y');
            $billNoDate = Carbon::createFromFormat('d/m/Y', $newBill['blPresentReadingDate'])->format('dmy');
            // return $billNoDate;
            $billNo = $newBill['billNo'];
            $adjAmt = 0;
            if (
              $billDetails['perviousCode'] == 'L' || $billDetails['perviousCode'] == 'N' || $billDetails['perviousCode'] == "I"
              && $newBill['blPresentStatus'] == 'O' || $newBill['blPresentStatus'] == 'C' || $newBill['blPresentStatus'] == 'H' ||
              $newBill['blPresentStatus'] == 'X' || $newBill['blPresentStatus'] == 'T' ||
              $newBill['blPresentStatus'] == 'W' || $newBill['blPresentStatus'] == 'A'
            ) {
              $adjAmt = $billDetails['averageAdjustmentSOP'] + $billDetails['averageAdjustmentED'] + $billDetails['averageAdjustmentOctroi'] + $billDetails['avgAdjustmentIDF'] + $billDetails['avgAdjustmentCowCess'] + $billDetails['avgAdjustmentWaterSewarageChargesCode'];
            } else {
              $adjAmt = 0;
            }
            // $adjAmt = $billDetails['averageAdjustmentSOP'] + $billDetails['averageAdjustmentED'] + $billDetails['averageAdjustmentOctroi'] + $billDetails['avgAdjustmentIDF'] + $billDetails['avgAdjustmentCowCess'] + $billDetails['avgAdjustmentWaterSewarageChargesCode'];
            $SUNDARYCHARGES = $billDetails['sundarySOP'] + $billDetails['sundaryED'] + $billDetails['sundaryOctroi'] + $billDetails['sundryIDF'] + $billDetails['sundryCowCess'] + $billDetails['sundryWaterSewarageChargesCode'];
            $SUNDARYALLOWANCE = $billDetails['allowanceSOP'] + $billDetails['allowanceED'] + $billDetails['allowanceOctroi'] + $billDetails['allowanceIDF'] + $billDetails['allowanceCowCess'] + $billDetails['allowanceWaterSewarageChargesCode'];
            // return $billNo;
            // Tariff type Converted to 3 digits as per telephonic conversation with it team on 24-07-2018 4-20 Pm
            $tariffType = '';
            if ($newBill->blTariffType == 1) {
              $tariffType = "DS";
            } elseif ($newBill->blTariffType == 3) {
              $tariffType = "BE";
            } elseif ($newBill->blTariffType == 5) {
              $tariffType = "WSD";
            } elseif ($newBill->blTariffType == 6) {
              $tariffType = "BPL";
            } elseif ($newBill->blTariffType == 0) {
              $tariffType = "BO";
            } elseif ($newBill->blTariffType == 2) {
              $tariffType = "NRS";
            } elseif ($newBill->blTariffType == 4) {
              $tariffType = "RLY";
            } elseif ($newBill->blTariffType == 8) {
              $tariffType = "CGO";
            }
            $params = array(
              'reqArr' => [
                'NonSAP_RequestParams_Below10kw' => [
                  'ACCOUNTNO' => $newBill['blMasterKey'],
                  'NAME' => $newBill['blConsumerName'],
                  'ADDRESS' => $billDetails['consumerAddress'], // From Bills Table
                  'ISSUEDATE' => $blPresentReadingDate,
                  'DUEDATECASH' => $blDueDateByCash, // Need to format
                  'DUEDATECHEQUE' => $blDueDateByCheque, // Need to format
                  'VILLCITYNAME' => "",
                  'SUBDIVNAME' => $billDetails['subDivisionName'], // Form Bills Table
                  'BILLNO' => $billNo, // Need to Generate
                  'CURRENTREADINGDATE' => $blcurrentReadingDate,
                  'PREVREADINGDATE' => $blpreviousReadingDate, // Frm bills Table
                  'BILLPERIOD' => $newBill['blDays'],
                  'TARIFFTYPE' => $tariffType,
                  'CONNECTEDLOAD' => $billDetails['connectedLoadInKW'], // Verify
                  'PREVBILLSTATUS' => '', //$billDetails['perviousCode'], // From bills Table
                  'METERNO' => $billDetails['meterNo'], // From bills Table
                  'SECURITYAMT' => $billDetails['securityDeposit'], // From bills Table
                  'BILLSTATUS' => $newBill['blPresentStatus'],
                  'BILLCYCLE' => $newBill['blBillingCycle'],
                  'BILLGROUP' => $newBill['blBillingGroup'],
                  'METERREADINGCURRENT' => $newBill['blPresentReading'], // Verify
                  'METERREADINGPREV' => $billDetails['perviousReading'], // From bills Table "Verify"
                  'LINECTRATIO' => $billDetails['lineCtRatio'], // From Bills Table
                  'METERCTRATIO' => $billDetails['meterCtRatio'], // From Bills Table
                  'METERMULTIPLIER' => $billDetails['meterMultiplier'], // From Bills Table
                  'OVERMETERMULTIPLIER' => $billDetails['overallMultiplingFactor'], // From Bills Table
                  'METERCODE' => $newBill['blPresentStatus'], // Verify
                  'UNITSCONSUMEDNEW' => $newBill['blUnitsConsumedNew'],
                  'UNITSCONSUMEDOLD' => $newBill['blUnitsConsumedOld'],
                  'TOTALUNITSCONSUMED' => $newBill['blUnitsConsumed'],
                  'CURRENTSOP' => $newBill['blCurrentSop'],
                  'CURRENTED' => $newBill['blCurrentEd'],
                  'OCTROI' => $newBill['blCurrentOctroi'],
                  'METERRENT' => $newBill['blCurrentMeterRent'],
                  'SERVICERENT' => $newBill['blCurrentServiceRent'],
                  'ADJAMT' => $adjAmt, // From Bills Table
                  'ADJPERIOD' => $billDetails['periodOfAdjustmentDetail'], // From Bills TAble
                  'ADJREASON' => $billDetails['reason'], // From Bills TAble
                  'CONSUNITS' => $newBill['blConcessionalUnits'], // Verify
                  'FIXEDCHARGES' => $newBill['blCurrentFixedChargesWithSign'], // Verify
                  'FUELCOST' => $newBill['blCurrentFCAChargesWithSign'], // Verify
                  'VOLTCLASSCHARGES' => '0', // $billDetails['othersvoltageSurchargeAmpunt'], // From Bills Table
                  'PREVTOTALARR' => $billDetails['previousArrear'], // From Bills Table
                  'CURRENTTOTALARREAR' => $billDetails['currentArrear'], // From Bills Table
                  'OTHERCHARGES' => $billDetails['others'], // Verify
                  'SUNDARYCHARGES' => $SUNDARYCHARGES, // Verify // Add field in Database
                  'SUNDARYALLOWANCE' => $SUNDARYALLOWANCE, // Verify
                  'CURRENTROUNDEDAMT' => $newBill['blCurrentRoundingAmt'],
                  'PREVROUNDEDAMT' => $billDetails['previousRoundingAmount'], // From Bills Table
                  'TOTALNETSOP' => $newBill['blNetSop'],
                  'TOTALNETED' => $newBill['blNetEd'],
                  'TOTALNETOCTROI' => $newBill['blNetOctroi'],
                  'TOTALAMT' => $newBill['blAmountBeforeDue'],
                  'TOTALSURCHARGE' => $newBill['blSurCharge'],
                  'TOTALAMTGROSS' => $newBill['blAmountAfterDue'],
                  'BILLYEAR' => Date('Y'),
                  'SUNDARYMESSAGE' => $billDetails['sundaryChargeDetail'], // From Bills Table
                  'SUNDARYDTFROM' => "", // Verify
                  'SUNDARYDTTO' => "", // Verify
                  'LINE1' => "", // Verify
                  'LINE2' => "", // Verify
                  'PREVCYCLECONSUMPTION1' => (int) substr($billDetails['lastSixMonthConsumption'], 0, 6), // Calculate "Verify"
                  'PREVCYCLECONSUMPTION2' => (int) substr($billDetails['lastSixMonthConsumption'], 6, 6), // Calculate "Verify"
                  'PREVCYCLECONSUMPTION3' => (int) substr($billDetails['lastSixMonthConsumption'], 12, 6), // Calculate "Verify"
                  'PREVCYCLECONSUMPTION4' => (int) substr($billDetails['lastSixMonthConsumption'], 18, 6), // Calculate "Verify"
                  'PREVCYCLECONSUMPTION5' => (int) substr($billDetails['lastSixMonthConsumption'], 24, 6), // Calculate "Verify"
                  'PREVCYCLECONSUMPTION6' => (int) substr($billDetails['lastSixMonthConsumption'], 30, 6), // Calculate "Verify"
                  'AMTPAID' => "0",
                  'AMTDATE' => "",
                  'WEBCCCR' => "", // Verify
                  'FINANCIALYEAR' => $fnYear,  // 2018-19
                  'SPOTREC' => "S", // Verify
                  'CURRIDF' => $newBill['blCurrentIDFWithSign'], // Verify
                  'CCOWCESS' => $newBill['blCurrentCowCessWithSign'], // Verify
                  'CWATCESS' => $newBill['blCurrentWaterSewageChargeWithSign'],
                  'TOTIDF' => $newBill['bltotalIDFWithSign'],
                  'TCOWCESS' => $newBill['blTotalCowCessWithSign'],
                  'TWATCESS' => $newBill['blTotalCurrentWaterSewarageChargeWithSign'],
                  'EXCOL' => "1",
                  'USERID' => "STRTECH",
                  'PASSCODE' => "STR@1PSPCL",
                  'VID' => "1"
                  ]
                ]

                );

            $resultt = $client->Push_NonSAP_Below10kw_Data($params);
          //   dd($result);
            // $result = json_encode($result);
            //
            // return $result;

            $result['soap'] = $resultt;
            // return 'ppppp';
            // return dd($result['soap']->Push_NonSAP_Below10kw_DataResult->NonSAP_ResponseParams);
            $updateSoap = Billed::find($newBill['id']);
            if ($result['soap']->Push_NonSAP_Below10kw_DataResult->NonSAP_ResponseParams->MSG == 'Exception: ORA-00001: unique constraint (ONLINEBILL.DSBELOW10KW_PK) violated
ORA-06512: at line 12') {
              $updateSoap->soapStatus = 'D';
            } elseif ($result['soap']->Push_NonSAP_Below10kw_DataResult->NonSAP_ResponseParams->MSG == 'Duplicate record found, primary key violated') {
              $updateSoap->soapStatus = 'D';
            } else {
              $updateSoap->soapStatus = $result['soap']->Push_NonSAP_Below10kw_DataResult->NonSAP_ResponseParams->STATUS;
            }

            $updateSoap->soapMessage = $result['soap']->Push_NonSAP_Below10kw_DataResult->NonSAP_ResponseParams->MSG;
            $updateSoap->soapCount = $updateSoap->soapCount + 1;
            $updateSoap->save();
            // return $updateSoap;
            // return $result;

          //   $xml = simplexml_load_string($result->NonSAP_ResponseParams);
          //   $res = $xml->STATUS;
          //   return $xml;
          //   echo "<pre>" ;
          //   foreach ($xml as $key => $value)
          //   {
          //     $key . " = " .$value . PHP_EOL;
          //       // foreach($value as $ekey => $eValue)
          //       // {
          //       //     print($ekey . " = " . $eValue . PHP_EOL);

          //       // }
          //   }

        } catch ( SoapFault $fault ) {
          // echo "error";
          // return $result;
            trigger_error ( "SOAP Fault: (faultcode: {$fault->faultcode}, faultstring: {$fault->faultstring})", E_USER_ERROR );
        }
        } else {
          // return dd($newBill->blPresentReadingDate);
            try {
            // $client = new SoapClient ( "some.aspx?WSDL" );
            $client = new SoapClient("https://billingdatareceiver.pspcl.in/NONSAP_SBM_DATA.ASMX?wsdl");
            $blPresentReadingDate = Carbon::createFromFormat('d/m/Y', $newBill['blPresentReadingDate'])->format('Y-m-d');
            $blPresentReadingTime = 'T'.'00:00:00';
            $time = $newBill['blTime'];
            $timeS1 = substr($time, 0, 2);
            $timeS2 = substr($time, 2, 2);
            $timeS3 = substr($time, 4, 2);
            $blTime = $timeS1.':'.$timeS2.':'.$timeS3;
            $blPresentReadingDate = $blPresentReadingDate.'T'.$blTime;

            $blDueDateByCash = Carbon::createFromFormat('d-m-Y', $newBill['blDueDateByCash'])->format('Y-m-d');
            $blDueTimeByCash = 'T'.'00:00:00';
            $blDueDateByCash = $blDueDateByCash.$blDueTimeByCash;

            $blDueDateByCheque = Carbon::createFromFormat('d-m-Y', $newBill['blDueDateByCheque'])->format('Y-m-d');
            $blDueTimeByCheque = 'T'.'00:00:00';
            $blDueDateByCheque = $blDueDateByCheque.$blDueTimeByCheque;

            $blcurrentReadingDate = Carbon::createFromFormat('d/m/Y', $newBill['blPresentReadingDate'])->format('d-m-Y');
            $blpreviousReadingDate = Carbon::createFromFormat('d/m/Y', $billDetails['previousReadingDate'])->format('d-m-Y');
            $billNoDate = Carbon::createFromFormat('d/m/Y', $newBill['blPresentReadingDate'])->format('dmy');
            // return $billNoDate;
            $billNo = $newBill['billNo'];
            $adjAmt = 0;
            if (
              $billDetails['perviousCode'] == 'L' || $billDetails['perviousCode'] == 'N' || $billDetails['perviousCode'] == "I"
              && $newBill['blPresentStatus'] == 'O' || $newBill['blPresentStatus'] == 'C' || $newBill['blPresentStatus'] == 'H' ||
              $newBill['blPresentStatus'] == 'X' || $newBill['blPresentStatus'] == 'T' ||
              $newBill['blPresentStatus'] == 'W' || $newBill['blPresentStatus'] == 'A'
            ) {
              $adjAmt = $billDetails['averageAdjustmentSOP'] + $billDetails['averageAdjustmentED'] + $billDetails['averageAdjustmentOctroi'] + $billDetails['avgAdjustmentIDF'] + $billDetails['avgAdjustmentCowCess'] + $billDetails['avgAdjustmentWaterSewarageChargesCode'];
            } else {
              $adjAmt = 0;
            }
            // $adjAmt = $billDetails['averageAdjustmentSOP'] + $billDetails['averageAdjustmentED'] + $billDetails['averageAdjustmentOctroi'] + $billDetails['avgAdjustmentIDF'] + $billDetails['avgAdjustmentCowCess'] + $billDetails['avgAdjustmentWaterSewarageChargesCode'];
            $SUNDARYCHARGES = $billDetails['sundarySOP'] + $billDetails['sundaryED'] + $billDetails['sundaryOctroi'] + $billDetails['sundryIDF'] + $billDetails['sundryCowCess'] + $billDetails['sundryWaterSewarageChargesCode'];
            $SUNDARYALLOWANCE = $billDetails['allowanceSOP'] + $billDetails['allowanceED'] + $billDetails['allowanceOctroi'] + $billDetails['allowanceIDF'] + $billDetails['allowanceCowCess'] + $billDetails['allowanceWaterSewarageChargesCode'];
            // Tariff type Converted to 3 digits as per telephonic conversation with it team on 24-07-2018 4-20 Pm
            $tariffType = '';
            if ($newBill->blTariffType == 1) {
              $tariffType = "DS";
            } elseif ($newBill->blTariffType == 3) {
              $tariffType = "BE";
            } elseif ($newBill->blTariffType == 5) {
              $tariffType = "WSD";
            } elseif ($newBill->blTariffType == 6) {
              $tariffType = "BPL";
            } elseif ($newBill->blTariffType == 0) {
              $tariffType = "BO";
            } elseif ($newBill->blTariffType == 2) {
              $tariffType = "NRS";
            } elseif ($newBill->blTariffType == 4) {
              $tariffType = "RLY";
            } elseif ($newBill->blTariffType == 8) {
              $tariffType = "CGO";
            }
            // return $billDetails['periodOfAdjustmentDetail'];
            $params = array(
              'reqArr' => [
                'NonSAP_RequestParams_Above10kw' => [
                  'ACCOUNTNO' => $newBill['blMasterKey'],
                  'NAME' => $newBill['blConsumerName'],
                  'ADDRESS' => $billDetails['consumerAddress'], // From Bills Table
                  'ISSUEDATE' => $blPresentReadingDate,
                  'DUEDATECASH' => $blDueDateByCash, // Need to format
                  'DUEDATECHEQUE' => $blDueDateByCheque, // Need to format
                  'VILLCITYNAME' => "",
                  'SUBDIVNAME' => $billDetails['subDivisionName'], // Form Bills Table
                  'BILLNO' => $billNo, // Need to Generate
                  'CURRENTREADINGDATE' => $blcurrentReadingDate,
                  'PREVREADINGDATE' => $blpreviousReadingDate, // Frm bills Table
                  'BILLPERIOD' => $newBill['blDays'],
                  'TARRIFTYPE' => $tariffType,
                  'CONNECTEDLOAD' => $billDetails['connectedLoadInKW'], // Verify
                  'PREVBILLSTATUS' => '', //$billDetails['perviousCode'], // From bills Table
                  'METERNO' => $billDetails['meterNo'], // From bills Table
                  'SECURITYAMT' => $billDetails['securityDeposit'], // From bills Table
                  'METERSTATUS' => $newBill['blPresentStatus'],
                  'BILLCYCLE' => $newBill['blBillingCycle'],
                  'BILLGROUP' => $newBill['blBillingGroup'],
                  'METERREADINGCURRENT' => $newBill['blPresentReading'], // Verify
                  'METERREADINGPREV' => $billDetails['perviousReading'], // From bills Table "Verify"
                  'LINECTRATIO' => $billDetails['lineCtRatio'], // From Bills Table
                  'METERCTRATIO' => $billDetails['meterCtRatio'], // From Bills Table
                  'METERMULTIPLIER' => $billDetails['meterMultiplier'], // From Bills Table
                  'OVERMETERMULTIPLIER' => $billDetails['overallMultiplingFactor'], // From Bills Table
                  'METERCODE' => $newBill['blPresentStatus'], // Verify
                  'UNITSCONSUMEDNEW' => $newBill['blUnitsConsumedNew'],
                  'UNITSCONSUMEDOLD' => $newBill['blUnitsConsumedOld'],
                  'TOTALUNITSCONSUMED' => $newBill['blUnitsConsumed'],
                  'CURRENTSOP' => $newBill['blCurrentSop'],
                  'CURRENTED' => $newBill['blCurrentEd'],
                  'CURRENTOCTROI' => $newBill['blCurrentOctroi'],
                  'METERRENT' => $newBill['blCurrentMeterRent'],
                  'SERVICERENT' => $newBill['blCurrentServiceRent'],
                  'TOTALADJAMT' => $adjAmt, // From Bills Table
                  'TOTALADJPERIOD' => $billDetails['periodOfAdjustmentDetail'], // From Bills TAble
                  'TOTALADJREASON' => $billDetails['reason'], // From Bills TAble
                  'CONCLUNITS' => $newBill['blConcessionalUnits'], // Verify
                  'FIXEDCHARGES' => $newBill['blCurrentFixedChargesWithSign'], // Verify
                  'FUELCOSTCHARGES' => $newBill['blCurrentFCAChargesWithSign'], // Verify
                  'VOLTAGECLASSCHARGES' => '0', // From Bills Table
                  'PREVTOTALARREARS' => $billDetails['previousArrear'], // From Bills Table
                  'CURRENTTOTALARREARS' => $billDetails['currentArrear'], // From Bills Table
                  'OTHERCHARGES' => $billDetails['others'], // Verify
                  'SUNDARYCHARGES' => $SUNDARYCHARGES, // Verify // Add field in Database
                  'SUNDARYALLOWANCES' => $SUNDARYALLOWANCE, // Verify
                  'CURRENTROUNDINGAMT' => $newBill['blCurrentRoundingAmt'],
                  'PREVROUNDAMT' => $billDetails['previousRoundingAmount'], // From Bills Table
                  'TOTALNETSOP' => $newBill['blNetSop'],
                  'TOTALNETED' => $newBill['blNetEd'],
                  'TOTALNETOCTROI' => $newBill['blNetOctroi'],
                  'TOTALAMT' => $newBill['blAmountBeforeDue'],
                  'TOTALSURCHARGE' => $newBill['blSurCharge'],
                  'TOTALAMTGROSS' => $newBill['blAmountAfterDue'],
                  'BILLYEAR' => Date('Y'),
                  'SUNDARYMESSAGE' => $billDetails['sundaryChargeDetail'], // From Bills Table
                  'SUNDARYDTFROM' => "", // Verify
                  'SUNDARYDTTO' => "", // Verify
                  'LINE1' => "", // Verify
                  'LINE2' => "", // Verify
                  'TOTALSURCHARGES' => "", // Verify
                  'TOTALGROSS' => "", // Verify
                  'DUEDATECASH2' => "", // Verify
                  'TOTALSURCHARGE2' => "", // Verify
                  'TOTALGROSS2' => "", // Verify
                  'CONSUMPTIONCYCLE1' => (int) substr($billDetails['lastSixMonthConsumption'], 0, 6), // Calculate "Verify"
                  'CONSUMPTIONCYCLE2' => (int) substr($billDetails['lastSixMonthConsumption'], 6, 6), // Calculate "Verify"
                  'CONSUMPTIONCYCLE3' => (int) substr($billDetails['lastSixMonthConsumption'], 12, 6), // Calculate "Verify"
                  'CONSUMPTIONCYCLE4' => (int) substr($billDetails['lastSixMonthConsumption'], 18, 6), // Calculate "Verify"
                  'CONSUMPTIONCYCLE5' => (int) substr($billDetails['lastSixMonthConsumption'], 24, 6), // Calculate "Verify"
                  'CONSUMPTIONCYCLE6' => (int) substr($billDetails['lastSixMonthConsumption'], 30, 6), // Calculate "Verify"
                  'AMTPAID' => "0",
                  'AMTDATE' => "",
                  'WEBCCCR' => "", // Verify
                  'FINYEAR' => $fnYear,  // 2018-19
                  'CURRIDF' => $newBill['blCurrentIDFWithSign'], // Verify
                  'CCOWCESS' => $newBill['blCurrentCowCessWithSign'], // Verify
                  'CWATCESS' => $newBill['blCurrentWaterSewageChargeWithSign'],
                  'TOTIDF' => $newBill['bltotalIDFWithSign'],
                  'TCOWCESS' => $newBill['blTotalCowCessWithSign'],
                  'TWATCESS' => $newBill['blTotalCurrentWaterSewarageChargeWithSign'],
                  'EXCOL' => "1",
                  'USERID' => "STRTECH",
                  'PASSCODE' => "STR@1PSPCL",
                  'VID' => "1"
                  ]
                ]

                );

            $resultt = $client->Push_NonSAP_Above10kw_Data($params);
          //   dd($result);
          $result['soap'] = $resultt;
          // return 'ppppp';
          // return dd($result['soap']->Push_NonSAP_Above10kw_DataResult->NonSAP_ResponseParams);
          $updateSoap = Billed::find($newBill['id']);
          // $updateSoap = Billed::find($newBill['id']);
            if ($result['soap']->Push_NonSAP_Above10kw_DataResult->NonSAP_ResponseParams->MSG == 'Exception: ORA-00001: unique constraint (ONLINEBILL.DSBELOW10KW_PK) violated
ORA-06512: at line 12') {
              $updateSoap->soapStatus = 'D';
            } elseif ($result['soap']->Push_NonSAP_Above10kw_DataResult->NonSAP_ResponseParams->MSG == 'Duplicate record found, primary key violated') {
              $updateSoap->soapStatus = 'D';
            } else {
              $updateSoap->soapStatus = $result['soap']->Push_NonSAP_Above10kw_DataResult->NonSAP_ResponseParams->STATUS;
            }
          // $updateSoap->soapStatus = $result['soap']->Push_NonSAP_Above10kw_DataResult->NonSAP_ResponseParams->STATUS;
          $updateSoap->soapMessage = $result['soap']->Push_NonSAP_Above10kw_DataResult->NonSAP_ResponseParams->MSG;
          $updateSoap->soapCount = $updateSoap->soapCount + 1;
          $updateSoap->save();
          // return $updateSoap;
          // return $result;

          //   $xml = simplexml_load_string($result->NonSAP_ResponseParams);
          //   $res = $xml->STATUS;
          //   return $xml;
          //   echo "<pre>" ;
          //   foreach ($xml as $key => $value)
          //   {
          //     $key . " = " .$value . PHP_EOL;
          //       // foreach($value as $ekey => $eValue)
          //       // {
          //       //     print($ekey . " = " . $eValue . PHP_EOL);

          //       // }
          //   }

        } catch ( SoapFault $fault ) {
          // echo "error";
          // return $result;
            trigger_error ( "SOAP Fault: (faultcode: {$fault->faultcode}, faultstring: {$fault->faultstring})", E_USER_ERROR );
        }
        }


        // $billsUpdate = Bill::where('id', $request->id)
        //                    ->where('status', 2)
        //                    ->update(
        //                      [
        //                        'status' => 3
        //                      ]
        //                    );
        //  if ($newBill) {
        //    $message['status'] = 'success';
        //    // return $newBill;
        //  } else {
        //    $message['status'] = 'Error';
        //  }


      // return Bill::find($request->id);

      // if ($readStatus == 3) {
      //   $message['status'] = 'Already Submitted';
      // } else {
      //
      // }
    }
    $result['status'] = 'Transfered Data To SOAP server Success';
    $result['count'] = $count;
    return view('message')->with('data', $result);

    // return $result;
      }
    }
