<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PreviousReadingCorrection extends Model
{
    protected $table = "tab_advice72";
    protected $fillable = [
      'sysId',
      'month',
      'inputCode',
      'sheetNo',
      'numberOfEnties',
      'pageNo',
      'billingCycle',
      'billingGroup',
      'subdivisionCode',
      'ledgerGroup',
      'accountNo',
      'checkDigit',
      'correctedReading',
      'averageConsumption',
      'remarks',
      'cUser',
      'cDate',
      'cTime',
      'uUser',
      'uDate',
      'uTime',
      'uReason',
      'dUser',
      'dReason',
      'eFlag',
      'eDate',
      'eTime',
      'eUser',
    ];
}
