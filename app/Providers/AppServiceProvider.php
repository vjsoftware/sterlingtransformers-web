<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use DB;
use Log;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
      $time = Date('d-m-Y');
        Schema::defaultStringLength(191);
        DB::listen(function($query) {
          // echo var_dump($query->bindings);
          // $impl = implode(", ",$query->bindings);

          Log::info(sprintf("%s (%s) : %s",$query->sql, (string) '', '1:11'));
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
