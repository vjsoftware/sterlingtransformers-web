<?php

namespace App\Providers;

use Laravel\Passport\Passport;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        $this->registerBillPolicies();

        Passport::routes();

        //
    }

    public function registerBillPolicies()
    {
      Gate::define('can-view', function($user) {
        return $user->hasAccess(['can-view']);
      });

      Gate::define('can-edit', function($user) {
        return $user->hasAccess(['can-edit']);
      });

      Gate::define('can-delete', function($user) {
        return $user->hasAccess(['can-delete']);
      });

      Gate::define('can-enter-data', function($user) {
        return $user->hasAccess(['can-enter-data']);
      });

      Gate::define('can-edit-data', function($user) {
        return $user->hasAccess(['can-edit-data']);
      });

      Gate::define('can-delete-data', function($user) {
        return $user->hasAccess(['can-delete-data']);
      });

      Gate::define('can-create-user', function($user) {
        return $user->hasAccess(['can-create-user']);
      });

      Gate::define('can-delete-user', function($user) {
        return $user->hasAccess(['can-delete-user']);
      });

      Gate::define('upload-bill', function($user) {
        return $user->hasAccess(['upload-bill']);
      });

      Gate::define('new-bill', function($user) {
        return $user->hasAccess(['new-bill']);
      });
    }
}
