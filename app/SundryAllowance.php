<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SundryAllowance extends Model
{
  protected $table = "tab_advice86";
  protected $fillable = [
    'sysId',
    'month',
    'inputCode',
    'sheetNo',
    'pageNo',
    'billingCycle',
    'billingGroup',
    'subdivisionCode',
    'ledgerGroup',
    'accountNo',
    'checkDigit',
    'sop',
    'ed',
    'oct',
    'code',
    'sundryRegister',
    'sundryItemNo',
    'controlNo',
    'remarks',
    'cUser',
    'cDate',
    'cTime',
    'uUser',
    'uDate',
    'uTime',
    'uReason',
    'dUser',
    'dReason',
    'eFlag',
    'eDate',
    'eTime',
    'eUser',
  ];
}
