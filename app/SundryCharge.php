<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SundryCharge extends Model
{
  protected $table = "tab_advice76";
  protected $fillable = [
    'sysId',
    'month',
    'inputCode',
    'sheetNo',
    'pageNo',
    'billingCycle',
    'billingGroup',
    'subdivisionCode',
    'ledgerGroup',
    'accountNo',
    'checkDigit',
    'sop',
    'ed',
    'oct',
    'idf',
    'cowCess',
    'waterCess',
    'code',
    'sundryRegister',
    'sundryPageNo',
    'noOfSundryCharges',
    'from',
    'to',
    'served',
    'remarks',
    'cUser',
    'cDate',
    'cTime',
    'uUser',
    'uDate',
    'uTime',
    'uReason',
    'dUser',
    'dReason',
    'eFlag',
    'eDate',
    'eTime',
    'eUser',
  ];
}
