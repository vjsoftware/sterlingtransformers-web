<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
// use App\OAuthServerException;
use League\OAuth2\Server\Exception\OAuthServerException;
use DB;


class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'role', 'simNumber'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function roles()
    {
      return $this->belongsToMany(Role::class, 'role_users');
    }

    public function files()
    {
      return $this->belongsToMany(File::class);
    }

    public function hasAccess(array $permissions)
    {
      foreach ($this->roles as $role) {
        if ($role->hasAccess($permissions)) {
          return true;
        }
      }
      return false;
    }

    public function findForPassport($username, $simNumber, $imeiNumber, $loginLat, $loginLong, $device_date) {
      // return $identifier;
      // echo username;
      $date = Date('d-m-Y');
      $dayCloseStatus = DB::select("SELECT * FROM user_logs WHERE userId = '$username' ORDER BY id DESC LIMIT 1");
      // return 'jjjjjj';
      if ($device_date != '') {
        if ($device_date == $date) {
          if ($dayCloseStatus) {
            if (1 != 1) {
              // return $message['status'] = "Day Close Pending please Contact Admin";
              throw new OAuthServerException('Day Close Pending please Contact Admin', 6, 'day_close', 401);
            } else {
              $user = $this->where('email', $username)->first();
              if ($user != null && $user->status == 1) {
                if($user != null && $user->simNumber != '') {
                  $login = $this->Where('email', $username)->Where('simNumber', $simNumber)->Where('imeiNumber', $imeiNumber)->first();
                  if ($login !== null) {
                    $dayCloseCheck = DB::select("SELECT * FROM user_logs WHERE userId = '$username' AND status = 'O' AND loginDate = '$date' ORDER BY id DESC LIMIT 1");
                    if ($dayCloseCheck) {
                      return $login;
                    } else {
                      $user = $this->where('email', $username)->first();
                      $loginDate = Date('d-m-Y');
                      $logintime = Date('H:i:s');
                      $lat = $loginLat;
                      $long = $loginLong;
                      $userLog = DB::insert("insert into user_logs (user, userId, loginDate, loginTime, loginLat, loginLong, status) values ('$user->id', '$username', '$loginDate', '$logintime', '$lat', '$long', 'O')");
                      if ($userLog) {
                          return $login;
                      } else {
                        // return $message['status'] = "User Log Error";
                        throw new OAuthServerException('User Log Error', 6, 'login_failed', 401);
                      }
                    }
                  } else {
                    // return $message['status'] = "Mobile Device Changed";
                    throw new OAuthServerException('Mobile Device Changed', 6, 'account_disabled', 401);
                  }
                } else {
                  $login = $this->where('email', $username)->first();
                  $login->simNumber = $simNumber;
                  $login->imeiNumber = $imeiNumber;
                  $login->save();
                  if ($login !== null) {
                    return $login;
                  } else {
                    // return $message['status'] = "Invalid Credientials";
                    throw new OAuthServerException('Invalid Credientials', 6, 'login_failed', 401);
                  }

                }
              } else {
                // return $message['status'] = "Account is disabled";
                throw new OAuthServerException('Account is disabled', 6, 'login_failed', 401);
              }
            }
          } else {
            $user = $this->where('email', $username)->first();
            if ($user != null && $user->status == 1) {
              if($user != null && $user->simNumber != '') {
                $login = $this->Where('email', $username)->Where('simNumber', $simNumber)->Where('imeiNumber', $imeiNumber)->first();
                if ($login !== null) {
                  $dayCloseCheck = DB::select("SELECT * FROM user_logs WHERE userId = '$username' AND status = 'O' AND loginDate = '$date' ORDER BY id DESC LIMIT 1");
                  if ($dayCloseCheck) {
                    return $login;
                  } else {
                    // return 'llll';
                    $user = $this->where('email', $username)->first();
                    $loginDate = Date('d-m-Y');
                    $logintime = Date('H:i:s');
                    $lat = $loginLat;
                    $long = $loginLong;
                    $userLog = DB::insert("insert into user_logs (user, userId, loginDate, loginTime, loginLat, loginLong, status) values ('$user->id', '$username', '$loginDate', '$logintime', '$lat', '$long', 'O')");
                    if ($userLog) {
                        return $login;
                    } else {
                      // return $message['status'] = "User Log Error";
                      throw new OAuthServerException('User Log Error', 6, 'login_failed', 401);
                    }
                  }
                } else {
                  // return $message['status'] = "Mobile Device Changed";
                  throw new OAuthServerException('Mobile Device Changed', 6, 'account_disabled', 401);
                }
              } else {
                $login = $this->where('email', $username)->first();
                $login->simNumber = $simNumber;
                $login->imeiNumber = $imeiNumber;
                $login->save();
                if ($login !== null) {
                  return $login;
                } else {
                  // return $message['status'] = "Invalid Credientials";
                  throw new OAuthServerException('Invalid Credientials', 6, 'login_failed', 401);
                }

              }
            } else {
              // return $message['status'] = "Account is disabled";
              throw new OAuthServerException('Account is disabled', 6, 'login_failed', 401);
            }
          }
        } else {
          throw new OAuthServerException("Please Check Mobile Date & Time", 6, 'date_error', 401);

        }
      } else {
        if ($dayCloseStatus) {
          if (1 != 1) {
            // return $message['status'] = "Day Close Pending please Contact Admin";
            throw new OAuthServerException('Day Close Pending please Contact Admin', 6, 'day_close', 401);
          } else {
            $user = $this->where('email', $username)->first();
            if ($user != null && $user->status == 1) {
              if($user != null && $user->simNumber != '') {
                $login = $this->Where('email', $username)->Where('simNumber', $simNumber)->Where('imeiNumber', $imeiNumber)->first();
                if ($login !== null) {
                  $dayCloseCheck = DB::select("SELECT * FROM user_logs WHERE userId = '$username' AND status = 'O' AND loginDate = '$date' ORDER BY id DESC LIMIT 1");
                  if ($dayCloseCheck) {
                    return $login;
                  } else {
                    $user = $this->where('email', $username)->first();
                    $loginDate = Date('d-m-Y');
                    $logintime = Date('H:i:s');
                    $lat = $loginLat;
                    $long = $loginLong;
                    $userLog = DB::insert("insert into user_logs (user, userId, loginDate, loginTime, loginLat, loginLong, status) values ('$user->id', '$username', '$loginDate', '$logintime', '$lat', '$long', 'O')");
                    if ($userLog) {
                        return $login;
                    } else {
                      // return $message['status'] = "User Log Error";
                      throw new OAuthServerException('User Log Error', 6, 'login_failed', 401);
                    }
                  }
                } else {
                  // return $message['status'] = "Mobile Device Changed";
                  throw new OAuthServerException('Mobile Device Changed', 6, 'account_disabled', 401);
                }
              } else {
                $login = $this->where('email', $username)->first();
                $login->simNumber = $simNumber;
                $login->imeiNumber = $imeiNumber;
                $login->save();
                if ($login !== null) {
                  return $login;
                } else {
                  // return $message['status'] = "Invalid Credientials";
                  throw new OAuthServerException('Invalid Credientials', 6, 'login_failed', 401);
                }

              }
            } else {
              // return $message['status'] = "Account is disabled";
              throw new OAuthServerException('Account is disabled', 6, 'login_failed', 401);
            }
          }
        } else {
          $user = $this->where('email', $username)->first();
          if ($user != null && $user->status == 1) {
            if($user != null && $user->simNumber != '') {
              $login = $this->Where('email', $username)->Where('simNumber', $simNumber)->Where('imeiNumber', $imeiNumber)->first();
              if ($login !== null) {
                $dayCloseCheck = DB::select("SELECT * FROM user_logs WHERE userId = '$username' AND status = 'O' AND loginDate = '$date' ORDER BY id DESC LIMIT 1");
                if ($dayCloseCheck) {
                  return $login;
                } else {
                  // return 'llll';
                  $user = $this->where('email', $username)->first();
                  $loginDate = Date('d-m-Y');
                  $logintime = Date('H:i:s');
                  $lat = $loginLat;
                  $long = $loginLong;
                  $userLog = DB::insert("insert into user_logs (user, userId, loginDate, loginTime, loginLat, loginLong, status) values ('$user->id', '$username', '$loginDate', '$logintime', '$lat', '$long', 'O')");
                  if ($userLog) {
                      return $login;
                  } else {
                    // return $message['status'] = "User Log Error";
                    throw new OAuthServerException('User Log Error', 6, 'login_failed', 401);
                  }
                }
              } else {
                // return $message['status'] = "Mobile Device Changed";
                throw new OAuthServerException('Mobile Device Changed', 6, 'account_disabled', 401);
              }
            } else {
              $login = $this->where('email', $username)->first();
              $login->simNumber = $simNumber;
              $login->imeiNumber = $imeiNumber;
              $login->save();
              if ($login !== null) {
                return $login;
              } else {
                // return $message['status'] = "Invalid Credientials";
                throw new OAuthServerException('Invalid Credientials', 6, 'login_failed', 401);
              }

            }
          } else {
            // return $message['status'] = "Account is disabled";
            throw new OAuthServerException('Account is disabled', 6, 'login_failed', 401);
          }
        }
      }




        // return $this->Where('email', $username)->Where('simNumber', $simNumber)->Where('imeiNumber', $imeiNumber)->first();
      }
}
