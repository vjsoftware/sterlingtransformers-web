<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tab_advice63 extends Model
{
  protected $fillable = [
    'month',
    'sysId',
    'inputCode',
    'month',
    'sheetNo',
    'pageNo',
    'numberOfEnties',
    'subdivisionCode',
    'ledgerGroup',
    'accountNo',
    'checkDigit',
    'sundryAllowanceAmount',
    'registerNo',
    'itemNo',
    'cashDeposit',
    'dateOfPayment',
    'ccrParticular',
    'remarks',
    'cUser',
    'cDate',
    'cTime',
    'uUser',
    'uDate',
    'uTime',
    'uReason',
    'dUser',
    'dReason',
    'eFlag',
    'eDate',
    'eTime',
    'eUser',
  ];
}
