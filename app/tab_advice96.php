<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tab_advice96 extends Model
{
  protected $fillable = [
    'sysId',
    'month',
    'inputCode',
    'pSub',
    'pSer',
    'subdivisionCode',
    'ledgerGroup',
    'billingCycle',
    'billingGroup',
    'registerNo',
    'pageNo',
    'rcoFee',
    'from',
    'to',
    'accountNo',
    'checkDigit',
    'remarks',
    'cUser',
    'cDate',
    'cTime',
    'uReason',
    'dReason',
  ];
}
