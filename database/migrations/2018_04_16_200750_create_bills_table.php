<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBillsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bills', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('fileId');
            $table->string('masterKey', 12);
            $table->string('masterKey1', 11);
            $table->string('subDivisionCode', 3);
            $table->string('accountNumber', 6);
            $table->string('meterNo', 10);
            $table->string('subDivisionName', 20);
            $table->string('consumerName', 25);
            $table->string('consumerAddress', 34);
            $table->string('phaseCode', 1);
            $table->string('tariffType', 1);
            $table->string('connectedLoadInKW', 7);
            $table->string('meterMultiplier', 8);
            $table->string('perviousReading', 8);
            $table->string('perviousCode', 1);
            $table->string('previousReadingDate', 10);
            $table->string('octroyFlag', 1);
            $table->string('arrearSOP', 10);
            $table->string('arrearED', 10);
            $table->string('arrearOctroi', 10);
            $table->string('surchargeToBeLeaved', 10);
            $table->string('averageAdjustmentSOP', 10);
            $table->string('averageAdjustmentED', 10);
            $table->string('averageAdjustmentOctroi', 10);
            $table->string('sundarySOP', 10);
            $table->string('sundaryED', 10);
            $table->string('sundaryOctroi', 10);
            $table->string('allowanceSOP', 10);
            $table->string('allowanceED', 10);
            $table->string('allowanceOctroi', 10);
            $table->string('currentMeterRent', 3);
            $table->string('previousRoundingAmount', 2);
            $table->string('beConcession', 5);
            $table->string('beNo', 1);
            $table->string('billingGroup', 1);
            $table->string('billingCycle', 2);
            $table->string('voltageClass', 1);
            $table->string('paymentCollectionCenter', 40);
            $table->string('complaintCenterPhoneNo', 40);
            $table->string('lineCtRatio', 5);
            $table->string('meterCtRatio', 5);
            $table->string('overallMultiplingFactor', 7);
            $table->string('securityDeposit', 6);
            $table->string('others', 6);
            $table->string('othersvoltageSurchargeAmpunt', 6);
            $table->string('previousArrear', 9);
            $table->string('currentArrear', 9);
            $table->string('adjustmentAmount', 7);
            $table->string('reason', 10);
            $table->string('sundaryChargeDetail', 20);
            $table->string('consumtionOfOldMeter', 8);
            $table->string('fixedCharges', 8);
            $table->string('fuelCostAdjustmentCharges', 8);
            $table->string('periodOfAdjustmentDetail', 10);
            $table->string('lastSixMonthConsumption', 36);
            $table->string('rentChargeUpto', 8);
            $table->string('currentServiceRent', 3);
            $table->string('avrageLNI', 6);
            $table->string('avrageOthers', 6);
            $table->string('cowCessCode', 1);
            $table->string('waterSewarageChargesCode', 1);
            $table->string('specialCategoryCode', 2);
            $table->string('arrearIDF', 9);
            $table->string('arrearCowCess', 9);
            $table->string('arrearWaterSewarageChargesCode', 9);
            $table->string('sundryIDF', 9);
            $table->string('sundryCowCess', 9);
            $table->string('sundryWaterSewarageChargesCode', 9);
            $table->string('allowanceIDF', 9);
            $table->string('allowanceCowCess', 9);
            $table->string('allowanceWaterSewarageChargesCode', 9);
            $table->string('avgAdjustmentIDF', 9);
            $table->string('avgAdjustmentCowCess', 9);
            $table->string('avgAdjustmentWaterSewarageChargesCode', 9);
            $table->integer('status')->default(0);
            $table->integer('meterReader')->nullable();
            $table->string('readStatus')->default(0);
            $table->integer('readUnits')->nullable();
            $table->datetime('readDate')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bills');
    }
}
