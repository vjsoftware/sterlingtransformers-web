<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBindsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('binds', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('meterReader');
            $table->integer('accountNoG');
            $table->integer('accountNoL');
            $table->string('subDivisionCode', 3);
            $table->integer('billingCycle');
            $table->integer('billingGroup');
            $table->string('ledgerCode');
            $table->integer('user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('binds');
    }
}
