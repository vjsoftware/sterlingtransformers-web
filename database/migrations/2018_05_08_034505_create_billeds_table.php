<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBilledsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('billeds', function (Blueprint $table) {
            $table->increments('id');
            $table->string('blMasterKey');
            $table->string('blCurrentMeterRent')->nullable();
            $table->string('blCurrentServiceRent')->nullable();
            $table->string('blPresentReading')->nullable();
            $table->string('blPresentReadingDate')->nullable();
            $table->string('blPresentStatus')->nullable();
            $table->string('blUnitsConsumed')->nullable();
            $table->string('blDays')->nullable();
            $table->string('blMMCFlag')->nullable();
            $table->string('blCurrentSop')->nullable();
            $table->string('blCurrentEd')->nullable();
            $table->string('blCurrentOctroi')->nullable();
            $table->string('blNetSop')->nullable();
            $table->string('blNetEd')->nullable();
            $table->string('blNetOctroi')->nullable();
            $table->string('blCurrentRoundingAmt')->nullable();
            $table->string('blAmountBeforeDue')->nullable();
            $table->string('blSurCharge')->nullable();
            $table->string('blDueDateByCash')->nullable();
            $table->string('blDueDateByCheque')->nullable();
            $table->string('blAmountAfterDue')->nullable();
            $table->string('blTime')->nullable();
            $table->string('blConcessionalUnits')->nullable();
            $table->string('blCurrentIDFWithSign')->nullable();
            $table->string('blCurrentCowCessWithSign')->nullable();
            $table->string('blCurrentWaterSewageChargeWithSign')->nullable();
            $table->string('bltotalIDFWithSign')->nullable();
            $table->string('blTotalCowCessWithSign')->nullable();
            $table->string('blTotalCurrentWaterSewarageChargeWithSign')->nullable();
            $table->string('blCurrentFCAChargesWithSign')->nullable();
            $table->string('blCurrentFixedChargesWithSign')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('billeds');
    }
}
