<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBatchDataFormsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('batch_data_forms', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sysId');
            $table->integer('inputCode');
            $table->integer('trType');
            $table->integer('subdivisionCode');
            $table->integer('octraiCode');
            $table->integer('ledgerGroup');
            $table->integer('billingGroup');
            $table->integer('billingSubGroup');
            $table->integer('villageName');
            $table->integer('accountNo');
            $table->string('consumerName')->nullable();
            $table->string('address')->nullable();
            $table->string('feederCode')->nullable();
            $table->integer('tariffType')->nullable();
            $table->integer('employeeConsessionCode')->nullable();
            $table->integer('voltClassCode')->nullable();
            $table->double('connectedLoad')->nullable();
            $table->integer('meterRatio')->nullable();
            $table->date('connectionDate');
            $table->integer('lineCtRatio')->nullable();
            $table->integer('meterMultiplier')->nullable();
            $table->integer('overallMultiply')->nullable();
            $table->integer('phaseCode')->nullable();
            $table->integer('meterSerialNo')->nullable();
            $table->integer('amps')->nullable();
            $table->string('mfsCode')->nullable();
            $table->integer('mcbRent')->nullable();
            $table->integer('meterReading')->nullable();
            $table->string('ruralUrbenConsumer')->nullable();
            $table->integer('meterLocationCode')->nullable();
            $table->integer('meterSecurity')->nullable();
            $table->integer('acd')->nullable();
            $table->integer('nrs')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('batch_data_forms');
    }
}
