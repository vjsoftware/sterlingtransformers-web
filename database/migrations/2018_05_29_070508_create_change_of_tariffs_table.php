<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChangeOfTariffsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('change_of_tariffs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('sysId');
            $table->string('inputCode');
            $table->string('sheetNo');
            $table->string('pageNo');
            $table->string('numberOfEnties');
            $table->string('billingCycle');
            $table->string('billingGroup');
            $table->string('subdivisionCode');
            $table->string('ledgerGroup');
            $table->string('accountNo');
            $table->string('checkDigit');
            $table->string('tariffType');
            $table->string('bE1st');
            $table->string('bE2nd');
            $table->string('bE3rd');
            $table->string('bE4th');
            $table->string('bE5th');
            $table->string('numberOfBEs');
            $table->string('voltClass');
            $table->string('govtConnectionCode');
            $table->string('typesOfNRS');
            $table->string('remarks');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('change_of_tariffs');
    }
}
