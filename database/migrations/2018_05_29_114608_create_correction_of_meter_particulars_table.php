<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCorrectionOfMeterParticularsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('correction_of_meter_particulars', function (Blueprint $table) {
            $table->increments('id');
            $table->string('sysId');
            $table->string('inputCode');
            $table->string('sheetNo');
            $table->string('numberOfEnties');
            $table->string('pageNo');
            $table->string('billingCycle');
            $table->string('billingGroup');
            $table->string('subdivisionCode');
            $table->string('ledgerGroup');
            $table->string('accountNo');
            $table->string('checkDigit');
            $table->string('phaseCode');
            $table->string('meterNo');
            $table->string('ampere');
            $table->string('meterMultipiler');
            $table->string('meterCtRatio');
            $table->string('lineCtRatio');
            $table->string('overAllMultiplyingFactor');
            $table->string('additionalMeterRentals');
            $table->string('numberOfDigits');
            $table->string('mfrsCode');
            $table->string('admCode');
            $table->string('remarks');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('correction_of_meter_particulars');
    }
}
