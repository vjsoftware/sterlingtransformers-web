<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user');
            $table->string('userId');
            $table->string('loginDate');
            $table->string('loginTime');
            $table->string('loginLat')->nullable();
            $table->string('loginLong')->nullable();
            $table->string('dayCloseDate')->nullable();
            $table->string('dayCloseTime')->nullable();
            $table->string('dayCloseLat')->nullable();
            $table->string('dayCloseLong')->nullable();
            $table->string('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_logs');
    }
}
