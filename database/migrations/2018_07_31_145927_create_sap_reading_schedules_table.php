<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSapReadingSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sap_reading_schedules', function (Blueprint $table) {
            $table->increments('id');
            $table->string('meterReader');
            $table->string('subDivisionCode');
            $table->string('billingGroup');
            $table->string('billingCycle');
            $table->string('MRU');
            $table->integer('totalConnections');
            $table->string('scheduledDate');
            $table->string('scheduledTime');
            $table->string('scheduledBy');
            $table->string('receivedFlag');
            $table->string('receivedDate');
            $table->string('receivedTime');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sap_reading_schedules');
    }
}
