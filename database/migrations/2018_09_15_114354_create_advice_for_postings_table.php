<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdviceForPostingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('advice_for_postings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('sysId');
            $table->string('inputCode');
            $table->string('numberOfEnties');
            $table->string('sheetNo');
            $table->string('pageNo');
            $table->string('subdivisionCode');
            $table->string('ledgerGroup');
            $table->string('accountNo');
            $table->string('checkDigit');
            $table->string('sundryAllowanceAmount');
            $table->string('registerNo');
            $table->string('itemNo');
            $table->string('cashDeposit');
            $table->string('dateOfPayment');
            $table->string('ccrParticular');
            $table->string('remarks');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('advice_for_postings');
    }
}
