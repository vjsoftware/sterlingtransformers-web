<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTabAdvice96sTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tab_advice96s', function (Blueprint $table) {
            $table->increments('id');
            $table->string('sysId');
            $table->string('inputCode');
            $table->string('pSub');
            $table->string('pSer');
            $table->string('subdivisionCode');
            $table->string('ledgerGroup');
            $table->string('billingCycle');
            $table->string('billingGroup');
            $table->string('registerNo');
            $table->string('pageNo');
            $table->string('rcoFee');
            $table->string('from');
            $table->string('to');
            $table->string('accountNo');
            $table->string('checkDigit');
            $table->string('remarks');
            $table->string('cUser');
            $table->string('cDate');
            $table->string('cTime');
            $table->string('uReason');
            $table->string('dReason');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tab_advice96s');
    }
}
