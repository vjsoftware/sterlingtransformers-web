<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTabAdvice63sTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tab_advice63s', function (Blueprint $table) {
            $table->increments('id');
            $table->string('sysId');
            $table->string('inputCode');
            $table->string('numberOfEnties');
            $table->string('sheetNo');
            $table->string('pageNo');
            $table->string('subdivisionCode');
            $table->string('ledgerGroup');
            $table->string('accountNo');
            $table->string('checkDigit');
            $table->string('sundryAllowanceAmount');
            $table->string('registerNo');
            $table->string('itemNo');
            $table->string('cashDeposit');
            $table->string('dateOfPayment');
            $table->string('ccrParticular');
            $table->string('remarks');
            $table->string('cUser');
            $table->string('cDate');
            $table->string('cTime');
            $table->string('uUser');
            $table->string('uDate');
            $table->string('uTime');
            $table->string('uReason');
            $table->string('dUser');
            $table->string('dReason');
            $table->string('eFlag');
            $table->string('eDate');
            $table->string('eTime');
            $table->string('eUser');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tab_advice63s');
    }
}
