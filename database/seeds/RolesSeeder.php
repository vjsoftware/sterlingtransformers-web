<?php

use Illuminate\Database\Seeder;

use App\Role;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user =  Role::create([
          'name' => 'User',
          'slug' => 'slug',
          'permissions' => json_encode([
            'view-bill' => true
          ])
        ]);

        $superUser =  Role::create([
          'name' => 'Super User',
          'slug' => 'super-user',
          'permissions' => json_encode([
            'view-bill' => true,
            'upload-bill' => true,
            'can-create-user' => true,
            'can-enter-data' => true,
            'can-view' => true,
            'can-edit' => true,
          ])
        ]);
    }
}
