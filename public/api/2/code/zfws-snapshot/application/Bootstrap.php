<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{

    protected function _initAppAutoload() {
        $autoloader = new Zend_Application_Module_Autoloader(array(
            'namespace' => 'Mta',
            'basePath' => APPLICATION_PATH,
        ));
        return $autoloader;
    }

    protected function _initRestroutes() {
        $this->bootstrap('frontController');
        $frontController = Zend_Controller_Front::getInstance();

        $router = $frontController->getRouter();

        $restRoute = new Mta_Rest_Route($frontController,array(),array('rest'));
        $router->addRoute('rest', $restRoute);

        
    }

    protected function _initExampleRoute() {
      $this->bootstrap("frontController");
      $frontController = Zend_Controller_Front::getInstance();
      $router = $frontController->getRouter();
      $route = new Zend_Controller_Router_Route(
                       '/archive/print/:id',
                       array(
                         'module'     => 'default',
                         'controller' => 'index',
                         'action'     => 'index'
                       ),
                       array('id' => '\d+')
                );
       $router->addRoute('test', $route);
    }

    protected function _initAccess() {

        $this->bootstrap('db');
        $db = $this->getResource('db');

        $access = new Mta_Access();
        $access->setDb($db);
        Zend_Registry::set('Mta_Access',$access);

    }

    protected function _initCache() {
        $frontendOptions = array(
            'automatic_serialization' => true,
            'lifetime' => 9800 // 3 hours
        );

        $backendOptions = array(
            'servers' => array(
                            array('host' => 'localhost',
                                  'port' => 11211,
                                  'persistent' => 1
                              )
                         )
        );

        $cache = Zend_Cache::factory('Core','Zend_Cache_Backend_Memcached',$frontendOptions,$backendOptions,false,true);

        Zend_Registry::set('Zend_Cache', $cache);

        // enable metadata cache for db
        // set the cache to be used with all table objects
        Zend_Db_Table_Abstract::setDefaultMetadataCache($cache);
    }
    
}

