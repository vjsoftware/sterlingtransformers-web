<?php

class Soap_Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{

    protected function _initAppAutoload() {
        $autoloader = new Zend_Application_Module_Autoloader(array(
            'namespace' => 'Mta',
            'basePath' => APPLICATION_PATH,
        ));
        return $autoloader;
    }

    public function run() {
        if (isset($_GET['WSDL'])) {
            $autodiscover = new Zend_Soap_AutoDiscover();
            Zend_Loader::loadClass('Mta_Service');
            $autodiscover->setClass('Mta_Service');
            $autodiscover->setUri('http://zfws/API.php?service=soap');
            $autodiscover->handle();
        } else {
            $options = array('soap_version' => SOAP_1_2);
            Zend_Loader::loadClass('Mta_Service');
            $server = new Zend_Soap_Server('http://zfws/API.php?service=soap&WSDL=1', $options);
            $server->setObject(new Mta_Service());
            $server->handle();
        }
    }
}

