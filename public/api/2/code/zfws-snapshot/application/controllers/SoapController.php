<?php

class SoapController extends Zend_Controller_Action {

    public function init() {
        //... some init stuff for your controller ...
    }

    public function indexAction() {
        //... here you could output some information about your service ...
    }

    public function soapAction() {
        if (APPLICATION_ENV == 'development') {
            ini_set("soap.wsdl_cache_enabled", 0);//for development
        }
        $options = array('soap_version' => SOAP_1_2);
        Zend_Loader::loadClass('Mta_Service');
        $server = new Zend_Soap_Server('http://zfws/soap/wsdl', $options);
        $server->setObject(new Mta_Service());
        $server->handle();
    }

    public function wsdlAction() {
        if (APPLICATION_ENV == 'development') {
            ini_set("soap.wsdl_cache_enabled", 0);//for development
        }
        $autodiscover = new Zend_Soap_AutoDiscover();
        $autodiscover->setClass('Mta_Service');
        $autodiscover->setUri('http://zfws/soap/soap');
        $autodiscover->handle();
    }

}

