<?php

class Docs_RestController extends Zend_Controller_Action {

    public function indexAction() {
        $controllers = scandir(APPLICATION_PATH . '/modules/rest/controllers');
        $info = array();
        foreach ($controllers as $controller) {
            if (substr($controller,-14) == 'Controller.php') {
                $filename = APPLICATION_PATH
                            . '/modules/rest/controllers/'.$controller;
                include_once($filename);
                $info[] = new Zend_Reflection_File($filename);
            }
        }
        $this->view->info = $info;
    }
    
}

