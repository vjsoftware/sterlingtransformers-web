<?php

class Docs_XmlrpcController extends Zend_Controller_Action {

    public function init() {
        
    }

    public function indexAction() {
        $filename = APPLICATION_PATH . '/../library/Mta/Service.php';
        include_once($filename);
        $reflect = new Zend_Reflection_File($filename);

        $classes = $reflect->getClasses();

        if (isset($classes[0])) {
            $this->view->methods = $classes[0]->getMethods();
            
        }
    }

    
    
}

