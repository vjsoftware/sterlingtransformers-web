<?php

class Rest_CustomersController extends Mta_Rest_Controller {

    public function indexAction() {
        $customer = new Mta_Model_Customer();
        $result = $customer->fetchAll();
        $this->_createResponse(200,$result);
    }

    public function postAction() {
        $data = file_get_contents('php://input');
        $dom = new DOMDocument();
        if(!$dom->loadXML((string) $data)) {
            $this->_createResponse(400);
        } else {
            $xml = simplexml_load_string((string) $data);
            if (isset($xml->data)) {
                $customer = new Mta_Model_Customer();
                $array['customer_name'] = $xml->data->name;
                $array['customer_address'] = $xml->data->address;
                $array['customer_zip'] = $xml->data->zip;
                $array['customer_email'] = $xml->data->email;
                $newid = $customer->insert($array);
                $this->_createResponse(201,'/rest/customer/'.$newid);
            } else {
                $this->_createResponse(400);
            }
        }
    }

}

