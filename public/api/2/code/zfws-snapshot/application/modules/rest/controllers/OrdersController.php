<?php

class Rest_OrdersController extends Mta_Rest_Controller {

    public function indexAction() {
        if (!$this->_cache || (!$result = $this->_cache->load('get_orders'))) {
            $order = new Mta_Model_Order();
            $result = $order->fetchAll();
            if ($this->_cache) {
                $this->_cache->save($result,'get_orders');
            }
            
        }
        $this->_createResponse(200,$result);
    }

    public function getAction() {
        $order = new Mta_Model_Order();
        $result = $order->find($this->_request->id);
        if ($result && $result->count()) {
            $this->_createResponse(200,$result);
        } else {
            $this->_createResponse(410,'Gone');
        }
    }

    public function postAction() {
        if ($this->_cache) {
            $this->_cache->remove('get_orders');
        }
        //handle the creation of the order here
    }

    /*
    public function putAction() {

    }

    public function deleteAction() {

    }*/

}

