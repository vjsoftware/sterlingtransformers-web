<?php

class Mta_Access {

    private $_db     = NULL;
    private $_acl    = NULL;
    private $_key    = false;
    private $_rpcmap = NULL;

    public function __construct($db = NULL) {
        if ($db) {
            $this->setDb($db);
            $this->_createAcl();
        }        
    }

    public function setDb($db) {
        if (isset($db) && $db instanceof Zend_Db_Adapter_Abstract) {
            $this->_db = $db;
        }
    }

    private function _createAcl() {
        $this->_acl = new Zend_Acl();

        $roles = $this->_db->fetchAssoc('select * from acl_roles');
        foreach($roles as $role) {
            $this->_acl->addRole($role['role_name']);
        }

        $resources = $this->_db->fetchAssoc('select * from acl_resources ORDER BY resource_parent ASC');
        foreach($resources as $resource) {
            $resource_parent = NULL;
            if (!empty($resource['resource_parent'])
                    &&  isset($resources[$resource['resource_parent']])) {
                    $parent_id = $resource['resource_parent'];
                    $resource_parent = $resources[$parent_id]['resource_name'];
            }
            $this->_acl->addResource($resource['resource_name'],$resource_parent);
        }

        $privileges = $this->_db->fetchPairs('select privilege_id,privilege_name from acl_privileges');

        $rules = $this->_db->fetchAssoc('select * from acl_rules');
        foreach($rules as $rule) {
            $privilege = NULL;
            if (isset($privileges[$rule['rule_privilege_id']])) {
                $privilege = $privileges[$rule['rule_privilege_id']];
            }
            $role = NULL;
            if (isset($roles[$rule['rule_role_id']])) {
                $role = $roles[$rule['rule_role_id']]['role_name'];
            }
            $resource = NULL;
            if (isset($resources[$rule['rule_resource_id']])) {
                $resource = $resources[$rule['rule_resource_id']]['resource_name'];
            }

            if ($rule['rule_allow'] > 0) {
                $this->_acl->allow($role,$resource,$privilege);
            } else {
                $this->_acl->deny($role,$resource,$privilege);
            }
        }
    }

    public function getAcl() {
        if (!isset($this->_acl)) {
            $this->_createAcl();
        }
        return $this->_acl;
        
    }

    public function getKeyRole($key = '') {
        $query = 'SELECT r.role_name FROM acl_roles r LEFT JOIN acl_apikeys k
                    ON r.role_id = k.apikey_role_id
                    WHERE k.apikey_key = ? and k.apikey_role_id IS NOT NULL';
        return $this->_db->fetchOne($query,$key);
    }

    private function _getMethodInfo($method = '') {
        if (!isset($this->_rpcmap)) {
            $this->_getAllMethodInfo();
        }
        if (isset($this->_rpcmap[$method])) {
            return $this->_rpcmap[$method];
        }
        return false;
    }

    private function _getAllMethodInfo() {
        $query = 'SELECT m.method_name, r.resource_name,p.privilege_name FROM acl_methods m LEFT JOIN acl_resources r ON m.method_resource_id = r.resource_id LEFT JOIN acl_privileges p ON m.method_privilege_id = p.privilege_id WHERE r.resource_id IS NOT NULL AND p.privilege_id IS NOT NULL';
        $this->_rpcmap = $this->_db->fetchAssoc($query);
    }


    public function setKey($key = '') {
        $this->_key = $key;
    }

    public function methodAllowed($method = '',$service = 'soap') {
        if (!isset($this->_acl)) {
            $this->_createAcl();
        }
        if ($this->_key  && !empty($method)) {
            $role = $this->getKeyRole($this->_key);
            if (!empty($role) && $this->_acl->isAllowed($role,$service,'use')) {
                $params = $this->_getMethodInfo(strtolower($method));
                if (is_array($params)) {
                    //echo $role." ".$params['resource_name']." ".$params['privilege_name'];
                    return $this->_acl->isAllowed($role,$params['resource_name'],$params['privilege_name']);
                }
            }
        }
        return false;

    }

}