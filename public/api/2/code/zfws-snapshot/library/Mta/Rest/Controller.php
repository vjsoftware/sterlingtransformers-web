<?php
class Mta_Rest_Controller extends Zend_Rest_Controller {

    private $_cache = false;

    public function init() {
        if (Zend_Controller_Action_HelperBroker::hasHelper('layout')) {
            $this->_helper->layout->disableLayout();
        }
        $this->_helper->viewRenderer->setNoRender(true);

        //set the response code
        $this->_response->setHttpResponseCode(501);

        //create an instance of our response object
        $this->_output = new Mta_Rest_Response();
        //pass the Accept headers
        $this->_output->setAcceptheaders($this->_request->getHeader('Accept'));
        //set code for the output/response object
        $this->_output->setCode(501);
        //set the body of the output/response object
        $this->_output->setBody('Method Not Implemented');

        if (Zend_Registry::isRegistered('Zend_Cache')) {
            $this->_cache = Zend_Registry::get('Zend_Cache');
        }

        if ($this->_request->apikey) {
            $controller = $this->_request->getControllerName();
            $action = $this->_request->getActionName();
            if ($this->_hasAccess($this->_request->apikey,$controller,$action) == false) {
                $this->_output->setCode(403);
                $this->_output->setBody('Access denied');
                header("HTTP/1.0 403 Access denied");
                header('content-type: '.$this->_output->getType());
                echo $this->_output->getOutput();
                exit();
            }
        } else {
            $this->_output->setCode(403);
            $this->_output->setBody('Access denied');
            header("HTTP/1.0 403 Access denied");
            header('content-type: '.$this->_output->getType());
            echo $this->_output->getOutput();
            exit();
        }
                
    }

    public function indexAction() {
        $this->_response->setHeader('content-type',
                                    $this->_output->getType(),
                                    true);
        $this->_response->setBody($this->_output->getOutput());
    }

    public function getAction() {
        $this->_response->setHeader('content-type',
                                    $this->_output->getType(),
                                    true);
        $this->_response->setBody($this->_output->getOutput());
    }

    public function postAction() {
        $this->_response->setHeader('content-type',
                                    $this->_output->getType(),
                                    true);
        $this->_response->setBody($this->_output->getOutput());
    }

    public function putAction() {
        $this->_response->setHeader('content-type',
                                    $this->_output->getType(),
                                    true);
        $this->_response->setBody($this->_output->getOutput());
    }

    public function deleteAction() {
        $this->_response->setHeader('content-type',
                                    $this->_output->getType(),
                                    true);
        $this->_response->setBody($this->_output->getOutput());
    }

    protected function _createResponse($code,$body,$type = 'auto') {
        $this->_response->setHttpResponseCode($code);
        $this->_output->setCode($code);
        $this->_output->setBody($body);

        $this->_response->setHeader('content-type',
                                    $this->_output->getType(),
                                    true);
        $this->_response->setBody($this->_output->getOutput($type));
    }

    protected function _hasAccess($apikey,$resource,$action) {

        if (Zend_Registry::isRegistered('Mta_Access')) {

            $access = Zend_Registry::get('Mta_Access');
            $role = $access->getKeyRole($apikey);
            if (empty($role)) {
                return false;
            }
            $acl = $access->getAcl();
            if ($acl->isAllowed($role,'rest','use')) {
                $priv = NULL;
                switch($action) {
                    case 'index':
                    case 'get':
                        $priv = 'read';
                        break;
                    case 'post':
                        $priv = 'create';
                        break;
                    case 'put':
                        $priv = 'update';
                        break;
                    case 'delete':
                        $priv = 'delete';
                        break;
                }
                //echo $role.' '.$resource.' '.$priv;
                if ($acl->isAllowed($role,$resource,$priv)) {
                    return true;
                }
            }
        }

        return false;

    }

}
?>
