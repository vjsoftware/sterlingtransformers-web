<?php

class Mta_Rest_Response {

    private $_code          = 200;
    private $_body          = NULL;
    private $_type          = 'xml';
    private $_json          = NULL;
    private $_xml           = NULL;
    private $_ctype         = 'application/xml';
    private $_acceptheaders = NULL;

    public function __construct($options = array()) {
        $options = (array) $options;

        if (isset($options['type'])) {
             $this->setType($options['type']);
        }

        if (isset($options['code'])) {
            $this->setCode($options['code']);
        }

        if (isset($options['body'])) {
            $this->setBody($options['body']);
        }

    }

    public function setType($type = 'auto') {
        if ($type == 'auto' && $this->_acceptheaders) {
            if (in_array('application/json',$this->_acceptheaders)) {
                $type = 'json';
            } elseif (in_array('application/xml',$this->_acceptheaders)) {
                $type = 'xml';
            }
        }

        if (in_array($type,array('xml','json'))) {
            $this->_type = $type;
            switch($type) {
                case 'xml':
                default:
                    $this->_ctype = 'application/xml';
                    break;
                case 'json':
                    $this->_ctype = 'application/json';
                    break;
            }
        }
    }

    public function setCode($code = 200) {
        $this->_code = $code;
    }

    public function getCode() {
        return $this->_code;
    }

    public function setBody($data = NULL) {
        $body = NULL;
        if ($data instanceof Zend_Db_Table_Rowset) {
            $data = $data->toArray();
            $cnt = 0;
            foreach ($data as $row) {
                $body[$cnt] = array();
                $body[$cnt] = array();
                foreach ($row as $column => $value) {
                    $body[$cnt][$column] = $value;
                }
                $cnt++;
            }
        } elseif (is_string($data) || is_numeric()) {
            $body = $data;
        }
        $this->_body = $body;
    }

    public function getBody() {
        return $this->_body;
    }

    public function getXml() {
        $xml = '<response>';
        $xml .= '<code>'.$this->_code.'</code>';
        if (is_string($this->_body)) {
            $xml .= '<msg>'.$this->_body.'</msg>';
        } elseif (is_array($this->_body)) {
            $xml .= '<msg>';
            foreach($this->_body as $items) {
                $xml .= '<item>';
                foreach ($items as $key => $val) {
                    $xml .= "<$key>$val</$key>";
                }
                $xml .= '</item>';
            }
            $xml .= '</msg>';
        }
        $xml .= '</response>';
        $this->_xml = simplexml_load_string($xml);
        return $this->_xml->asXML();
    }

    public function getJson() {
        $this->_json = new stdClass();
        $this->_json->code = $this->_code;
        $this->_json->body = $this->_body;
        return Zend_Json::encode($this->_json);
    }

    public function getOutput($type = NULL) {
        if (!is_null($type)) {
            $this->setType($type);
        }
        switch($this->_type) {
            case 'json':
                return $this->getJson();
                break;
            case 'xml':
            default:
                return $this->getXml();
                break;
        }
     }

     public function getCtype() {
         return $this->_ctype;
     }

     public function getType() {
         return $this->_type;
     }

     public function setAcceptheaders($headerstring = '') {
         $this->_acceptheaders = explode(',',$headerstring);
     }

}
?>
