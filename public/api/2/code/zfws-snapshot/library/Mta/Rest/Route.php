<?php
class Mta_Rest_Route extends Zend_Rest_Route {

    public function match($path, $partial = false) { 

        $uri = $path->getRequestUri();
        $parts = explode('/',$uri);
        if ($parts['1'] == 'rest' && isset($parts['2'])) {
            if(preg_match('/([0-9]+[.])+([0-9]+)/',$parts['2'])) {
                $version = $parts['2'];
                unset($parts['2']);
                $parts = array_values($parts);

                $front = Zend_Controller_Front::getInstance();
                $front->setControllerDirectory(
                        array(
                            'default' => APPLICATION_PATH.'/controllers',
                            'rest' => APPLICATION_PATH.'/modules/rest/controllers/'.$version
                            )
                        ,'rest');
            }

            //here we add support for /rest/2.0/orders/apikey/12345
            //and  /rest/orders/apikey/12345
            //print_r($parts);
            if (isset($parts[3])) {
                $cnt = 0;
                $newparts = array();
                if (!is_numeric($parts[3])) {
                    while($cnt < 3) {
                        $newparts[] = array_shift($parts);
                        $cnt++;
                    }
                } else {//we maintain support for 'id'
                    while($cnt < 4) {
                        $newparts[] = array_shift($parts);
                        $cnt++;
                    }
                }
                $uri = implode('/',$newparts);
                $additional = $parts;
            } else {
                $uri = implode('/',$parts);
                $additional = false;
            }

            $path->setRequestUri($uri);
            $path->setPathInfo($uri);
        }
        
        $result = parent::match($path, $partial);

        //after we let Zend_Rest_Route do its job, we set the additional
        //parameters like apikey detected above
        if ($additional && is_array($additional)) {
            $key = true;
            for($i = 0; $i < count($additional); $i++) {
                if($i%2 == 0) {
                    $result[$additional[$i]] = $additional[$i + 1];
                }
            }
        }

        return $result;
    }
}

?>
