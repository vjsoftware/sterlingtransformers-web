<?php
/* 
 * Our first, rough attempt at a class for the webservice under construction
 */

/**
 * This class will allow for the exposure of the desired methods
 *
 * @author jonas
 */
class Mta_Service {

  private $_stock = false;
  private $_order = false;
  private $_product = false;

  /**
   * used by mocking in unit tests
   *
   * @param object $model
   * @param string $classtype tipe of model set, hinted by unit test
   */
  public function __setModel($model,$classtype = NULL) {

      if (!isset($classtype)) {
          $classtype = get_class($model);
      }
      switch($classtype) {
          case 'Mta_Model_Product':
              $this->_product = $model;
          case 'Mta_Model_Order':
              $this->_order = $model;
          case 'Mta_Model_Stock':
              $this->_stock = $model;
              break;
      }
  }


  /**
   * Returns a list of products for the customer
   *
   * @param   int $customer_id  customer identifier
   * @param   int $category     (optional) category
   * @return  array|object
   */
  public function getCatalogue($customer_id,$category = '*') {
      Zend_Loader::loadFile(APPLICATION_PATH . '/models/Product.php');
      $catalogue = new Mta_Model_Product();
      return $catalogue->getItems($customer_id,$category);
  }


  /**
   * Returns number of items in stock for the product.
   *
   * @param   int $product_id  product identifier
   * @return  int              quantity available
   */
  public function getStockFigures($product_id) {
      if ($product_id > 0) {
        if (!$this->_stock) {
            Zend_Loader::loadFile(APPLICATION_PATH . '/models/Stock.php');
            $this->_stock = new Mta_Model_Stock();
        }
        return $this->_stock->getAvailable($product_id);
      } else {
          throw new Mta_Exception('Product id must be greater then 0','888');
      }
      
  }

  /**
   * Create a backorder for a given product id
   *
   * @param   int $product_id   product identifier
   * @param   int $quantity
   *
   * @return  int  backorder id
   */
  public function createBackOrder($product_id,$quantity) {
      Zend_Loader::loadFile(APPLICATION_PATH . '/models/Order.php');
      $order = new Mta_Model_Order();
      return $order->backOrder($product_id,$quantity);
  }


  /**
   * Get the location of an image for the product
   *
   * @param   int $product_id   product identifier
   *
   * @return  string  image URL
   */
  public function getImageLocation($product_id) {
      Zend_Loader::loadFile(APPLICATION_PATH . '/models/Product.php');
      $product = new Mta_Model_Product();
      return $product->getImage($product_id);      
  }


  /**
   * Create an order precising product, customer and the product count
   *
   * @param   int $product_id   product identifier
   * @param   int $customer_id  customer identifier
   * @param   int $productcnt   nr. items ordered
   *
   * @return  int  order id for the newly created order
   */
  public function createOrder($product_id,$customer_id,$productcnt) {
      Zend_Loader::loadFile(APPLICATION_PATH . '/models/Order.php');
      $order = new Mta_Model_Order();
      return $order->create($product_id,$customer_id,$cnt);
  }

  /**
   * Get the status of an existing order
   *
   * @param   int $order_id   order identifier
   *
   * @return  string  message containing a code
   */
  public function getOrderStatus($order_id) {
      Zend_Loader::loadFile(APPLICATION_PATH . '/models/Order.php');
      $order = new Mta_Model_Order();
      return $order->getStatus($order_id);
  }

  //.... and so on ...


}