<?php

class Mta_Servicewrapper {

    public function __call($method_name, $arguments) {
        $service = new Mta_Service();
        if (method_exists($service, $method_name)) {
            if ($this->_hasAccess($arguments[0],$method_name)) {
                unset($arguments[0]);
                return call_user_func_array(array($service, $method_name), $arguments);
            } else {
                return new SoapFault('Sender','403 : Access forbidden');
            }
        } else {
            return new SoapFault('Sender','404 : Service not found');
        }

    }

    /**
     * check if apikey has access to method
     *
     * @param string $apikey
     * @param string $method
     * @return bool
     */
    protected function _hasAccess($apikey,$method) {

        if (Zend_Registry::isRegistered('Mta_Access')) {
            $access = Zend_Registry::get('Mta_Access');
            $access->setKey($apikey);
            return $access->methodAllowed($method);

        }
        //else
        return false;

    }
}