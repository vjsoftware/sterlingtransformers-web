<?php

require_once 'Zend/Server/Interface.php';
require_once 'Zend/Soap/Server.php';

class Mta_Soap_Server extends Zend_Soap_Server {

    /*
     * call parent constructor
     *
     */
    public function __construct($wsdl = null, array $options = null) {

        parent::__construct($wsdl,$options);
        
    }

    /**
     * Handle a request
     * overload the default ZF method
     *
     * @param string $request Optional request
     * @return void|string
     */
    public function handle($request = null) {
        if (null === $request) {
            $request = file_get_contents('php://input');
        }
        if (strlen($request) == 0) {
            $soap = $this->_getSoap();
            $soap->fault(401 , 'Message contains no XML');
        }
        if ($this->_checkAllow($_SERVER['REMOTE_ADDR'])) {
            //error_log(PHP_EOL.date('Ymd h:i:s')
            //        . " REQUEST: ".print_r($request,1),3,'/tmp/webservice.log');
            $dom = new DOMDocument();
            if(!$dom->loadXML($request)) {
                $soap = $this->_getSoap();
                $soap->fault(401 , 'Message contains invalid XML');
            } else {
                //strip out api_key stuff
                $xml = simplexml_load_string($request);


                $url = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];

                $children  = (array) $xml->children('http://schemas.xmlsoap.org/soap/envelope/')
                                         ->Body
                                         ->children($url);
                $methods = array_keys($children);
                $method =  $methods[0];

                $soap = $this->_getSoap();
                
                $apikey = (string) $xml->children('http://schemas.xmlsoap.org/soap/envelope/')
                                        ->Body
                                        ->children($url)
                                        ->{$method}
                                        ->children()
                                        ->apikey;

                if ($this->_hasAccess($apikey,$method)) {
                    //strip api info
                    unset($xml->children('http://schemas.xmlsoap.org/soap/envelope/')
                                        ->Body
                                        ->children($url)
                                        ->{$method}
                                        ->children()
                                        ->apikey);

                    $request = $xml->asXml();

                    //remove whitespace & empty lines
                    $request = trim($request);
                    $request = preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", "\n", $request);
                    return parent::handle($request);
                    $response = parent::getLastResponse();
                } else {
                    $soap = $this->_getSoap();
                    $soap->fault(403 , 'Access forbidden');
                }
            }

            //error_log(PHP_EOL.date('Ymd h:i:s')
            //        . " RESPONSE: ".print_r($response,1),3,'/tmp/webservice.log');
            return $response;
        } else {
            //log limit exceeded for given IP

            //return SOAP fault
            $soap = $this->_getSoap();
            $soap->fault(509 , 'Limit Exceeded');
        }
        
    }

    /**
     * Check if this IP isn't firing too much requests
     *
     * @param string $ip
     * @return bool
     */
    protected function _checkAllow($ip) {
        //if too much return false
        
        //else
        return true;

    }
    
    /**
     * check if apikey has access to method
     *
     * @param string $apikey
     * @param string $method
     * @return bool
     */
    protected function _hasAccess($apikey,$method) {
        //if apikey has no access, return false
        if (Zend_Registry::isRegistered('Mta_Access')) {

            $access = Zend_Registry::get('Mta_Access');
            $access->setKey($apikey);
            return $access->methodAllowed($method);

        } else {
            die('No Mta_Access found');
        }

        //else
        return false;

    }

}
