<?php

// Define path to application directory
defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../application'));

// Define application environment
defined('APPLICATION_ENV')
    || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));

// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
    realpath(APPLICATION_PATH . '/../library'),
    get_include_path(),
)));

switch ($_GET['service']) {
   case 'xmlrpc':
      $bsinfo = array('class' => 'XmlRpc_Bootstrap',
            	      'path'  => APPLICATION_PATH . '/ServiceBootstrap.php');
      break;
   case 'soap':
	$bsinfo = array('class' => 'Soap_Bootstrap',
            	        'path'  => APPLICATION_PATH . '/ServiceBootstrap.php');
      break;
}

/** Zend_Application */
require_once 'Zend/Application.php';

$app = new Zend_Application(
            APPLICATION_ENV,
            array(
                'bootstrap' => $bsinfo,
                'config' => APPLICATION_PATH . '/configs/application.ini',
            )
        );
$app->bootstrap()
    ->run();