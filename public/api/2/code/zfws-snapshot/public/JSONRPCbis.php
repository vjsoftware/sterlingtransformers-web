<?php
// Define path to application directory
defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../application'));

// Define application environment
defined('APPLICATION_ENV')
    || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));

// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
    realpath(APPLICATION_PATH . '/../library'),
    get_include_path(),
)));

/** Zend_Application */
require_once 'Zend/Application.php';

$application = new Zend_Application(
    APPLICATION_ENV,
    APPLICATION_PATH . '/configs/application.ini'
);

$application->bootstrap('db');//bootstrap db resource for example


$cacheFile = dirname(__FILE__) . '/../data/cache/jsonrpc.cache';
$server = new Zend_Json_Server();

Zend_Loader::loadClass('Mta_Service');
$server->setClass('MTA_Service');

if ('GET' == $_SERVER['REQUEST_METHOD']) {
    if (!($smd = Zend_Json_Server_Cache::getSmd($cacheFile, $server))) {
        Zend_Json_Server_Cache::saveSmd($cacheFile, $server);
        $smd = $server->getServiceMap();
    }
    $server->setTarget('/JSONRPCbis.php');
    header('Content-Type: application/json');
    echo $smd;
    return;
}
//$server->handle();
$request = new Zend_Json_Server_Request_Http();
$server->setAutoEmitResponse(false);
$response = $server->handle($request);
echo $response;

