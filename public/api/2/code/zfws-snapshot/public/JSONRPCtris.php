<?php
// Define path to application directory
defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../application'));

// Define application environment
defined('APPLICATION_ENV')
    || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));

// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
    realpath(APPLICATION_PATH . '/../library'),
    get_include_path(),
)));

/** Zend_Application */
require_once 'Zend/Application.php';

$application = new Zend_Application(
    APPLICATION_ENV,
    APPLICATION_PATH . '/configs/application.ini'
);

$application->bootstrap('db');//bootstrap db resource for example

$access = new Mta_Access();
$db = $application->getBootstrap()->getResource('db');
$access->setDb($db);
Zend_Registry::set('Mta_Access',$access);

$server = new Zend_Json_Server();
Zend_Loader::loadClass('Mta_Service');
$server->setClass('MTA_Service');

$request = new Zend_Json_Server_Request_Http();
$params = $request->getParams();
if (isset($params[0]) && isset($params[0]['apikey'])) {
    //check access for key
    if (Zend_Registry::isRegistered('Mta_Access')) {
        $method = strtolower($request->getMethod());
        $access = Zend_Registry::get('Mta_Access');
        $access->setKey($params[0]['apikey']);
        if ($access->methodAllowed($method)) {
            $request->setParams($params[1]);
        } else {
            $server->fault('Not allowed','','API key '.$params[0]['apikey'].' not allowed');
        }
    } else {
        $server->fault('Access control error','','error encountered during ACL check');
    }
} else {
    $server->fault('Not allowed','','API key missing');
}
$server->setAutoEmitResponse(false);
$response = $server->handle($request);
echo $response;

