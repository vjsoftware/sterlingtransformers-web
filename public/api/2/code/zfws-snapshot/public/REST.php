<?php
// Define path to application directory
defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../application'));

// Define application environment
defined('APPLICATION_ENV')
    || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));

// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
    realpath(APPLICATION_PATH . '/../library'),
    get_include_path(),
)));

/** Zend_Application */
require_once 'Zend/Application.php';

$application = new Zend_Application(
    APPLICATION_ENV,
    APPLICATION_PATH . '/configs/application.ini'
);

$application->bootstrap('db');//bootstrap db resource for example

Zend_Loader::loadClass('Mta_Service');
$server = new Zend_Rest_Server();
$server->setClass('Mta_Service');
if (isset($_GET['__info__'])) {
    header('HTTP/1.1 200 Ok');  
    header('Content-type: text/xml');
    $xml = '<?xml version="1.0" encoding="UTF-8"?>';
    $xml .= '<response>';
    $xml .= '<operations>';
    foreach($server->getFunctions() as $function)  {
        $xml .= '<method>'.$function->getName().'</method>';
    }
    $xml .= '</operations>';
    $xml .= '<code>200</code>';
    $xml .= '</response>';
    echo $xml;
} else {
    $server->handle();
}

