<?php
// Define path to application directory
defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../application'));

// Define application environment
defined('APPLICATION_ENV')
    || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));

// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
    realpath(APPLICATION_PATH . '/../library'),
    get_include_path(),
)));

/** Zend_Application */
require_once 'Zend/Application.php';

$application = new Zend_Application(
    APPLICATION_ENV,
    APPLICATION_PATH . '/configs/application.ini'
);

$application->bootstrap('db');//bootstrap db resource for example

if (isset($_GET['v'])) {
    die('version found:' . $_GET['v']);
}

$access = new Mta_Access();
$db = $application->getBootstrap()->getResource('db');
$access->setDb($db);
Zend_Registry::set('Mta_Access',$access);

ini_set("soap.wsdl_cache_enabled", 0);//for development
if (isset($_GET['WSDL'])) {
    $autodiscover = new Mta_Soap_AutoDiscover();
    $autodiscover->setClass('Mta_Service');
    $autodiscover->setUri('http://zfws/SOAP.php');
    $autodiscover->handle();
} elseif (isset($_GET['INTERNALWSDL'])) {
    $autodiscover = new Zend_Soap_AutoDiscover();
    $autodiscover->setClass('Mta_Service');
    $autodiscover->setUri('http://zfws/SOAP.php');
    $autodiscover->handle();
} else {
    $options = array('soap_version' => SOAP_1_2);
    $server = new Mta_Soap_Server('http://zfws/SOAP.php?INTERNALWSDL=1', $options);
    $server->setObject(new Mta_Service());
    $server->handle();
}