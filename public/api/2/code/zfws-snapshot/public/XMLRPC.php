<?php
// Define path to application directory
defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../application'));

// Define application environment
defined('APPLICATION_ENV')
    || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));

// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
    realpath(APPLICATION_PATH . '/../library'),
    get_include_path(),
)));

/** Zend_Application */
require_once 'Zend/Application.php';

$application = new Zend_Application(
    APPLICATION_ENV,
    APPLICATION_PATH . '/configs/application.ini'
);

$application->bootstrap('db');//bootstrap db resource for example

Zend_Loader::loadClass('Mta_Exception');
Zend_XmlRpc_Server_Fault::attachFaultException('Mta_Exception');

$cacheFile = dirname(__FILE__) . '/../data/cache/xmlrpc.cache';
$server = new Zend_XmlRpc_Server();
if (!Zend_XmlRpc_Server_Cache::get($cacheFile, $server)) {
    Zend_Loader::loadClass('Mta_Service');
    $server->setClass('MTA_Service','zfws');
    Zend_XmlRpc_Server_Cache::save($cacheFile, $server);
}
echo $server->handle();