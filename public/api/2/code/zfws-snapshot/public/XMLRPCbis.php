<?php
// Define path to application directory
defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../application'));

// Define application environment
defined('APPLICATION_ENV')
    || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));

// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
    realpath(APPLICATION_PATH . '/../library'),
    get_include_path(),
)));

/** Zend_Application */
require_once 'Zend/Application.php';

$application = new Zend_Application(
    APPLICATION_ENV,
    APPLICATION_PATH . '/configs/application.ini'
);

$application->bootstrap('db');//bootstrap db resource for example

Zend_Loader::loadClass('Mta_Exception');
Zend_XmlRpc_Server_Fault::attachFaultException('Mta_Exception');

$cacheFile = dirname(__FILE__) . '/../data/cache/xmlrpc.cache';
$server = new Zend_XmlRpc_Server();
if (!Zend_XmlRpc_Server_Cache::get($cacheFile, $server)) {
    Zend_Loader::loadClass('Mta_Service');
    $server->setClass('MTA_Service','zfws');
    Zend_XmlRpc_Server_Cache::save($cacheFile, $server);
}

$access = new Mta_Access();
$db = $application->getBootstrap()->getResource('db');
$access->setDb($db);
Zend_Registry::set('Mta_Access',$access);

$data = file_get_contents('php://input');
$dom = new DOMDocument();
if(!$dom->loadXML((string) $data)) {
    echo $server->fault('Invalid XML',400);
} else {
    $xml = simplexml_load_string((string) $data);
    if (isset($xml->methodName) && isset($xml->params) && isset($xml->params->param[0])) {
        //check access for key
        if (Zend_Registry::isRegistered('Mta_Access')) {

            $access = Zend_Registry::get('Mta_Access');
            $access->setKey($xml->params->param[0]->value);
            if ($access->methodAllowed(strtolower(str_replace('zfws.','',$xml->methodName)),'xmlrpc')) {
                //then strip it out
                unset($xml->params->param[0]);
                $xmlstring = (string) $xml->asXml();

                //remove whitespace & empty lines
                $xmlstring = trim($xmlstring);
                $xmlstring = preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", "\n", $xmlstring);

                $request = new Zend_XmlRpc_Request();
                $request->loadXml($xmlstring);
                echo $server->handle($request);
            } else {
                echo $server->fault('Access not allowed',403);
            }

        } else {
            echo $server->fault('Error during access control',500);
        }
        
    } else {
        echo $server->fault('Missing API key',403);
    }
    
}