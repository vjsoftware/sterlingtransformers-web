<?php
// Define path to application directory
defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../application'));

// Define application environment
defined('APPLICATION_ENV')
    || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));

// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
    realpath(APPLICATION_PATH . '/../library'),
    get_include_path(),
)));

/** Zend_Application */
require_once 'Zend/Application.php';

$application = new Zend_Application(
    APPLICATION_ENV,
    APPLICATION_PATH . '/configs/application.ini'
);

$application->bootstrap('db');//bootstrap db resource for example


$acl = new Zend_Acl();

$acl->addResource('api');
$acl->addResource('rest','api');
$acl->addResource('soap','api');
$acl->addResource('xmlrpc','api');
$acl->addResource('jsonrpc','api');
$acl->addResource('reports');
$acl->addResource('admin');

$acl->addRole('anonymous');
$acl->addRole('customer');
$acl->addRole('finance department');
$acl->addRole('staff',array('customer','finance department'));
$acl->addRole('manager',array('customer','finance department'));

$acl->deny('anonymous','api');
$acl->deny('anonymous','reports');

$acl->allow('customer','api');
$acl->allow('finance department','reports');
$acl->allow('manager','admin');


echo "<br />Access checks";
echo "<br />anonymous -> rest:";
echo $acl->isAllowed('anonymous', 'rest') ? 'allowed' : 'denied';
echo "<br />customer -> rest:";
echo $acl->isAllowed('customer', 'rest') ? 'allowed' : 'denied';
echo "<br />customer -> soap:";
echo $acl->isAllowed('customer', 'soap') ? 'allowed' : 'denied';
echo "<br />finance department -> reports:";
echo $acl->isAllowed('finance department', 'reports') ? 'allowed' : 'denied';
echo "<br />finance department -> jsonrpc:";
echo $acl->isAllowed('finance department', 'jsonrpc') ? 'allowed' : 'denied';
echo "<br />staff -> xmlrpc:";
echo $acl->isAllowed('staff', 'xmlrpc') ? 'allowed' : 'denied';

