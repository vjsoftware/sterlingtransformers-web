<?php

/*
 * here we print the same tables as in acltest2, but now we load the ACL from our
 * class Mta_Access
 */

// Define path to application directory
defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../application'));

// Define application environment
defined('APPLICATION_ENV')
    || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));

// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
    realpath(APPLICATION_PATH . '/../library'),
    get_include_path(),
)));

/** Zend_Application */
require_once 'Zend/Application.php';

$application = new Zend_Application(
    APPLICATION_ENV,
    APPLICATION_PATH . '/configs/application.ini'
);

$config = new Zend_Config_Ini(APPLICATION_PATH . "/configs/application.ini", APPLICATION_ENV,$options);
$db = Zend_Db::factory($config->resources->db);

$access = new Mta_Access();
$access->setDb($db);
$acl = $access->getAcl();

//print_r($acl);

//yep, this code is far from stellar. It's just for demonstration purposes...

$roles = $db->fetchAssoc('select * from acl_roles');
$resources = $db->fetchAssoc('select * from acl_resources ORDER BY resource_parent ASC');
$privileges = $db->fetchPairs('select privilege_id,privilege_name from acl_privileges');


$decorator = new Zend_Text_Table_Decorator_Ascii();

echo "<h3>Access on main parts</h3>";
$table1 = new Zend_Text_Table(array('columnWidths' => array(20, 5, 5, 5)));
$table1->setDecorator($decorator);
$table1->appendRow(array('','API','data','admin'));
foreach($roles as $role) {
    $result = array();
    $result[] = $role['role_name'];
    $result[] = $acl->isAllowed($role['role_name'],'api','use') ? 'x' : ' ';
    $result[] = $acl->isAllowed($role['role_name'],'data','use') ? 'x' : ' ';
    $result[] = $acl->isAllowed($role['role_name'],'admin','use') ? 'x' : ' ';
    $table1->appendRow($result);
    
}

$services = array('soap','rest','xmlrpc','jsonrpc');
$serviceprivs = array('use');

echo '<pre>'.$table1.'</pre>';

echo "<h3>Access on different web services</h3>";
$widths2 = array(20, 8, 8, 8, 8);
$table2 = new Zend_Text_Table(array('columnWidths' => $widths2));
$table2->setDecorator($decorator);
$table2->appendRow(array('','soap','rest','xmlrpc','jsonrpc'));
foreach($roles as $role) {
    $result = array();
    $result[] = $role['role_name'];

    foreach($services as $service) {
        foreach($serviceprivs as $priv) {
            //echo '<br />'.$role.' '.$service.' '.$priv;
            $result[] = $acl->isAllowed($role['role_name'],$service,$priv) ? 'x' : ' ';
        }
    }
    $table2->appendRow($result);

}
echo '<pre>'.$table2.'</pre>';

$objects = array('orders','invoices','products','stock','shipping');
$serviceprivs = array('create','read','update','delete');

echo "<h3>Access on different kinds of data</h3>";
$widths3 = array(20, 8, 4, 6, 6, 8, 4, 6, 6, 8, 4, 6, 6, 8, 4, 6, 6, 8, 4, 6, 6);
$table3 = new Zend_Text_Table(array('columnWidths' => $widths3));
$table3->setDecorator($decorator);
$header1 = array('','orders','','','','invoices','','','','products','','','','stock','','','','shipping');
$table3->appendRow($header1);
$header2 = array('');
for ($i = 0; $i < 5; $i++) {
    $header2 = array_merge($header2,$serviceprivs);
}
$table3->appendRow($header2);
foreach($roles as $role) {
    $result = array();
    $result[] = $role['role_name'];

    foreach($objects as $object) {
        foreach($serviceprivs as $priv) {
            //echo '<br />'.$role.' '.$object.' '.$priv;
            $result[] = $acl->isAllowed($role['role_name'],$object,$priv) ? 'x' : ' ';
        }
    }
    $table3->appendRow($result);
}
echo '<pre>'.$table3.'</pre>';

echo $acl->isAllowed('partner', 'soap','use') ? 'allowed' : 'denied';
echo $acl->isAllowed('partner', 'orders','read') ? 'allowed' : 'denied';