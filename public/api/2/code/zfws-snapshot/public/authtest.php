<?php
if (!isset($_SERVER['PHP_AUTH_USER']) ||
        !isset($_SERVER['PHP_AUTH_PW']) ||
           $_SERVER['PHP_AUTH_USER'] != 'zfws' ||
                $_SERVER['PHP_AUTH_PW'] != 'zfws') {
                    header('HTTP/1.0 401 Unauthorized');
                    header('WWW-Authenticate: Basic realm="Restricted"');
                    die('Wrong credentials');
} else {
    echo 'Logged in';
}
