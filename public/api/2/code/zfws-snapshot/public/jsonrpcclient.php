<?php
set_include_path(implode(PATH_SEPARATOR, array(
    realpath('../library'),
    get_include_path(),
)));

require_once('Zend/Loader.php');
Zend_Loader::loadClass('Zend_Http_Client');
Zend_Loader::loadClass('Zend_Json');

$request = new stdClass();
$request->jsonrpc = '2.0';
$request->method = 'getStockFigures';
$request->params = array(1);
$request->id = time();

$json = Zend_Json::encode($request);

echo 'Request: <pre>';
echo Zend_Json::prettyPrint($json);
echo '</pre>';


$client = new Zend_Http_Client('http://zfws/JSONRPC.php');
$response = $client->setRawData($json, 'application/json')->request('POST');

echo 'Response: <pre>';
echo Zend_Json::prettyPrint($response->getBody());
echo '</pre>';

$request = new stdClass();
$request->jsonrpc = '2.0';
$request->method = 'getNonExisting';
$request->params = array(1);
$request->id = time();

$json = Zend_Json::encode($request);
$client = new Zend_Http_Client('http://zfws/JSONRPC.php');
$response = $client->setRawData($json, 'application/json')->request('POST');

echo 'Response containing error: <pre>';
echo Zend_Json::prettyPrint($response->getBody());
echo '</pre>';

//$string = '{"transport":"POST","envelope":"JSON-RPC-1.0","contentType":"application\/json","SMDVersion":"2.0","services":{"getCatalogue":{"envelope":"JSON-RPC-1.0","transport":"POST","parameters":[{"type":"integer","name":"customer_id","optional":false},{"type":"integer","name":"category","optional":true,"default":"*"}],"returns":["array","array","object","object"]},"getStockFigures":{"envelope":"JSON-RPC-1.0","transport":"POST","parameters":[{"type":"integer","name":"product_id","optional":false}],"returns":"integer"},"createBackOrder":{"envelope":"JSON-RPC-1.0","transport":"POST","parameters":[{"type":"integer","name":"product_id","optional":false},{"type":"integer","name":"quantity","optional":false}],"returns":"integer"},"getImageLocation":{"envelope":"JSON-RPC-1.0","transport":"POST","parameters":[{"type":"integer","name":"product_id","optional":false}],"returns":"string"},"createOrder":{"envelope":"JSON-RPC-1.0","transport":"POST","parameters":[{"type":"integer","name":"product_id","optional":false},{"type":"integer","name":"customer_id","optional":false},{"type":"integer","name":"productcnt","optional":false}],"returns":"integer"},"getOrderStatus":{"envelope":"JSON-RPC-1.0","transport":"POST","parameters":[{"type":"integer","name":"order_id","optional":false}],"returns":"string"}},"methods":{"getCatalogue":{"envelope":"JSON-RPC-1.0","transport":"POST","parameters":[{"type":"integer","name":"customer_id","optional":false},{"type":"integer","name":"category","optional":true,"default":"*"}],"returns":["array","array","object","object"]},"getStockFigures":{"envelope":"JSON-RPC-1.0","transport":"POST","parameters":[{"type":"integer","name":"product_id","optional":false}],"returns":"integer"},"createBackOrder":{"envelope":"JSON-RPC-1.0","transport":"POST","parameters":[{"type":"integer","name":"product_id","optional":false},{"type":"integer","name":"quantity","optional":false}],"returns":"integer"},"getImageLocation":{"envelope":"JSON-RPC-1.0","transport":"POST","parameters":[{"type":"integer","name":"product_id","optional":false}],"returns":"string"},"createOrder":{"envelope":"JSON-RPC-1.0","transport":"POST","parameters":[{"type":"integer","name":"product_id","optional":false},{"type":"integer","name":"customer_id","optional":false},{"type":"integer","name":"productcnt","optional":false}],"returns":"integer"},"getOrderStatus":{"envelope":"JSON-RPC-1.0","transport":"POST","parameters":[{"type":"integer","name":"order_id","optional":false}],"returns":"string"}}}';
$client = new Zend_Http_Client('http://zfws/JSONRPC.php');
$response = $client->request('GET');
echo 'SMD: <pre>';
echo Zend_Json::prettyPrint($response->getBody());
echo '</pre>';