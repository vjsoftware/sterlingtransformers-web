<?php
set_include_path(implode(PATH_SEPARATOR, array(
    realpath('../library'),
    get_include_path(),
)));

require_once('Zend/Loader.php');
Zend_Loader::loadClass('Zend_Http_Client');
Zend_Loader::loadClass('Zend_Json');

$request = new stdClass();
$request->jsonrpc = '2.0';
$request->method = 'getStockFigures';
$request->params = array(array('apikey' => oon4Choh), array(1));
$request->id = time();

$json = Zend_Json::encode($request);

echo 'Request: <pre>';
echo Zend_Json::prettyPrint($json);
echo '</pre>';


$client = new Zend_Http_Client('http://zfws/JSONRPCtris.php');
$response = $client->setRawData($json, 'application/json')->request('POST');

echo 'Response: <pre>';
echo Zend_Json::prettyPrint($response->getBody());
echo '</pre>';