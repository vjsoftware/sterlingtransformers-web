<?php
class ExampleClass {

    public function __call($name, $arguments) {
        echo "We do not support {$name} in ".__CLASS__;
    }

}

$object = new ExampleClass();
$object->nonExistingMethod();
