<?php
error_reporting(E_ERROR);//we suppress warnings encountered by file_get_contents
                         //like when a 410 is received in response
echo '<p>1. get the list</p>';
$xmlstring = file_get_contents('http://zfws/restserver.php');
$xmlobj = simplexml_load_string($xmlstring);
echo '<pre>'.htmlentities($xmlobj->asXML()).'</pre>';//for debugging

echo '<p>2. get an existing item</p>';
$xmlstring = file_get_contents('http://zfws/restserver.php?id=1');
$responseheaders = explode(' ',$http_response_header[0]);
if ($responseheaders['1'] == '200') {
    $xmlobj = simplexml_load_string($xmlstring);
    echo '<pre>'.htmlentities($xmlobj->asXML()).'</pre>';//for debugging
} else {
    echo $http_response_header[0];
}

echo '<p>3. get a non-existing item</p>';
$xmlstring = file_get_contents('http://zfws/restserver.php?id=-1');
$responseheaders = explode(' ',$http_response_header[0]);
if ($responseheaders['1'] == '200') {
    $xmlobj = simplexml_load_string($xmlstring);
    echo '<pre>'.htmlentities($xmlobj->asXML()).'</pre>';//for debugging
} else {
    echo $http_response_header[0];
    $f=fopen('http://zfws/restserver.php?id=-1','rb');
    $data='';
    while(!feof($f))
        $data.=fread($f,$size);
    fclose($f);
    echo '<pre>'.htmlentities($data).'</pre>';//for debugging
}

echo '<p>4. get a response to a non-supported action</p>';
$options = array(
  'http'=>array(
    'method'=>"POST",
    'content'=>http_build_query(array('id'=> 123))
));
$context = stream_context_create($options);
$xmlstring = file_get_contents('http://zfws/restserver.php',false,$context);
$responseheaders = explode(' ',$http_response_header[0]);
if ($responseheaders['1'] == '2OO') {
    $xmlobj = simplexml_load_string($xmlstring);
    echo '<pre>'.htmlentities($xmlobj->asXML()).'</pre>';//for debugging
} else {
    echo $http_response_header[0];
}

