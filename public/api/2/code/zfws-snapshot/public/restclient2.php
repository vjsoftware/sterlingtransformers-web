<?php
error_reporting(E_ERROR);//we suppress warnings encountered by file_get_contents
                         //like when a 410 is received in response
echo '<p>1. get the list</p>';
$xmlstring = file_get_contents('http://zfws/restserver2.php');
$xmlobj = simplexml_load_string($xmlstring);
echo '<pre>'.htmlentities($xmlobj->asXML()).'</pre>';//for debugging

echo '<p>2. create a new item</p>';
echo '<p>order info submitted:</p>';
$msg = 'New order info submitted at '.date('Ymd H:i:s');
echo '<pre>'.$msg.'</pre>';
//build xml request
$xml = '<?xml version="1.0" encoding="UTF-8"?>';
$xml .= '<request>';
$xml .= '<data>'.$msg.'</data>';
$xml .= '</request>';

$options = array(
  'http'=>array(
    'method'=>"POST",
    'content'=> $xml
));
$context = stream_context_create($options);
$xmlstring = file_get_contents('http://zfws/restserver2.php',false,$context);
$responseheaders = explode(' ',$http_response_header[0]);
if ($responseheaders['1'] == '2OO') {
    $xmlobj = simplexml_load_string($xmlstring);
    echo '<pre>'.htmlentities($xmlobj->asXML()).'</pre>';//for debugging
} else {
    echo $http_response_header[0];
}

echo '<p>3. get the list again</p>';
$xmlstring = file_get_contents('http://zfws/restserver2.php');
$xmlobj = simplexml_load_string($xmlstring);
echo '<pre>'.htmlentities($xmlobj->asXML()).'</pre>';//for debugging


echo '<p>4. update order 1</p>';
echo '<p>order info submitted:</p>';
$msg = 'New info for order 1 submitted at '.date('Ymd H:i:s');
echo '<pre>'.$msg.'</pre>';
//build xml request
$xml = '<?xml version="1.0" encoding="UTF-8"?>';
$xml .= '<request>';
$xml .= '<data>'.$msg.'</data>';
$xml .= '</request>';

$options = array(
  'http'=>array(
    'method'=>"PUT",
    'content'=> $xml
));
$context = stream_context_create($options);
$xmlstring = file_get_contents('http://zfws/restserver2.php?id=1',false,$context);
$responseheaders = explode(' ',$http_response_header[0]);
if ($responseheaders['1'] == '2OO') {
    $xmlobj = simplexml_load_string($xmlstring);
    echo '<pre>'.htmlentities($xmlobj->asXML()).'</pre>';//for debugging
} else {
    echo $http_response_header[0];
}