<?php

// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
    realpath('../library'),
    get_include_path(),
)));

require_once('Zend/Loader.php');
Zend_Loader::loadClass('Zend_Rest_Client');
$client = new Zend_Rest_Client('http://zfws/REST.php');
$result = $client->getStockFigures(1)->get();
if ($result->isSuccess()) {
    echo $result;
}