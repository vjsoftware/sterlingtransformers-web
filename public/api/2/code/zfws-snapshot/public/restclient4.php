<?php

// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
    realpath('../library'),
    get_include_path(),
)));

require_once('Zend/Loader.php');
Zend_Loader::loadClass('Zend_Http_Client');

$xml  = '<order>';
$xml .= '<data>';
$xml .= '<productid>123456</productid>';
$xml .= '<productcnt>10</productcnt>';
$xml .= '</data>';
$xml .= '</order>';
$client = new Zend_Http_Client('http://zfws/restserver2.php');
$response = $client->setRawData($xml, 'text/xml')->request('POST');
echo '<pre>';
print_r($response);
echo '</pre>';