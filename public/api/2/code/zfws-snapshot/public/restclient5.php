<?php

// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
    realpath('../library'),
    get_include_path(),
)));

require_once('Zend/Loader.php');
Zend_Loader::loadClass('Zend_Http_Client');

$client = new Zend_Http_Client('http://zfws/rest/products/');
$response = $client->request();
echo $response->getMessage();
echo $response->getBody();