<?php

// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
    realpath('../library'),
    get_include_path(),
)));

require_once('Zend/Loader.php');
Zend_Loader::loadClass('Zend_Http_Client');

$xml  = '<order>';
$xml .= '<data>';
$array['customer_name'] = $xml->data->name;
$xml .= '<name>New customer today</name>';
$xml .= '<address>Street</address>';
$xml .= '<zip>1000</zip>';
$xml .= '<email>example@example.com</email>';
$xml .= '</data>';
$xml .= '</order>';
$client = new Zend_Http_Client('http://zfws/rest/customers');
$response = $client->setRawData($xml, 'text/xml')->request('POST');
echo '<pre>';
echo $response->getBody();
echo '</pre>';