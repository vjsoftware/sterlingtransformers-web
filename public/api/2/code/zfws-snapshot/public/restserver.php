<?php

function sendResponse($status,$payload) {
    $respcodes = array( 200 => 'OK',
                        201 => 'Created',
                        204 => 'No Content',
                        301 => 'Moved permanantly',
                        400 => 'Bad Request',
                        404 => 'Not found',
                        410 => 'Gone',
                        501 => 'Not implemented'
                        );
    header('HTTP/1.1 ' . $status . ' ' . $respcodes[$status]);  
    header('Content-type: text/xml');
    $xml = '<?xml version="1.0" encoding="UTF-8"?>';
    $xml .= '<response>';
    $xml .= '<code>'.$status.'</code>';
    $xml .= '<message>'.$respcodes[$status].'</message>';
    if (!empty($payload)) {
        $xml .= '<data>';
        if (is_array($payload)) {
            foreach($payload as $item) {
                $xml .= '<item>'.(string) $item.'</item>';
            }
        } else {
            $xml .= (string) $payload;
        }
        $xml .= '</data>';
    }
    $xml .= '</response>';
    echo $xml;
}
$data = NULL;
switch($_SERVER['REQUEST_METHOD']) {
    case 'GET':
        $type = 'GET';
        $data = $_GET;
        if (isset($_GET['id'])) {
            $id = $_GET['id'];
        }
        break;
    case 'POST':
    case 'PUT':
    case 'DELETE':
        $data = file_get_contents('php://input');
        
        if (isset($_GET['id'])) {
            $id = $_GET['id'];
        }
        $type = $_SERVER['REQUEST_METHOD'];
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            if (isset($_GET['_method'])) {
                switch($_GET['_method']) {
                    case 'PUT':
                    case 'DELETE':
                        $type = $_GET['_method'];
                    default:
                        $type = 'UNKNOWN';
                }
            }
        }
        break;
    default:
        $type = 'UNKNOWN';
        break;
}
$filedir = '../data/orders/';
switch ($type) {
    case 'GET':
        if (is_numeric($id)) {
            $file = $filedir.'ORDER_'.(int) $id.'.txt';
            if (file_exists($file)) {
                echo sendResponse(200,file_get_contents($file));
            } else {
                echo sendResponse(410,'');
            }
        } else {
            $listing = scandir($filedir);
            $list = array();
            foreach($listing as $file) {
                if ($file !== '.' && $file !== '..' && $file !== '.svn') {
                    $list[] = basename($file,'.txt');
                }
            }
            echo sendResponse(200,$list);
        }
        break;
    case 'POST':
        
    case 'DELETE':
    case 'PUT':

    case 'UNKNOWN':
        echo sendResponse(501,'');//not yet implemented
        break;

}


