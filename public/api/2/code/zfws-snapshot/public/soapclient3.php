<?php
class Order {

    public $productid;
    public $customerid;
    public $productcnt;

    public function __construct($productid,$customerid,$productcnt) {
        $this->productid  = $productid;
        $this->customerid = $customerid;
        $this->productcnt = $productcnt;
    }
}

class Orders {

    /** @var Order[] */
    public $orders;


}

$order1 = new Order(1,2,3);
$order2 = new Order(4,5,6);
$orders = new Orders();
$orders->orders = array($order1,$order2);
try {
    $classmap = array('Orders' => 'Orders','Order' => 'Order');
    $client = new SoapClient('http://zfws/soapserver3.wsdl',array('classmap' => $classmap));
    echo $client->placeOrders($orders);
} catch (Exception $e) {
    echo $e->getMessage();
}
