<?php
function someMathConstant($constant) {
  switch($constant) {
    case 'pi':
       return '3.14159265';
       break;
    case 'e':
       return 'be rational';
       break;
    default:
       return 'unknown variable';
       break;
  }
}

//or - for the example using a class:
class someMath {

    public function someMathConstant($constant) {
        switch($constant) {
            case 'pi':
               return '3.14159265 (from class)';
               break;
            case 'e':
               return 'be rational (from class)';
               break;
            default:
               return 'unknown variable (from class)';
               break;
        }
    }

}


$server = new SoapServer(null, array('uri' => 'http://zfws/','soap_version'   => SOAP_1_2));
$server->addFunction("someMathConstant");
//for the example using a class, comment the line above and uncomment the line below
//$server->setClass('someMath');
$server->handle();