<?php

class Order {

    /** @var string */
    public $productid;
    /** @var string */
    public $customerid;
    /** @var int */
    public $productcnt;

    public function __construct($productid,$customerid,$productcnt) {
        $this->productid  = $productid;
        $this->customerid = $customerid;
        $this->productcnt = $productcnt;
    }
}

class Orders {

    /** @var Order[] */
    public $orders;
    
}

class Backend {

    /**
     * @param Orders $orders
     * @return string
     */
    public function placeOrders($orders) {
        return print_r($orders,1);
    }


}


$classmap = array('Orders' => 'Orders','Order' => 'Order');
$server = new SoapServer('soapserver3.wsdl',array('classmap' => $classmap));
$server->setClass("Backend");
$server->handle();