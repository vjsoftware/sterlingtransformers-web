<?php

class Order {

    /** @var string */
    public $productid;
    /** @var string */
    public $customerid;
    /** @var int */
    public $productcnt;

    public function __construct($productid,$customerid,$productcnt) {
        $this->productid  = $productid;
        $this->customerid = $customerid;
        $this->productcnt = $productcnt;
    }
}

class Orders {

    /** @var Order[] */
    public $orders;


}

class Backend {

    /**
     * @param Orders $orders
     * @return string
     */
    public function placeOrders($orders) {
        return print_r($orders,1);
    }


}


// Define path to application directory
defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../application'));

// Define application environment
defined('APPLICATION_ENV')
    || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));

// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
    realpath(APPLICATION_PATH . '/../library'),
    get_include_path(),
)));

/** Zend_Application */
require_once 'Zend/Application.php';

// Create application, bootstrap, and run
$application = new Zend_Application(
    APPLICATION_ENV,
    APPLICATION_PATH . '/configs/application.ini'
);

$autodiscover = new Zend_Soap_AutoDiscover('Zend_Soap_Wsdl_Strategy_ArrayOfTypeComplex');

$autodiscover->setClass('Backend');
$autodiscover->setUri('http://zfws/soapserver3.php');
$autodiscover->handle();