<?php
//error_reporting(E_ALL);//uncomment and xu_rpc_* related notices will appear
include("xmlrpcutils/utils.php");
$result = xu_rpc_http_concise(
                                array(
                                    'method' => 'constant2',
                                    'args'   => 'pi',
                                    'host'   => 'zfws',
                                    'uri'    => '/xmlrpcserver.php',
                                    'port'   => 80
                                )
                            );
if (xmlrpc_is_fault($result)) {
    echo 'Fault: ['.$result['faultCode'].'] '.$result['faultString'];
} else {
    echo '<pre>'.$result.'</pre>';
}


set_include_path(implode(PATH_SEPARATOR, array(
    realpath('../library'),
    get_include_path(),
)));

require_once('Zend/Loader.php');
Zend_Loader::loadClass('Zend_Http_Client');
$xml = '<?xml version="1.0" encoding="iso-8859-1"?>';
$xml .= '<methodCall>';
$xml .= '<methodName>constant</methodName>';
$xml .= '<params><param><value><string>pi</string></value></param></params>';
$xml .= '</methodCall>';
$client = new Zend_Http_Client('http://zfws/xmlrpcserver.php');
$response = $client->setRawData($xml, 'text/xml')->request('POST');
echo '<pre>';
echo htmlentities($response->getBody());
echo '</pre>';