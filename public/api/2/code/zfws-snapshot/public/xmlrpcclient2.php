<?php

set_include_path(implode(PATH_SEPARATOR, array(
    realpath('../library'),
    get_include_path(),
)));

require_once('Zend/Loader.php');
Zend_Loader::loadClass('Zend_XmlRpc_Client');
$client = new Zend_XmlRpc_Client('http://zfws/XMLRPC.php');


$result = $client->call('system.listMethods');
echo '<pre>';
print_r($result);
echo '</pre>';

$result = $client->call('zfws.getStockFigures',array(1));
echo '<pre>';
print_r($result);
echo '</pre>';

$result = $client->getProxy()->system->listMethods();
echo '<pre>';
print_r($result);
echo '</pre>';

$result = $client->getProxy()->zfws->getStockFigures(1);
echo '<pre>';
print_r($result);
echo '</pre>';

$request = new Zend_XmlRpc_Request();
$request->setMethod('zfws.getStockFigures');
$request->setParams(array(1));
$result = $client->doRequest($request);
echo '<pre>';
print_r($client->getLastRequest());
echo '</pre>';
echo '<pre>';
print_r($client->getLastResponse());
echo '</pre>';

$result = $client->getProxy()->system->methodHelp('zfws.getStockFigures');
echo '<pre>';
print_r($result);
echo '</pre>';

$result = $client->getProxy()->system->methodSignature('zfws.getStockFigures');
echo '<pre>';
print_r($result);
echo '</pre>';

$result = $client->getProxy()->zfws->getOrderStatus('');//this is casted to 1
echo '<pre>';
print_r($result);
echo '</pre>';

try {
    $client = new Zend_XmlRpc_Client('http://zfws/XMLRPC.php');
    $result = $client->call('system.listMethods');
} catch (Zend_XmlRpc_Client_FaultException $e) {
    echo 'Zend_XmlRpc_Client_FaultException: ['.$e->getCode().'] '.$e->getMessage();
} catch (Zend_XmlRpc_Client_HttpException $e) {
    echo 'Zend_XmlRpc_Client_HttpException: ['.$e->getCode().'] '.$e->getMessage();
} catch (Zend_Http_Client_Adapter_Exception $e) {
    echo 'Zend_Http_Client_Adapter_Exception: ['.$e->getCode().'] '.$e->getMessage();
}


$result = $client->call('system.multicall',
                        array(
                            array(
                                array(
                                     'methodName'   => 'zfws.getStockFigures',
                                     'params'      => array(1)
                                     ),
                                array(
                                     'methodName'   => 'zfws.getOrderStatus',
                                     'params'      => array(1)
                                     )
                            )
                        )
        );
echo '<pre>';
print_r($result);
echo '</pre>';

try {
    $result = $client->getProxy()->zfws->getStockFigures(0);
    echo '<pre>';
    print_r($result);
    echo '</pre>';
} catch (Exception $e) {
    echo get_class($e).' ['.$e->getCode().'] '.$e->getMessage();
}