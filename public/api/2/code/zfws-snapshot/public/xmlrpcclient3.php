<?php

set_include_path(implode(PATH_SEPARATOR, array(
    realpath('../library'),
    get_include_path(),
)));

require_once('Zend/Loader.php');
Zend_Loader::loadClass('Zend_Http_Client');

$xml = '<?xml version="1.0" encoding="UTF-8"?>';
$xml .= '<methodCall>';
$xml .= '<methodName>zfws.getStockFigures</methodName>';
$xml .= '<params>';
$xml .= '<param><value>oon4Choh</value></param>';
$xml .= '<param><value><int>1</int></value></param>';
$xml .= '</params>';
$xml .= '</methodCall>';
$client = new Zend_Http_Client('http://zfws/XMLRPCbis.php');
$response = $client->setRawData($xml, 'text/xml')->request('POST');
echo '<pre>';
echo $response->getBody();
echo '</pre>';

