<?php
function someMathConstant($methodname,$params,$additional) {
  //return func_get_args();//for debugging
  $constant = NULL;
  if ($params[0]) {
      $constant = $params[0];
  }
  switch($constant) {
    case 'pi':
       return '3.14159265';
       break;
       return 'unknown variable';
       break;
  }
}

//or - for the example using a class:
class someMath {
    public function someMathConstant($methodname,$params,$additional) {
        //return func_get_args();//for debugging
        $constant = NULL;
        if ($params[0]) {
            $constant = $params[0];
        }
        switch($constant) {
            case 'pi':
               return '3.14159265 (from class)';
               break;
            case 'e':
               return 'be rational (from class)';
               break;
            default:
               return 'unknown variable (from class)';
               break;
        }
    }

}

$xmlrpcserver = xmlrpc_server_create();
xmlrpc_server_register_method($xmlrpcserver, 'constant', 'someMathConstant');
//for the example using a class, comment the line above and uncomment the 2 lines below
//$math = new someMath();
//xmlrpc_server_register_method($xmlrpcserver, "constant", array(&$math, "someMathConstant"));
$request = file_get_contents('php://input');
$response = xmlrpc_server_call_method($xmlrpcserver, $request, '');
print trim($response);//added trim, to avoid a newline
xmlrpc_server_destroy($xmlrpcserver);
