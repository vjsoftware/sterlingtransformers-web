#!/usr/bin/env php
<?php
$string = '<?xml version="1.0" encoding="UTF-8"?>';
$string .= '<request><sometag>value1</sometag><othertag>value2</othertag></request>';
$handle = curl_init('http://zfws/SOAP.php');

curl_setopt($handle, CURLOPT_POST, 1);
curl_setopt($handle, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($handle, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
curl_setopt($handle, CURLOPT_POSTFIELDS, $string);

$data = curl_exec($handle);
curl_close($handle);
echo $data;
