#!/usr/bin/env php
<?php
//test stream wrappers
$url = 'http://zfws/authtest.php';
$auth = base64_encode('zfws:zfws');

$options = array( 'http' => array
                           (
                            'method' => 'GET',
                            'header' => array("Authorization: Basic $auth")
                           )
                    );
$context = stream_context_create($options);
$result = file_get_contents($url,false,$context);

echo $result.PHP_EOL;
