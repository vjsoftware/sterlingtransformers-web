
 CREATE TABLE `acl_roles` (
`role_id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`role_name` VARCHAR( 64 ) NOT NULL
) ENGINE = InnoDB;

INSERT INTO acl_roles VALUES (1,'anonymous');
INSERT INTO acl_roles VALUES (2,'customer');
INSERT INTO acl_roles VALUES (3,'admin');
INSERT INTO acl_roles VALUES (4,'partner');
INSERT INTO acl_roles VALUES (5,'wholesale partner');
INSERT INTO acl_roles VALUES (6,'webshop');

 CREATE TABLE `acl_resources` (
`resource_id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`resource_parent` INT NULL ,
`resource_name` VARCHAR( 255 ) NOT NULL
) ENGINE = InnoDB;

INSERT INTO acl_resources VALUES (1,NULL,'api');
INSERT INTO acl_resources VALUES (2,NULL,'reports');
INSERT INTO acl_resources VALUES (3,NULL,'admin');
INSERT INTO acl_resources VALUES (4,1,'soap');
INSERT INTO acl_resources VALUES (5,1,'rest');
INSERT INTO acl_resources VALUES (6,1,'xmlrpc');
INSERT INTO acl_resources VALUES (7,1,'jsonrpc');

INSERT INTO acl_resources VALUES (10,NULL,'data');
INSERT INTO acl_resources VALUES (11,10,'orders');
INSERT INTO acl_resources VALUES (12,10,'invoices');
INSERT INTO acl_resources VALUES (13,10,'products');
INSERT INTO acl_resources VALUES (14,10,'stock');
INSERT INTO acl_resources VALUES (15,10,'shipping');

 CREATE TABLE `acl_privileges` (
`privilege_id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`privilege_name` VARCHAR( 64 ) NOT NULL
) ENGINE = InnoDB;

INSERT INTO acl_privileges VALUES (1,'use');
INSERT INTO acl_privileges VALUES (2,'create');
INSERT INTO acl_privileges VALUES (3,'read');
INSERT INTO acl_privileges VALUES (4,'update');
INSERT INTO acl_privileges VALUES (5,'delete');

CREATE TABLE `acl_rules` (
`rule_id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`rule_role_id` INT NULL ,
`rule_resource_id` INT NULL ,
`rule_privilege_id` INT NULL ,
`rule_allow` TINYINT( 1 ) NOT NULL DEFAULT '1'
) ENGINE = InnoDB;

-- RULE ROLE RESOURCE PRIVILEGE ALLOW

-- deny access to anonymous on reports, api and admin
INSERT INTO acl_rules VALUES ('',1,1,NULL,0);
INSERT INTO acl_rules VALUES ('',1,2,NULL,0);
INSERT INTO acl_rules VALUES ('',1,3,NULL,0);

-- allow customer use of api and data
-- allow customer to create an order
INSERT INTO acl_rules VALUES ('',2,1,1,1);
INSERT INTO acl_rules VALUES ('',2,10,1,1);
INSERT INTO acl_rules VALUES ('',2,11,2,1);

-- allow admin use of admin, api and data
INSERT INTO acl_rules VALUES ('',3,3,1,1);
INSERT INTO acl_rules VALUES ('',3,1,1,1);
INSERT INTO acl_rules VALUES ('',3,10,1,1);


-- RULE ROLE RESOURCE PRIVILEGE ALLOW


-- allow a partner the use of api and data
-- allow a partner to read stock
-- allow a partner to create and read orders
-- allow a partner to read the products
INSERT INTO acl_rules VALUES ('',4,1,1,1);
INSERT INTO acl_rules VALUES ('',4,10,1,1);
INSERT INTO acl_rules VALUES ('',4,14,3,1);
INSERT INTO acl_rules VALUES ('',4,11,2,1);
INSERT INTO acl_rules VALUES ('',4,11,3,1);
INSERT INTO acl_rules VALUES ('',4,13,3,1);

-- allow a wholesale partner the use of api and data
-- allow a wholesale partner to read stock
-- allow a wholesale partner to create,read,update and delete orders
-- allow a wholesale partner to read the products
INSERT INTO acl_rules VALUES ('',5,1,1,1);
INSERT INTO acl_rules VALUES ('',5,10,1,1);
INSERT INTO acl_rules VALUES ('',5,14,3,1);
INSERT INTO acl_rules VALUES ('',5,11,2,1);
INSERT INTO acl_rules VALUES ('',5,11,3,1);
INSERT INTO acl_rules VALUES ('',5,11,4,1);
INSERT INTO acl_rules VALUES ('',5,11,5,1);
INSERT INTO acl_rules VALUES ('',5,13,3,1);

-- RULE ROLE RESOURCE PRIVILEGE ALLOW

-- allow the webshop the use of api and data
-- allow the webshop to read stock
-- allow the webshop to create,read,update and delete orders
-- allow the webshop to read the products
-- allow the webshop to create and read invoices
-- allow the webshop to read shipping info
INSERT INTO acl_rules VALUES ('',6,1,1,1);
INSERT INTO acl_rules VALUES ('',6,10,1,1);
INSERT INTO acl_rules VALUES ('',6,14,3,1);
INSERT INTO acl_rules VALUES ('',6,11,2,1);
INSERT INTO acl_rules VALUES ('',6,11,3,1);
INSERT INTO acl_rules VALUES ('',6,11,4,1);
INSERT INTO acl_rules VALUES ('',6,11,5,1);
INSERT INTO acl_rules VALUES ('',6,13,3,1);
INSERT INTO acl_rules VALUES ('',6,12,2,1);
INSERT INTO acl_rules VALUES ('',6,12,3,1);
INSERT INTO acl_rules VALUES ('',6,15,3,1);


-- customer can use the soap and rest service only
-- all roles except anonymous can use any web service
INSERT INTO acl_rules VALUES ('',2,6,1,0);
INSERT INTO acl_rules VALUES ('',2,7,1,0);
-- the rest of the 'use' priv is inherited from the 'api' resource


-- table linking RPC methods with a resource and
-- privilege
 CREATE TABLE `acl_methods` (
`method_name` VARCHAR( 128 ) NOT NULL ,
`method_resource_id` INT NOT NULL ,
`method_privilege_id` INT NOT NULL ,
PRIMARY KEY ( `method_name` )
) ENGINE = InnoDB;

INSERT INTO acl_methods VALUES ('getcatalogue',13,3);
INSERT INTO acl_methods VALUES ('getstockfigures',14,3);
INSERT INTO acl_methods VALUES ('createbackorder',11,2);
INSERT INTO acl_methods VALUES ('getimagelocation',13,3);
INSERT INTO acl_methods VALUES ('createorder',11,2);
INSERT INTO acl_methods VALUES ('updateorder',11,4);
INSERT INTO acl_methods VALUES ('deleteorder',11,5);
INSERT INTO acl_methods VALUES ('getorderstatus',11,3);
INSERT INTO acl_methods VALUES ('geteinvoice',12,3);
INSERT INTO acl_methods VALUES ('createinvoice',12,2);
INSERT INTO acl_methods VALUES ('updateinvoice',12,4);
INSERT INTO acl_methods VALUES ('deleteinvoice',12,5);


 CREATE TABLE `acl_apikeys` (
`apikey_key` VARCHAR( 128 ) NOT NULL ,
`apikey_consumer_id` INT NOT NULL ,
`apikey_role_id` INT NOT NULL ,
PRIMARY KEY ( `apikey_key` )
) ENGINE = InnoDB;

-- customer role
INSERT INTO acl_apikeys VALUES ('yoh7Ixoh',1234,2);
-- admin role
INSERT INTO acl_apikeys VALUES ('in4Gah3T',4567,3);
-- partner role
INSERT INTO acl_apikeys VALUES ('oon4Choh',8910,4);
-- wholesale partner role
INSERT INTO acl_apikeys VALUES ('fihaig2I',1112,5);
-- webshop role
INSERT INTO acl_apikeys VALUES ('cuo4sohD',1314,6);

