SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

CREATE SCHEMA IF NOT EXISTS `zfws` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE `zfws`;

-- -----------------------------------------------------
-- Table `zfws`.`categories`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `zfws`.`categories` (
  `category_id` INT NOT NULL ,
  `category_parent_id` INT NULL ,
  `category_name` VARCHAR(128) NULL ,
  PRIMARY KEY (`category_id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `zfws`.`products`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `zfws`.`products` (
  `product_id` INT NOT NULL ,
  `product_code` VARCHAR(255) NULL ,
  `product_name` VARCHAR(255) NULL ,
  `product_category_id` INT NULL ,
  `product_image_id` VARCHAR(45) NULL ,
  PRIMARY KEY (`product_id`) ,
  INDEX `fk_products_1` (`product_category_id` ASC) ,
  CONSTRAINT `fk_products_1`
    FOREIGN KEY (`product_category_id` )
    REFERENCES `zfws`.`categories` (`category_id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `zfws`.`tags`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `zfws`.`tags` (
  `tag_label` VARCHAR(128) NOT NULL ,
  `tag_product_id` INT NULL ,
  UNIQUE KEY `unique` (`tag_label`,`tag_product_id`),
  INDEX `fk_tags_1` (`tag_product_id` ASC) ,
  CONSTRAINT `fk_tags_1`
    FOREIGN KEY (`tag_product_id`)
    REFERENCES `zfws`.`products` (`product_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `zfws`.`stock`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `zfws`.`stock` (
  `stock_product_id` INT NOT NULL ,
  `stock_count` INT NULL ,
  PRIMARY KEY (`stock_product_id`) ,
  INDEX `fk_stock_1` (`stock_product_id` ASC) ,
  CONSTRAINT `fk_stock_1`
    FOREIGN KEY (`stock_product_id` )
    REFERENCES `zfws`.`products` (`product_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `zfws`.`customers`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `zfws`.`customers` (
  `customer_id` INT NOT NULL ,
  `customer_name` VARCHAR(128) NULL ,
  `customer_address` VARCHAR(255) NULL ,
  `customer_zip` VARCHAR(12) NULL ,
  `customer_email` VARCHAR(255) NULL ,
  PRIMARY KEY (`customer_id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `zfws`.`orders`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `zfws`.`orders` (
  `order_id` INT NOT NULL ,
  `order_customer_id` INT NULL ,
  PRIMARY KEY (`order_id`) ,
  INDEX `fk_orders_1` (`order_customer_id` ASC) ,
  CONSTRAINT `fk_orders_1`
    FOREIGN KEY (`order_customer_id`)
    REFERENCES `zfws`.`customers` (`customer_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `zfws`.`shippings`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `zfws`.`shippings` (
  `shipping_id` INT NOT NULL ,
  `shipping_cost` INT NULL ,
  `shipping_name` VARCHAR(45) NULL ,
  PRIMARY KEY (`shipping_id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `zfws`.`invoices`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `zfws`.`invoices` (
  `invoice_id` INT NOT NULL ,
  `invoice_amount` INT NULL ,
  `invoice_desc` VARCHAR(255) NULL ,
  `invoice_shipping_id` INT NULL ,
  `invoice_customer_id` INT NULL ,
  PRIMARY KEY (`invoice_id`) ,
  INDEX `fk_invoices_1` (`invoice_customer_id` ASC) ,
  INDEX `fk_invoices_2` (`invoice_shipping_id` ASC) ,
  CONSTRAINT `fk_invoices_1`
    FOREIGN KEY (`invoice_customer_id`)
    REFERENCES `zfws`.`customers` (`customer_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_invoices_2`
    FOREIGN KEY (`invoice_shipping_id`)
    REFERENCES `zfws`.`shippings` (`shipping_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `zfws`.`order_x_invoice`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `zfws`.`order_x_invoice` (
  `order_id` INT NULL ,
  `invoice_id` INT NULL ,
  INDEX `fk_order_x_invoice_1` (`order_id` ASC) ,
  INDEX `fk_order_x_invoice_2` (`invoice_id` ASC) ,
  CONSTRAINT `fk_order_x_invoice_1`
    FOREIGN KEY (`order_id`)
    REFERENCES `zfws`.`orders` (`order_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_order_x_invoice_2`
    FOREIGN KEY (`invoice_id`)
    REFERENCES `zfws`.`invoices` (`invoice_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `zfws`.`order_x_product`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `zfws`.`order_x_product` (
  `order_id` INT NULL ,
  `product_id` INT NULL ,
  `product_count` INT NULL ,
  INDEX `fk_order_x_product_1` (`order_id` ASC) ,
  INDEX `fk_order_x_product_2` (`product_id` ASC) ,
  CONSTRAINT `fk_order_x_product_1`
    FOREIGN KEY (`order_id`)
    REFERENCES `zfws`.`orders` (`order_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_order_x_product_2`
    FOREIGN KEY (`product_id`)
    REFERENCES `zfws`.`products` (`product_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

ALTER TABLE `categories` CHANGE `category_id` `category_id` INT( 11 ) NOT NULL AUTO_INCREMENT;
ALTER TABLE `customers` CHANGE `customer_id` `customer_id` INT( 11 ) NOT NULL AUTO_INCREMENT;
ALTER TABLE `products` CHANGE `product_id` `product_id` INT( 11 ) NOT NULL AUTO_INCREMENT;
ALTER TABLE `orders` CHANGE `order_id` `order_id` INT( 11 ) NOT NULL AUTO_INCREMENT;
ALTER TABLE `invoices` CHANGE `invoice_id` `invoice_id` INT( 11 ) NOT NULL AUTO_INCREMENT;
ALTER TABLE `shippings` CHANGE `shipping_id` `shipping_id` INT( 11 ) NOT NULL AUTO_INCREMENT;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
