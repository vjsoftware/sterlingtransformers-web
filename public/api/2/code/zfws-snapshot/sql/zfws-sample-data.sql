

use zfws;

INSERT INTO `zfws`.`categories` (`category_id`, `category_parent_id`, `category_name`) VALUES
(1, NULL, 'Wooden toys'),
(2, 1, 'Animals'),
(3, NULL, 'Electric toys'),
(4, 3, 'Cars');


INSERT INTO `zfws`.`products` (`product_id`, `product_code`, `product_name`, `product_category_id`, `product_image_id`) VALUES
(1, 'WD-GIRAFE1', 'Nice wooden girafe', 2, NULL),
(2, 'EL-RACER1', 'Electric race car', 4, NULL),
(3, 'WD-ELEPH1', 'Big elephant', 2, NULL),
(4, 'WD-RHINO1', 'Small rhino', 2, NULL);

INSERT INTO `zfws`.`tags` (`tag_label`, `tag_product_id`) VALUES
('eco', 1),
('eco', 3),
('eco', 4),
('rc', 2);


INSERT INTO `zfws`.`stock` (`stock_product_id` ,`stock_count`) VALUES ('1', '120'), ('2', '66'), ('3', '21'), ('4', '245');

INSERT INTO `zfws`.`customers` (
`customer_id` ,
`customer_name` ,
`customer_address` ,
`customer_zip` ,
`customer_email`
)
VALUES (
NULL , 'Jan Janssens', 'Main street 5', '8888', 'jan@example.com'
), (
NULL , 'Peter Peeters', 'Small street 22', '6666', 'peter@example.com'
);

INSERT INTO `zfws`.`shippings` (
`shipping_id` ,
`shipping_cost` ,
`shipping_name`
)
VALUES (
NULL , '5', 'Postal'
), (
NULL , '20', 'Express'
);

 INSERT INTO `zfws`.`invoices` (
`invoice_id` ,
`invoice_amount` ,
`invoice_desc` ,
`invoice_shipping_id` ,
`invoice_customer_id`
)
VALUES (
NULL , '200', '2 girafes', '1', '1'
), (
NULL , '300', '1 rc car', '2', '2'
);

INSERT INTO `zfws`.`orders` (
`order_id` ,
`order_customer_id`
)
VALUES (
NULL , '1'
), (
NULL , '2'
);

 INSERT INTO `zfws`.`order_x_invoice` (
`order_id` ,
`invoice_id`
)
VALUES (
'1', '1'
), (
'2', '2'
);

 INSERT INTO `zfws`.`order_x_product` (
`order_id` ,
`product_id` ,
`product_count`
)
VALUES (
'1', '1', '2'
), (
'2', '2', '1'
);

