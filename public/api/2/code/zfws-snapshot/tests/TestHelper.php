<?php
ob_start();
if (!defined('BASE_PATH')) {
    define('BASE_PATH', realpath(dirname(__FILE__) . '/../'));
}
define('APPLICATION_PATH', BASE_PATH . '/application');

//Include path
set_include_path(
    '.'
    . PATH_SEPARATOR . BASE_PATH . '/library'
    . PATH_SEPARATOR . APPLICATION_PATH . '/models'
    . PATH_SEPARATOR . get_include_path()
);

// Define application environment
define('APPLICATION_ENV', 'testing');

//require_once 'Zend/Session.php';
//Zend_Session::$_unitTestEnabled = true;

//register our own autoloading function
function _zfwsAutoload($path) {
    include str_replace('_','/',$path) . '.php';
    return $path;
}
require_once 'Zend/Loader/Autoloader.php';

$autoloader = Zend_Loader_Autoloader::getInstance();
$autoloader->setDefaultAutoloader('_zfwsAutoload');

$application = new Zend_Application(
            APPLICATION_ENV,
            APPLICATION_PATH . '/configs/application.ini'
        );
$application->getBootstrap()->bootstrap();
$frontController = Zend_Controller_Front::getInstance();
$frontController->setControllerDirectory(APPLICATION_PATH . '/controllers','default');
$frontController->addModuleDirectory(APPLICATION_PATH . '/modules');
$frontController->setDefaultModule('default');


