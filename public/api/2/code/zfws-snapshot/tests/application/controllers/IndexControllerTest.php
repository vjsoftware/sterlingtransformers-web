<?php

class IndexControllerTest extends Zend_Test_PHPUnit_ControllerTestCase // PHPUnit_Framework_TestCase
{

    public function setUp()
    {
         
    }

    public function testIndexAction() {
        $this->dispatch('/');
        $this->assertModule('default');
        $this->assertController('index');
        $this->assertAction('index');
    }

    
    public function tearDown()
    {
        /* Tear Down Routine */
    }

    
}

