<?php

class RESTTest extends PHPUnit_Framework_TestCase {

    public function testClientCall() {

        $client = new Zend_Http_Client('http://zfws/rest/orders/apikey/');
        $response = $client->request();
        //echo $response->getBody();
        $this->assertEquals(403,$response->getStatus());


        //key should probably be a test key
        $client = new Zend_Http_Client('http://zfws/rest/orders/apikey/oon4Choh');
        $response = $client->request();
        //echo $response->getBody();
        $this->assertEquals(200,$response->getStatus());

    }
   
}
?>
