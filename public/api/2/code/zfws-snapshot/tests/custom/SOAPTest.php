<?php

class SOAPTest extends PHPUnit_Framework_TestCase {

    public function testClientCall() {

        $clientMock = $this->getMockFromWsdl('http://zfws/SOAP.php?WSDL=1',
                                             'MTA');

        $result = '321';
        $clientMock->expects($this->any())
                   ->method('getStockFigures')
                   ->will($this->returnValue($result));

        $this->assertEquals($result,$clientMock->getStockFigures('oon4Choh',1));
    }

    public function testServerSetupAndHandle() {
        $options = array('soap_version' => SOAP_1_2);
        $server = new Zend_Soap_Server('http://zfws/SOAPbis.php?WSDL=1', $options);
        $server->setClass('Mta_Servicewrapper');

        $request = <<<END
<soapenv:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:soap="http://zfws/SOAPbis.php">
   <soapenv:Header/>
   <soapenv:Body>
      <soap:getStockFigures soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
         <apikey xsi:type="xsd:string">oon4Choh</apikey>
         <product_id xsi:type="xsd:int">1</product_id>
      </soap:getStockFigures>
   </soapenv:Body>
</soapenv:Envelope>
END;
        $server->setReturnResponse(true);
        $result = $server->handle($request);

        $expected = <<<END
<SOAP-ENV:Envelope SOAP-ENV:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns1="http://zfws/SOAPbis.php" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/">
   <SOAP-ENV:Body>
      <ns1:getStockFiguresResponse>
         <return xsi:type="xsd:int">111</return>
      </ns1:getStockFiguresResponse>
   </SOAP-ENV:Body>
</SOAP-ENV:Envelope>
END;
        $resultdom = new DOMDocument();
        $resultdom->loadXML($result);

        $expecteddom = new DOMDocument();
        $expecteddom->loadXML($expected);

        $this->assertEqualXMLStructure($resultdom,$expecteddom);

    }
    
}
?>
