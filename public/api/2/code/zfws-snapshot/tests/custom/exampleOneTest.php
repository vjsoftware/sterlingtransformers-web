<?php

function someMathConstant($constant) {
  switch($constant) {
    case 'pi':
       return '3.14159265';
       break;
    case 'e':
       return 'be rational';
       break;
    default:
       return 'unknown variable';
       break;
  }
}

class ExampleOneTest extends PHPUnit_Framework_TestCase {

    public function testSomeMathConstant() {
        $this->assertEquals('3.14159265', someMathConstant('pi'));
        $this->assertEquals('be rational', someMathConstant('e'));
        $this->assertEquals('unknown variable', someMathConstant(''));
    }
}
?>
