<?php

class ExampleTwoTest extends PHPUnit_Framework_TestCase {

    public function testStubService() {

        //create a stub for the Mta_Service class
        $stub = $this->getMock('Mta_Service');

        //configure
        $stub->expects($this->any())
             ->method('getStockFigures')
             ->will($this->returnValue(10));

        //check assertions
        $this->assertType(PHPUnit_Framework_Constraint_IsType::TYPE_INT,
                         $stub->getStockFigures(1));

        $this->assertEquals(10,$stub->getStockFigures(1));
    }

    public function testMockService() {

        //create a mock Mta_Model_Stock
        $mock = $this->getMock('Mta_Model_Stock',array('getAvailable'));

        //configure mock
        $mock->expects($this->once())
             ->method('getAvailable')
             ->with($this->equalTo('1'));

        //create an instance of Mta_Service
        $service = new Mta_Service();
        
        //pass the mock as Mta_Model_Stock
        $service->__setModel($mock,'Mta_Model_Stock');

        $service->getStockFigures(1);
        
    }
    
}
?>
