<?php

require_once __DIR__ . '/vendor/autoload.php';

error_reporting(E_ALL);

class SAP_PreData
{
    function insertValue($value = '') {
            if ($value == '' || $value == null) {
                $value = '""';
            } else {
                $value = (string) "'$value'";
            }
            // die($value);
            return $value;
        }

        function get_client_ip() {
    $ipaddress = '';
    if (getenv('HTTP_CLIENT_IP'))
        $ipaddress = getenv('HTTP_CLIENT_IP');
    else if(getenv('HTTP_X_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
    else if(getenv('HTTP_X_FORWARDED'))
        $ipaddress = getenv('HTTP_X_FORWARDED');
    else if(getenv('HTTP_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_FORWARDED_FOR');
    else if(getenv('HTTP_FORWARDED'))
      $ipaddress = getenv('HTTP_FORWARDED');
    else if(getenv('REMOTE_ADDR'))
        $ipaddress = getenv('REMOTE_ADDR');
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
}

    // function log(){
    //     $ipaddress = $this->get_client_ip();
    //  $newLog = "INSERT INTO `sap_log` (ip) VALUES($ipaddress)";
    //  $result = $conn->query($newLog);
    // }



    /**
     * SAP_RequestParams
     *
     * @param string $subDivisionCode
     * @param string $subDivisionName
     * @param string $MRU
     * @param string $MR_DOCNumber
     * @param string $neighbourMeterId
     * @param string $connectedPoleNINNumber
     * @param string $consumerLegacyNumber
     * @param string $contractAcNumber
     * @param string $consumerName
     * @param string $houseNumber
     * @param string $streetNumber
     * @param string $address
     * @param string $category
     * @param string $subCategory
     * @param string $moveInFlag
     * @param string $categoryChangeFlag
     * @param string $categoryChangeDate
     * @param string $oldCategory
     * @param string $oldSubCategory
     * @param string $industryCode
     * @param string $billingCycle
     * @param string $billingCycleType
     * @param string $billingGroup
     * @param string $triVectorMeterFlag
     * @param string $installation
     * @param string $meterMnfSerialNumber
     * @param string $meterManufacturerName
     * @param string $meterSAPSerialNumber
     * @param string $multiplicationFactor
     * @param string $overallMf
     * @param string $oldMf
     * @param string $numberOfDigits
     * @param string $lineCtRatio
     * @param string $externalPtRatio
     * @param string $phaseCode
     * @param string $sanctionedLoadKW
     * @param string $contractedDemandKVA
     * @param string $sanctionedLoadChangeFlag
     * @param string $sanctionedLoadChangeDate
     * @param string $oldSanctionedLoadKW
     * @param string $admissibleVoltage
     * @param string $supplyVoltage
     * @param string $noOfBOardEmployees6
     * @param string $noOfBOardEmployees7
     * @param string $noOfBOardEmployees8
     * @param string $scheduleMeterReadDate
     * @param string $previousReadingDate
     * @param string $previousBillDate
     * @param string $previousReadingKWH
     * @param string $previousReadingKVA
     * @param string $previousReadingKVAH
     * @param string $previousMeterStatus
     * @param string $previousBillType
     * @param string $meterChangeFlag
     * @param string $meterChangeDate
     * @param string $totalOldMeterConsumptionUnitsKWH
     * @param string $totalOldMeterConsumptionUnitsKVAH
     * @param string $oldMeterTotalRentAmount
     * @param string $oldMeterTotalServiceRentAmount
     * @param string $oldMeterTotalServiceChargeAmount
     * @param string $oldMeterTotalMBCRentAmount
     * @param string $newMeterInitialReadingKWH
     * @param string $newMeterInitialReadingKVAH
     * @param string $reconnectionFlag
     * @param string $reconnectionDate
     * @param string $consumptionBeforeDisConnectionKWH
     * @param string $consumptionBeforeDisConnectionKVAH
     * @param string $periodBeforeDisConnection
     * @param string $edExemptedFlag
     * @param string $edExemptionExpiry
     * @param string $octraiChargesApplicableFlag
     * @param string $meterRentRate
     * @param string $serviceRentRate
     * @param string $peakLoadExemptionCharges
     * @param string $peakLoadExemptionChargesExpiry
     * @param string $MMTSCorrectionFactor
     * @param string $shuntCapacitorChargesApplicableFlag
     * @param string $capacityOfCapacitor
     * @param string $MBCRentRate
     * @param string $serviceChargeRate
     * @param string $seasonStartDate
     * @param string $seasonEndDate
     * @param string $sundryChargesSOP
     * @param string $sundryChargesED
     * @param string $sundryChargesOCTRAI
     * @param string $sundryAllowancesSOP
     * @param string $sundryAllowancesED
     * @param string $sundryAllowancesOCTRAI
     * @param string $waterChargesProvisionalAmount
     * @param string $otherCharges
     * @param string $roundAdjustmentAmount
     * @param string $adjustmentAmountSOP
     * @param string $adjustmentAmountED
     * @param string $adjustmentAmountOCTRAI
     * @param string $provisionalAdjustmentAmountSOP
     * @param string $provisionalAdjustmentAmountED
     * @param string $provisionalAdjustmentAmountOCTRAI
     * @param string $arrearSOPCurrentYear
     * @param string $arrearEDCurrentYear
     * @param string $arrearOCTRAICurrentYear
     * @param string $arrearSOPPreviousYear
     * @param string $arrearEDPreviousYear
     * @param string $arrearOCTRAIPreviousYear
     * @param string $advanceConsumptionDeposit
     * @param string $courtCaseAmount
     * @param string $previousKWHCycle1
     * @param string $previousKWHCycle2
     * @param string $previousKWHCycle3
     * @param string $previousKWHCycle4
     * @param string $previousKWHCycle5
     * @param string $previousKWHCycle6
     * @param string $averageKWHofAbove6Cycles
     * @param string $samePeriodLastYearConsumptionKWH
     * @param string $PF1
     * @param string $PF2
     * @param string $PF3
     * @param string $averageofAboveThreePF
     * @param string $standardPFOfConsumerCategory
     * @param string $MD1
     * @param string $MD2
     * @param string $gaushalaFlag
     * @param string $arrearWaterChargesCurrentYear
     * @param string $arrearWaterChargesPreviousYear
     * @param string $dataExpiryDate
     * @param string $maxOfAboveSixMDI
     * @param string $factorForCustomerCategory
     * @param string $DValueInLDHFNumberOfWorkingDaysPerMonthForCustomerCategory
     * @param string $HValueInLDHFSupplyHoursPerDayForCustomerCategory
     * @param string $previousTotalEstimatedConsumptionKWH
     * @param string $previousTotalEstimatedConsumptionKVAH
     * @param string $previousEstimatedMDI
     * @param string $nearestCashCounter
     * @param string $complaintCenterPhoneNumber
     * @param string $previousPaymentStatus
     * @param string $reasonForAdjustment
     * @param string $miscExpensesDetails
     * @param string $miscExpenses
     * @param string $meterSecurityAmount
     * @param string $waterChargesApplicableFlag
     * @param string $transformerCode
     * @param string $oldMeterRent
     * @param string $expirationDateOfOldMeterRent
     * @param string $oldMBCRent
     * @param string $expirationDateOfOldMBCRent
     * @param string $oldServiceCharges
     * @param string $expirationDateOfOldServiceCharges
     * @param string $meterLocation
     * @param string $meterType
     * @param string $version
     * @param string $ML
     * @param string $MT
     * @param string $mobileNo
     * @param string $feederCode
     * @param string $SCWSDAmountWithHeld
     * @param string $previousFyCons
     * @param string $muncipalTaxApplicableFlag
     * @param string $PLogicApplicableFlag
     * @param string $limitForPNore
     * @return string $greetings
     */

    public function SAP_RequestParams
    (
      $subDivisionCode,
      $subDivisionName,
      $MRU,
      $MR_DOCNumber,
      $neighbourMeterId,
      $connectedPoleNINNumber,
      $consumerLegacyNumber,
      $contractAcNumber,
      $consumerName,
      $houseNumber,
      $streetNumber,
      $address,
      $category,
      $subCategory,
      $moveInFlag,
      $categoryChangeFlag,
      $categoryChangeDate,
      $oldCategory,
      $oldSubCategory,
      $industryCode,
      $billingCycle,
      $billingCycleType,
      $billingGroup,
      $triVectorMeterFlag,
      $installation,
      $meterMnfSerialNumber,
      $meterManufacturerName,
      $meterSAPSerialNumber,
      $multiplicationFactor,
      $overallMf,
      $oldMf,
      $numberOfDigits,
      $lineCtRatio,
      $externalPtRatio,
      $phaseCode,
      $sanctionedLoadKW,
      $contractedDemandKVA,
      $sanctionedLoadChangeFlag,
      $sanctionedLoadChangeDate,
      $oldSanctionedLoadKW,
      $admissibleVoltage,
      $supplyVoltage,
      $noOfBOardEmployees6,
      $noOfBOardEmployees7,
      $noOfBOardEmployees8,
      $scheduleMeterReadDate,
      $previousReadingDate,
      $previousBillDate,
      $previousReadingKWH,
      $previousReadingKVA,
      $previousReadingKVAH,
      $previousMeterStatus,
      $previousBillType,
      $meterChangeFlag,
      $meterChangeDate,
      $totalOldMeterConsumptionUnitsKWH,
      $totalOldMeterConsumptionUnitsKVAH,
      $oldMeterTotalRentAmount,
      $oldMeterTotalServiceRentAmount,
      $oldMeterTotalServiceChargeAmount,
      $oldMeterTotalMBCRentAmount,
      $newMeterInitialReadingKWH,
      $newMeterInitialReadingKVAH,
      $reconnectionFlag,
      $reconnectionDate,
      $consumptionBeforeDisConnectionKWH,
      $consumptionBeforeDisConnectionKVAH,
      $periodBeforeDisConnection,
      $edExemptedFlag,
      $edExemptionExpiry,
      $octraiChargesApplicableFlag,
      $meterRentRate,
      $serviceRentRate,
      $peakLoadExemptionCharges,
      $peakLoadExemptionChargesExpiry,
      $MMTSCorrectionFactor,
      $shuntCapacitorChargesApplicableFlag,
      $capacityOfCapacitor,
      $MBCRentRate,
      $serviceChargeRate,
      $seasonStartDate,
      $seasonEndDate,
      $sundryChargesSOP,
      $sundryChargesED,
      $sundryChargesOCTRAI,
      $sundryAllowancesSOP,
      $sundryAllowancesED,
      $sundryAllowancesOCTRAI,
      $waterChargesProvisionalAmount,
      $otherCharges,
      $roundAdjustmentAmount,
      $adjustmentAmountSOP,
      $adjustmentAmountED,
      $adjustmentAmountOCTRAI,
      $provisionalAdjustmentAmountSOP,
      $provisionalAdjustmentAmountED,
      $provisionalAdjustmentAmountOCTRAI,
      $arrearSOPCurrentYear,
      $arrearEDCurrentYear,
      $arrearOCTRAICurrentYear,
      $arrearSOPPreviousYear,
      $arrearEDPreviousYear,
      $arrearOCTRAIPreviousYear,
      $advanceConsumptionDeposit,
      $courtCaseAmount,
      $previousKWHCycle1,
      $previousKWHCycle2,
      $previousKWHCycle3,
      $previousKWHCycle4,
      $previousKWHCycle5,
      $previousKWHCycle6,
      $averageKWHofAbove6Cycles,
      $samePeriodLastYearConsumptionKWH,
      $PF1,
      $PF2,
      $PF3,
      $averageofAboveThreePF,
      $standardPFOfConsumerCategory,
      $MD1,
      $MD2,
      $gaushalaFlag,
      $arrearWaterChargesCurrentYear,
      $arrearWaterChargesPreviousYear,
      $dataExpiryDate,
      $maxOfAboveSixMDI,
      $factorForCustomerCategory,
      $DValueInLDHFNumberOfWorkingDaysPerMonthForCustomerCategory,
      $HValueInLDHFSupplyHoursPerDayForCustomerCategory,
      $previousTotalEstimatedConsumptionKWH,
      $previousTotalEstimatedConsumptionKVAH,
      $previousEstimatedMDI,
      $nearestCashCounter,
      $complaintCenterPhoneNumber,
      $previousPaymentStatus,
      $reasonForAdjustment,
      $miscExpensesDetails,
      $miscExpenses,
      $meterSecurityAmount,
      $waterChargesApplicableFlag,
      $transformerCode,
      $oldMeterRent,
      $expirationDateOfOldMeterRent,
      $oldMBCRent,
      $expirationDateOfOldMBCRent,
      $oldServiceCharges,
      $expirationDateOfOldServiceCharges,
      $meterLocation,
      $meterType,
      $version,
      $ML,
      $MT,
      $mobileNo,
      $feederCode,
      $SCWSDAmountWithHeld,
      $previousFyCons,
      $muncipalTaxApplicableFlag,
      $PLogicApplicableFlag,
      $limitForPNore
      )
    {
      $servername = "aa14q5yjieztd35.ckmy9kpzrgql.ap-south-1.rds.amazonaws.com";
      $username = "pspcl";
      $password = "pspclpassword";
      $dbname = "ebdb";

      // Create connection
      $conn = new mysqli($servername, $username, $password, $dbname);
      // Check connection
      if ($conn->connect_error) {
          die("Connection failed: " . $conn->connect_error);
      }
        $ipaddress = $_SERVER['REMOTE_ADDR'];
        $ip = Date('h:i:s');


        $subDivisionCode = $this->insertValue($subDivisionCode);
        $subDivisionName = $this->insertValue($subDivisionName);
        $MRU = $this->insertValue($MRU);
        $MR_DOCNumber = $this->insertValue($MR_DOCNumber);
        $neighbourMeterId = $this->insertValue($neighbourMeterId);
        $connectedPoleNINNumber = $this->insertValue($connectedPoleNINNumber);
        $consumerLegacyNumber = $this->insertValue($consumerLegacyNumber);
        $contractAcNumber = $this->insertValue($contractAcNumber);
        $consumerName = $this->insertValue($consumerName);
        $houseNumber = $this->insertValue($houseNumber);
        $streetNumber = $this->insertValue($streetNumber);
        $address = $this->insertValue($address);
        $category = $this->insertValue($category);
        $subCategory = $this->insertValue($subCategory);
        $moveInFlag = $this->insertValue($moveInFlag);
        $categoryChangeFlag = $this->insertValue($categoryChangeFlag);
        $categoryChangeDate = $this->insertValue($categoryChangeDate);
        $oldCategory = $this->insertValue($oldCategory);
        $oldSubCategory = $this->insertValue($oldSubCategory);
        $industryCode = $this->insertValue($industryCode);
        $billingCycle = $this->insertValue($billingCycle);
        $billingCycleType = $this->insertValue($billingCycleType);
        $billingGroup = $this->insertValue($billingGroup);
        $triVectorMeterFlag = $this->insertValue($triVectorMeterFlag);
        $installation = $this->insertValue($installation);
        $meterMnfSerialNumber = $this->insertValue($meterMnfSerialNumber);
        $meterManufacturerName = $this->insertValue($meterManufacturerName);
        $meterSAPSerialNumber = $this->insertValue($meterSAPSerialNumber);
        $multiplicationFactor = $this->insertValue($multiplicationFactor);
        $overallMf = $this->insertValue($overallMf);
        $oldMf = $this->insertValue($oldMf);
        $numberOfDigits = $this->insertValue($numberOfDigits);
        $lineCtRatio = $this->insertValue($lineCtRatio);
        $externalPtRatio = $this->insertValue($externalPtRatio);
        $phaseCode = $this->insertValue($phaseCode);
        $sanctionedLoadKW = $this->insertValue($sanctionedLoadKW);
        $contractedDemandKVA = $this->insertValue($contractedDemandKVA);
        $sanctionedLoadChangeFlag = $this->insertValue($sanctionedLoadChangeFlag);
        $sanctionedLoadChangeDate = $this->insertValue($sanctionedLoadChangeDate);
        $oldSanctionedLoadKW = $this->insertValue($oldSanctionedLoadKW);
        $admissibleVoltage = $this->insertValue($admissibleVoltage);
        $supplyVoltage = $this->insertValue($supplyVoltage);
        $noOfBOardEmployees6 = $this->insertValue($noOfBOardEmployees6);
        $noOfBOardEmployees7 = $this->insertValue($noOfBOardEmployees7);
        $noOfBOardEmployees8 = $this->insertValue($noOfBOardEmployees8);
        $scheduleMeterReadDate = $this->insertValue($scheduleMeterReadDate);
        $previousReadingDate = $this->insertValue($previousReadingDate);
        $previousBillDate = $this->insertValue($previousBillDate);
        $previousReadingKWH = $this->insertValue($previousReadingKWH);
        $previousReadingKVA = $this->insertValue($previousReadingKVA);
        $previousReadingKVAH = $this->insertValue($previousReadingKVAH);
        $previousMeterStatus = $this->insertValue($previousMeterStatus);
        $previousBillType = $this->insertValue($previousBillType);
        $meterChangeFlag = $this->insertValue($meterChangeFlag);
        $meterChangeDate = $this->insertValue($meterChangeDate);
        $totalOldMeterConsumptionUnitsKWH = $this->insertValue($totalOldMeterConsumptionUnitsKWH);
        $totalOldMeterConsumptionUnitsKVAH = $this->insertValue($totalOldMeterConsumptionUnitsKVAH);
        $oldMeterTotalRentAmount = $this->insertValue($oldMeterTotalRentAmount);
        $oldMeterTotalServiceRentAmount = $this->insertValue($oldMeterTotalServiceRentAmount);
        $oldMeterTotalServiceChargeAmount = $this->insertValue($oldMeterTotalServiceChargeAmount);
        $oldMeterTotalMBCRentAmount = $this->insertValue($oldMeterTotalMBCRentAmount);
        $newMeterInitialReadingKWH = $this->insertValue($newMeterInitialReadingKWH);
        $newMeterInitialReadingKVAH = $this->insertValue($newMeterInitialReadingKVAH);
        $reconnectionFlag = $this->insertValue($reconnectionFlag);
        $reconnectionDate = $this->insertValue($reconnectionDate);
        $consumptionBeforeDisConnectionKWH = $this->insertValue($consumptionBeforeDisConnectionKWH);
        $consumptionBeforeDisConnectionKVAH = $this->insertValue($consumptionBeforeDisConnectionKVAH);
        $periodBeforeDisConnection = $this->insertValue($periodBeforeDisConnection);
        $edExemptedFlag = $this->insertValue($edExemptedFlag);
        $edExemptionExpiry = $this->insertValue($edExemptionExpiry);
        $octraiChargesApplicableFlag = $this->insertValue($octraiChargesApplicableFlag);
        $meterRentRate = $this->insertValue($meterRentRate);
        $serviceRentRate = $this->insertValue($serviceRentRate);
        $peakLoadExemptionCharges = $this->insertValue($peakLoadExemptionCharges);
        $peakLoadExemptionChargesExpiry = $this->insertValue($peakLoadExemptionChargesExpiry);
        $MMTSCorrectionFactor = $this->insertValue($MMTSCorrectionFactor);
        $shuntCapacitorChargesApplicableFlag = $this->insertValue($shuntCapacitorChargesApplicableFlag);
        $capacityOfCapacitor = $this->insertValue($capacityOfCapacitor);
        $MBCRentRate = $this->insertValue($MBCRentRate);
        $serviceChargeRate = $this->insertValue($serviceChargeRate);
        $seasonStartDate = $this->insertValue($seasonStartDate);
        $seasonEndDate = $this->insertValue($seasonEndDate);
        $sundryChargesSOP = $this->insertValue($sundryChargesSOP);
        $sundryChargesED = $this->insertValue($sundryChargesED);
        $sundryChargesOCTRAI = $this->insertValue($sundryChargesOCTRAI);
        $sundryAllowancesSOP = $this->insertValue($sundryAllowancesSOP);
        $sundryAllowancesED = $this->insertValue($sundryAllowancesED);
        $sundryAllowancesOCTRAI = $this->insertValue($sundryAllowancesOCTRAI);
        $waterChargesProvisionalAmount = $this->insertValue($waterChargesProvisionalAmount);
        $otherCharges = $this->insertValue($otherCharges);
        $roundAdjustmentAmount = $this->insertValue($roundAdjustmentAmount);
        $adjustmentAmountSOP = $this->insertValue($adjustmentAmountSOP);
        $adjustmentAmountED = $this->insertValue($adjustmentAmountED);
        $adjustmentAmountOCTRAI = $this->insertValue($adjustmentAmountOCTRAI);
        $provisionalAdjustmentAmountSOP = $this->insertValue($provisionalAdjustmentAmountSOP);
        $provisionalAdjustmentAmountED = $this->insertValue($provisionalAdjustmentAmountED);
        $provisionalAdjustmentAmountOCTRAI = $this->insertValue($provisionalAdjustmentAmountOCTRAI);
        $arrearSOPCurrentYear = $this->insertValue($arrearSOPCurrentYear);
        $arrearEDCurrentYear = $this->insertValue($arrearEDCurrentYear);
        $arrearOCTRAICurrentYear = $this->insertValue($arrearOCTRAICurrentYear);
        $arrearSOPPreviousYear = $this->insertValue($arrearSOPPreviousYear);
        $arrearEDPreviousYear = $this->insertValue($arrearEDPreviousYear);
        $arrearOCTRAIPreviousYear = $this->insertValue($arrearOCTRAIPreviousYear);
        $advanceConsumptionDeposit = $this->insertValue($advanceConsumptionDeposit);
        $courtCaseAmount = $this->insertValue($courtCaseAmount);
        $previousKWHCycle1 = $this->insertValue($previousKWHCycle1);
        $previousKWHCycle2 = $this->insertValue($previousKWHCycle2);
        $previousKWHCycle3 = $this->insertValue($previousKWHCycle3);
        $previousKWHCycle4 = $this->insertValue($previousKWHCycle4);
        $previousKWHCycle5 = $this->insertValue($previousKWHCycle5);
        $previousKWHCycle6 = $this->insertValue($previousKWHCycle6);
        $averageKWHofAbove6Cycles = $this->insertValue($averageKWHofAbove6Cycles);
        $samePeriodLastYearConsumptionKWH = $this->insertValue($samePeriodLastYearConsumptionKWH);
        $PF1 = $this->insertValue($PF1);
        $PF2 = $this->insertValue($PF2);
        $PF3 = $this->insertValue($PF3);
        $averageofAboveThreePF = $this->insertValue($averageofAboveThreePF);
        $standardPFOfConsumerCategory = $this->insertValue($standardPFOfConsumerCategory);
        $MD1 = $this->insertValue($MD1);
        $MD2 = $this->insertValue($MD2);
        $gaushalaFlag = $this->insertValue($gaushalaFlag);
        $arrearWaterChargesCurrentYear = $this->insertValue($arrearWaterChargesCurrentYear);
        $arrearWaterChargesPreviousYear = $this->insertValue($arrearWaterChargesPreviousYear);
        $dataExpiryDate = $this->insertValue($dataExpiryDate);
        $maxOfAboveSixMDI = $this->insertValue($maxOfAboveSixMDI);
        $factorForCustomerCategory = $this->insertValue($factorForCustomerCategory);
        $DValueInLDHFNumberOfWorkingDaysPerMonthForCustomerCategory = $this->insertValue($DValueInLDHFNumberOfWorkingDaysPerMonthForCustomerCategory);
        $HValueInLDHFSupplyHoursPerDayForCustomerCategory = $this->insertValue($HValueInLDHFSupplyHoursPerDayForCustomerCategory);
        $previousTotalEstimatedConsumptionKWH = $this->insertValue($previousTotalEstimatedConsumptionKWH);
        $previousTotalEstimatedConsumptionKVAH = $this->insertValue($previousTotalEstimatedConsumptionKVAH);
        $previousEstimatedMDI = $this->insertValue($previousEstimatedMDI);
        $nearestCashCounter = $this->insertValue($nearestCashCounter);
        $complaintCenterPhoneNumber = $this->insertValue($complaintCenterPhoneNumber);
        $previousPaymentStatus = $this->insertValue($previousPaymentStatus);
        $reasonForAdjustment = $this->insertValue($reasonForAdjustment);
        $miscExpensesDetails = $this->insertValue($miscExpensesDetails);
        $miscExpenses = $this->insertValue($miscExpenses);
        $meterSecurityAmount = $this->insertValue($meterSecurityAmount);
        $waterChargesApplicableFlag = $this->insertValue($waterChargesApplicableFlag);
        $transformerCode = $this->insertValue($transformerCode);
        $oldMeterRent = $this->insertValue($oldMeterRent);
        $expirationDateOfOldMeterRent = $this->insertValue($expirationDateOfOldMeterRent);
        $oldMBCRent = $this->insertValue($oldMBCRent);
        $expirationDateOfOldMBCRent = $this->insertValue($expirationDateOfOldMBCRent);
        $oldServiceCharges = $this->insertValue($oldServiceCharges);
        $expirationDateOfOldServiceCharges = $this->insertValue($expirationDateOfOldServiceCharges);
        $meterLocation = $this->insertValue($meterLocation);
        $meterType = $this->insertValue($meterType);
        $version = $this->insertValue($version);
        $ML = $this->insertValue($ML);
        $MT = $this->insertValue($MT);
        $mobileNo = $this->insertValue($mobileNo);
        $feederCode = $this->insertValue($feederCode);
        $SCWSDAmountWithHeld = $this->insertValue($SCWSDAmountWithHeld);
        $previousFyCons = $this->insertValue($previousFyCons);
        $muncipalTaxApplicableFlag = $this->insertValue($muncipalTaxApplicableFlag);
        $PLogicApplicableFlag = $this->insertValue($PLogicApplicableFlag);
        $limitForPNore = $this->insertValue($limitForPNore);

    //   die ($consumerLegacyNumber);

    $newLog = "INSERT INTO `sap_logg` (text) VALUES ($MR_DOCNumber)";
         $logResult = $conn->query($newLog);
        //  2018-06-22 05:37:16
         $date = date('Y-m-d H:i:s');
         $time = date('H:i:s');

      $sql = "INSERT INTO `sap_server` (subDivisionCode, subDivisionName, MRU, MR_DOCNumber, neighbourMeterId, connectedPoleNINNumber, consumerLegacyNumber, contractAcNumber, consumerName, houseNumber, streetNumber, address, category, subCategory, moveInFlag, categoryChangeFlag, categoryChangeDate, oldCategory, oldSubCategory, industryCode, billingCycle, billingCycleType, billingGroup, triVectorMeterFlag, installation, meterMnfSerialNumber, meterManufacturerName, meterSAPSerialNumber, multiplicationFactor, overallMf, oldMf, numberOfDigits, lineCtRatio, externalPtRatio, phaseCode, sanctionedLoadKW, contractedDemandKVA, sanctionedLoadChangeFlag, sanctionedLoadChangeDate, oldSanctionedLoadKW, admissibleVoltage, supplyVoltage, noOfBOardEmployees6, noOfBOardEmployees7, noOfBOardEmployees8, scheduleMeterReadDate, previousReadingDate, previousBillDate, previousReadingKWH, previousReadingKVA, previousReadingKVAH, previousMeterStatus, previousBillType, meterChangeFlag, meterChangeDate, totalOldMeterConsumptionUnitsKWH, totalOldMeterConsumptionUnitsKVAH, oldMeterTotalRentAmount, oldMeterTotalServiceRentAmount, oldMeterTotalServiceChargeAmount, oldMeterTotalMBCRentAmount, newMeterInitialReadingKWH, newMeterInitialReadingKVAH, reconnectionFlag, reconnectionDate, consumptionBeforeDisConnectionKWH, consumptionBeforeDisConnectionKVAH, periodBeforeDisConnection, edExemptedFlag, edExemptionExpiry, octraiChargesApplicableFlag, meterRentRate, serviceRentRate, peakLoadExemptionCharges, peakLoadExemptionChargesExpiry, MMTSCorrectionFactor, shuntCapacitorChargesApplicableFlag, capacityOfCapacitor, MBCRentRate, serviceChargeRate, seasonStartDate, seasonEndDate, sundryChargesSOP, sundryChargesED, sundryChargesOCTRAI, sundryAllowancesSOP, sundryAllowancesED, sundryAllowancesOCTRAI, waterChargesProvisionalAmount, otherCharges, roundAdjustmentAmount, adjustmentAmountSOP, adjustmentAmountED, adjustmentAmountOCTRAI, provisionalAdjustmentAmountSOP, provisionalAdjustmentAmountED, provisionalAdjustmentAmountOCTRAI, arrearSOPCurrentYear, arrearEDCurrentYear, arrearOCTRAICurrentYear, arrearSOPPreviousYear, arrearEDPreviousYear, arrearOCTRAIPreviousYear, advanceConsumptionDeposit, courtCaseAmount, previousKWHCycle1, previousKWHCycle2, previousKWHCycle3, previousKWHCycle4, previousKWHCycle5, previousKWHCycle6, averageKWHofAbove6Cycles, samePeriodLastYearConsumptionKWH, PF1, PF2, PF3, averageofAboveThreePF, standardPFOfConsumerCategory, MD1, MD2, gaushalaFlag, arrearWaterChargesCurrentYear, arrearWaterChargesPreviousYear, dataExpiryDate, maxOfAboveSixMDI, factorDay, dValueDays, hValueDay, previousTotalEstimatedConsumptionKWH, previousTotalEstimatedConsumptionKVAH, previousEstimatedMDI, nearestCashCounter, complaintCenterPhoneNumber, previousPaymentStatus, reasonForAdjustment, miscExpensesDetails, miscExpenses, meterSecurityAmount, waterChargesApplicableFlag, transformerCode, oldMeterRent, expirationDateOfOldMeterRent, oldMBCRent, expirationDateOfOldMBCRent, oldServiceCharges, expirationDateOfOldServiceCharges, meterLocation, meterType, version, ML, MT, mobileNo, feederCode, SCWSDAmountWithHeld, previousFyCons, muncipalTaxApplicableFlag, PLogicApplicableFlag, limitForPNore)

      VALUES (
        $subDivisionCode,
        $subDivisionName,
        $MRU,
        $MR_DOCNumber,
        $neighbourMeterId,
        $connectedPoleNINNumber,
        $consumerLegacyNumber,
        $contractAcNumber,
        $consumerName,
        $houseNumber,
        $streetNumber,
        $address,
        $category,
        $subCategory,
        $moveInFlag,
        $categoryChangeFlag,
        $categoryChangeDate,
        $oldCategory,
        $oldSubCategory,
        $industryCode,
        $billingCycle,
        $billingCycleType,
        $billingGroup,
        $triVectorMeterFlag,
        $installation,
        $meterMnfSerialNumber,
        $meterManufacturerName,
        $meterSAPSerialNumber,
        $multiplicationFactor,
        $overallMf,
        $oldMf,
        $numberOfDigits,
        $lineCtRatio,
        $externalPtRatio,
        $phaseCode,
        $sanctionedLoadKW,
        $contractedDemandKVA,
        $sanctionedLoadChangeFlag,
        $sanctionedLoadChangeDate,
        $oldSanctionedLoadKW,
        $admissibleVoltage,
        $supplyVoltage,
        $noOfBOardEmployees6,
        $noOfBOardEmployees7,
        $noOfBOardEmployees8,
        $scheduleMeterReadDate,
        $previousReadingDate,
        $previousBillDate,
        $previousReadingKWH,
        $previousReadingKVA,
        $previousReadingKVAH,
        $previousMeterStatus,
        $previousBillType,
        $meterChangeFlag,
        $meterChangeDate,
        $totalOldMeterConsumptionUnitsKWH,
        $totalOldMeterConsumptionUnitsKVAH,
        $oldMeterTotalRentAmount,
        $oldMeterTotalServiceRentAmount,
        $oldMeterTotalServiceChargeAmount,
        $oldMeterTotalMBCRentAmount,
        $newMeterInitialReadingKWH,
        $newMeterInitialReadingKVAH,
        $reconnectionFlag,
        $reconnectionDate,
        $consumptionBeforeDisConnectionKWH,
        $consumptionBeforeDisConnectionKVAH,
        $periodBeforeDisConnection,
        $edExemptedFlag,
        $edExemptionExpiry,
        $octraiChargesApplicableFlag,
        $meterRentRate,
        $serviceRentRate,
        $peakLoadExemptionCharges,
        $peakLoadExemptionChargesExpiry,
        $MMTSCorrectionFactor,
        $shuntCapacitorChargesApplicableFlag,
        $capacityOfCapacitor,
        $MBCRentRate,
        $serviceChargeRate,
        $seasonStartDate,
        $seasonEndDate,
        $sundryChargesSOP,
        $sundryChargesED,
        $sundryChargesOCTRAI,
        $sundryAllowancesSOP,
        $sundryAllowancesED,
        $sundryAllowancesOCTRAI,
        $waterChargesProvisionalAmount,
        $otherCharges,
        $roundAdjustmentAmount,
        $adjustmentAmountSOP,
        $adjustmentAmountED,
        $adjustmentAmountOCTRAI,
        $provisionalAdjustmentAmountSOP,
        $provisionalAdjustmentAmountED,
        $provisionalAdjustmentAmountOCTRAI,
        $arrearSOPCurrentYear,
        $arrearEDCurrentYear,
        $arrearOCTRAICurrentYear,
        $arrearSOPPreviousYear,
        $arrearEDPreviousYear,
        $arrearOCTRAIPreviousYear,
        $advanceConsumptionDeposit,
        $courtCaseAmount,
        $previousKWHCycle1,
        $previousKWHCycle2,
        $previousKWHCycle3,
        $previousKWHCycle4,
        $previousKWHCycle5,
        $previousKWHCycle6,
        $averageKWHofAbove6Cycles,
        $samePeriodLastYearConsumptionKWH,
        $PF1,
        $PF2,
        $PF3,
        $averageofAboveThreePF,
        $standardPFOfConsumerCategory,
        $MD1,
        $MD2,
        $gaushalaFlag,
        $arrearWaterChargesCurrentYear,
        $arrearWaterChargesPreviousYear,
        $dataExpiryDate,
        $maxOfAboveSixMDI,
        $factorForCustomerCategory,
        $DValueInLDHFNumberOfWorkingDaysPerMonthForCustomerCategory,
        $HValueInLDHFSupplyHoursPerDayForCustomerCategory,
        $previousTotalEstimatedConsumptionKWH,
        $previousTotalEstimatedConsumptionKVAH,
        $previousEstimatedMDI,
        $nearestCashCounter,
        $complaintCenterPhoneNumber,
        $previousPaymentStatus,
        $reasonForAdjustment,
        $miscExpensesDetails,
        $miscExpenses,
        $meterSecurityAmount,
        $waterChargesApplicableFlag,
        $transformerCode,
        $oldMeterRent,
        $expirationDateOfOldMeterRent,
        $oldMBCRent,
        $expirationDateOfOldMBCRent,
        $oldServiceCharges,
        $expirationDateOfOldServiceCharges,
        $meterLocation,
        $meterType,
        $version,
        $ML,
        $MT,
        $mobileNo,
        $feederCode,
        $SCWSDAmountWithHeld,
        $previousFyCons,
        $muncipalTaxApplicableFlag,
        $PLogicApplicableFlag,
        $limitForPNore
      )";

      if ($conn->query($sql) === TRUE) {
          $newLog = "INSERT INTO `response_logs` (mr, status, message) VALUES ($MR_DOCNumber, 'S', '')";
               $logResult = $conn->query($newLog);
        //   return new SoapVar(
        //         array(
        //             'SAP_ResponseParamsOut' => new SoapVar(
        //                 array(
        //                 'MR_DOC_NO' => $MR_DOCNumber,
        //                 'STATUS' => "S",
        //                 'MSG' => ""
        //                     ), SOAP_ENC_OBJECT
        //                 )

        //             ), SOAP_ENC_OBJECT );
        return "Y";
        // $result = $this->SAP_ResponseParams($MR_DOCNumber);
        // return $result;
      } else {
        $result = $this->SAP_ResponseParams($MR_DOCNumber, $conn->error);
        return $result;
          // echo "Error: " . $sql . "<br>" . $conn->error;
      }

      $conn->close();

    }
    /**
     * SAP_ResponseParams.
     *
     * @param string $MR_DOC_NO
     * @param string $status
     * @param string $MSG
     * @return array $return
     */
    public function SAP_ResponseParams($MR_DOC_NO, $status = '', $msg = '')
    {
        $result = $MR_DOC_NO.'#';
        $result .= $STATUS;
        $servername = "aa14q5yjieztd35.ckmy9kpzrgql.ap-south-1.rds.amazonaws.com";
      $username = "pspcl";
      $password = "pspclpassword";
      $dbname = "ebdb";

      // Create connection
      $conn = new mysqli($servername, $username, $password, $dbname);
      // Check connection
      if ($conn->connect_error) {
          die("Connection failed: " . $conn->connect_error);
      }
    //   $date = "date('Y-m-d H:i:s')";
        $sql = "SELECT * FROM sap_server WHERE MR_DOCNumber = $MR_DOC_NO";
        $result = $conn->query($sql);
        if($result->num_rows > 0) {
            $newLog = "INSERT INTO `response_logs` (mr, status, message) VALUES ($MR_DOC_NO, 'S', '')";
               $logResult = $conn->query($newLog);
            return new SoapVar(
                array(
                    'SAP_ResponseParams' => new SoapVar(
                        array(
                        'MR_DOC_NO' => $MR_DOC_NO,
                        'STATUS' => "S",
                        'MSG' => ""
                            ), SOAP_ENC_OBJECT
                        )

                    ), SOAP_ENC_OBJECT );
        } else {
            $newLog = "INSERT INTO `response_logs` (mr, status, message) VALUES ($MR_DOC_NO, 'N', '')";
               $logResult = $conn->query($newLog);
               return new SoapVar(
                array(
                    'SAP_ResponseParams' => new SoapVar(
                        array(
                        'MR_DOC_NO' => $MR_DOC_NO,
                        'STATUS' => "N",
                        'MSG' => $msg
                            ), SOAP_ENC_OBJECT
                        )

                    ), SOAP_ENC_OBJECT );
        }

    }

}
/**
 * This function ...
 *
 * @param string $ttt
 * @return string
 */
function tttr($ttt) {
  return $ttt;
}

$serverUrl = "http://www.sterlingtransformers.com/api/index.php";
$options = [
    'uri' => $serverUrl,
];
$server = new Zend\Soap\Server(null, $options);

if (isset($_GET['wsdl'])) {
    $soapAutoDiscover = new \Zend\Soap\AutoDiscover(new \Zend\Soap\Wsdl\ComplexTypeStrategy\ArrayOfTypeSequence());
    $soapAutoDiscover->setBindingStyle(array('style' => 'document'));
    $soapAutoDiscover->setOperationBodyStyle(array('use' => 'literal'));
    $soapAutoDiscover->setClass('SAP_PreData');
    $soapAutoDiscover->setUri($serverUrl);

    header("Content-Type: text/xml");
    echo $soapAutoDiscover->generate()->toXml();
} else {
    $soap = new \Zend\Soap\Server($serverUrl . '?wsdl');
    $soap->setObject(new \Zend\Soap\Server\DocumentLiteralWrapper(new SAP_PreData()));
    $soap->handle();
}
