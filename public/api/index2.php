<?php

require_once __DIR__ . '/vendor/autoload.php';

class Book {
    /** @var string */
    public $title;
    /** @var string */
    public $author;
    /** @var string */
    public $authorr;
    /** @var Tag[] */
    public $tags;
}
class Tag {
    /** @var string */
    public $tag;
}
class Webservice {
    /**
    *
    * @param Book $book
    * @return bool
    */
    public function insertBook($book) {
        return true;
    }
}

$classmap = array('Book' => 'Book','Tag' => 'Tag');
$server = new SoapServer('bookservice.wsdl',array('classmap' => $classmap));
$server->setClass("Webservice");
$server->handle();
