<?php

date_default_timezone_set("Asia/Kolkata");

function push_SAP_RequestParams($subDivisionCode, $subDivisionName, $MRU, $MR_DOCNumber, $neighbourMeterId, $connectedPoleNINNumber, $consumerLegacyNumber, $contractAcNumber, $consumerName, $houseNumber, $streetNumber, $address, $category, $subCategory, $moveInFlag, $categoryChangeFlag, $categoryChangeDate, $oldCategory, $oldSubCategory, $industryCode, $billingCycle, $billingCycleType, $billingGroup, $triVectorMeterFlag, $installation, $meterMnfSerialNumber, $meterManufacturerName, $meterSAPSerialNumber, $multiplicationFactor, $overallMf, $oldMf, $numberOfDigits, $lineCtRatio, $externalPtRatio, $phaseCode, $sanctionedLoadKW, $contractedDemandKVA, $sanctionedLoadChangeFlag, $sanctionedLoadChangeDate, $oldSanctionedLoadKW, $admissibleVoltage, $supplyVoltage, $noOfBOardEmployees6, $noOfBOardEmployees7, $noOfBOardEmployees8, $scheduleMeterReadDate, $previousReadingDate, $previousBillDate, $previousReadingKWH, $previousReadingKVA, $previousReadingKVAH, $previousMeterStatus, $previousBillType, $meterChangeFlag, $meterChangeDate, $totalOldMeterConsumptionUnitsKWH, $totalOldMeterConsumptionUnitsKVAH, $oldMeterTotalRentAmount, $oldMeterTotalServiceRentAmount, $oldMeterTotalServiceChargeAmount, $oldMeterTotalMBCRentAmount, $newMeterInitialReadingKWH, $newMeterInitialReadingKVAH, $reconnectionFlag, $reconnectionDate, $consumptionBeforeDisConnectionKWH, $consumptionBeforeDisConnectionKVAH, $periodBeforeDisConnection, $edExemptedFlag, $edExemptionExpiry, $octraiChargesApplicableFlag, $meterRentRate, $serviceRentRate, $peakLoadExemptionCharges, $peakLoadExemptionChargesExpiry, $MMTSCorrectionFactor, $shuntCapacitorChargesApplicableFlag, $capacityOfCapacitor, $MBCRentRate, $serviceChargeRate, $seasonStartDate, $seasonEndDate, $sundryChargesSOP, $sundryChargesED, $sundryChargesOCTRAI, $sundryAllowancesSOP, $sundryAllowancesED, $sundryAllowancesOCTRAI, $waterChargesProvisionalAmount, $otherCharges, $roundAdjustmentAmount, $adjustmentAmountSOP, $adjustmentAmountED, $adjustmentAmountOCTRAI, $provisionalAdjustmentAmountSOP, $provisionalAdjustmentAmountED, $provisionalAdjustmentAmountOCTRAI, $arrearSOPCurrentYear, $arrearEDCurrentYear, $arrearOCTRAICurrentYear, $arrearSOPPreviousYear, $arrearEDPreviousYear, $arrearOCTRAIPreviousYear, $advanceConsumptionDeposit, $courtCaseAmount, $previousKWHCycle1, $previousKWHCycle2, $previousKWHCycle3, $previousKWHCycle4, $previousKWHCycle5, $previousKWHCycle6, $averageKWHofAbove6Cycles, $samePeriodLastYearConsumptionKWH, $PF1, $PF2, $PF3, $averageofAboveThreePF, $standardPFOfConsumerCategory, $MD1, $MD2, $gaushalaFlag, $arrearWaterChargesCurrentYear, $arrearWaterChargesPreviousYear, $dataExpiryDate, $maxOfAboveSixMDI, $factorForCustomerCategory, $DValueInLDHFNumberOfWorkingDaysPerMonthForCustomerCategory, $HValueInLDHFSupplyHoursPerDayForCustomerCategory, $previousTotalEstimatedConsumptionKWH, $previousTotalEstimatedConsumptionKVAH, $previousEstimatedMDI, $nearestCashCounter, $complaintCenterPhoneNumber, $previousPaymentStatus, $reasonForAdjustment, $miscExpensesDetails, $miscExpenses, $meterSecurityAmount, $waterChargesApplicableFlag, $transformerCode, $oldMeterRent, $expirationDateOfOldMeterRent, $oldMBCRent, $expirationDateOfOldMBCRent, $oldServiceCharges, $expirationDateOfOldServiceCharges, $meterLocation, $meterType, $version, $ML, $MT, $mobileNo, $feederCode, $SCWSDAmountWithHeld, $previousFyCons, $muncipalTaxApplicableFlag, $PLogicApplicableFlag, $limitForPNore) {

	$servername = "pspcl.ckmy9kpzrgql.ap-south-1.rds.amazonaws.com";
	$username = "pspcl";
	$password = "pspclpassword";
	$dbname = "ebdb";

	// Create connection
	$created_at = Date('Y-m-d H:i:s');
	$conn=mysqli_connect($servername, $username, $password, $dbname);
	if ($conn) {

		$preCheck = "SELECT * FROM `sap_server` where MR_DOCNumber = '$MR_DOCNumber'";
		$result = $conn->query($preCheck);
		if ($result->num_rows >= 1) {
			$result = [
				'mr' => $MR_DOCNumber,
				'status' => 'D',
				'msg' => ''
			];
		} else {
			// $postValues = $subDivisionCode.'-'.
			// $subDivisionName.'-'.
			// $MRU.'-'.
			// $MR_DOCNumber.'-'.
			// $neighbourMeterId.'-'.
			// $connectedPoleNINNumber.'-'.
			// $consumerLegacyNumber.'-'.
			// $contractAcNumber.'-'.
			// $consumerName.'-'.
			// $houseNumber.'-'.
			// $streetNumber.'-'.
			// $address.'-'.
			// $category.'-'.
			// $subCategory.'-'.
			// $moveInFlag.'-'.
			// $categoryChangeFlag.'-'.
			// $categoryChangeDate.'-'.
			// $oldCategory.'-'.
			// $oldSubCategory.'-'.
			// $industryCode.'-'.
			// $billingCycle.'-'.
			// $billingCycleType.'-'.
			// $billingGroup.'-'.
			// $triVectorMeterFlag.'-'.
			// $installation.'-'.
			// $meterMnfSerialNumber.'-'.
			// $meterManufacturerName.'-'.
			// $meterSAPSerialNumber.'-'.
			// $multiplicationFactor.'-'.
			// $overallMf.'-'.
			// $oldMf.'-'.
			// $numberOfDigits.'-'.
			// $lineCtRatio.'-'.
			// $externalPtRatio.'-'.
			// $phaseCode.'-'.
			// $sanctionedLoadKW.'-'.
			// $contractedDemandKVA.'-'.
			// $sanctionedLoadChangeFlag.'-'.
			// $sanctionedLoadChangeDate.'-'.
			// $oldSanctionedLoadKW.'-'.
			// $admissibleVoltage.'-'.
			// $supplyVoltage.'-'.
			// $noOfBOardEmployees6.'-'.
			// $noOfBOardEmployees7.'-'.
			// $noOfBOardEmployees8.'-'.
			// $scheduleMeterReadDate.'-'.
			// $previousReadingDate.'-'.
			// $previousBillDate.'-'.
			// $previousReadingKWH.'-'.
			// $previousReadingKVA.'-'.
			// $previousReadingKVAH.'-'.
			// $previousMeterStatus.'-'.
			// $previousBillType.'-'.
			// $meterChangeFlag.'-'.
			// $meterChangeDate.'-'.
			// $totalOldMeterConsumptionUnitsKWH.'-'.
			// $totalOldMeterConsumptionUnitsKVAH.'-'.
			// $oldMeterTotalRentAmount.'-'.
			// $oldMeterTotalServiceRentAmount.'-'.
			// $oldMeterTotalServiceChargeAmount.'-'.
			// $oldMeterTotalMBCRentAmount.'-'.
			// $newMeterInitialReadingKWH.'-'.
			// $newMeterInitialReadingKVAH.'-'.
			// $reconnectionFlag.'-'.
			// $reconnectionDate.'-'.
			// $consumptionBeforeDisConnectionKWH.'-'.
			// $consumptionBeforeDisConnectionKVAH.'-'.
			// $periodBeforeDisConnection.'-'.
			// $edExemptedFlag.'-'.
			// $edExemptionExpiry.'-'.
			// $octraiChargesApplicableFlag.'-'.
			// $meterRentRate.'-'.
			// $serviceRentRate.'-'.
			// $peakLoadExemptionCharges.'-'.
			// $peakLoadExemptionChargesExpiry.'-'.
			// $MMTSCorrectionFactor.'-'.
			// $shuntCapacitorChargesApplicableFlag.'-'.
			// $capacityOfCapacitor.'-'.
			// $MBCRentRate.'-'.
			// $serviceChargeRate.'-'.
			// $seasonStartDate.'-'.
			// $seasonEndDate.'-'.
			// $sundryChargesSOP.'-'.
			// $sundryChargesED.'-'.
			// $sundryChargesOCTRAI.'-'.
			// $sundryAllowancesSOP.'-'.
			// $sundryAllowancesED.'-'.
			// $sundryAllowancesOCTRAI.'-'.
			// $waterChargesProvisionalAmount.'-'.
			// $otherCharges.'-'.
			// $roundAdjustmentAmount.'-'.
			// $adjustmentAmountSOP.'-'.
			// $adjustmentAmountED.'-'.
			// $adjustmentAmountOCTRAI.'-'.
			// $provisionalAdjustmentAmountSOP.'-'.
			// $provisionalAdjustmentAmountED.'-'.
			// $provisionalAdjustmentAmountOCTRAI.'-'.
			// $arrearSOPCurrentYear.'-'.
			// $arrearEDCurrentYear.'-'.
			// $arrearOCTRAICurrentYear.'-'.
			// $arrearSOPPreviousYear.'-'.
			// $arrearEDPreviousYear.'-'.
			// $arrearOCTRAIPreviousYear.'-'.
			// $advanceConsumptionDeposit.'-'.
			// $courtCaseAmount.'-'.
			// $previousKWHCycle1.'-'.
			// $previousKWHCycle2.'-'.
			// $previousKWHCycle3.'-'.
			// $previousKWHCycle4.'-'.
			// $previousKWHCycle5.'-'.
			// $previousKWHCycle6.'-'.
			// $averageKWHofAbove6Cycles.'-'.
			// $samePeriodLastYearConsumptionKWH.'-'.
			// $PF1.'-'.
			// $PF2.'-'.
			// $PF3.'-'.
			// $averageofAboveThreePF.'-'.
			// $standardPFOfConsumerCategory.'-'.
			// $MD1.'-'.
			// $MD2.'-'.
			// $gaushalaFlag.'-'.
			// $arrearWaterChargesCurrentYear.'-'.
			// $arrearWaterChargesPreviousYear.'-'.
			// $dataExpiryDate.'-'.
			// $maxOfAboveSixMDI.'-'.
			// $factorForCustomerCategory.'-'.
			// $DValueInLDHFNumberOfWorkingDaysPerMonthForCustomerCategory.'-'.
			// $HValueInLDHFSupplyHoursPerDayForCustomerCategory.'-'.
			// $previousTotalEstimatedConsumptionKWH.'-'.
			// $previousTotalEstimatedConsumptionKVAH.'-'.
			// $previousEstimatedMDI.'-'.
			// $nearestCashCounter.'-'.
			// $complaintCenterPhoneNumber.'-'.
			// $previousPaymentStatus.'-'.
			// $reasonForAdjustment.'-'.
			// $miscExpensesDetails.'-'.
			// $miscExpenses.'-'.
			// $meterSecurityAmount.'-'.
			// $waterChargesApplicableFlag.'-'.
			// $transformerCode.'-'.
			// $oldMeterRent.'-'.
			// $expirationDateOfOldMeterRent.'-'.
			// $oldMBCRent.'-'.
			// $expirationDateOfOldMBCRent.'-'.
			// $oldServiceCharges.'-'.
			// $expirationDateOfOldServiceCharges.'-'.
			// $meterLocation.'-'.
			// $meterType.'-'.
			// $version.'-'.
			// $ML.'-'.
			// $MT.'-'.
			// $mobileNo.'-'.
			// $feederCode.'-'.
			// $SCWSDAmountWithHeld.'-'.
			// $previousFyCons.'-'.
			// $muncipalTaxApplicableFlag.'-'.
			// $PLogicApplicableFlag.'-'.
			// $limitForPNore.'-'.
			// $created_at.'-'.
			// $created_at;
			// $sap_log = "INSERT INTO `sap_log` (ip) VALUES ('$postValues')";
			// mysqli_query($conn,$sap_log);
			$query = "INSERT INTO `sap_server` (subDivisionCode, subDivisionName, MRU, MR_DOCNumber, neighbourMeterId, connectedPoleNINNumber, consumerLegacyNumber, contractAcNumber, consumerName, houseNumber, streetNumber, address, category, subCategory, moveInFlag, categoryChangeFlag, categoryChangeDate, oldCategory, oldSubCategory, industryCode, billingCycle, billingCycleType, billingGroup, triVectorMeterFlag, installation, meterMnfSerialNumber, meterManufacturerName, meterSAPSerialNumber, multiplicationFactor, overallMf, oldMf, numberOfDigits, lineCtRatio, externalPtRatio, phaseCode, sanctionedLoadKW, contractedDemandKVA, sanctionedLoadChangeFlag, sanctionedLoadChangeDate, oldSanctionedLoadKW, admissibleVoltage, supplyVoltage, noOfBOardEmployees6, noOfBOardEmployees7, noOfBOardEmployees8, scheduleMeterReadDate, previousReadingDate, previousBillDate, previousReadingKWH, previousReadingKVA, previousReadingKVAH, previousMeterStatus, previousBillType, meterChangeFlag, meterChangeDate, totalOldMeterConsumptionUnitsKWH, totalOldMeterConsumptionUnitsKVAH, oldMeterTotalRentAmount, oldMeterTotalServiceRentAmount, oldMeterTotalServiceChargeAmount, oldMeterTotalMBCRentAmount, newMeterInitialReadingKWH, newMeterInitialReadingKVAH, reconnectionFlag, reconnectionDate, consumptionBeforeDisConnectionKWH, consumptionBeforeDisConnectionKVAH, periodBeforeDisConnection, edExemptedFlag, edExemptionExpiry, octraiChargesApplicableFlag, meterRentRate, serviceRentRate, peakLoadExemptionCharges, peakLoadExemptionChargesExpiry, MMTSCorrectionFactor, shuntCapacitorChargesApplicableFlag, capacityOfCapacitor, MBCRentRate, serviceChargeRate, seasonStartDate, seasonEndDate, sundryChargesSOP, sundryChargesED, sundryChargesOCTRAI, sundryAllowancesSOP, sundryAllowancesED, sundryAllowancesOCTRAI, waterChargesProvisionalAmount, otherCharges, roundAdjustmentAmount, adjustmentAmountSOP, adjustmentAmountED, adjustmentAmountOCTRAI, provisionalAdjustmentAmountSOP, provisionalAdjustmentAmountED, provisionalAdjustmentAmountOCTRAI, arrearSOPCurrentYear, arrearEDCurrentYear, arrearOCTRAICurrentYear, arrearSOPPreviousYear, arrearEDPreviousYear, arrearOCTRAIPreviousYear, advanceConsumptionDeposit, courtCaseAmount, previousKWHCycle1, previousKWHCycle2, previousKWHCycle3, previousKWHCycle4, previousKWHCycle5, previousKWHCycle6, averageKWHofAbove6Cycles, samePeriodLastYearConsumptionKWH, PF1, PF2, PF3, averageofAboveThreePF, standardPFOfConsumerCategory, MD1, MD2, gaushalaFlag, arrearWaterChargesCurrentYear, arrearWaterChargesPreviousYear, dataExpiryDate, maxOfAboveSixMDI, factorDay, dValueDays, hValueDay, previousTotalEstimatedConsumptionKWH, previousTotalEstimatedConsumptionKVAH, previousEstimatedMDI, nearestCashCounter, complaintCenterPhoneNumber, previousPaymentStatus, reasonForAdjustment, miscExpensesDetails, miscExpenses, meterSecurityAmount, waterChargesApplicableFlag, transformerCode, oldMeterRent, expirationDateOfOldMeterRent, oldMBCRent, expirationDateOfOldMBCRent, oldServiceCharges, expirationDateOfOldServiceCharges, meterLocation, meterType, version, ML, MT, mobileNo, feederCode, SCWSDAmountWithHeld, previousFyCons, muncipalTaxApplicableFlag, PLogicApplicableFlag, limitForPNore, created_at, updated_at)
 		 VALUES (
 			 '$subDivisionCode',
 			 '$subDivisionName',
 			 '$MRU',
 			 '$MR_DOCNumber',
 			 '$neighbourMeterId',
 			 '$connectedPoleNINNumber',
 			 '$consumerLegacyNumber',
 			 '$contractAcNumber',
 			 '$consumerName',
 			 '$houseNumber',
 			 '$streetNumber',
 			 '$address',
 			 '$category',
 			 '$subCategory',
 			 '$moveInFlag',
 			 '$categoryChangeFlag',
 			 '$categoryChangeDate',
 			 '$oldCategory',
 			 '$oldSubCategory',
 			 '$industryCode',
 			 '$billingCycle',
 			 '$billingCycleType',
 			 '$billingGroup',
 			 '$triVectorMeterFlag',
 			 '$installation',
 			 '$meterMnfSerialNumber',
 			 '$meterManufacturerName',
 			 '$meterSAPSerialNumber',
 			 '$multiplicationFactor',
 			 '$overallMf',
 			 '$oldMf',
 			 '$numberOfDigits',
 			 '$lineCtRatio',
 			 '$externalPtRatio',
 			 '$phaseCode',
 			 '$sanctionedLoadKW',
 			 '$contractedDemandKVA',
 			 '$sanctionedLoadChangeFlag',
 			 '$sanctionedLoadChangeDate',
 			 '$oldSanctionedLoadKW',
 			 '$admissibleVoltage',
 			 '$supplyVoltage',
 			 '$noOfBOardEmployees6',
 			 '$noOfBOardEmployees7',
 			 '$noOfBOardEmployees8',
 			 '$scheduleMeterReadDate',
 			 '$previousReadingDate',
 			 '$previousBillDate',
 			 '$previousReadingKWH',
 			 '$previousReadingKVA',
 			 '$previousReadingKVAH',
 			 '$previousMeterStatus',
 			 '$previousBillType',
 			 '$meterChangeFlag',
 			 '$meterChangeDate',
 			 '$totalOldMeterConsumptionUnitsKWH',
 			 '$totalOldMeterConsumptionUnitsKVAH',
 			 '$oldMeterTotalRentAmount',
 			 '$oldMeterTotalServiceRentAmount',
 			 '$oldMeterTotalServiceChargeAmount',
 			 '$oldMeterTotalMBCRentAmount',
 			 '$newMeterInitialReadingKWH',
 			 '$newMeterInitialReadingKVAH',
 			 '$reconnectionFlag',
 			 '$reconnectionDate',
 			 '$consumptionBeforeDisConnectionKWH',
 			 '$consumptionBeforeDisConnectionKVAH',
 			 '$periodBeforeDisConnection',
 			 '$edExemptedFlag',
 			 '$edExemptionExpiry',
 			 '$octraiChargesApplicableFlag',
 			 '$meterRentRate',
 			 '$serviceRentRate',
 			 '$peakLoadExemptionCharges',
 			 '$peakLoadExemptionChargesExpiry',
 			 '$MMTSCorrectionFactor',
 			 '$shuntCapacitorChargesApplicableFlag',
 			 '$capacityOfCapacitor',
 			 '$MBCRentRate',
 			 '$serviceChargeRate',
 			 '$seasonStartDate',
 			 '$seasonEndDate',
 			 '$sundryChargesSOP',
 			 '$sundryChargesED',
 			 '$sundryChargesOCTRAI',
 			 '$sundryAllowancesSOP',
 			 '$sundryAllowancesED',
 			 '$sundryAllowancesOCTRAI',
 			 '$waterChargesProvisionalAmount',
 			 '$otherCharges',
 			 '$roundAdjustmentAmount',
 			 '$adjustmentAmountSOP',
 			 '$adjustmentAmountED',
 			 '$adjustmentAmountOCTRAI',
 			 '$provisionalAdjustmentAmountSOP',
 			 '$provisionalAdjustmentAmountED',
 			 '$provisionalAdjustmentAmountOCTRAI',
 			 '$arrearSOPCurrentYear',
 			 '$arrearEDCurrentYear',
 			 '$arrearOCTRAICurrentYear',
 			 '$arrearSOPPreviousYear',
 			 '$arrearEDPreviousYear',
 			 '$arrearOCTRAIPreviousYear',
 			 '$advanceConsumptionDeposit',
 			 '$courtCaseAmount',
 			 '$previousKWHCycle1',
 			 '$previousKWHCycle2',
 			 '$previousKWHCycle3',
 			 '$previousKWHCycle4',
 			 '$previousKWHCycle5',
 			 '$previousKWHCycle6',
 			 '$averageKWHofAbove6Cycles',
 			 '$samePeriodLastYearConsumptionKWH',
 			 '$PF1',
 			 '$PF2',
 			 '$PF3',
 			 '$averageofAboveThreePF',
 			 '$standardPFOfConsumerCategory',
 			 '$MD1',
 			 '$MD2',
 			 '$gaushalaFlag',
 			 '$arrearWaterChargesCurrentYear',
 			 '$arrearWaterChargesPreviousYear',
 			 '$dataExpiryDate',
 			 '$maxOfAboveSixMDI',
 			 '$factorForCustomerCategory',
 			 '$DValueInLDHFNumberOfWorkingDaysPerMonthForCustomerCategory',
 			 '$HValueInLDHFSupplyHoursPerDayForCustomerCategory',
 			 '$previousTotalEstimatedConsumptionKWH',
 			 '$previousTotalEstimatedConsumptionKVAH',
 			 '$previousEstimatedMDI',
 			 '$nearestCashCounter',
 			 '$complaintCenterPhoneNumber',
 			 '$previousPaymentStatus',
 			 '$reasonForAdjustment',
 			 '$miscExpensesDetails',
 			 '$miscExpenses',
 			 '$meterSecurityAmount',
 			 '$waterChargesApplicableFlag',
 			 '$transformerCode',
 			 '$oldMeterRent',
 			 '$expirationDateOfOldMeterRent',
 			 '$oldMBCRent',
 			 '$expirationDateOfOldMBCRent',
 			 '$oldServiceCharges',
 			 '$expirationDateOfOldServiceCharges',
 			 '$meterLocation',
 			 '$meterType',
 			 '$version',
 			 '$ML',
 			 '$MT',
 			 '$mobileNo',
 			 '$feederCode',
 			 '$SCWSDAmountWithHeld',
 			 '$previousFyCons',
 			 '$muncipalTaxApplicableFlag',
 			 '$PLogicApplicableFlag',
 			 '$limitForPNore',
 			 '$created_at',
 			 '$created_at'
 			 )";

 			// $result = mysqli_query($conn,$query);
 			if (mysqli_query($conn,$query) === TRUE) {
 				$result = [
 					'mr' => $MR_DOCNumber,
 					'status' => 'S',
 					'msg' => ''
 				];
 			} else {
 				$result = [
 				 'mr' => $MR_DOCNumber,
 				 'status' => 'N',
 				 'msg' => mysqli_error($conn)
 			 ];
 			}
		}

			return $result;
	}
	return false;
}

require('lib/nusoap.php');

$server = new soap_server();
$server->configureWSDL('SAP_RequestParams', 'tem:managedata');

$server->register(
'push_SAP_RequestParams',
array(
	'subDivisionCode' =>'xsd:string',
	'subDivisionName' =>'xsd:string',
	'MRU' =>'xsd:string',
	'MR_DOCNumber' =>'xsd:string',
	'neighbourMeterId' =>'xsd:string',
	'connectedPoleNINNumber' =>'xsd:string',
	'consumerLegacyNumber' =>'xsd:string',
	'contractAcNumber' =>'xsd:string',
	'consumerName' =>'xsd:string',
	'houseNumber' =>'xsd:string',
	'streetNumber' =>'xsd:string',
	'address' =>'xsd:string',
	'category' =>'xsd:string',
	'subCategory' =>'xsd:string',
	'moveInFlag' =>'xsd:string',
	'categoryChangeFlag' =>'xsd:string',
	'categoryChangeDate' =>'xsd:string',
	'oldCategory' =>'xsd:string',
	'oldSubCategory' =>'xsd:string',
	'industryCode' =>'xsd:string',
	'billingCycle' =>'xsd:string',
	'billingCycleType' =>'xsd:string',
	'billingGroup' =>'xsd:string',
	'triVectorMeterFlag' =>'xsd:string',
	'installation' =>'xsd:string',
	'meterMnfSerialNumber' =>'xsd:string',
	'meterManufacturerName' =>'xsd:string',
	'meterSAPSerialNumber' =>'xsd:string',
	'multiplicationFactor' =>'xsd:string',
	'overallMf' =>'xsd:string',
	'oldMf' =>'xsd:string',
	'numberOfDigits' =>'xsd:string',
	'lineCtRatio' =>'xsd:string',
	'externalPtRatio' =>'xsd:string',
	'phaseCode' =>'xsd:string',
	'sanctionedLoadKW' =>'xsd:string',
	'contractedDemandKVA' =>'xsd:string',
	'sanctionedLoadChangeFlag' =>'xsd:string',
	'sanctionedLoadChangeDate' =>'xsd:string',
	'oldSanctionedLoadKW' =>'xsd:string',
	'admissibleVoltage' =>'xsd:string',
	'supplyVoltage' =>'xsd:string',
	'noOfBOardEmployees6' =>'xsd:string',
	'noOfBOardEmployees7' =>'xsd:string',
	'noOfBOardEmployees8' =>'xsd:string',
	'scheduleMeterReadDate' =>'xsd:string',
	'previousReadingDate' =>'xsd:string',
	'previousBillDate' =>'xsd:string',
	'previousReadingKWH' =>'xsd:string',
	'previousReadingKVA' =>'xsd:string',
	'previousReadingKVAH' =>'xsd:string',
	'previousMeterStatus' =>'xsd:string',
	'previousBillType' =>'xsd:string',
	'meterChangeFlag' =>'xsd:string',
	'meterChangeDate' =>'xsd:string',
	'totalOldMeterConsumptionUnitsKWH' =>'xsd:string',
	'totalOldMeterConsumptionUnitsKVAH' =>'xsd:string',
	'oldMeterTotalRentAmount' =>'xsd:string',
	'oldMeterTotalServiceRentAmount' =>'xsd:string',
	'oldMeterTotalServiceChargeAmount' =>'xsd:string',
	'oldMeterTotalMBCRentAmount' =>'xsd:string',
	'newMeterInitialReadingKWH' =>'xsd:string',
	'newMeterInitialReadingKVAH' =>'xsd:string',
	'reconnectionFlag' =>'xsd:string',
	'reconnectionDate' =>'xsd:string',
	'consumptionBeforeDisConnectionKWH' =>'xsd:string',
	'consumptionBeforeDisConnectionKVAH' =>'xsd:string',
	'periodBeforeDisConnection' =>'xsd:string',
	'edExemptedFlag' =>'xsd:string',
	'edExemptionExpiry' =>'xsd:string',
	'octraiChargesApplicableFlag' =>'xsd:string',
	'meterRentRate' =>'xsd:string',
	'serviceRentRate' =>'xsd:string',
	'peakLoadExemptionCharges' =>'xsd:string',
	'peakLoadExemptionChargesExpiry' =>'xsd:string',
	'MMTSCorrectionFactor' =>'xsd:string',
	'shuntCapacitorChargesApplicableFlag' =>'xsd:string',
	'capacityOfCapacitor' =>'xsd:string',
	'MBCRentRate' =>'xsd:string',
	'serviceChargeRate' =>'xsd:string',
	'seasonStartDate' =>'xsd:string',
	'seasonEndDate' =>'xsd:string',
	'sundryChargesSOP' =>'xsd:string',
	'sundryChargesED' =>'xsd:string',
	'sundryChargesOCTRAI' =>'xsd:string',
	'sundryAllowancesSOP' =>'xsd:string',
	'sundryAllowancesED' =>'xsd:string',
	'sundryAllowancesOCTRAI' =>'xsd:string',
	'waterChargesProvisionalAmount' =>'xsd:string',
	'otherCharges' =>'xsd:string',
	'roundAdjustmentAmount' =>'xsd:string',
	'adjustmentAmountSOP' =>'xsd:string',
	'adjustmentAmountED' =>'xsd:string',
	'adjustmentAmountOCTRAI' =>'xsd:string',
	'provisionalAdjustmentAmountSOP' =>'xsd:string',
	'provisionalAdjustmentAmountED' =>'xsd:string',
	'provisionalAdjustmentAmountOCTRAI' =>'xsd:string',
	'arrearSOPCurrentYear' =>'xsd:string',
	'arrearEDCurrentYear' =>'xsd:string',
	'arrearOCTRAICurrentYear' =>'xsd:string',
	'arrearSOPPreviousYear' =>'xsd:string',
	'arrearEDPreviousYear' =>'xsd:string',
	'arrearOCTRAIPreviousYear' =>'xsd:string',
	'advanceConsumptionDeposit' =>'xsd:string',
	'courtCaseAmount' =>'xsd:string',
	'previousKWHCycle1' =>'xsd:string',
	'previousKWHCycle2' =>'xsd:string',
	'previousKWHCycle3' =>'xsd:string',
	'previousKWHCycle4' =>'xsd:string',
	'previousKWHCycle5' =>'xsd:string',
	'previousKWHCycle6' =>'xsd:string',
	'averageKWHofAbove6Cycles' =>'xsd:string',
	'samePeriodLastYearConsumptionKWH' =>'xsd:string',
	'PF1' =>'xsd:string',
	'PF2' =>'xsd:string',
	'PF3' =>'xsd:string',
	'averageofAboveThreePF' =>'xsd:string',
	'standardPFOfConsumerCategory' =>'xsd:string',
	'MD1' =>'xsd:string',
	'MD2' =>'xsd:string',
	'gaushalaFlag' =>'xsd:string',
	'arrearWaterChargesCurrentYear' =>'xsd:string',
	'arrearWaterChargesPreviousYear' =>'xsd:string',
	'dataExpiryDate' =>'xsd:string',
	'maxOfAboveSixMDI' =>'xsd:string',
	'factorForCustomerCategory' =>'xsd:string',
	'DValueInLDHFNumberOfWorkingDaysPerMonthForCustomerCategory' =>'xsd:string',
	'HValueInLDHFSupplyHoursPerDayForCustomerCategory' =>'xsd:string',
	'previousTotalEstimatedConsumptionKWH' =>'xsd:string',
	'previousTotalEstimatedConsumptionKVAH' =>'xsd:string',
	'previousEstimatedMDI' =>'xsd:string',
	'nearestCashCounter' =>'xsd:string',
	'complaintCenterPhoneNumber' =>'xsd:string',
	'previousPaymentStatus' =>'xsd:string',
	'reasonForAdjustment' =>'xsd:string',
	'miscExpensesDetails' =>'xsd:string',
	'miscExpenses' =>'xsd:string',
	'meterSecurityAmount' =>'xsd:string',
	'waterChargesApplicableFlag' =>'xsd:string',
	'transformerCode' =>'xsd:string',
	'oldMeterRent' =>'xsd:string',
	'expirationDateOfOldMeterRent' =>'xsd:string',
	'oldMBCRent' =>'xsd:string',
	'expirationDateOfOldMBCRent' =>'xsd:string',
	'oldServiceCharges' =>'xsd:string',
	'expirationDateOfOldServiceCharges' =>'xsd:string',
	'meterLocation' =>'xsd:string',
	'meterType' =>'xsd:string',
	'version' =>'xsd:string',
	'ML' =>'xsd:string',
	'MT' =>'xsd:string',
	'mobileNo' =>'xsd:string',
	'feederCode' =>'xsd:string',
	'SCWSDAmountWithHeld' =>'xsd:string',
	'previousFyCons' =>'xsd:string',
	'muncipalTaxApplicableFlag' =>'xsd:string',
	'PLogicApplicableFlag' =>'xsd:string',
	'limitForPNore' =>'xsd:string'
),
array('mr' => 'xsd:string', 'status' => 'xsd:string', 'msg' => 'xsd:string'),
	'tem:myserviceswsdl',
	'tem:myserviceswsdl#push_SAP_RequestParams',
	'rpc',
	'literal',
	'insert data into mysql'
);

$server->service(file_get_contents("php://input"));

?>
