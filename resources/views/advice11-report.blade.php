@php
  use App\User;
  use App\Bill;
  use App\sapInput;
@endphp
<!doctype html>
<!--[if lte IE 9]>     <html lang="en" class="no-focus lt-ie10 lt-ie10-msg"> <![endif]-->
<!--[if gt IE 9]><!--> <html lang="en" class="no-focus"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">

        <title>Advice 11 | STERLING Transformers</title>

        <meta name="description" content="Codebase - Bootstrap 4 Admin Template &amp; UI Framework created by pixelcave and published on Themeforest">
        <meta name="author" content="pixelcave">
        <meta name="robots" content="noindex, nofollow">
        <meta name="csrf-token" content="{{ csrf_token() }}" />

        <!-- Open Graph Meta -->
        <meta property="og:title" content="Codebase - Bootstrap 4 Admin Template &amp; UI Framework">
        <meta property="og:site_name" content="Codebase">
        <meta property="og:description" content="Codebase - Bootstrap 4 Admin Template &amp; UI Framework created by pixelcave and published on Themeforest">
        <meta property="og:type" content="website">
        <meta property="og:url" content="">
        <meta property="og:image" content="">

        <!-- Icons -->
        <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
        <link rel="shortcut icon" href="{{ config('app.url') }}/assets/img/favicons/favicon.png">
        <link rel="icon" type="image/png" sizes="192x192" href="{{ config('app.url') }}/assets/img/favicons/favicon-192x192.png">
        <link rel="apple-touch-icon" sizes="180x180" href="{{ config('app.url') }}/assets/img/favicons/apple-touch-icon-180x180.png">
        <!-- END Icons -->

        <!-- Stylesheets -->
        <!-- Page JS Plugins CSS -->
        <link rel="stylesheet" href="{{ config('app.url') }}/assets/js/plugins/datatables/dataTables.bootstrap4.min.css">

        <!-- Codebase framework -->
        <link rel="stylesheet" id="css-main" href="{{ config('app.url') }}/assets/css/codebase.min.css">

        <!-- Stylesheets -->
        <link rel="stylesheet" href="{{ config('app.url') }}/assets/js/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css"> {{--
        <link rel="stylesheet" href="assets/js/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css"> --}}
        <link rel="stylesheet" href="{{ config('app.url') }}/assets/js/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css">
        {{-- <link rel="stylesheet" href="{{ config('app.url') }}/assets/js/plugins/select2/select2.min.css">
        <link rel="stylesheet" href="{{ config('app.url') }}/assets/js/plugins/select2/select2-bootstrap.min.css">
        <link rel="stylesheet" href="{{ config('app.url') }}/assets/js/plugins/jquery-tags-input/jquery.tagsinput.min.css">
        <link rel="stylesheet" href="{{ config('app.url') }}/assets/js/plugins/jquery-auto-complete/jquery.auto-complete.min.css">
        <link rel="stylesheet" href="{{ config('app.url') }}/assets/js/plugins/ion-rangeslider/css/ion.rangeSlider.min.css">
        <link rel="stylesheet" href="{{ config('app.url') }}/assets/js/plugins/ion-rangeslider/css/ion.rangeSlider.skinHTML5.min.css">
        <link rel="stylesheet" href="{{ config('app.url') }}/assets/js/plugins/dropzonejs/min/dropzone.min.css"> --}}
        <!-- Codebase framework -->
        <link rel="stylesheet" id="css-main" href="{{ config('app.url') }}/assets/css/codebase.min.css">

        <!-- You can include a specific file from css/themes/ folder to alter the default color theme of the template. eg: -->
        <!-- <link rel="stylesheet" id="css-theme" href="assets/css/themes/flat.min.css"> -->
        <!-- END Stylesheets -->
    </head>
    <body>
        <!-- Page Container -->
        <!--
            Available classes for #page-container:

        GENERIC

            'enable-cookies'                            Remembers active color theme between pages (when set through color theme helper Codebase() -> uiHandleTheme())

        SIDEBAR & SIDE OVERLAY

            'sidebar-r'                                 Right Sidebar and left Side Overlay (default is left Sidebar and right Side Overlay)
            'sidebar-mini'                              Mini hoverable Sidebar (screen width > 991px)
            'sidebar-o'                                 Visible Sidebar by default (screen width > 991px)
            'sidebar-o-xs'                              Visible Sidebar by default (screen width < 992px)
            'sidebar-inverse'                           Dark themed sidebar

            'side-overlay-hover'                        Hoverable Side Overlay (screen width > 991px)
            'side-overlay-o'                            Visible Side Overlay by default

            'side-scroll'                               Enables custom scrolling on Sidebar and Side Overlay instead of native scrolling (screen width > 991px)

        HEADER

            ''                                          Static Header if no class is added
            'page-header-fixed'                         Fixed Header

        HEADER STYLE

            ''                                          Classic Header style if no class is added
            'page-header-modern'                        Modern Header style
            'page-header-inverse'                       Dark themed Header (works only with classic Header style)
            'page-header-glass'                         Light themed Header with transparency by default
                                                        (absolute position, perfect for light images underneath - solid light background on scroll if the Header is also set as fixed)
            'page-header-glass page-header-inverse'     Dark themed Header with transparency by default
                                                        (absolute position, perfect for dark images underneath - solid dark background on scroll if the Header is also set as fixed)

        MAIN CONTENT LAYOUT

            ''                                          Full width Main Content if no class is added
            'main-content-boxed'                        Full width Main Content with a specific maximum width (screen width > 1200px)
            'main-content-narrow'                       Full width Main Content with a percentage width (screen width > 1200px)
        -->
        <div id="page-container" class="sidebar-o side-scroll page-header-modern main-content-boxed">
            <!-- Side Overlay-->
            @include('includes/headerAndSidebar')
            <!-- END Header -->

            <!-- Main Container -->
            <main id="main-container">
                <!-- Page Content -->
                <div class="content">
                    <h2 class="content-heading">Mini Ledger Non SAP</h2>

                    <!-- Dynamic Table Full -->
                    <div class="block">
                        {{-- <div class="block-header block-header-default">
                            <h3 class="block-title">Dynamic Table <small></small></h3>
                        </div> --}}
                        <div class="block-content block-content-full">
                            <!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/be_tables_datatables.js -->
                              <form class="" action="{{ route('reportPost') }}" method="post" id="bindLedgerSap">
                                @csrf
                                <div class="form-group row">
                                  <div class="col-md-2">
                                      <label for="subdivisionCode">Sub Division Code <span class="text-danger">*</span> </label>
                                      <select class="form-control" name="subdivisionCode" id="subdivisionCode" required>
                                              <option value="">Select</option>
                                              <option value="H11">H11</option>
                                              <option value="H12">H12</option>
                                              <option value="H13">H13</option>
                                              <option value="H14">H14</option>
                                              <option value="H15">H15</option>
                                              <option value="H21">H21</option>
                                              <option value="H22">H22</option>
                                              <option value="H23">H23</option>
                                              <option value="H24">H24</option>
                                              <option value="H25">H25</option>
                                              <option value="H26">H26</option>
                                              <option value="H27">H27</option>
                                              <option value="H31">H31</option>
                                              <option value="H32">H32</option>
                                              <option value="H33">H33</option>
                                              <option value="J12">H12</option>
                                              <option value="H34">H34</option>
                                              <option value="H35">H35</option>
                                              <option value="H36">H36</option>
                                              <option value="H39">H39</option>
                                              <option value="H41">H41</option>
                                              <option value="H42">H42</option>
                                              <option value="H43">H43</option>
                                              <option value="H44">H44</option>
                                              <option value="J11">J11</option>
                                              <option value="J13">J13</option>
                                              <option value="J14">J14</option>
                                              <option value="J15">J15</option>
                                              <option value="J16">J16</option>
                                              <option value="J17">J17</option>
                                              <option value="J21">J21</option>
                                              <option value="J22">J22</option>
                                              <option value="J23">J23</option>
                                              <option value="J41">J41</option>
                                              <option value="J42">J42</option>
                                              <option value="J43">J43</option>
                                              <option value="J44">J44</option>
                                              <option value="J45">J45</option>
                                              <option value="J46">J46</option>
                                              <option value="J51">J51</option>
                                              <option value="J52">J52</option>
                                              <option value="J53">J53</option>
                                              <option value="J54">J54</option>
                                              <option value="J55">H12</option>
                                              <option value="J56">J56</option>
                                              <option value="N21">N21</option>
                                              <option value="N22">N22</option>
                                              <option value="N23">N23</option>
                                              <option value="N24">N24</option>
                                              <option value="N25">N25</option>
                                              <option value="N26">N26</option>
                                              <option value="N31">N31</option>
                                              <option value="N32">N32</option>
                                              <option value="N33">N33</option>
                                              <option value="N34">N34</option>
                                              <option value="N35">N35</option>
                                              <option value="N36">N36</option>
                                              <option value="N41">N41</option>
                                              <option value="N42">N42</option>
                                              <option value="N43">N43</option>
                                              <option value="N44">N44</option>
                                              <option value="N45">N45</option>
                                              <option value="X21">X21</option>
                                              <option value="X22">X22</option>
                                              <option value="X23">X23</option>
                                              <option value="X24">X24</option>
                                              <option value="X25">X25</option>
                                              <option value="X26">X26</option>
                                              <option value="X31">X31</option>
                                              <option value="X32">X32</option>
                                              <option value="X33">X33</option>
                                              <option value="X34">X34</option>
                                              <option value="X35">X35</option>
                                              <option value="X41">X41</option>
                                              <option value="X42">X42</option>
                                              <option value="X43">X43</option>
                                              <option value="X44">X44</option>
                                              <option value="X45">X45</option>
                                              <option value="X51">X51</option>
                                              <option value="X52">X52</option>
                                              <option value="X53">X53</option>
                                              <option value="X54">X54</option>
                                              <option value="X55">X55</option>
                                              <option value="X56">X56</option>
                                              <option value="X11">X11</option>
                                              <option value="X12">X12</option>
                                              <option value="X13">X13</option>
                                              <option value="X14">X14</option>
                                              <option value="X15">X15</option>
                                              <option value="X16">X16</option>
                                            </select>
                                      @if ($errors->has('subdivisionCode'))
                                      <span class="help-block">
                                       <strong class="text-danger">{{ $errors->first('subdivisionCode') }}</strong>
                                        </span> @endif
                                  </div>
                                  <div class="col-md-2">
                                      <label for="billingGroup">Billing Group <span class="text-danger">*</span> </label>
                                      <select class="form-control" name="billingGroup" id="billingGroup" required>
                                        <option value="">Select</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="8">8</option>
                                      </select>
                                      @if ($errors->has('billingGroup'))
                                      <span class="help-block">
                                       <strong class="text-danger">{{ $errors->first('billingGroup') }}</strong>
                                        </span> @endif
                                  </div>


                                      <div class="col-md-1">
                                        <label for="accountNoL">. </label>
                                          <button type="submit" class="btn btn-alt-primary">Submit</button>
                                      </div>


                                  </div>

                              </form>

                            @if (isset($data) && $data != '')
                              <table class="table-bordered js-dataTable-full">
                                  <thead>
                                      <tr>
                                          <td>S.No</td>
                                          <td>Sys Id</td>
                                          <td>tr Type</td>
                                          <td>sub div Code</td>
                                          <td>octrai Code</td>
                                          <td>ledger Group</td>
                                          <td>bg</td>
                                          <td>bsg</td>
                                          <td>village Name</td>
                                          <td>ac No</td>
                                          <td>consumer</td>
                                          <td>Action</td>
                                      </tr>
                                  </thead>
                                  <tbody>

                                      @php
                                        $report = $data;
                                        $sno = 1;
                                      @endphp
                                      @foreach ($report as $record)
                                        <tr>
                                            <td class="">{{ $sno++ }}</td>
                                            <td class="">{{ $record->sysId }}</td>
                                            <td class="">{{ $record->trType }}</td>
                                            <td class="">{{ $record->subdivisionCode }}</td>
                                            <td class="">{{ $record->octraiCode }}</td>
                                            <td class="">{{ $record->ledgerGroup }}</td>
                                            <td class="">{{ $record->billingGroup }}</td>
                                            <td class="">{{ $record->billingSubGroup }}</td>
                                            <td class="">{{ $record->villageName }}</td>
                                            <td class="">{{ $record->accountNo }}</td>
                                            <td class="">{{ $record->consumerName }}</td>
                                            <td class=""><a href="#">Edit</a> / <a href="#">Delete</a></td>
                                        </tr>

                                      @endforeach

                                  </tbody>
                              </table>
                            @endif
                        </div>
                    </div>
                    <!-- END Dynamic Table Full -->
                </div>
                <!-- END Page Content -->
            </main>
            <!-- END Main Container -->

            <!-- Footer -->
            @include('includes/footer')
            <!-- END Footer -->
        </div>
        <!-- END Page Container -->

        <!-- Codebase Core JS -->
        <script src="{{ config('app.url') }}/assets/js/core/jquery.min.js"></script>
        <script src="{{ config('app.url') }}/assets/js/core/bootstrap.bundle.min.js"></script>
        <script src="{{ config('app.url') }}/assets/js/core/jquery.slimscroll.min.js"></script>
        <script src="{{ config('app.url') }}/assets/js/core/jquery.scrollLock.min.js"></script>
        <script src="{{ config('app.url') }}/assets/js/core/jquery.appear.min.js"></script>
        <script src="{{ config('app.url') }}/assets/js/core/jquery.countTo.min.js"></script>
        <script src="{{ config('app.url') }}/assets/js/core/js.cookie.min.js"></script>
        <script src="{{ config('app.url') }}/assets/js/codebase.js"></script>

        <!-- Page JS Plugins -->
        {{-- <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script> --}}
        <script src="{{ config('app.url') }}/assets/js/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="{{ config('app.url') }}/assets/js/plugins/datatables/dataTables.bootstrap4.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>

        <!-- Page JS Code -->
        <script src="{{ config('app.url') }}/assets/js/pages/be_tables_datatables.js"></script>
        <script src="{{ config('app.url') }}/assets/js/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
        {{-- <script src="{{ config('app.url') }}/assets/js/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script> --}}
        {{-- <script src="{{ config('app.url') }}/assets/js/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js"></script> --}}
        {{-- <script src="{{ config('app.url') }}/assets/js/plugins/select2/select2.full.min.js"></script> --}}
        <script src="{{ config('app.url') }}/assets/js/plugins/jquery-tags-input/jquery.tagsinput.min.js"></script>
        <script src="{{ config('app.url') }}/assets/js/plugins/jquery-auto-complete/jquery.auto-complete.min.js"></script>
        {{-- <script src="{{ config('app.url') }}/assets/js/plugins/masked-inputs/jquery.maskedinput.min.js"></script> --}}
        {{-- <script src="{{ config('app.url') }}/assets/js/plugins/ion-rangeslider/js/ion.rangeSlider.min.js"></script> --}}
        {{-- <script src="{{ config('app.url') }}/assets/js/plugins/dropzonejs/min/dropzone.min.js"></script> --}}

        <!-- Page JS Code -->
        <script src="{{ config('app.url') }}/assets/js/pages/be_forms_plugins.js"></script>

        <script>
            jQuery(function() {
                // Init page helpers (BS Datepicker + BS Colorpicker + BS Maxlength + Select2 + Masked Input + Range Sliders + Tags Inputs plugins)
                Codebase.helpers(['datepicker']);
            });
        </script>

        <script type="text/javascript">
        function getGroupCycleAjaxNonSap(e) {

          $('#billingGroup')
              .empty()
              .append('<option value="">Select</option>')
          ;
          $('#billingCycle')
              .empty()
              .append('<option value="">Select</option>')
          ;
          $('#ledgerCode')
              .empty()
              .append('<option value="">Select</option>')
          ;

          // console.log(e.value);
          var subDivisionCode = e.value;
          $.ajax({
            headers: {
               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
           },
          type: "POST",
          url: '/api/getGroupCycleAjaxNonSap',
          data: {subDivisionCode: subDivisionCode},
            success: function( data ) {
                console.log(data);
                $.each(data.billingGroup, function(key, value) {
                     $('#billingGroup')
                         .append($("<option></option>")
                                    .attr("value",value.billingGroup)
                                    .text(value.billingGroup));
                });
                // $.each(data.billingCycle, function(key, value) {
                //   // console.log(value.billingCycle);
                //      $('#billingCycle')
                //          .append($("<option></option>")
                //                     .attr("value",value.billingCycle)
                //                     .text(value.billingCycle));
                // });
                // $.each(data.ledgerCodes, function(key, value) {
                //   // console.log(value);
                //      $('#ledgerCode')
                //          .append($("<option></option>")
                //                     .attr("value",value.ledgerCode)
                //                     .text(value.ledgerCode));
                // });
                // $("#ajaxResponse").append("<div>"+msg+"</div>");
            }
          });
        }

        function getGroupCycleAjaxNonSapBc(e) {

          // console.log(e.value);
          $('#billingCycle')
              .empty()
              .append('<option value="">Select</option>')
          ;
          $('#ledgerCode')
              .empty()
              .append('<option value="">Select</option>')
          ;

          // console.log(e.value);
          var subDivisionCode = $('#subDivisionCode').val();
          var billingGroup = e.value;
          $.ajax({
            headers: {
               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
           },
          type: "POST",
          url: '/api/getGroupCycleAjaxNonSapBc',
          data: {subDivisionCode: subDivisionCode, billingGroup: billingGroup},
            success: function( data ) {
                console.log(data);
                $.each(data.billingCycle, function(key, value) {
                  // console.log(value.billingCycle);
                     $('#billingCycle')
                         .append($("<option></option>")
                                    .attr("value",value.billingCycle)
                                    .text(value.billingCycle));
                });
                // $("#ajaxResponse").append("<div>"+msg+"</div>");
            }
          });
        }

          function getmeterReaders(e) {

            $('#meterReaders')
                .empty()
                .append('<option value="">Select</option>')
            ;

            // console.log(e.value);
            var subDivisionCode = $('#subDivisionCode').val();
            var billingGroup = $('#billingGroup').val();
            var billingCycle = $('#billingCycle').val();
            var from = $('#from').val();
            var to = $('#to').val();
            console.log(subDivisionCode);
            console.log(from);
            console.log(to);
            $.ajax({
              headers: {
                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
             },
            type: "POST",
            url: '/api/get-meter-reader-sub-bg-bc-non-sap',
            data: {subDivisionCode: subDivisionCode, billingGroup: billingGroup, billingCycle: billingCycle, from: from, to: to},
              success: function( data ) {
                  console.log(data);
                  $.each(data.meterReaders, function(key, value) {
                       $('#meterReaders')
                           .append($("<option></option>")
                                      .attr("value",value.meterReader)
                                      .text(value.meterReader));
                  });
              }
            });
          }



        </script>
    </body>
</html>
