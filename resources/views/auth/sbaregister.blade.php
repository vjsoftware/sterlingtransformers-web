@php
  use App\userId;
@endphp
<!doctype html>
<!--[if lte IE 9]>     <html lang="en" class="no-focus lt-ie10 lt-ie10-msg"> <![endif]-->
<!--[if gt IE 9]><!-->
<html lang="en" class="no-focus">
<!--<![endif]-->

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">

    <title>Spot Billing Assistant Registration - STERLING Transformers</title>

    <meta name="description" content="Codebase - Bootstrap 4 Admin Template &amp; UI Framework created by pixelcave and published on Themeforest">
    <meta name="author" content="pixelcave">
    <meta name="robots" content="noindex, nofollow">

    <!-- Open Graph Meta -->
    <meta property="og:title" content="Codebase - Bootstrap 4 Admin Template &amp; UI Framework">
    <meta property="og:site_name" content="Codebase">
    <meta property="og:description" content="Codebase - Bootstrap 4 Admin Template &amp; UI Framework created by pixelcave and published on Themeforest">
    <meta property="og:type" content="website">
    <meta property="og:url" content="">
    <meta property="og:image" content="">

    <!-- Icons -->
    <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
    <link rel="shortcut icon" href="{{ config('app.url') }}/assets/img/favicons/favicon.png">
    <link rel="icon" type="image/png" sizes="192x192" href="{{ config('app.url') }}/assets/img/favicons/favicon-192x192.png">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ config('app.url') }}/assets/img/favicons/apple-touch-icon-180x180.png">
    <!-- END Icons -->

    <!-- Stylesheets -->
    <!-- Codebase framework -->
    <link rel="stylesheet" id="css-main" href="{{ config('app.url') }}/assets/css/codebase.min.css">

    <!-- You can include a specific file from css/themes/ folder to alter the default color theme of the template. eg: -->
    <!-- <link rel="stylesheet" id="css-theme" href="assets/css/themes/flat.min.css"> -->
    <!-- END Stylesheets -->
</head>

<body>
    <!-- Page Container -->
    <!--
            Available classes for #page-container:

        GENERIC

            'enable-cookies'                            Remembers active color theme between pages (when set through color theme helper Codebase() -> uiHandleTheme())

        SIDEBAR & SIDE OVERLAY

            'sidebar-r'                                 Right Sidebar and left Side Overlay (default is left Sidebar and right Side Overlay)
            'sidebar-mini'                              Mini hoverable Sidebar (screen width > 991px)
            'sidebar-o'                                 Visible Sidebar by default (screen width > 991px)
            'sidebar-o-xs'                              Visible Sidebar by default (screen width < 992px)
            'sidebar-inverse'                           Dark themed sidebar

            'side-overlay-hover'                        Hoverable Side Overlay (screen width > 991px)
            'side-overlay-o'                            Visible Side Overlay by default

            'side-scroll'                               Enables custom scrolling on Sidebar and Side Overlay instead of native scrolling (screen width > 991px)

        HEADER

            ''                                          Static Header if no class is added
            'page-header-fixed'                         Fixed Header

        HEADER STYLE

            ''                                          Classic Header style if no class is added
            'page-header-modern'                        Modern Header style
            'page-header-inverse'                       Dark themed Header (works only with classic Header style)
            'page-header-glass'                         Light themed Header with transparency by default
                                                        (absolute position, perfect for light images underneath - solid light background on scroll if the Header is also set as fixed)
            'page-header-glass page-header-inverse'     Dark themed Header with transparency by default
                                                        (absolute position, perfect for dark images underneath - solid dark background on scroll if the Header is also set as fixed)

        MAIN CONTENT LAYOUT

            ''                                          Full width Main Content if no class is added
            'main-content-boxed'                        Full width Main Content with a specific maximum width (screen width > 1200px)
            'main-content-narrow'                       Full width Main Content with a percentage width (screen width > 1200px)
        -->
    <div id="page-container" class="sidebar-o side-scroll page-header-modern main-content-boxed">
        <!-- Side Overlay-->

        <!-- END Side Overlay -->

        <!-- Sidebar -->
        <!--
                Helper classes

                Adding .sidebar-mini-hide to an element will make it invisible (opacity: 0) when the sidebar is in mini mode
                Adding .sidebar-mini-show to an element will make it visible (opacity: 1) when the sidebar is in mini mode
                    If you would like to disable the transition, just add the .sidebar-mini-notrans along with one of the previous 2 classes

                Adding .sidebar-mini-hidden to an element will hide it when the sidebar is in mini mode
                Adding .sidebar-mini-visible to an element will show it only when the sidebar is in mini mode
                    - use .sidebar-mini-visible-b if you would like to be a block when visible (display: block)
            -->
        @include('includes/headerAndSidebar')

        <!-- Main Container -->
        <main id="main-container">
            <!-- Page Content -->
            <div class="content">
                <!-- Bootstrap Design -->
                <h2 class="content-heading">Create Spot Billing Assistant</h2>
                <div class="row">
                    <div class="col-md-12">
                        <!-- Default Elements -->
                        <div class="block">
                            <div class="block-header block-header-default">
                                <h3 class="block-title">Form</h3>
                            </div>
                            <div class="block-content">
                                <form class="" action="{{ route('sbaRegister') }}" method="post" enctype="multipart/form-data">
                                    @csrf
                                    <div class="form-group row">
                                        <div class="col-md-3">
                                            <label for="name">Name<span class="text-danger">*</span> </label>
                                            <input type="text" class="form-control" id="name" name="name" placeholder="Name" value="{{ old('name') }}" required> {{--
                                            <div class="form-text text-muted">Further info about this input</div> --}} @if ($errors->has('name'))
                                            <span class="help-block">
                                             <strong class="text-danger">{{ $errors->first('name') }}</strong>
                                              </span> @endif
                                        </div>
                                        <div class="col-md-3">
                                            <label for="fatherName">Father Name<span class="text-danger">*</span> </label>
                                            <input type="text" class="form-control" id="fatherName" name="fatherName" placeholder="Father Name" value="{{ old('fatherName') }}" required> {{--
                                            <div class="form-text text-muted">Further info about this input</div> --}} @if ($errors->has('fatherName'))
                                            <span class="help-block">
                                             <strong class="text-danger">{{ $errors->first('fatherName') }}</strong>
                                              </span> @endif
                                        </div>
                                        <div class="col-md-3">
                                            <label for="emailId">Email<span class="text-danger">*</span> </label>
                                            <input type="email" class="form-control" id="emailId" name="emailId" placeholder="Email Id" value="{{ old('emailId') }}" required>
                                             @if ($errors->has('emailId'))
                                            <span class="help-block">
                                             <strong class="text-danger">{{ $errors->first('emailId') }}</strong>
                                              </span>
                                             @endif
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-3">
                                            <label for="address">Address<span class="text-danger">*</span> </label>
                                            <input type="text" class="form-control" id="address" name="address" placeholder="Address" value="{{ old('address') }}" required> {{--
                                            <div class="form-text text-muted">Further info about this input</div> --}} @if ($errors->has('address'))
                                            <span class="help-block">
                                             <strong class="text-danger">{{ $errors->first('address') }}</strong>
                                              </span> @endif
                                        </div>
                                        <div class="col-md-3">
                                            <label for="permanentAddress">Permanent Address</label>
                                            <input type="text" class="form-control" id="permanentAddress" name="permanentAddress" placeholder="Permanent Address" value="{{ old('permanentAddress') }}"> {{--
                                            <div class="form-text text-muted">Further info about this input</div> --}} @if ($errors->has('permanentAddress'))
                                            <span class="help-block">
                                             <strong class="text-danger">{{ $errors->first('permanentAddress') }}</strong>
                                              </span> @endif
                                        </div>
                                        <div class="col-md-3">
                                            <label for="communicationAddress">Communication Address</label>
                                            <input type="text" class="form-control" id="communicationAddress" name="communicationAddress" placeholder="Communication Address" value="{{ old('communicationAddress') }}"> {{--
                                            <div class="form-text text-muted">Further info about this input</div> --}} @if ($errors->has('communicationAddress'))
                                            <span class="help-block">
                                             <strong class="text-danger">{{ $errors->first('communicationAddress') }}</strong>
                                              </span> @endif
                                        </div>
                                    </div>
                                    @php
                                      $userId = userId::find(1);
                                    @endphp
                                    <div class="form-group row">
                                        <div class="col-md-3">
                                            <label for="email">User Id<span class="text-danger">*</span> </label>
                                            <input type="hidden" class="form-control" id="email" name="email" placeholder="User Id" value="{{ old('email') ? old('email') : $userId->current_count }}" required>
                                            <input type="text" class="form-control" id="" name="" placeholder="User Id" value="{{ old('email') ? old('email') : $userId->current_count }}" disabled>
                                            {{--
                                            <div class="form-text text-muted">Further info about this input</div> --}}
                                            @if ($errors->has('email'))
                                            <span class="help-block">
                                             <strong class="text-danger">{{ $errors->first('email') }}</strong>
                                              </span> @endif
                                        </div>
                                        <div class="col-md-3">
                                            <label for="userArea">User Area<span class="text-danger">*</span> </label>
                                            <select class="form-control" name="userArea" id="userArea">
                                              <option value="">Select</option>
                                              <option value="1">Punjab</option>
                                              <option value="1">Huryana</option>
                                            </select>
                                            @if ($errors->has('userArea'))
                                            <span class="help-block">
                                             <strong class="text-danger">{{ $errors->first('userArea') }}</strong>
                                              </span> @endif
                                        </div>
                                        <div class="col-md-3">
                                            <label for="mobile">Mobile Number<span class="text-danger">*</span> </label>
                                            <input type="text" class="form-control" id="mobile" name="mobile" placeholder="Mobile Number" value="{{ old('mobile') }}" required> {{--
                                            <div class="form-text text-muted">Further info about this input</div> --}} @if ($errors->has('mobile'))
                                            <span class="help-block">
                                             <strong class="text-danger">{{ $errors->first('mobile') }}</strong>
                                              </span> @endif
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-3">
                                            <label for="aadhaarNo">Aadhar number<span class="text-danger">*</span> </label>
                                            <input type="text" class="form-control" id="aadhaarNo" name="aadhaarNo" placeholder="Aadhar Number" value="{{ old('aadhaarNo') }}" required> {{--
                                            <div class="form-text text-muted">Further info about this input</div> --}} @if ($errors->has('aadhaarNo'))
                                            <span class="help-block">
                                             <strong class="text-danger">{{ $errors->first('aadhaarNo') }}</strong>
                                              </span> @endif
                                        </div>
                                        <div class="col-md-3">
                                            <label for="simNumber">Sim Number</label>
                                            <input type="text" class="form-control" id="simNumber" name="simNumber" value="{{ old('simNumber') }}" placeholder="Sim Number" > {{--
                                            <div class="form-text text-muted">Further info about this input</div> --}} @if ($errors->has('simNumber'))
                                            <span class="help-block">
                                             <strong class="text-danger">{{ $errors->first('simNumber') }}</strong>
                                              </span> @endif
                                        </div>
                                        <div class="col-md-3">
                                            <label for="imeiNumber">IMEI Number</label>
                                            <input type="text" class="form-control" id="imeiNumber" name="imeiNumber" value="{{ old('imeiNumber') }}" placeholder="IMEI Number"> {{--
                                            <div class="form-text text-muted">Further info about this input</div> --}} @if ($errors->has('imeiNumber'))
                                            <span class="help-block">
                                             <strong class="text-danger">{{ $errors->first('imeiNumber') }}</strong>
                                              </span> @endif
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-3">
                                            <label for="photo">Photo<span class="text-danger">*</span> </label>
                                            <input type="file" class="form-control" id="photo" name="photo" required> {{--
                                            <div class="form-text text-muted">Further info about this input</div> --}} @if ($errors->has('photo'))
                                            <span class="help-block">
                                             <strong class="text-danger">{{ $errors->first('photo') }}</strong>
                                              </span> @endif
                                        </div>
                                        <div class="col-md-3">
                                            <label for="aadhaarPhoto">Aadhar Photo<span class="text-danger">*</span> </label>
                                            <input type="file" class="form-control" id="aadhaarPhoto" name="aadhaarPhoto" required> {{--
                                            <div class="form-text text-muted">Further info about this input</div> --}} @if ($errors->has('aadhaarPhoto'))
                                            <span class="help-block">
                                             <strong class="text-danger">{{ $errors->first('aadhaarPhoto') }}</strong>
                                              </span> @endif
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        {{-- <div class="col-md-3">
                                            <label for="password">Password<span class="text-danger">*</span> </label>
                                            <input type="text" class="form-control" id="password" name="password" placeholder="Password" required>
                                            @if ($errors->has('password'))
                                            <span class="help-block">
                                             <strong class="text-danger">{{ $errors->first('password') }}</strong>
                                              </span> @endif
                                        </div> --}}
                                        {{--
                                        <div class="col-md-3">
                                            <label for="subDivisionCode"> Confirm Password<span class="text-danger">*</span> </label>
                                            <input type="password" class="form-control" id="password" name="password_confirmation" placeholder="Confirm Password" required>
                                        </div> --}}
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-1">
                                            <label for="">. </label>
                                            <button type="submit" class="btn btn-alt-primary">Create Spot Billing Assistant</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- END Default Elements -->
                    </div>

                </div>

                <!-- END Bootstrap Design -->
            </div>
            <!-- END Page Content -->
        </main>
        <!-- END Main Container -->

        <!-- Footer -->
        @include('includes/footer')
        <!-- END Footer -->
    </div>
    <!-- END Page Container -->

    <!-- Codebase Core JS -->
    <script src="{{ config('app.url') }}/assets/js/core/jquery.min.js"></script>
    <script src="{{ config('app.url') }}/assets/js/core/bootstrap.bundle.min.js"></script>
    <script src="{{ config('app.url') }}/assets/js/core/jquery.slimscroll.min.js"></script>
    <script src="{{ config('app.url') }}/assets/js/core/jquery.scrollLock.min.js"></script>
    <script src="{{ config('app.url') }}/assets/js/core/jquery.appear.min.js"></script>
    <script src="{{ config('app.url') }}/assets/js/core/jquery.countTo.min.js"></script>
    <script src="{{ config('app.url') }}/assets/js/core/js.cookie.min.js"></script>
    <script src="{{ config('app.url') }}/assets/js/codebase.js"></script>

    <!-- Page JS Plugins -->
    <script src="{{ config('app.url') }}/assets/js/plugins/chartjs/Chart.bundle.min.js"></script>

    <!-- Page JS Code -->
    <script src="{{ config('app.url') }}/assets/js/pages/be_pages_dashboard.js"></script>
</body>

</html>

{{-- @extends('layouts.app') @section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Upload</div>

                <div class="card-body">
                    @can ('upload-bill') yes
                    <form class="" action="{{ route('uploadStore') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <input type="file" name="file">
                        <input type="submit" name="submit" value="Upload">
                    </form>
                    @endcan
                </div>
            </div>
        </div>
    </div>
</div>
@endsection --}}
