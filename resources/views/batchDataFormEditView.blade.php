@php function getValue($old = "", $newValue) { if ($old != '') { $name = old('name'); } else { $name = $newValue; } return $name; } @endphp

<!doctype html>
<!--[if lte IE 9]>     <html lang="en" class="no-focus lt-ie10 lt-ie10-msg"> <![endif]-->
<!--[if gt IE 9]><!-->
<html lang="en" class="no-focus">
<!--<![endif]-->

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">

    <title>Advice-11 Edit View  STERLING Transformers</title>

    <meta name="description" content="Codebase - Bootstrap 4 Admin Template &amp; UI Framework created by pixelcave and published on Themeforest">
    <meta name="author" content="pixelcave">
    <meta name="robots" content="noindex, nofollow">

    <!-- Open Graph Meta -->
    <meta property="og:title" content="Codebase - Bootstrap 4 Admin Template &amp; UI Framework">
    <meta property="og:site_name" content="Codebase">
    <meta property="og:description" content="Codebase - Bootstrap 4 Admin Template &amp; UI Framework created by pixelcave and published on Themeforest">
    <meta property="og:type" content="website">
    <meta property="og:url" content="">
    <meta property="og:image" content="">

    <!-- Icons -->
    <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
    <link rel="shortcut icon" href="{{ config('app.url') }}/assets/img/favicons/favicon.png">
    <link rel="icon" type="image/png" sizes="192x192" href="{{ config('app.url') }}/assets/img/favicons/favicon-192x192.png">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ config('app.url') }}/assets/img/favicons/apple-touch-icon-180x180.png">
    <!-- END Icons -->

    <!-- Stylesheets -->
    <!-- Codebase framework -->
    <link rel="stylesheet" id="css-main" href="{{ config('app.url') }}/assets/css/codebase.min.css">
    <link rel="stylesheet" href="{{config('app.url')}}/assets/js/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css">

    <!-- You can include a specific file from css/themes/ folder to alter the default color theme of the template. eg: -->
    <!-- <link rel="stylesheet" id="css-theme" href="assets/css/themes/flat.min.css"> -->
    <!-- END Stylesheets -->
</head>

<body>
    <!-- Page Container -->
    <!--
            Available classes for #page-container:

        GENERIC

            'enable-cookies'                            Remembers active color theme between pages (when set through color theme helper Codebase() -> uiHandleTheme())

        SIDEBAR & SIDE OVERLAY

            'sidebar-r'                                 Right Sidebar and left Side Overlay (default is left Sidebar and right Side Overlay)
            'sidebar-mini'                              Mini hoverable Sidebar (screen width > 991px)
            'sidebar-o'                                 Visible Sidebar by default (screen width > 991px)
            'sidebar-o-xs'                              Visible Sidebar by default (screen width < 992px)
            'sidebar-inverse'                           Dark themed sidebar

            'side-overlay-hover'                        Hoverable Side Overlay (screen width > 991px)
            'side-overlay-o'                            Visible Side Overlay by default

            'side-scroll'                               Enables custom scrolling on Sidebar and Side Overlay instead of native scrolling (screen width > 991px)

        HEADER

            ''                                          Static Header if no class is added
            'page-header-fixed'                         Fixed Header

        HEADER STYLE

            ''                                          Classic Header style if no class is added
            'page-header-modern'                        Modern Header style
            'page-header-inverse'                       Dark themed Header (works only with classic Header style)
            'page-header-glass'                         Light themed Header with transparency by default
                                                        (absolute position, perfect for light images underneath - solid light background on scroll if the Header is also set as fixed)
            'page-header-glass page-header-inverse'     Dark themed Header with transparency by default
                                                        (absolute position, perfect for dark images underneath - solid dark background on scroll if the Header is also set as fixed)

        MAIN CONTENT LAYOUT

            ''                                          Full width Main Content if no class is added
            'main-content-boxed'                        Full width Main Content with a specific maximum width (screen width > 1200px)
            'main-content-narrow'                       Full width Main Content with a percentage width (screen width > 1200px)
        -->
    <div id="page-container" class="sidebar-o side-scroll page-header-modern main-content-boxed">
        <!-- Side Overlay-->

        <!-- END Side Overlay -->

        <!-- Sidebar -->
        <!--
                Helper classes

                Adding .sidebar-mini-hide to an element will make it invisible (opacity: 0) when the sidebar is in mini mode
                Adding .sidebar-mini-show to an element will make it visible (opacity: 1) when the sidebar is in mini mode
                    If you would like to disable the transition, just add the .sidebar-mini-notrans along with one of the previous 2 classes

                Adding .sidebar-mini-hidden to an element will hide it when the sidebar is in mini mode
                Adding .sidebar-mini-visible to an element will show it only when the sidebar is in mini mode
                    - use .sidebar-mini-visible-b if you would like to be a block when visible (display: block)
            -->
        @include('includes/headerAndSidebar')

        <!-- Main Container -->
        <main id="main-container">
            <!-- Page Content -->
            <div class="content">
                <!-- Bootstrap Design -->
                <h2 class="content-heading">Advice-11 Edit</h2>
                <div class="row">
                    <div class="col-md-12">
                        <!-- Default Elements -->
                        <div class="block">
                            <div class="block-header block-header-default">
                                <h3 class="block-title">Form</h3>
                            </div>
                            <div class="block-content">
                                <form class="" id="submitForm" action="{{route('batchDataFormEditUpdate') }}" method="post" enctype="multipart/form-data">
                                    @csrf
                                    <div class="form-group row">
                                      <div class="col-md-2">
                                          <label for="month">Month<span class="text-danger">*</span> </label>
                                          <select class="form-control" name="month" id="month" required>
                                                  <option value="{{ getValue(old('month'), $data->month) }}">{{ getValue(old('month'), $data->month) }}</option>
                                                  <option value="01">01</option>
                                                  <option value="02">02</option>
                                                  <option value="03">03</option>
                                                  <option value="04">04</option>
                                                  <option value="05">05</option>
                                                  <option value="06">06</option>
                                                  <option value="07">07</option>
                                                  <option value="08">08</option>
                                                  <option value="09">09</option>
                                                  <option value="10">10</option>
                                                  <option value="11">11</option>
                                                  <option value="12">12</option>
                                                </select>


                                      {{--
                                          <div class="form-text text-muted">Further info about this input</div> --}} @if ($errors->has('sysId'))
                                          <span class="help-block">
                                           <strong class="text-danger">{{ $errors->first('month') }}</strong>
                                            </span> @endif
                                      </div>
                                      <div class="col-md-2">
                                          <label for="sysId">System ID<span class="text-danger">*</span> </label>
                                            <input type="text" class="form-control" id="sysId" name="sysId" placeholder="System ID" value="{{ getValue(old('sysId'), $data->sysId) }}">
                                            <input type="hidden" class="form-control" name="id" placeholder="System ID" value="{{ getValue(old('id'), $data->id) }}">
                                      {{--
                                          <div class="form-text text-muted">Further info about this input</div> --}} @if ($errors->has('sysId'))
                                          <span class="help-block">
                                           <strong class="text-danger">{{ $errors->first('sysId') }}</strong>
                                            </span> @endif
                                      </div>
                                      <div class="col-md-2">
                                          <label for="inputCode">Input Code<span class="text-danger">*</span> </label>
                                          <input type="text" class="form-control" id="inputCode" name="inputCode" placeholder="Input Code" value="{{ getValue(old('inputCode'), $data->inputCode) }}">
                                      {{--
                                          <div class="form-text text-muted">Further info about this input</div> --}} @if ($errors->has('inputCode'))
                                          <span class="help-block">
                                           <strong class="text-danger">{{ $errors->first('inputCode') }}</strong>
                                            </span> @endif
                                      </div>
                                      <div class="col-md-2">
                                          <label for="trType">TR Type<span class="text-danger">*</span> </label>
                                          <input type="text" class="form-control" id="trType" name="trType" placeholder="TR Type" value="{{ getValue(old('trType'), $data->trType) }}">
                                      {{--
                                          <div class="form-text text-muted">Further info about this input</div> --}} @if ($errors->has('trType'))
                                          <span class="help-block">
                                           <strong class="text-danger">{{ $errors->first('trType') }}</strong>
                                            </span> @endif
                                      </div>
                                      <div class="col-md-2">
                                          <label for="subdivisionCode">Sub Division Code <span class="text-danger">*</span> </label>
                                          <select class="form-control" name="subdivisionCode" id="subdivisionCode" required>
                                                  <option value="{{ getValue(old('subdivisionCode'), $data->subdivisionCode) }}">{{ getValue(old('subdivisionCode'), $data->subdivisionCode) }}</option>
                                                  <option value="H11">H11</option>
                                                  <option value="H12">H12</option>
                                                  <option value="H13">H13</option>
                                                  <option value="H14">H14</option>
                                                  <option value="H15">H15</option>
                                                  <option value="H21">H21</option>
                                                  <option value="H22">H22</option>
                                                  <option value="H23">H23</option>
                                                  <option value="H24">H24</option>
                                                  <option value="H25">H25</option>
                                                  <option value="H26">H26</option>
                                                  <option value="H27">H27</option>
                                                  <option value="H31">H31</option>
                                                  <option value="H32">H32</option>
                                                  <option value="H33">H33</option>
                                                  <option value="J12">H12</option>
                                                  <option value="H34">H34</option>
                                                  <option value="H35">H35</option>
                                                  <option value="H36">H36</option>
                                                  <option value="H39">H39</option>
                                                  <option value="H41">H41</option>
                                                  <option value="H42">H42</option>
                                                  <option value="H43">H43</option>
                                                  <option value="H44">H44</option>
                                                  <option value="J11">J11</option>
                                                  <option value="J13">J13</option>
                                                  <option value="J14">J14</option>
                                                  <option value="J15">J15</option>
                                                  <option value="J16">J16</option>
                                                  <option value="J17">J17</option>
                                                  <option value="J21">J21</option>
                                                  <option value="J22">J22</option>
                                                  <option value="J23">J23</option>
                                                  <option value="J41">J41</option>
                                                  <option value="J42">J42</option>
                                                  <option value="J43">J43</option>
                                                  <option value="J44">J44</option>
                                                  <option value="J45">J45</option>
                                                  <option value="J46">J46</option>
                                                  <option value="J51">J51</option>
                                                  <option value="J52">J52</option>
                                                  <option value="J53">J53</option>
                                                  <option value="J54">J54</option>
                                                  <option value="J55">H12</option>
                                                  <option value="J56">J56</option>
                                                  <option value="N21">N21</option>
                                                  <option value="N22">N22</option>
                                                  <option value="N23">N23</option>
                                                  <option value="N24">N24</option>
                                                  <option value="N25">N25</option>
                                                  <option value="N26">N26</option>
                                                  <option value="N31">N31</option>
                                                  <option value="N32">N32</option>
                                                  <option value="N33">N33</option>
                                                  <option value="N34">N34</option>
                                                  <option value="N35">N35</option>
                                                  <option value="N36">N36</option>
                                                  <option value="N41">N41</option>
                                                  <option value="N42">N42</option>
                                                  <option value="N43">N43</option>
                                                  <option value="N44">N44</option>
                                                  <option value="N45">N45</option>
                                                  <option value="X21">X21</option>
                                                  <option value="X22">X22</option>
                                                  <option value="X23">X23</option>
                                                  <option value="X24">X24</option>
                                                  <option value="X25">X25</option>
                                                  <option value="X26">X26</option>
                                                  <option value="X31">X31</option>
                                                  <option value="X32">X32</option>
                                                  <option value="X33">X33</option>
                                                  <option value="X34">X34</option>
                                                  <option value="X35">X35</option>
                                                  <option value="X41">X41</option>
                                                  <option value="X42">X42</option>
                                                  <option value="X43">X43</option>
                                                  <option value="X44">X44</option>
                                                  <option value="X45">X45</option>
                                                  <option value="X51">X51</option>
                                                  <option value="X52">X52</option>
                                                  <option value="X53">X53</option>
                                                  <option value="X54">X54</option>
                                                  <option value="X55">X55</option>
                                                  <option value="X56">X56</option>
                                                  <option value="X11">X11</option>
                                                  <option value="X12">X12</option>
                                                  <option value="X13">X13</option>
                                                  <option value="X14">X14</option>
                                                  <option value="X15">X15</option>
                                                  <option value="X16">X16</option>
                                                </select>
                                      {{--
                                          <div class="form-text text-muted">Further info about this input</div> --}} @if ($errors->has('subdivisionCode'))
                                          <span class="help-block">
                                           <strong class="text-danger">{{ $errors->first('subdivisionCode') }}</strong>
                                            </span> @endif
                                      </div>
                                      <div class="col-md-2">
                                          <label for="octraiCode">Octrai Code <span class="text-danger">*</span> </label>
                                          <select class="form-control" name="octraiCode" id="octraiCode" required>
                                                  <option value="{{ getValue(old('octraiCode'), $data->octraiCode) }}">{{ getValue(old('octraiCode'), $data->octraiCode) }}</option>
                                                  <option value="Y">Y</option>
                                                  <option value="N">N</option>
                                                </select>
                                      {{--
                                          <div class="form-text text-muted">Further info about this input</div> --}} @if ($errors->has('octraiCode'))
                                          <span class="help-block">
                                           <strong class="text-danger">{{ $errors->first('octraiCode') }}</strong>
                                            </span> @endif
                                      </div>
                                      <div class="col-md-2">
                                          <label for="ledgerGroup">Ledger Group <span class="text-danger">*</span> </label>
                                          <input type="text" class="form-control" id="ledgerGroup" name="ledgerGroup" placeholder="Ledger Group" value="{{ getValue(old('ledgerGroup'), $data->ledgerGroup) }}">
                                      {{--
                                          <div class="form-text text-muted">Further info about this input</div> --}} @if ($errors->has('ledgerGroup'))
                                          <span class="help-block">
                                           <strong class="text-danger">{{ $errors->first('ledgerGroup') }}</strong>
                                            </span> @endif
                                      </div>
                                      <div class="col-md-2">
                                          <label for="billingGroup">Billing Group <span class="text-danger">*</span> </label>
                                          <select class="form-control" name="billingGroup" id="billingGroup" required>
                                            <option value="{{ getValue(old('billingGroup'), $data->billingGroup) }}">{{ getValue(old('billingGroup'), $data->billingGroup) }}</option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="8">8</option>
                                          </select>
                                      {{--
                                          <div class="form-text text-muted">Further info about this input</div> --}} @if ($errors->has('billingGroup'))
                                          <span class="help-block">
                                           <strong class="text-danger">{{ $errors->first('billingGroup') }}</strong>
                                            </span> @endif
                                      </div>
                                      <div class="col-md-2">
                                          <label for="billingSubGroup">Billing Sub Group <span class="text-danger">*</span> </label>
                                          <input type="text" class="form-control" name="billingSubGroup" id="billingSubGroup" value="{{ getValue(old('billingSubGroup'), $data->billingSubGroup) }}" placeholder="Billing Sub Group"/>
                                          @if ($errors->has('billingSubGroup'))
                                          <span class="help-block">
                                           <strong class="text-danger">{{ $errors->first('billingSubGroup') }}</strong>
                                            </span> @endif
                                      </div>
                                      <div class="col-md-2">
                                          <label for="villageName">Name of Village <span class="text-danger">*</span> </label>
                                          <input type="text" class="form-control" id="villageName" name="villageName" placeholder="Name of Village" value="{{ getValue(old('villageName'), $data->villageName) }}">
                                      {{--
                                          <div class="form-text text-muted">Further info about this input</div> --}} @if ($errors->has('villageName'))
                                          <span class="help-block">
                                           <strong class="text-danger">{{ $errors->first('villageName') }}</strong>
                                            </span> @endif
                                      </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-md-3">
                                            <label for="accountNo">Account Number<span class="text-danger">*</span> </label>
                                            <input type="number" class="form-control" id="accountNo" name="accountNo" placeholder="Account Number" value="{{ getValue(old('accountNo'), $data->accountNo) }}" required > {{--
                                            <div class="form-text text-muted">Further info about this input</div> --}} @if ($errors->has('accountNo'))
                                            <span class="help-block">
                                             <strong class="text-danger">{{ $errors->first('accountNo') }}</strong>
                                              </span> @endif
                                        </div>
                                        <div class="col-md-3">
                                            <label for="consumerName">Consumer Name<span class="text-danger">*</span> </label>
                                            <input type="text" class="form-control" id="consumerName" name="consumerName" placeholder="Consumer Name" value="{{ getValue(old('consumerName'), $data->consumerName) }}" > {{--
                                            <div class="form-text text-muted">Further info about this input</div> --}} @if ($errors->has('consumerName'))
                                            <span class="help-block">
                                             <strong class="text-danger">{{ $errors->first('consumerName') }}</strong>
                                              </span> @endif
                                        </div>
                                        <div class="col-md-3">
                                            <label for="address">Address<span class="text-danger">*</span> </label>
                                            <input type="text" class="form-control" id="address" name="address" placeholder="Address" value="{{ getValue(old('address'), $data->address) }}" > {{--
                                            <div class="form-text text-muted">Further info about this input</div> --}} @if ($errors->has('address'))
                                            <span class="help-block">
                                             <strong class="text-danger">{{ $errors->first('address') }}</strong>
                                              </span> @endif
                                        </div>
                                        <div class="col-md-3">
                                            <label for="feederCode">Feeder Code<span class="text-danger">*</span> </label>
                                            <input type="text" class="form-control" id="feederCode" name="feederCode" placeholder="Feeder Code" value="{{ getValue(old('feederCode'), $data->feederCode) }}" > {{--
                                            <div class="form-text text-muted">Further info about this input</div> --}} @if ($errors->has('feederCode'))
                                            <span class="help-block">
                                             <strong class="text-danger">{{ $errors->first('feederCode') }}</strong>
                                              </span> @endif
                                        </div>
                                        <div class="col-md-3">
                                            <label for="tariffType">Tariff Type Code<span class="text-danger">*</span> </label>
                                            <input type="text" class="form-control" id="tariffType" name="tariffType" placeholder="Tariff Type Code" value="{{ getValue(old('tariffType'), $data->tariffType) }}" > {{--
                                            <div class="form-text text-muted">Further info about this input</div> --}} @if ($errors->has('tariffType'))
                                            <span class="help-block">
                                             <strong class="text-danger">{{ $errors->first('tariffType') }}</strong>
                                              </span> @endif
                                        </div>


                                        <div class="col-md-3">
                                            <label for="employeeConsessionCode">Consession Code</label>
                                            <input type="text" class="form-control" id="employeeConsessionCode" name="employeeConsessionCode" placeholder="Employee Consession Code" value="{{ getValue(old('employeeConsessionCode'), $data->employeeConsessionCode) }}"> {{--
                                            <div class="form-text text-muted">Further info about this input</div> --}} @if ($errors->has('employeeConsessionCode'))
                                            <span class="help-block">
                                             <strong class="text-danger">{{ $errors->first('employeeConsessionCode') }}</strong>
                                              </span> @endif
                                        </div>
                                        <div class="col-md-3">
                                            <label for="voltClassCode">Volt Class Code</label>
                                            <input type="text" class="form-control" id="voltClassCode" name="voltClassCode" placeholder="Volt Class Code" value="{{ getValue(old('voltClassCode'), $data->voltClassCode) }}"> {{--
                                            <div class="form-text text-muted">Further info about this input</div> --}} @if ($errors->has('voltClassCode'))
                                            <span class="help-block">
                                             <strong class="text-danger">{{ $errors->first('voltClassCode') }}</strong>
                                              </span> @endif
                                        </div>
                                        <div class="col-md-3">
                                            <label for="connectedLoad">Connected Load(KW)</label>
                                            <input type="text" class="form-control" id="connectedLoad" name="connectedLoad" placeholder="Connected Load" value="{{ getValue(old('connectedLoad'), $data->connectedLoad) }}"> {{--
                                            <div class="form-text text-muted">Further info about this input</div> --}} @if ($errors->has('connectedLoad'))
                                            <span class="help-block">
                                             <strong class="text-danger">{{ $errors->first('connectedLoad') }}</strong>
                                              </span> @endif
                                        </div>
                                        <div class="col-md-3">
                                            <label for="meterRatio">Meter Ratio<span class="text-danger">*</span> </label>
                                            <input type="text" class="form-control" id="meterRatio" name="meterRatio" placeholder="Meter Ratio" value="{{ getValue(old('meterRatio'), $data->meterRatio) }}" > {{--
                                            <div class="form-text text-muted">Further info about this input</div> --}} @if ($errors->has('meterRatio'))
                                            <span class="help-block">
                                             <strong class="text-danger">{{ $errors->first('meterRatio') }}</strong>
                                              </span> @endif
                                        </div>
                                        <div class="col-md-3">
                                            <label for="connectionDate">Date of Connection<span class="text-danger">*</span> </label>
                                            <input type="text" class="js-datepicker form-control" id="connectionDate" name="connectionDate"  data-week-start="1" data-autoclose="true" data-today-highlight="true" data-date-format="dd/mm/yyyy" placeholder="dd/mm/yyyy" value="{{ getValue(old('connectionDate'), $data->connectionDate) }}"autocomplete="off"  > {{--
                                            <div class="form-text text-muted">Further info about this input</div> --}} @if ($errors->has('connectionDate'))
                                            <span class="help-block">
                                             <strong class="text-danger">{{ $errors->first('connectionDate') }}</strong>
                                              </span> @endif
                                        </div>
                                        <div class="col-md-3">
                                            <label for="lineCtRatio">Line C.T Ratio<span class="text-danger">*</span> </label>
                                            <input type="text" class="form-control" id="lineCtRatio" name="lineCtRatio" placeholder="Line C.T Ratio"  value="{{ getValue(old('lineCtRatio'), $data->lineCtRatio) }}" > {{--
                                            <div class="form-text text-muted">Further info about this input</div> --}} @if ($errors->has('lineCtRatio'))
                                            <span class="help-block">
                                             <strong class="text-danger">{{ $errors->first('lineCtRatio') }}</strong>
                                              </span> @endif
                                        </div>


                                        <div class="col-md-3">
                                            <label for="meterMultiplier">Meter Multiplier<span class="text-danger">*</span> </label>
                                            <input type="text" class="form-control" id="meterMultiplier" name="meterMultiplier" placeholder="Meter Multiplier" value="{{ getValue(old('meterMultiplier'), $data->meterMultiplier) }}" > {{--
                                            <div class="form-text text-muted">Further info about this input</div> --}} @if ($errors->has('meterMultiplier'))
                                            <span class="help-block">
                                             <strong class="text-danger">{{ $errors->first('meterMultiplier') }}</strong>
                                              </span> @endif
                                        </div>
                                        <div class="col-md-3">
                                            <label for="overallMultiply">Overall Multiplying Factor<span class="text-danger">*</span> </label>
                                            <input type="text" class="form-control" id="overallMultiply" name="overallMultiply" placeholder="Overall Multiplying Factor" value="{{ getValue(old('overallMultiply'), $data->overallMultiply) }}" > {{--
                                            <div class="form-text text-muted">Further info about this input</div> --}} @if ($errors->has('overallMultiply'))
                                            <span class="help-block">
                                             <strong class="text-danger">{{ $errors->first('overallMultiply') }}</strong>
                                              </span> @endif
                                        </div>
                                        <div class="col-md-3">
                                            <label for="phaseCode">Phase Code</label>
                                            <input type="text" class="form-control" id="phaseCode" name="phaseCode" value="{{ getValue(old('phaseCode'), $data->phaseCode) }}" placeholder="Phase Code" > {{--
                                            <div class="form-text text-muted">Further info about this input</div> --}} @if ($errors->has('phaseCode'))
                                            <span class="help-block">
                                             <strong class="text-danger">{{ $errors->first('phaseCode') }}</strong>
                                              </span> @endif
                                        </div>
                                        <div class="col-md-3">
                                            <label for="meterSerialNo">Meter Serial Number</label>
                                            <input type="text" class="form-control" id="meterSerialNo" name="meterSerialNo" value="{{ getValue(old('meterSerialNo'), $data->meterSerialNo) }}" placeholder="Meter Serial Number"> {{--
                                            <div class="form-text text-muted">Further info about this input</div> --}} @if ($errors->has('meterSerialNo'))
                                            <span class="help-block">
                                             <strong class="text-danger">{{ $errors->first('meterSerialNo') }}</strong>
                                              </span> @endif
                                        </div>
                                        <div class="col-md-3">
                                            <label for="amps">Amps</label>
                                            <input type="text" class="form-control" id="amps" name="amps" value="{{ getValue(old('amps'), $data->amps) }}" placeholder="Amps"> {{--
                                            <div class="form-text text-muted">Further info about this input</div> --}} @if ($errors->has('amps'))
                                            <span class="help-block">
                                             <strong class="text-danger">{{ $errors->first('amps') }}</strong>
                                              </span> @endif
                                        </div>
                                      <div class="col-md-3">
                                          <label for="mfsCode">Mfs Code</label>
                                          <input type="text" class="form-control" id="mfsCode" name="mfsCode" value="{{ getValue(old('mfsCode'), $data->mfsCode) }}" placeholder="Mfs Code"> {{--
                                          <div class="form-text text-muted">Further info about this input</div> --}} @if ($errors->has('mfsCode'))
                                          <span class="help-block">
                                           <strong class="text-danger">{{ $errors->first('mfsCode') }}</strong>
                                            </span> @endif
                                      </div>
                                      <div class="col-md-3">
                                          <label for="mcbRent">MCB Rent</label>
                                          <input type="text" class="form-control" id="mcbRent" name="mcbRent" value="{{ getValue(old('mcbRent'), $data->mcbRent) }}" placeholder="MCB Rent"> {{--
                                          <div class="form-text text-muted">Further info about this input</div> --}} @if ($errors->has('mcbRent'))
                                          <span class="help-block">
                                           <strong class="text-danger">{{ $errors->first('mcbRent') }}</strong>
                                            </span> @endif
                                      </div>
                                      <div class="col-md-3">
                                          <label for="meterReading">Meter Reading</label>
                                          <input type="text" class="form-control" id="meterReading" name="meterReading" value="{{ getValue(old('meterReading'), $data->meterReading) }}" placeholder="Meter Reading"> {{--
                                          <div class="form-text text-muted">Further info about this input</div> --}} @if ($errors->has('meterReading'))
                                          <span class="help-block">
                                           <strong class="text-danger">{{ $errors->first('meterReading') }}</strong>
                                            </span> @endif
                                      </div>
                                      <div class="col-md-3">
                                          <label for="ruralUrbenConsumer">Rural/Urban Consumer</label>
                                          <input type="text" class="form-control" id="ruralUrbenConsumer" name="ruralUrbenConsumer" value="{{ getValue(old('ruralUrbenConsumer'), $data->ruralUrbenConsumer) }}" placeholder="Rural/Urban Consumer"> {{--
                                          <div class="form-text text-muted">Further info about this input</div> --}} @if ($errors->has('ruralUrbenConsumer'))
                                          <span class="help-block">
                                           <strong class="text-danger">{{ $errors->first('ruralUrbenConsumer') }}</strong>
                                            </span> @endif
                                      </div>
                                      <div class="col-md-3">
                                          <label for="meterLocationCode">Meter Location Code</label>
                                          <input type="text" class="form-control" id="meterLocationCode" name="meterLocationCode" value="{{ getValue(old('meterLocationCode'), $data->meterLocationCode) }}" placeholder="Meter Location Code"> {{--
                                          <div class="form-text text-muted">Further info about this input</div> --}} @if ($errors->has('meterLocationCode'))
                                          <span class="help-block">
                                           <strong class="text-danger">{{ $errors->first('meterLocationCode') }}</strong>
                                            </span> @endif
                                      </div>
                                      <div class="col-md-3">
                                          <label for="meterSecurity">Meter Security</label>
                                          <input type="text" class="form-control" id="meterSecurity" name="meterSecurity" value="{{ getValue(old('meterSecurity'), $data->meterSecurity) }}" placeholder="Meter Security"> {{--
                                          <div class="form-text text-muted">Further info about this input</div> --}} @if ($errors->has('meterSecurity'))
                                          <span class="help-block">
                                           <strong class="text-danger">{{ $errors->first('meterSecurity') }}</strong>
                                            </span> @endif
                                      </div>
                                      <div class="col-md-3">
                                          <label for="acd">ACD</label>
                                          <input type="text" class="form-control" id="acd" name="acd" value="{{ getValue(old('acd'), $data->acd) }}" placeholder="ACD"> {{--
                                          <div class="form-text text-muted">Further info about this input</div> --}} @if ($errors->has('acd'))
                                          <span class="help-block">
                                           <strong class="text-danger">{{ $errors->first('acd') }}</strong>
                                            </span> @endif
                                      </div>
                                      <div class="col-md-3">
                                          <label for="nrs">NRS Type</label>
                                          <input type="text" class="form-control" id="nrs" name="nrs" value="{{ getValue(old('nrs'), $data->nrs) }}" placeholder="NRS Type">

                                          @if ($errors->has('nrs'))
                                          <span class="help-block">
                                           <strong class="text-danger">{{ $errors->first('nrs') }}</strong>
                                            </span> @endif
                                      </div>
                                      <div class="col-md-3">
                                          <label for="uReason">Reason</label>
                                          {{-- <input type="text" class="form-control" id="uReason" name="uReason" value="{{ old('uReason') }}" placeholder="Update Reason"> --}}
                                          <textarea name="uReason" class="form-control" value="{{ old('uReason') }}" placeholder="Update Reason"></textarea>

                                          @if ($errors->has('uReason'))
                                          <span class="help-block">
                                           <strong class="text-danger">{{ $errors->first('uReason') }}</strong>
                                            </span> @endif
                                      </div>
                                      <div class="col-md-1">
                                          <label for="">. </label>
                                          <input type="hidden" class="form-control" id="cUser" name="cUser" value="{{ Auth::user()->email }}">
                                          <input type="hidden" class="form-control" id="cDate" name="cDate" value="{{ Date('d-m-Y') }}">
                                          <input type="hidden" class="form-control" id="cTime" name="cTime" value="{{ Date('H:i:s') }}">
                                          <button type="submit" id="submit" class="btn btn-alt-primary">Submit</button>

                                      </div>
                                      {{-- <div class="col-md-1">
                                          <label for="">. </label>
                                          <button type="submit" id="submit" name="delete" class="btn btn-alt-danger">Delete</button>
                                      </div> --}}
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- END Default Elements -->
                    </div>

                </div>

                <!-- END Bootstrap Design -->
            </div>
            <!-- END Page Content -->
        </main>
        <!-- END Main Container -->

        <!-- Footer -->
        @include('includes/footer')
        <!-- END Footer -->
    </div>
    <!-- END Page Container -->

    <!-- Codebase Core JS -->
    <script src="{{ config('app.url') }}/assets/js/core/jquery.min.js"></script>
    <script src="{{ config('app.url') }}/assets/js/core/bootstrap.bundle.min.js"></script>
    <script src="{{ config('app.url') }}/assets/js/core/jquery.slimscroll.min.js"></script>
    <script src="{{ config('app.url') }}/assets/js/core/jquery.scrollLock.min.js"></script>
    <script src="{{ config('app.url') }}/assets/js/core/jquery.appear.min.js"></script>
    <script src="{{ config('app.url') }}/assets/js/core/jquery.countTo.min.js"></script>
    <script src="{{ config('app.url') }}/assets/js/core/js.cookie.min.js"></script>
    <script src="{{ config('app.url') }}/assets/js/codebase.js"></script>

    <!-- Page JS Plugins -->
    <script src="{{ config('app.url') }}/assets/js/plugins/chartjs/Chart.bundle.min.js"></script>
    <script src="{{config('app.url')}}/assets/js/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>

    <!-- Page JS Code -->
    <script src="{{ config('app.url') }}/assets/js/pages/be_pages_dashboard.js"></script>

    <script>
            jQuery(function () {
                // Init page helpers (BS Datepicker + BS Colorpicker + BS Maxlength + Select2 + Masked Input + Range Sliders + Tags Inputs plugins)
                Codebase.helpers(['datepicker', 'colorpicker', 'maxlength', 'select2', 'masked-inputs', 'rangeslider', 'tags-inputs']);
            });
        </script>

        {{-- <script type="text/javascript">
        $('#submit').on('click', function functionName(e) {
          e.preventDefault();
          var datastring = $("#submitForm").serialize();
          $.ajax({
            headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
              type: "POST",
              url: "{{ route('batchAjax') }}",
              data: datastring,
              success: function(data) {
                // console.log(data);
                   alert('Data send');
                   $('#accountNo').val('');
                   $('#consumerName').val('');
                   $('#address').val('');
                   $('#feederCode').val('');
                   $('#tariffType').val('');
                   $('#employeeConsessionCode').val('');
                   $('#voltClassCode').val('');
                   $('#connectedLoad').val('');
                   $('#meterRatio').val('');
                   $('#connectionDate').val('');
                   $('#lineCtRatio').val('');
                   $('#meterMultiplier').val('');
                   $('#overallMultiply').val('');
                   $('#phaseCode').val('');
                   $('#meterSerialNo').val('');
                   $('#amps').val('');
                   $('#mfsCode').val('');
                   $('#mcbRent').val('');
                   $('#meterReading').val('');
                   $('#ruralUrbenConsumer').val('');
                   $('#meterLocationCode').val('');
                   $('#meterSecurity').val('');
                   $('#acd').val('');
                   $('#nrs').val('');
              }
          });
          console.log('save');
        });
        </script> --}}
</body>

</html>

{{-- @extends('layouts.app') @section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Upload</div>

                <div class="card-body">
                    @can ('upload-bill') yes
                    <form class="" action="{{ route('uploadStore') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <input type="file" name="file">
                        <input type="submit" name="submit" value="Upload">
                    </form>
                    @endcan
                </div>
            </div>
        </div>
    </div>
</div>
@endsection --}}
