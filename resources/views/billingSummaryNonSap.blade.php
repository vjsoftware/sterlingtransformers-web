<!doctype html>
<!--[if lte IE 9]>     <html lang="en" class="no-focus lt-ie10 lt-ie10-msg"> <![endif]-->
<!--[if gt IE 9]><!--> <html lang="en" class="no-focus"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">

        <title>Billing Summary Non SAP {{ $data['sub'] }}-{{ $data['bg'] }}-{{$data['bc']}} &nbsp;&nbsp;&nbsp;Date: {{ Date('d-m-Y') }}</title>

        <meta name="description" content="Codebase - Bootstrap 4 Admin Template &amp; UI Framework created by pixelcave and published on Themeforest">
        <meta name="author" content="pixelcave">
        <meta name="robots" content="noindex, nofollow">
        <meta name="csrf-token" content="{{ csrf_token() }}" />

        <!-- Open Graph Meta -->
        <meta property="og:title" content="Codebase - Bootstrap 4 Admin Template &amp; UI Framework">
        <meta property="og:site_name" content="Codebase">
        <meta property="og:description" content="Codebase - Bootstrap 4 Admin Template &amp; UI Framework created by pixelcave and published on Themeforest">
        <meta property="og:type" content="website">
        <meta property="og:url" content="">
        <meta property="og:image" content="">

        <!-- Icons -->
        <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
        <link rel="shortcut icon" href="{{ config('app.url') }}/assets/img/favicons/favicon.png">
        <link rel="icon" type="image/png" sizes="192x192" href="{{ config('app.url') }}/assets/img/favicons/favicon-192x192.png">
        <link rel="apple-touch-icon" sizes="180x180" href="{{ config('app.url') }}/assets/img/favicons/apple-touch-icon-180x180.png">
        <!-- END Icons -->

        <!-- Stylesheets -->
        <!-- Page JS Plugins CSS -->
        <link rel="stylesheet" href="{{ config('app.url') }}/assets/js/plugins/datatables/dataTables.bootstrap4.min.css">

        <!-- Codebase framework -->
        <link rel="stylesheet" id="css-main" href="{{ config('app.url') }}/assets/css/codebase.css">

        <!-- You can include a specific file from css/themes/ folder to alter the default color theme of the template. eg: -->
        <!-- <link rel="stylesheet" id="css-theme" href="assets/css/themes/flat.min.css"> -->
        <!-- END Stylesheets -->
    </head>
    <body>
        <!-- Page Container -->
        <!--
            Available classes for #page-container:

        GENERIC

            'enable-cookies'                            Remembers active color theme between pages (when set through color theme helper Codebase() -> uiHandleTheme())

        SIDEBAR & SIDE OVERLAY

            'sidebar-r'                                 Right Sidebar and left Side Overlay (default is left Sidebar and right Side Overlay)
            'sidebar-mini'                              Mini hoverable Sidebar (screen width > 991px)
            'sidebar-o'                                 Visible Sidebar by default (screen width > 991px)
            'sidebar-o-xs'                              Visible Sidebar by default (screen width < 992px)
            'sidebar-inverse'                           Dark themed sidebar

            'side-overlay-hover'                        Hoverable Side Overlay (screen width > 991px)
            'side-overlay-o'                            Visible Side Overlay by default

            'side-scroll'                               Enables custom scrolling on Sidebar and Side Overlay instead of native scrolling (screen width > 991px)

        HEADER

            ''                                          Static Header if no class is added
            'page-header-fixed'                         Fixed Header

        HEADER STYLE

            ''                                          Classic Header style if no class is added
            'page-header-modern'                        Modern Header style
            'page-header-inverse'                       Dark themed Header (works only with classic Header style)
            'page-header-glass'                         Light themed Header with transparency by default
                                                        (absolute position, perfect for light images underneath - solid light background on scroll if the Header is also set as fixed)
            'page-header-glass page-header-inverse'     Dark themed Header with transparency by default
                                                        (absolute position, perfect for dark images underneath - solid dark background on scroll if the Header is also set as fixed)

        MAIN CONTENT LAYOUT

            ''                                          Full width Main Content if no class is added
            'main-content-boxed'                        Full width Main Content with a specific maximum width (screen width > 1200px)
            'main-content-narrow'                       Full width Main Content with a percentage width (screen width > 1200px)
        -->
        <div id="page-container" class="sidebar-o side-scroll page-header-modern main-content-boxed">
            <!-- Side Overlay-->
            @include('includes/headerAndSidebar')
            <!-- END Header -->

            <!-- Main Container -->
            <main id="main-container">
                <!-- Page Content -->
                <div class="content">
                    <h2 class="content-heading">Billing Summary NON-SAP</h2>

                    <!-- Dynamic Table Full -->
                    <div class="block">
                        {{-- <div class="block-header block-header-default">
                            <h3 class="block-title">Dynamic Table <small></small></h3>
                        </div> --}}
                        <div class="block-content block-content-full">
                          <form class="" action="{{ route('billingSummaryNonSap') }}" method="post" id="">
                            @csrf
                            <div class="form-group row">
                                  <div class="col-md-3">
                                    <label for="subDivisionCode">Sub Division Code <span class="text-danger">*</span> </label>
                                      <select class="form-control" id="subDivisionCode" name="subDivisionCode" onchange="getGroupCycleAjaxNonSap(this)">
                                        {{-- Sub Division Code's --}}
                                        <option value="">Select</option>
                                        @php
                                          // $sub = sapInput::groupBy('subDivisionCode');
                                          // $sub = sapInput::select('subDivisionCode')->groupBy('subDivisionCode')->take(20)->get();
                                          $sub = DB::select("SELECT distinct subDivisionCode FROM bills group by subDivisionCode");
                                          // $subDivision = '3213';
                                          // $billingsGroups = DB::select("select distinct billingGroup from sap_inputs WHERE subDivisionCode in ('$subDivision') group by billingGroup");
                                          // $billingsCycles = DB::select("select distinct billingCycle from sap_inputs WHERE subDivisionCode in ('$subDivision') group by billingCycle");
                                          // $ledgerCodes = DB::select("SELECT distinct MRU FROM sap_inputs group by MRU");
                                          // dd($sub);
                                        @endphp
                                        @foreach ($sub as $subDivisionCode)
                                          @if (isset($data) && count($data['sub']) > 0)
                                            <option value="{{ $subDivisionCode->subDivisionCode }}" @if ($subDivisionCode->subDivisionCode == $data['sub']) selected @endif>{{ $subDivisionCode->subDivisionCode }}</option>
                                        @else
                                          <option value="{{ $subDivisionCode->subDivisionCode }}" >{{ $subDivisionCode->subDivisionCode }}</option>
                                        @endif
                                        @endforeach
                                      </select>
                                      {{-- <input type="text" class="form-control" id="subDivisionCode" name="subDivisionCode" placeholder="Sub Division Code" required> --}}
                                      {{-- <div class="form-text text-muted">Further info about this input</div> --}}
                                      @if ($errors->has('subDivisionCode'))
                                   <span class="help-block">
                                   <strong class="text-danger">{{ $errors->first('subDivisionCode') }}</strong>
                                    </span>
                                      @endif
                                  </div>
                                  <div class="col-md-3">
                                    <label for="billingGroup">Billing Group <span class="text-danger">*</span> </label>
                                    <select class="form-control" name="billingGroup" id="billingGroup" required onchange="getGroupCycleAjaxNonSapBc(this)">
                                      <option value="">Select</option>
                                      @if (isset($data) && count($data['sub']) > 0)
                                        @php
                                          $sub = $data['sub'];
                                          $bg = $data['bg'];
                                          $bc = $data['bc'];
                                          $meterReaders = DB::select("select distinct billingGroup from bills where subDivisionCode = '$sub'");
                                        @endphp
                                        @foreach ($meterReaders as $meterReader)
                                          <option value="{{ $meterReader->billingGroup }}" @if ($meterReader->billingGroup == $data['bg']) selected @endif>{{ $meterReader->billingGroup }}</option>
                                        @endforeach
                                      @endif
                                    </select>
                                      {{-- <div class="form-text text-muted">Further info about this input</div> --}}
                                      @if ($errors->has('billingGroup'))
                                   <span class="help-block">
                                   <strong class="text-danger">{{ $errors->first('billingGroup') }}</strong>
                                    </span>
                                      @endif
                                  </div>
                                  <div class="col-md-2">
                                    <label for="billingCycle">Billing Cycle <span class="text-danger">*</span></label>
                                    <select class="form-control" name="billingCycle" id="billingCycle" required onchange="getmeterReaders(this)">
                                      <option value="">Select</option>
                                      @if (isset($data) && count($data['sub']) > 0)
                                        @php
                                          $sub = $data['sub'];
                                          $bg = $data['bg'];
                                          $bc = $data['bc'];
                                          $meterReaders = DB::select("select distinct billingCycle from bills where subDivisionCode = '$sub'");
                                        @endphp
                                        @foreach ($meterReaders as $meterReader)
                                          <option value="{{ $meterReader->billingCycle }}" @if ($meterReader->billingCycle == $data['bc']) selected @endif>{{ $meterReader->billingCycle }}</option>
                                        @endforeach
                                      @endif
                                    </select>
                                      {{-- <div class="form-text text-muted">Further info about this input</div> --}}
                                      @if ($errors->has('billingCycle'))
                                   <span class="help-block">
                                   <strong class="text-danger">{{ $errors->first('billingCycle') }}</strong>
                                    </span>
                                      @endif
                                  </div>
                                  <div class="col-md-1">
                                    <label for="accountNoL">. </label>
                                      <button type="submit" class="btn btn-alt-primary">Submit</button>
                                  </div>


                              </div>

                          </form>
                            <!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/be_tables_datatables.js -->
                            <table class="table table-bordered table-striped js-dataTable-full responsive">
                                <thead>
                                    <tr>
                                        <th>S.No</th>
                                        <th>Sub Div Code</th>
                                        <th>Sub Div Name</th>
                                        <th>BG</th>
                                        <th>BC</th>
                                        <th>Leder</th>
                                        <th>Tot Con</th>
                                        <th>Billed</th>
                                        <th>Un Billed</th>
                                    </tr>
                                </thead>
                                <tbody>
                                  @if (isset($data))
                                    @php
                                      $sno = 1;
                                    @endphp
                                    @foreach ($data['data'] as $sba)
                                      <tr>
                                          <td class="" style="padding: 0px;">{{ $sno++ }}</td>
                                          <td class="" style="padding: 0px;">{{ $sba->subDivisionCode }}</td>
                                          <td class="" style="padding: 0px;">{{ $sba->subDivisionName }}</td>
                                          <td class="font-w600" style="padding: 0px;">{{ $sba->billingGroup }}</td>
                                          <td class="font-w600" style="padding: 0px;">{{ $sba->billingCycle }}</td>
                                          <td class="font-w600" style="padding: 0px;">{{ $sba->MRU }}</td>
                                          <td class="font-w600" style="padding: 0px;">{{ $sba->totalCons }}</td>
                                          @php
                                            $totBilled = DB::select("select count(*) totBilled from bills where bill_status = 'Y' and subDivisionCode = '$sba->subDivisionCode' and billingGroup = '$sba->billingGroup' and billingCycle = '$sba->billingCycle' and SUBSTRING(masterKey, 4, 4) = '$sba->MRU'");
                                            // $totUnBilled = DB::select("select count(*) totBilled from sap_inputs where bill_status = 'N' and subDivisionCode = '$sba->subDivisionCode' and billingGroup = '$sba->billingGroup' and billingCycle = '$sba->billingCycle' and MRU = '$sba->MRU'");
                                            // dd($totBilled[0]->totBilled);
                                          @endphp
                                          <td class="font-w600" style="padding: 0px;">{{ $totBilled[0]->totBilled }}</td>
                                          <td class="font-w600" style="padding: 0px;">{{ $sba->totalCons - $totBilled[0]->totBilled }}</td>
                                      </tr>
                                    @endforeach
                                  @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- END Dynamic Table Full -->
                </div>
                <!-- END Page Content -->
            </main>
            <!-- END Main Container -->

            <!-- Footer -->
            @include('includes/footer')
            <!-- END Footer -->
        </div>
        <!-- END Page Container -->

        <!-- Codebase Core JS -->
        <script src="{{ config('app.url') }}/assets/js/core/jquery.min.js"></script>
        <script src="{{ config('app.url') }}/assets/js/core/bootstrap.bundle.min.js"></script>
        <script src="{{ config('app.url') }}/assets/js/core/jquery.slimscroll.min.js"></script>
        <script src="{{ config('app.url') }}/assets/js/core/jquery.scrollLock.min.js"></script>
        <script src="{{ config('app.url') }}/assets/js/core/jquery.appear.min.js"></script>
        <script src="{{ config('app.url') }}/assets/js/core/jquery.countTo.min.js"></script>
        <script src="{{ config('app.url') }}/assets/js/core/js.cookie.min.js"></script>
        <script src="{{ config('app.url') }}/assets/js/codebase.js"></script>

        <!-- Page JS Plugins -->
        {{-- <script src="{{ config('app.url') }}/assets/js/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="{{ config('app.url') }}/assets/js/plugins/datatables/dataTables.bootstrap4.min.js"></script> --}}
        <script src="{{ config('app.url') }}/assets/js/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="{{ config('app.url') }}/assets/js/plugins/datatables/dataTables.bootstrap4.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>

        <!-- Page JS Code -->
        <script src="{{ config('app.url') }}/assets/js/pages/be_tables_datatables.js"></script>
        <script>
          $(document).ready(function(){
              $('[data-toggle="tooltip"]').tooltip();
          });
        </script>
        <script type="text/javascript">
        function getGroupCycleAjaxNonSap(e) {

          $('#billingGroup')
              .empty()
              .append('<option value="">Select</option>')
          ;
          $('#billingCycle')
              .empty()
              .append('<option value="">Select</option>')
          ;
          $('#ledgerCode')
              .empty()
              .append('<option value="">Select</option>')
          ;

          // console.log(e.value);
          var subDivisionCode = e.value;
          $.ajax({
            headers: {
               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
           },
          type: "POST",
          url: '/api/getGroupCycleAjaxNonSap',
          data: {subDivisionCode: subDivisionCode},
            success: function( data ) {
                console.log(data);
                $.each(data.billingGroup, function(key, value) {
                     $('#billingGroup')
                         .append($("<option></option>")
                                    .attr("value",value.billingGroup)
                                    .text(value.billingGroup));
                });
                // $.each(data.billingCycle, function(key, value) {
                //   // console.log(value.billingCycle);
                //      $('#billingCycle')
                //          .append($("<option></option>")
                //                     .attr("value",value.billingCycle)
                //                     .text(value.billingCycle));
                // });
                // $.each(data.ledgerCodes, function(key, value) {
                //   // console.log(value);
                //      $('#ledgerCode')
                //          .append($("<option></option>")
                //                     .attr("value",value.ledgerCode)
                //                     .text(value.ledgerCode));
                // });
                // $("#ajaxResponse").append("<div>"+msg+"</div>");
            }
          });
        }

        function getGroupCycleAjaxNonSapBc(e) {

          // console.log(e.value);
          $('#billingCycle')
              .empty()
              .append('<option value="">Select</option>')
          ;
          $('#ledgerCode')
              .empty()
              .append('<option value="">Select</option>')
          ;

          // console.log(e.value);
          var subDivisionCode = $('#subDivisionCode').val();
          var billingGroup = e.value;
          $.ajax({
            headers: {
               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
           },
          type: "POST",
          url: '/api/getGroupCycleAjaxNonSapBc',
          data: {subDivisionCode: subDivisionCode, billingGroup: billingGroup},
            success: function( data ) {
                console.log(data);
                $.each(data.billingCycle, function(key, value) {
                  // console.log(value.billingCycle);
                     $('#billingCycle')
                         .append($("<option></option>")
                                    .attr("value",value.billingCycle)
                                    .text(value.billingCycle));
                });
                // $("#ajaxResponse").append("<div>"+msg+"</div>");
            }
          });
        }

          function getmeterReaders(e) {

            $('#meterReaders')
                .empty()
                .append('<option value="">Select</option>')
            ;

            // console.log(e.value);
            var subDivisionCode = $('#subDivisionCode').val();
            var billingGroup = $('#billingGroup').val();
            var billingCycle = $('#billingCycle').val();
            var from = $('#from').val();
            var to = $('#to').val();
            console.log(subDivisionCode);
            console.log(from);
            console.log(to);
            $.ajax({
              headers: {
                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
             },
            type: "POST",
            url: '/api/get-meter-reader-sub-bg-bc-non-sap',
            data: {subDivisionCode: subDivisionCode, billingGroup: billingGroup, billingCycle: billingCycle, from: from, to: to},
              success: function( data ) {
                  console.log(data);
                  $.each(data.meterReaders, function(key, value) {
                       $('#meterReaders')
                           .append($("<option></option>")
                                      .attr("value",value.meterReader)
                                      .text(value.meterReader));
                  });
              }
            });
          }



        </script>
    </body>
</html>
