<!doctype html>
<!--[if lte IE 9]>     <html lang="en" class="no-focus lt-ie10 lt-ie10-msg"> <![endif]-->
<!--[if gt IE 9]><!--> <html lang="en" class="no-focus"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">

        <title>Bind Complete Ledger ( NON - SAP )- STERLING Transformers</title>

        <meta name="description" content="Codebase - Bootstrap 4 Admin Template &amp; UI Framework created by pixelcave and published on Themeforest">
        <meta name="author" content="pixelcave">
        <meta name="robots" content="noindex, nofollow">
        <meta name="csrf-token" content="{{ csrf_token() }}" />

        <!-- Open Graph Meta -->
        <meta property="og:title" content="Codebase - Bootstrap 4 Admin Template &amp; UI Framework">
        <meta property="og:site_name" content="Codebase">
        <meta property="og:description" content="Codebase - Bootstrap 4 Admin Template &amp; UI Framework created by pixelcave and published on Themeforest">
        <meta property="og:type" content="website">
        <meta property="og:url" content="">
        <meta property="og:image" content="">

        <!-- Icons -->
        <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
        <link rel="shortcut icon" href="{{ config('app.url') }}/assets/img/favicons/favicon.png">
        <link rel="icon" type="image/png" sizes="192x192" href="{{ config('app.url') }}/assets/img/favicons/favicon-192x192.png">
        <link rel="apple-touch-icon" sizes="180x180" href="{{ config('app.url') }}/assets/img/favicons/apple-touch-icon-180x180.png">
        <!-- END Icons -->

        <!-- Stylesheets -->
        <!-- Codebase framework -->
        <link rel="stylesheet" id="css-main" href="{{ config('app.url') }}/assets/css/codebase.css">

        <!-- You can include a specific file from css/themes/ folder to alter the default color theme of the template. eg: -->
        <!-- <link rel="stylesheet" id="css-theme" href="assets/css/themes/flat.min.css"> -->
        <!-- END Stylesheets -->
    </head>
    <body>
        <!-- Page Container -->
        <!--
            Available classes for #page-container:

        GENERIC

            'enable-cookies'                            Remembers active color theme between pages (when set through color theme helper Codebase() -> uiHandleTheme())

        SIDEBAR & SIDE OVERLAY

            'sidebar-r'                                 Right Sidebar and left Side Overlay (default is left Sidebar and right Side Overlay)
            'sidebar-mini'                              Mini hoverable Sidebar (screen width > 991px)
            'sidebar-o'                                 Visible Sidebar by default (screen width > 991px)
            'sidebar-o-xs'                              Visible Sidebar by default (screen width < 992px)
            'sidebar-inverse'                           Dark themed sidebar

            'side-overlay-hover'                        Hoverable Side Overlay (screen width > 991px)
            'side-overlay-o'                            Visible Side Overlay by default

            'side-scroll'                               Enables custom scrolling on Sidebar and Side Overlay instead of native scrolling (screen width > 991px)

        HEADER

            ''                                          Static Header if no class is added
            'page-header-fixed'                         Fixed Header

        HEADER STYLE

            ''                                          Classic Header style if no class is added
            'page-header-modern'                        Modern Header style
            'page-header-inverse'                       Dark themed Header (works only with classic Header style)
            'page-header-glass'                         Light themed Header with transparency by default
                                                        (absolute position, perfect for light images underneath - solid light background on scroll if the Header is also set as fixed)
            'page-header-glass page-header-inverse'     Dark themed Header with transparency by default
                                                        (absolute position, perfect for dark images underneath - solid dark background on scroll if the Header is also set as fixed)

        MAIN CONTENT LAYOUT

            ''                                          Full width Main Content if no class is added
            'main-content-boxed'                        Full width Main Content with a specific maximum width (screen width > 1200px)
            'main-content-narrow'                       Full width Main Content with a percentage width (screen width > 1200px)
        -->
        <div id="page-container" class="sidebar-o side-scroll page-header-modern main-content-boxed">
            <!-- Side Overlay-->

            <!-- END Side Overlay -->

            <!-- Sidebar -->
            <!--
                Helper classes

                Adding .sidebar-mini-hide to an element will make it invisible (opacity: 0) when the sidebar is in mini mode
                Adding .sidebar-mini-show to an element will make it visible (opacity: 1) when the sidebar is in mini mode
                    If you would like to disable the transition, just add the .sidebar-mini-notrans along with one of the previous 2 classes

                Adding .sidebar-mini-hidden to an element will hide it when the sidebar is in mini mode
                Adding .sidebar-mini-visible to an element will show it only when the sidebar is in mini mode
                    - use .sidebar-mini-visible-b if you would like to be a block when visible (display: block)
            -->
@include('includes/headerAndSidebar')

            <!-- Main Container -->
            <main id="main-container">
                <!-- Page Content -->
                <div class="content">
                    <!-- Bootstrap Design -->
                    <h2 class="content-heading">Bind Complete Ledger ( NON - SAP )</h2>
                    <div class="row">
                        <div class="col-md-12">
                            <!-- Default Elements -->
                            <div class="block">
                                <div class="block-header block-header-default">
                                    <h3 class="block-title">Form</h3>
                                    <div class="block-options">
                                        <button type="button" class="btn-block-option">
                                            <i class="si si-wrench"></i>
                                        </button>
                                    </div>
                                </div>
                                <div class="block-content">
                                    <form class="" action="{{ route('bindPostLedger') }}" method="post" id="bindLedger">
                                      @csrf
                                      <div class="form-group row">
                                            <div class="col-md-3">
                                              <label for="subDivisionCode">Sub Division Code <span class="text-danger">*</span> </label>
                                                <select class="form-control" id="subDivisionCode" name="subDivisionCode" onchange="getGroupCycleAjaxNonSap(this)">
                                                  {{-- Sub Division Code's --}}
                                                  <option value="">Select</option>
                                                  @php
                                                    $sub = DB::select("SELECT SUBSTRING(masterKey, 1, 3) subDivCode FROM bills group by SUBSTRING(masterKey, 1, 3)");
                                                    // $subDivision = $sub[0]->subDivCode;
                                                    // $billingsGroups = DB::select("select distinct billingGroup from bills WHERE SUBSTRING(masterKey, 1, 3) in ('$subDivision') group by billingGroup");
                                                    // $billingsCycles = DB::select("select distinct billingCycle from bills WHERE SUBSTRING(masterKey, 1, 3) in ('O22') group by billingCycle");
                                                    // $ledgerCodes = DB::select("SELECT SUBSTRING(masterKey, 4, 4) ledgerCode FROM bills group by SUBSTRING(masterKey, 4, 4)");
                                                    // dd($sub);
                                                  @endphp
                                                  @foreach ($sub as $subDivision)
                                                    <option value="{{ $subDivision->subDivCode }}">{{ $subDivision->subDivCode }}</option>
                                                  @endforeach
                                                </select>
                                                {{-- <input type="text" class="form-control" id="subDivisionCode" name="subDivisionCode" placeholder="Sub Division Code" required> --}}
                                                {{-- <div class="form-text text-muted">Further info about this input</div> --}}
                                                @if ($errors->has('subDivisionCode'))
                                             <span class="help-block">
                                             <strong class="text-danger">{{ $errors->first('subDivisionCode') }}</strong>
                                              </span>
                                                @endif
                                            </div>
                                            <div class="col-md-3">
                                              <label for="billingGroup">Billing Group <span class="text-danger">*</span> </label>
                                              <select class="form-control" name="billingGroup" id="billingGroup" required onchange="getGroupCycleAjaxNonSapBc(this)">
                                                <option value="">Select</option>
                                                {{-- @foreach ($billingsGroups as $billingGroup)
                                                  <option value="{{ $billingGroup->billingGroup }}">{{ $billingGroup->billingGroup }}</option>
                                                @endforeach --}}
                                              </select>
                                                {{-- <div class="form-text text-muted">Further info about this input</div> --}}
                                                @if ($errors->has('billingGroup'))
                                             <span class="help-block">
                                             <strong class="text-danger">{{ $errors->first('billingGroup') }}</strong>
                                              </span>
                                                @endif
                                            </div>
                                            <div class="col-md-3">
                                              <label for="billingCycle">Billing Cycle <span class="text-danger">*</span></label>
                                              <select class="form-control" name="billingCycle" id="billingCycle" required onchange="getGroupCycleAjaxNonSapLc(this)">
                                                <option value="">Select</option>
                                                {{-- @foreach ($billingsCycles as $billingCycle)
                                                  <option value="{{ $billingCycle->billingCycle }}">{{ $billingCycle->billingCycle }}</option>
                                                @endforeach --}}
                                              </select>
                                                {{-- <div class="form-text text-muted">Further info about this input</div> --}}
                                                @if ($errors->has('billingCycle'))
                                             <span class="help-block">
                                             <strong class="text-danger">{{ $errors->first('billingCycle') }}</strong>
                                              </span>
                                                @endif
                                            </div>
                                            <div class="col-md-3">
                                              <label for="ledgerCode">Ledger Code <span class="text-danger">*</span> </label>
                                                <select class="form-control" id="ledgerCode" name="ledgerCode">
                                                  <option value="">Select</option>
                                                  {{-- @foreach ($ledgerCodes as $ledgerCode)
                                                    <option value="{{ $ledgerCode->ledgerCode }}">{{ $ledgerCode->ledgerCode }}</option>
                                                  @endforeach --}}
                                                </select>
                                                {{-- <input type="text" class="form-control" id="ledgerCode" name="ledgerCode" placeholder="Ledger Code" required> --}}
                                                {{-- <div class="form-text text-muted">Further info about this input</div> --}}
                                                @if ($errors->has('ledgerCode'))
                                             <span class="help-block">
                                             <strong class="text-danger">{{ $errors->first('ledgerCode') }}</strong>
                                              </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                          <div class="col-md-3">
                                            <label for="meterReader">Meter Reader <span class="text-danger">*</span></label>
                                            <select class="form-control" name="meterReader" id="meterReader" required>
                                              <option value="">Select</option>
                                              @if (isset($data))
                                                @foreach ($data as $meterReader)
                                                  <option value="{{ $meterReader->id }}">{{ $meterReader->name . ' - ' . $meterReader->email }}</option>
                                                @endforeach
                                              @endif
                                            </select>
                                            @if ($errors->has('meterReader'))
                                         <span class="help-block">
                                         <strong class="text-danger">{{ $errors->first('meterReader') }}</strong>
                                          </span>
                                            @endif
                                          </div>
                                            <div class="col-md-1">
                                              <label for="accountNoL">. </label>
                                                <button type="submit" class="btn btn-alt-primary">Submit</button>
                                            </div>
                                        </div>
                                    </form>

                                    <div class="col-12">
                                      <span id="status" class="text-warning"></span>
                                    </div>
                                </div>
                            </div>
                            <!-- END Default Elements -->
                        </div>

                    </div>

                    <!-- END Bootstrap Design -->
                </div>
                <!-- END Page Content -->
            </main>
            <!-- END Main Container -->

            <!-- Footer -->
            @include('includes/footer')
            <!-- END Footer -->
        </div>
        <!-- END Page Container -->

        <!-- Codebase Core JS -->
        <script src="{{ config('app.url') }}/assets/js/core/jquery.min.js"></script>
        <script src="{{ config('app.url') }}/assets/js/core/bootstrap.bundle.min.js"></script>
        <script src="{{ config('app.url') }}/assets/js/core/jquery.slimscroll.min.js"></script>
        <script src="{{ config('app.url') }}/assets/js/core/jquery.scrollLock.min.js"></script>
        <script src="{{ config('app.url') }}/assets/js/core/jquery.appear.min.js"></script>
        <script src="{{ config('app.url') }}/assets/js/core/jquery.countTo.min.js"></script>
        <script src="{{ config('app.url') }}/assets/js/core/js.cookie.min.js"></script>
        <script src="{{ config('app.url') }}/assets/js/codebase.js"></script>

        <!-- Page JS Plugins -->
        <script src="{{ config('app.url') }}/assets/js/plugins/chartjs/Chart.bundle.min.js"></script>

        <!-- Page JS Code -->
        <script src="{{ config('app.url') }}/assets/js/pages/be_pages_dashboard.js"></script>
        <script type="text/javascript">
          function getGroupCycleAjaxNonSap(e) {

            $('#billingGroup')
                .empty()
                .append('<option value="">Select</option>')
            ;
            $('#billingCycle')
                .empty()
                .append('<option value="">Select</option>')
            ;
            $('#ledgerCode')
                .empty()
                .append('<option value="">Select</option>')
            ;

            // console.log(e.value);
            var subDivisionCode = e.value;
            $.ajax({
              headers: {
                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
             },
            type: "POST",
            url: '/api/getGroupCycleAjaxNonSap',
            data: {subDivisionCode: subDivisionCode},
              success: function( data ) {
                  console.log(data);
                  $.each(data.billingGroup, function(key, value) {
                       $('#billingGroup')
                           .append($("<option></option>")
                                      .attr("value",value.billingGroup)
                                      .text(value.billingGroup));
                  });
                  // $.each(data.billingCycle, function(key, value) {
                  //   // console.log(value.billingCycle);
                  //      $('#billingCycle')
                  //          .append($("<option></option>")
                  //                     .attr("value",value.billingCycle)
                  //                     .text(value.billingCycle));
                  // });
                  // $.each(data.ledgerCodes, function(key, value) {
                  //   // console.log(value);
                  //      $('#ledgerCode')
                  //          .append($("<option></option>")
                  //                     .attr("value",value.ledgerCode)
                  //                     .text(value.ledgerCode));
                  // });
                  // $("#ajaxResponse").append("<div>"+msg+"</div>");
              }
            });
          }

          function getGroupCycleAjaxNonSapBc(e) {

            // console.log(e.value);
            $('#billingCycle')
                .empty()
                .append('<option value="">Select</option>')
            ;
            $('#ledgerCode')
                .empty()
                .append('<option value="">Select</option>')
            ;

            // console.log(e.value);
            var subDivisionCode = $('#subDivisionCode').val();
            var billingGroup = e.value;
            $.ajax({
              headers: {
                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
             },
            type: "POST",
            url: '/api/getGroupCycleAjaxNonSapBc',
            data: {subDivisionCode: subDivisionCode, billingGroup: billingGroup},
              success: function( data ) {
                  console.log(data);
                  $.each(data.billingCycle, function(key, value) {
                    // console.log(value.billingCycle);
                       $('#billingCycle')
                           .append($("<option></option>")
                                      .attr("value",value.billingCycle)
                                      .text(value.billingCycle));
                  });
                  // $("#ajaxResponse").append("<div>"+msg+"</div>");
              }
            });
          }

          // Get Ledger Code
          function getGroupCycleAjaxNonSapLc(e) {

            // console.log(e.value);
            $('#ledgerCode')
                .empty()
                .append('<option value="">Select</option>')
            ;

            // console.log(e.value);
            var subDivisionCode = $('#subDivisionCode').val();
            var billingGroup = $('#billingGroup').val();
            var billingCycle = e.value;
            $.ajax({
              headers: {
                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
             },
            type: "POST",
            url: '/api/getGroupCycleAjaxNonSapLc',
            data: {subDivisionCode: subDivisionCode, billingGroup: billingGroup, billingCycle: billingCycle},
              success: function( data ) {
                  console.log(data);
                  $.each(data.ledgerCodes, function(key, value) {
                    // console.log(value.ledgerCode);
                       $('#ledgerCode')
                           .append($("<option></option>")
                                      .attr("value",value.ledgerCode)
                                      .text(value.ledgerCode));
                  });
                  // $("#ajaxResponse").append("<div>"+msg+"</div>");
              }
            });
          }


          $('#bindLedger').submit(function(ev) {
              ev.preventDefault(); // to stop the form from submitting
              /* Validations go here */
              // console.log('pppppppppppppppp');
              var subDivisionCode = $('#subDivisionCode').val();
              var billingGroup = $('#billingGroup').val();
              var billingCycle = $('#billingCycle').val();
              var ledgerCode = $('#ledgerCode').val();
              var meterReader = $('#meterReader').val();
              console.log(subDivisionCode);
              console.log(billingGroup);
              console.log(billingCycle);
              console.log(ledgerCode);
              console.log(meterReader);

              $.ajax({
                headers: {
                   'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
               },
              type: "POST",
              url: '/api/bindLedgerNonSap',
              data: {subDivisionCode: subDivisionCode, billingGroup: billingGroup, billingCycle: billingCycle, ledgerCode: ledgerCode, meterReader: meterReader},
                success: function(data) {
                    console.log(data);
                    if (data.status == 'Y') {
                      var status = "Bind Successful";
                    } else if (data.status == 'N') {
                      var status = "Bind failed";
                    } else if (data.status == 'A') {
                      var status = "Already Binded";
                    } else {
                      var status = "Contact Admin";
                    }

                    $('#status').html(status);
                }
              });
          });
        </script>
    </body>
</html>







{{-- @extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Upload</div>

                <div class="card-body">
                    @can ('upload-bill')
                      yes
                      <form class="" action="{{ route('uploadStore') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <input type="file" name="file" >
                        <input type="submit" name="submit" value="Upload">
                      </form>
                    @endcan
                </div>
            </div>
        </div>
    </div>
</div>
@endsection --}}
