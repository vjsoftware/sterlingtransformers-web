@php function getValue($old = "", $newValue) { if ($old != '') { $name = old('name'); } else { $name = $newValue; } return $name; } @endphp

<!doctype html>
<!--[if lte IE 9]>     <html lang="en" class="no-focus lt-ie10 lt-ie10-msg"> <![endif]-->
<!--[if gt IE 9]><!-->
<html lang="en" class="no-focus">
<!--<![endif]-->

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">

    <title>Advice-80 Delete View  STERLING Transformers</title>

    <meta name="description" content="Codebase - Bootstrap 4 Admin Template &amp; UI Framework created by pixelcave and published on Themeforest">
    <meta name="author" content="pixelcave">
    <meta name="robots" content="noindex, nofollow">

    <!-- Open Graph Meta -->
    <meta property="og:title" content="Codebase - Bootstrap 4 Admin Template &amp; UI Framework">
    <meta property="og:site_name" content="Codebase">
    <meta property="og:description" content="Codebase - Bootstrap 4 Admin Template &amp; UI Framework created by pixelcave and published on Themeforest">
    <meta property="og:type" content="website">
    <meta property="og:url" content="">
    <meta property="og:image" content="">

    <!-- Icons -->
    <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
    <link rel="shortcut icon" href="{{ config('app.url') }}/assets/img/favicons/favicon.png">
    <link rel="icon" type="image/png" sizes="192x192" href="{{ config('app.url') }}/assets/img/favicons/favicon-192x192.png">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ config('app.url') }}/assets/img/favicons/apple-touch-icon-180x180.png">
    <!-- END Icons -->

    <!-- Stylesheets -->
    <!-- Codebase framework -->
    <link rel="stylesheet" id="css-main" href="{{ config('app.url') }}/assets/css/codebase.min.css">
    <link rel="stylesheet" href="{{config('app.url')}}/assets/js/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css">

    <!-- You can include a specific file from css/themes/ folder to alter the default color theme of the template. eg: -->
    <!-- <link rel="stylesheet" id="css-theme" href="assets/css/themes/flat.min.css"> -->
    <!-- END Stylesheets -->
</head>

<body>
    <!-- Page Container -->
    <!--
            Available classes for #page-container:

        GENERIC

            'enable-cookies'                            Remembers active color theme between pages (when set through color theme helper Codebase() -> uiHandleTheme())

        SIDEBAR & SIDE OVERLAY

            'sidebar-r'                                 Right Sidebar and left Side Overlay (default is left Sidebar and right Side Overlay)
            'sidebar-mini'                              Mini hoverable Sidebar (screen width > 991px)
            'sidebar-o'                                 Visible Sidebar by default (screen width > 991px)
            'sidebar-o-xs'                              Visible Sidebar by default (screen width < 992px)
            'sidebar-inverse'                           Dark themed sidebar

            'side-overlay-hover'                        Hoverable Side Overlay (screen width > 991px)
            'side-overlay-o'                            Visible Side Overlay by default

            'side-scroll'                               Enables custom scrolling on Sidebar and Side Overlay instead of native scrolling (screen width > 991px)

        HEADER

            ''                                          Static Header if no class is added
            'page-header-fixed'                         Fixed Header

        HEADER STYLE

            ''                                          Classic Header style if no class is added
            'page-header-modern'                        Modern Header style
            'page-header-inverse'                       Dark themed Header (works only with classic Header style)
            'page-header-glass'                         Light themed Header with transparency by default
                                                        (absolute position, perfect for light images underneath - solid light background on scroll if the Header is also set as fixed)
            'page-header-glass page-header-inverse'     Dark themed Header with transparency by default
                                                        (absolute position, perfect for dark images underneath - solid dark background on scroll if the Header is also set as fixed)

        MAIN CONTENT LAYOUT

            ''                                          Full width Main Content if no class is added
            'main-content-boxed'                        Full width Main Content with a specific maximum width (screen width > 1200px)
            'main-content-narrow'                       Full width Main Content with a percentage width (screen width > 1200px)
        -->
    <div id="page-container" class="sidebar-o side-scroll page-header-modern main-content-boxed">
        <!-- Side Overlay-->

        <!-- END Side Overlay -->

        <!-- Sidebar -->
        <!--
                Helper classes

                Adding .sidebar-mini-hide to an element will make it invisible (opacity: 0) when the sidebar is in mini mode
                Adding .sidebar-mini-show to an element will make it visible (opacity: 1) when the sidebar is in mini mode
                    If you would like to disable the transition, just add the .sidebar-mini-notrans along with one of the previous 2 classes

                Adding .sidebar-mini-hidden to an element will hide it when the sidebar is in mini mode
                Adding .sidebar-mini-visible to an element will show it only when the sidebar is in mini mode
                    - use .sidebar-mini-visible-b if you would like to be a block when visible (display: block)
            -->
        @include('includes/headerAndSidebar')

        <!-- Main Container -->
        <main id="main-container">
            <!-- Page Content -->
            <div class="content">
                <!-- Bootstrap Design -->
                <h2 class="content-heading">Advice-80 Delete</h2>
                <div class="row">
                    <div class="col-md-12">
                        <!-- Default Elements -->
                        <div class="block">
                            <div class="block-header block-header-default">
                                <h3 class="block-title">Form</h3>
                            </div>
                            <div class="block-content">
                                <form class="" id="submitForm" action="{{route('changeOfAcdDelete') }}" method="post" enctype="multipart/form-data">
                                    @csrf
                                    <div class="form-group row">


                                      <div class="col-md-3">
                                          <label for="dReason">Reason</label>
                                          {{-- <input type="text" class="form-control" id="uReason" name="uReason" value="{{ old('uReason') }}" placeholder="Update Reason"> --}}
                                          <textarea name="dReason" class="form-control" value="{{ old('dReason') }}" placeholder="Update Reason"></textarea>

                                          @if ($errors->has('dReason'))
                                          <span class="help-block">
                                           <strong class="text-danger">{{ $errors->first('dReason') }}</strong>
                                            </span> @endif
                                      </div>
                                      <div class="col-md-1">
                                          <label for="">. </label>
                                          <input type="hidden" class="form-control" id="id" name="id" value="{{ getValue(old('id'), $data->id) }}">

                                          <button type="submit" id="submit" name="delete" class="btn btn-alt-danger">Delete</button>

                                      </div>
                                      {{-- <div class="col-md-1">
                                          <label for="">. </label>
                                          <button type="submit" id="submit" name="delete" class="btn btn-alt-danger">Delete</button>
                                      </div> --}}
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- END Default Elements -->
                    </div>

                </div>

                <!-- END Bootstrap Design -->
            </div>
            <!-- END Page Content -->
        </main>
        <!-- END Main Container -->

        <!-- Footer -->
        @include('includes/footer')
        <!-- END Footer -->
    </div>
    <!-- END Page Container -->

    <!-- Codebase Core JS -->
    <script src="{{ config('app.url') }}/assets/js/core/jquery.min.js"></script>
    <script src="{{ config('app.url') }}/assets/js/core/bootstrap.bundle.min.js"></script>
    <script src="{{ config('app.url') }}/assets/js/core/jquery.slimscroll.min.js"></script>
    <script src="{{ config('app.url') }}/assets/js/core/jquery.scrollLock.min.js"></script>
    <script src="{{ config('app.url') }}/assets/js/core/jquery.appear.min.js"></script>
    <script src="{{ config('app.url') }}/assets/js/core/jquery.countTo.min.js"></script>
    <script src="{{ config('app.url') }}/assets/js/core/js.cookie.min.js"></script>
    <script src="{{ config('app.url') }}/assets/js/codebase.js"></script>

    <!-- Page JS Plugins -->
    <script src="{{ config('app.url') }}/assets/js/plugins/chartjs/Chart.bundle.min.js"></script>
    <script src="{{config('app.url')}}/assets/js/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>

    <!-- Page JS Code -->
    <script src="{{ config('app.url') }}/assets/js/pages/be_pages_dashboard.js"></script>

    <script>
            jQuery(function () {
                // Init page helpers (BS Datepicker + BS Colorpicker + BS Maxlength + Select2 + Masked Input + Range Sliders + Tags Inputs plugins)
                Codebase.helpers(['datepicker', 'colorpicker', 'maxlength', 'select2', 'masked-inputs', 'rangeslider', 'tags-inputs']);
            });
        </script>

        {{-- <script type="text/javascript">
        $('#submit').on('click', function functionName(e) {
          e.preventDefault();
          var datastring = $("#submitForm").serialize();
          $.ajax({
            headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
              type: "POST",
              url: "{{ route('batchAjax') }}",
              data: datastring,
              success: function(data) {
                // console.log(data);
                   alert('Data send');
                   $('#accountNo').val('');
                   $('#consumerName').val('');
                   $('#address').val('');
                   $('#feederCode').val('');
                   $('#tariffType').val('');
                   $('#employeeConsessionCode').val('');
                   $('#voltClassCode').val('');
                   $('#connectedLoad').val('');
                   $('#meterRatio').val('');
                   $('#connectionDate').val('');
                   $('#lineCtRatio').val('');
                   $('#meterMultiplier').val('');
                   $('#overallMultiply').val('');
                   $('#phaseCode').val('');
                   $('#meterSerialNo').val('');
                   $('#amps').val('');
                   $('#mfsCode').val('');
                   $('#mcbRent').val('');
                   $('#meterReading').val('');
                   $('#ruralUrbenConsumer').val('');
                   $('#meterLocationCode').val('');
                   $('#meterSecurity').val('');
                   $('#acd').val('');
                   $('#nrs').val('');
              }
          });
          console.log('save');
        });
        </script> --}}
</body>

</html>

{{-- @extends('layouts.app') @section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Upload</div>

                <div class="card-body">
                    @can ('upload-bill') yes
                    <form class="" action="{{ route('uploadStore') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <input type="file" name="file">
                        <input type="submit" name="submit" value="Upload">
                    </form>
                    @endcan
                </div>
            </div>
        </div>
    </div>
</div>
@endsection --}}
