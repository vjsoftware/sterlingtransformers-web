@php
  use App\sapOutput;
@endphp
<!doctype html>
<!--[if lte IE 9]>     <html lang="en" class="no-focus lt-ie10 lt-ie10-msg"> <![endif]-->
<!--[if gt IE 9]><!--> <html lang="en" class="no-focus"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">

        <title>Consumer Enquiry Form - STERLING Transformers</title>

        <meta name="description" content="Codebase - Bootstrap 4 Admin Template &amp; UI Framework created by pixelcave and published on Themeforest">
        <meta name="author" content="pixelcave">
        <meta name="robots" content="noindex, nofollow">

        <!-- Open Graph Meta -->
        <meta property="og:title" content="Codebase - Bootstrap 4 Admin Template &amp; UI Framework">
        <meta property="og:site_name" content="Codebase">
        <meta property="og:description" content="Codebase - Bootstrap 4 Admin Template &amp; UI Framework created by pixelcave and published on Themeforest">
        <meta property="og:type" content="website">
        <meta property="og:url" content="">
        <meta property="og:image" content="">

        <!-- Icons -->
        <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
        <link rel="shortcut icon" href="{{ config('app.url') }}/assets/img/favicons/favicon.png">
        <link rel="icon" type="image/png" sizes="192x192" href="{{ config('app.url') }}/assets/img/favicons/favicon-192x192.png">
        <link rel="apple-touch-icon" sizes="180x180" href="{{ config('app.url') }}/assets/img/favicons/apple-touch-icon-180x180.png">
        <!-- END Icons -->

        <!-- Stylesheets -->
        <!-- Codebase framework -->
        <link rel="stylesheet" id="css-main" href="{{ config('app.url') }}/assets/css/codebase.min.css">

        <!-- You can include a specific file from css/themes/ folder to alter the default color theme of the template. eg: -->
        <!-- <link rel="stylesheet" id="css-theme" href="assets/css/themes/flat.min.css"> -->
        <!-- END Stylesheets -->
    </head>
    <body>
        <!-- Page Container -->
        <!--
            Available classes for #page-container:

        GENERIC

            'enable-cookies'                            Remembers active color theme between pages (when set through color theme helper Codebase() -> uiHandleTheme())

        SIDEBAR & SIDE OVERLAY

            'sidebar-r'                                 Right Sidebar and left Side Overlay (default is left Sidebar and right Side Overlay)
            'sidebar-mini'                              Mini hoverable Sidebar (screen width > 991px)
            'sidebar-o'                                 Visible Sidebar by default (screen width > 991px)
            'sidebar-o-xs'                              Visible Sidebar by default (screen width < 992px)
            'sidebar-inverse'                           Dark themed sidebar

            'side-overlay-hover'                        Hoverable Side Overlay (screen width > 991px)
            'side-overlay-o'                            Visible Side Overlay by default

            'side-scroll'                               Enables custom scrolling on Sidebar and Side Overlay instead of native scrolling (screen width > 991px)

        HEADER

            ''                                          Static Header if no class is added
            'page-header-fixed'                         Fixed Header

        HEADER STYLE

            ''                                          Classic Header style if no class is added
            'page-header-modern'                        Modern Header style
            'page-header-inverse'                       Dark themed Header (works only with classic Header style)
            'page-header-glass'                         Light themed Header with transparency by default
                                                        (absolute position, perfect for light images underneath - solid light background on scroll if the Header is also set as fixed)
            'page-header-glass page-header-inverse'     Dark themed Header with transparency by default
                                                        (absolute position, perfect for dark images underneath - solid dark background on scroll if the Header is also set as fixed)

        MAIN CONTENT LAYOUT

            ''                                          Full width Main Content if no class is added
            'main-content-boxed'                        Full width Main Content with a specific maximum width (screen width > 1200px)
            'main-content-narrow'                       Full width Main Content with a percentage width (screen width > 1200px)
        -->
        <div id="page-container" class="sidebar-o side-scroll page-header-modern main-content-boxed">
            <!-- Side Overlay-->

            <!-- END Side Overlay -->

            <!-- Sidebar -->
            <!--
                Helper classes

                Adding .sidebar-mini-hide to an element will make it invisible (opacity: 0) when the sidebar is in mini mode
                Adding .sidebar-mini-show to an element will make it visible (opacity: 1) when the sidebar is in mini mode
                    If you would like to disable the transition, just add the .sidebar-mini-notrans along with one of the previous 2 classes

                Adding .sidebar-mini-hidden to an element will hide it when the sidebar is in mini mode
                Adding .sidebar-mini-visible to an element will show it only when the sidebar is in mini mode
                    - use .sidebar-mini-visible-b if you would like to be a block when visible (display: block)
            -->
@include('includes/headerAndSidebar')

            <!-- Main Container -->
            <main id="main-container">
                <!-- Page Content -->
                <div class="content">
                    <!-- Bootstrap Design -->
                    <h2 class="content-heading">Consumer Enquiry Form</h2>
                    <div class="row">
                        <div class="col-md-12">
                            <!-- Default Elements -->
                            <div class="block">
                                <div class="block-header block-header-default">
                                    <h3 class="block-title">Form</h3>
                                </div>
                                <div class="block-content">
                                    <form class="" action="{{ route('consumerEnquiry') }}" method="post" enctype="multipart/form-data">
                                      @csrf
                                      <div class="form-group row" >
                                        <div class="col-3">
                                          <label class="col-12" for="legacyNumber">Legacy Number <span class="text-danger">*</span></label>
                                          <div class="col-12">
                                            <input type="text" class="form-control" id="legacyNumber" name="legacyNumber">
                                          </div>
                                        </div>
                                        <div class="col-3">
                                          <label class="col-12" for="acNumber">Contract AC Number <span class="text-danger">*</span></label>
                                          <div class="col-12">
                                            <input type="text" class="form-control" id="acNumber" name="acNumber">
                                          </div>
                                        </div>
                                        <div class="col-3">
                                          <label class="col-12" for="meterNumber">Meter Number <span class="text-danger">*</span></label>
                                          <div class="col-12">
                                            <input type="text" class="form-control" id="meterNumber" name="meterNumber">
                                          </div>
                                        </div>
                                        <div class="col-3">
                                          <label class="col-12"></label>
                                          <div class="col-3">
                                            <button type="submit" class="btn btn-alt-primary">Search</button>
                                          </div>

                                        </div>
                                          @if ($errors->has('file'))
                                       <span class="help-block">
                                       <strong class="text-danger">{{ $errors->first('file') }}</strong>
                                        </span>
                                          @endif
                                      </div>
                                        <div class="form-group row">

                                        </div>
                                    </form>
                                    @if (isset($data) && $data['status'] == "Y")
                                      <div class="col-12">
                                        <div class="col-4">
                                          <span class="">Legacy Number &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </span><span>{{ $data['data'][0]->consumerLegacyNumber }}</span>

                                        </div>
                                        <div class="col-3">
                                          <span class="">Contract AC Number : </span><span>{{ $data['data'][0]->contractAcNumber }}</span>
                                        </div>
                                        <div class="col-3">
                                          <span class="">Meter Number &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </span><span>{{ $data['data'][0]->meterMnfSerialNumber }}</span>
                                        </div>
                                        <br>
                                        <table class="table table-bordered table-striped js-dataTable-full">
                                            <thead>


                                                  @if (isset($data))
                                                    @php
                                                      $sno = 1;
                                                    @endphp
                                                    {{-- <tr>
                                                        <th>Consumer Name</th>
                                                        <th>Address</th>
                                                        <th>Sub Divison Code</th>
                                                        <th>Ledger Number</th>
                                                        <th>Billing Group</th>
                                                        <th>Billing Cycle</th>
                                                        <th>Meter Reader</th>
                                                        <th>Received By</th>
                                                        <th>Billing Status</th>
                                                        <th>Billed By</th>
                                                    </tr> --}}
                                                    @foreach ($data['data'] as $input)
                                                          <tr>
                                                            <td>Consumer Name</td>
                                                            <td>{{ $input->consumerName }}</td>
                                                          </tr>
                                                          <tr>
                                                            <td>Address</td>
                                                            <td>{{ $input->address }}</td>
                                                          </tr>
                                                          <tr>
                                                            <td>Sub Divison Code</td>
                                                            <td>{{ $input->subDivisionCode }}</td>
                                                          </tr>
                                                          <tr>
                                                            <td>Ledger Number</td>
                                                            <td>{{ $input->MRU }}</td>
                                                          </tr>
                                                          <tr>
                                                            <td>Billing Group</td>
                                                            <td>{{ $input->billingGroup }}</td>
                                                          </tr>
                                                          <tr>
                                                            <td>Billing Cycle</td>
                                                            <td>{{ $input->billingCycle }}</td>
                                                          </tr>
                                                          <tr>
                                                            <td>Meter Reader</td>
                                                            <td>{{ $input->meterReader }}</td>
                                                          </tr>
                                                          <tr>
                                                            <td>Received By</td>
                                                            <td>{{ $input->received_by }}</td>
                                                          </tr>
                                                          <tr>
                                                            <td>Billing Status</td>
                                                            <td>{{ $input->bill_status }}</td>
                                                          </tr>
                                                          @if ($input->bill_status == "Y")
                                                            @php
                                                              $getMeterReader = sapOutput::where('contractAcNumber', $input->contractAcNumber)->get();
                                                              // dd($getMeterReader[0]->meterReader);
                                                            @endphp
                                                            <tr>
                                                              <td>Billed By</td>
                                                              <td data-toggle="tooltip" title="">{{ $getMeterReader[0]->meterReader }}</td>
                                                            </tr>
                                                            <tr>
                                                              <td>Billed At</td>
                                                              <td data-toggle="tooltip" title="">{{ $getMeterReader[0]->billDate }}</td>
                                                            </tr>
                                                          @else
                                                            <tr>
                                                              <td>Billed By</td>
                                                              <td></td>
                                                            </tr>
                                                          @endif
                                                      </tr>
                                                    @endforeach
                                                  @endif

                                            </thead>
                                        </table>
                                      </div>
                                    @else
                                      <div class="col-3">
                                        No Records Found
                                      </div>
                                    @endif


                                </div>
                            </div>
                            <!-- END Default Elements -->
                        </div>

                    </div>

                    <!-- END Bootstrap Design -->
                </div>
                <!-- END Page Content -->
            </main>
            <!-- END Main Container -->

            <!-- Footer -->
            @include('includes/footer')
            <!-- END Footer -->
        </div>
        <!-- END Page Container -->

        <!-- Codebase Core JS -->
        <script src="{{ config('app.url') }}/assets/js/core/jquery.min.js"></script>
        <script src="{{ config('app.url') }}/assets/js/core/bootstrap.bundle.min.js"></script>
        <script src="{{ config('app.url') }}/assets/js/core/jquery.slimscroll.min.js"></script>
        <script src="{{ config('app.url') }}/assets/js/core/jquery.scrollLock.min.js"></script>
        <script src="{{ config('app.url') }}/assets/js/core/jquery.appear.min.js"></script>
        <script src="{{ config('app.url') }}/assets/js/core/jquery.countTo.min.js"></script>
        <script src="{{ config('app.url') }}/assets/js/core/js.cookie.min.js"></script>
        <script src="{{ config('app.url') }}/assets/js/codebase.js"></script>

        <!-- Page JS Plugins -->
        <script src="{{ config('app.url') }}/assets/js/plugins/chartjs/Chart.bundle.min.js"></script>

        <!-- Page JS Code -->
        <script src="{{ config('app.url') }}/assets/js/pages/be_pages_dashboard.js"></script>
    </body>
</html>







{{-- @extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Upload</div>

                <div class="card-body">
                    @can ('upload-bill')
                      yes
                      <form class="" action="{{ route('uploadStore') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <input type="file" name="file" >
                        <input type="submit" name="submit" value="Upload">
                      </form>
                    @endcan
                </div>
            </div>
        </div>
    </div>
</div>
@endsection --}}
