<!doctype html>
<!--[if lte IE 9]>     <html lang="en" class="no-focus lt-ie10 lt-ie10-msg"> <![endif]-->
<!--[if gt IE 9]><!--> <html lang="en" class="no-focus"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">

        <title>Day Close List Non SAP| STERLING Transformers</title>

        <meta name="description" content="Codebase - Bootstrap 4 Admin Template &amp; UI Framework created by pixelcave and published on Themeforest">
        <meta name="author" content="pixelcave">
        <meta name="robots" content="noindex, nofollow">

        <!-- Open Graph Meta -->
        <meta property="og:title" content="Codebase - Bootstrap 4 Admin Template &amp; UI Framework">
        <meta property="og:site_name" content="Codebase">
        <meta property="og:description" content="Codebase - Bootstrap 4 Admin Template &amp; UI Framework created by pixelcave and published on Themeforest">
        <meta property="og:type" content="website">
        <meta property="og:url" content="">
        <meta property="og:image" content="">

        <!-- Icons -->
        <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
        <link rel="shortcut icon" href="{{ config('app.url') }}/assets/img/favicons/favicon.png">
        <link rel="icon" type="image/png" sizes="192x192" href="{{ config('app.url') }}/assets/img/favicons/favicon-192x192.png">
        <link rel="apple-touch-icon" sizes="180x180" href="{{ config('app.url') }}/assets/img/favicons/apple-touch-icon-180x180.png">
        <!-- END Icons -->

        <!-- Stylesheets -->
        <!-- Page JS Plugins CSS -->
        <link rel="stylesheet" href="{{ config('app.url') }}/assets/js/plugins/datatables/dataTables.bootstrap4.min.css">

        <!-- Codebase framework -->
        <link rel="stylesheet" id="css-main" href="{{ config('app.url') }}/assets/css/codebase.css">

        <!-- You can include a specific file from css/themes/ folder to alter the default color theme of the template. eg: -->
        <!-- <link rel="stylesheet" id="css-theme" href="assets/css/themes/flat.min.css"> -->
        <!-- END Stylesheets -->
    </head>
    <body>
        <!-- Page Container -->
        <!--
            Available classes for #page-container:

        GENERIC

            'enable-cookies'                            Remembers active color theme between pages (when set through color theme helper Codebase() -> uiHandleTheme())

        SIDEBAR & SIDE OVERLAY

            'sidebar-r'                                 Right Sidebar and left Side Overlay (default is left Sidebar and right Side Overlay)
            'sidebar-mini'                              Mini hoverable Sidebar (screen width > 991px)
            'sidebar-o'                                 Visible Sidebar by default (screen width > 991px)
            'sidebar-o-xs'                              Visible Sidebar by default (screen width < 992px)
            'sidebar-inverse'                           Dark themed sidebar

            'side-overlay-hover'                        Hoverable Side Overlay (screen width > 991px)
            'side-overlay-o'                            Visible Side Overlay by default

            'side-scroll'                               Enables custom scrolling on Sidebar and Side Overlay instead of native scrolling (screen width > 991px)

        HEADER

            ''                                          Static Header if no class is added
            'page-header-fixed'                         Fixed Header

        HEADER STYLE

            ''                                          Classic Header style if no class is added
            'page-header-modern'                        Modern Header style
            'page-header-inverse'                       Dark themed Header (works only with classic Header style)
            'page-header-glass'                         Light themed Header with transparency by default
                                                        (absolute position, perfect for light images underneath - solid light background on scroll if the Header is also set as fixed)
            'page-header-glass page-header-inverse'     Dark themed Header with transparency by default
                                                        (absolute position, perfect for dark images underneath - solid dark background on scroll if the Header is also set as fixed)

        MAIN CONTENT LAYOUT

            ''                                          Full width Main Content if no class is added
            'main-content-boxed'                        Full width Main Content with a specific maximum width (screen width > 1200px)
            'main-content-narrow'                       Full width Main Content with a percentage width (screen width > 1200px)
        -->
        <div id="page-container" class="sidebar-o side-scroll page-header-modern main-content-boxed">
            <!-- Side Overlay-->
            @include('includes/headerAndSidebar')
            <!-- END Header -->

            <!-- Main Container -->
            <main id="main-container">
                <!-- Page Content -->
                <div class="content">
                    <h2 class="content-heading">Day Close List ( Non SAP )</h2>

                    <!-- Dynamic Table Full -->
                    <div class="block">
                        {{-- <div class="block-header block-header-default">
                            <h3 class="block-title">Dynamic Table <small></small></h3>
                        </div> --}}
                        <div class="block-content block-content-full">
                            <!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/be_tables_datatables.js -->
                            <table class="table table-bordered table-striped js-dataTable-full">
                                <thead>
                                    <tr>
                                        <th>S.No</th>
                                        <th>User Id</th>
                                        <th>Name</th>
                                        <th>Login Date</th>
                                        <th>Login Time</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                  @if (isset($data))
                                    @php
                                      $sno = 1;
                                    @endphp
<<<<<<< HEAD:resources/views/sundryAllowanceEdit.blade.php


                                    @foreach ($data as $record)
                                      <tr>
                                          <td class="">{{ $sno++ }}</td>
                                          <td class="">{{ $record->sysId }}</td>
                                          <td class="">{{ $record->inputCode }}</td>
                                          <td class="">{{ $record->sheetNo }}</td>
                                          <td class="">{{ $record->pageNo }}</td>
                                          <td class="">{{ $record->billingCycle }}</td>
                                          <td class="">{{ $record->billingGroup }}</td>
                                          <td class="">{{ $record->subdivisionCode}}</td>
                                          <td class="">{{ $record->accountNo}}</td>
                                          <td class="">{{ $record->checkDigit}}</td>
                                          <td class="">{{ $record->sop}}</td>
                                          <td class=""><a href="{{ config('app.url') }}/sundryAllowanceEditView/{{ $record->id }}">Edit</a></td>
                                          <td class=""><a class="text-danger" href="{{ config('app.url') }}/sundryAllowanceDeleteView/{{ $record->id }}">Delete</a></td>
                                          {{-- <td class=""><a href="{{ config('app.url') }}/batchDataFormEditView/{{ $record->id }}">Delete</a></td> --}}
                                          {{-- <td class="">{{ $record->reader }}</td>
                                          <td class="">{{ $record->connections }}</td>
                                          <td class="">{{ $record->ocount }}</td>
                                          <td class="">{{ $record->lcount }}</td>
                                          <td class="">{{ $record->pcount }}</td>
                                          <td class="">{{ $record->icount }}</td>
                                          <td class="font-w600">{{ $record->dcount }}</td>
                                          <td class="font-w600">{{ $record->fcount }}</td>
                                          <td class="font-w600">{{ $record->mcount }}</td>
                                          <td class="font-w600">{{ $record->rcount }}</td>
                                          <td class="font-w600">{{ $record->gcount }}</td>
                                          <td class="font-w600">{{ $record->hcount }}</td>
                                          <td class="font-w600">{{ $record->tcount }}</td>
                                          <td class="font-w600">{{ $record->scount }}</td> --}}
=======
                                    @foreach ($data as $sba)
                                      <tr id="{{$sba->id}}">
                                          <td class="" style="padding: 0px;">{{ $sno++ }}</td>
                                          <td class="" style="padding: 0px;">{{ $sba->userId }}</td>
                                          <td class="" style="padding: 0px;">{{ $sba->name }}</td>
                                          <td class="font-w600" style="padding: 0px;">{{ $sba->loginDate }}</td>
                                          <td class="font-w600" style="padding: 0px;">{{ $sba->loginTime }}</td>
                                          <td class="font-w600" style="padding: 0px; cursor: pointer;" onclick="dayclose({{ $sba->id }})">Reset</td>
                                          {{-- <td class="font-w600" style="padding: 0px;"><a href="{{ config('app.url') }}/sba-register-edit/{{ $sba->id }}">Edit</a></td> --}}
>>>>>>> 20c762cf7745e3853d9591f55cbcf6300a071cf0:resources/views/day-close-non-sap.blade.php
                                      </tr>
                                    @endforeach
                                  @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- END Dynamic Table Full -->
                </div>
                <!-- END Page Content -->
            </main>
            <!-- END Main Container -->

            <!-- Footer -->
            @include('includes/footer')
            <!-- END Footer -->
        </div>
        <!-- END Page Container -->

        <!-- Codebase Core JS -->
        <script src="{{ config('app.url') }}/assets/js/core/jquery.min.js"></script>
        <script src="{{ config('app.url') }}/assets/js/core/bootstrap.bundle.min.js"></script>
        <script src="{{ config('app.url') }}/assets/js/core/jquery.slimscroll.min.js"></script>
        <script src="{{ config('app.url') }}/assets/js/core/jquery.scrollLock.min.js"></script>
        <script src="{{ config('app.url') }}/assets/js/core/jquery.appear.min.js"></script>
        <script src="{{ config('app.url') }}/assets/js/core/jquery.countTo.min.js"></script>
        <script src="{{ config('app.url') }}/assets/js/core/js.cookie.min.js"></script>
        <script src="{{ config('app.url') }}/assets/js/codebase.js"></script>

        <!-- Page JS Plugins -->
        <script src="{{ config('app.url') }}/assets/js/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="{{ config('app.url') }}/assets/js/plugins/datatables/dataTables.bootstrap4.min.js"></script>

        <!-- Page JS Code -->
        <script src="{{ config('app.url') }}/assets/js/pages/be_tables_datatables.js"></script>
        <script>
          $(document).ready(function(){
              $('[data-toggle="tooltip"]').tooltip();
          });
        </script>
        <script type="text/javascript">
          function dayclose(deleteId) {
            // alert(deleteId);
            // var division = $('#subDivisionCode').val();
            // var group = e.value;
            // var table = $('#table').val();
            $.ajax({
              headers: {
                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
             },
            type: "POST",
            url: '/api/dayCloseSap',
            data: {deleteId: deleteId},
              success: function( data ) {
                // console.log(data);
                $('#'+deleteId+'').remove();
                  // $("#ajaxResponse").append("<div>"+msg+"</div>");
              }
            });
          }
        </script>
    </body>
</html>
