@php
  use App\User;
@endphp
<!doctype html>
<!--[if lte IE 9]>     <html lang="en" class="no-focus lt-ie10 lt-ie10-msg"> <![endif]-->
<!--[if gt IE 9]><!-->
<html lang="en" class="no-focus">
<!--<![endif]-->

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">

    <title>Dashboard | STERLING Transformers</title>

    <meta name="description" content="Codebase - Bootstrap 4 Admin Template &amp; UI Framework created by pixelcave and published on Themeforest">
    <meta name="author" content="pixelcave">
    <meta name="robots" content="noindex, nofollow">

    <!-- Open Graph Meta -->
    <meta property="og:title" content="Codebase - Bootstrap 4 Admin Template &amp; UI Framework">
    <meta property="og:site_name" content="Codebase">
    <meta property="og:description" content="Codebase - Bootstrap 4 Admin Template &amp; UI Framework created by pixelcave and published on Themeforest">
    <meta property="og:type" content="website">
    <meta property="og:url" content="">
    <meta property="og:image" content="">

    <!-- Icons -->
    <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
    <link rel="shortcut icon" href="{{ config('app.url') }}/assets/img/favicon.png">
    {{-- <link rel="icon" type="image/png" sizes="192x192" href="{{ config('app.url') }}/assets/img/favicons/favicon-192x192.png">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ config('app.url') }}/assets/img/favicons/apple-touch-icon-180x180.png"> --}}
    <!-- END Icons -->

    <!-- Stylesheets -->
    <!-- Codebase framework -->
    <link rel="stylesheet" id="css-main" href="{{ config('app.url') }}/assets/css/codebase.min.css">

    <!-- You can include a specific file from css/themes/ folder to alter the default color theme of the template. eg: -->
    <!-- <link rel="stylesheet" id="css-theme" href="assets/css/themes/flat.min.css"> -->
    <!-- END Stylesheets -->
</head>

<body>
    <!-- Page Container -->
    <!--
            Available classes for #page-container:

        GENERIC

            'enable-cookies'                            Remembers active color theme between pages (when set through color theme helper Codebase() -> uiHandleTheme())

        SIDEBAR & SIDE OVERLAY

            'sidebar-r'                                 Right Sidebar and left Side Overlay (default is left Sidebar and right Side Overlay)
            'sidebar-mini'                              Mini hoverable Sidebar (screen width > 991px)
            'sidebar-o'                                 Visible Sidebar by default (screen width > 991px)
            'sidebar-o-xs'                              Visible Sidebar by default (screen width < 992px)
            'sidebar-inverse'                           Dark themed sidebar

            'side-overlay-hover'                        Hoverable Side Overlay (screen width > 991px)
            'side-overlay-o'                            Visible Side Overlay by default

            'side-scroll'                               Enables custom scrolling on Sidebar and Side Overlay instead of native scrolling (screen width > 991px)

        HEADER

            ''                                          Static Header if no class is added
            'page-header-fixed'                         Fixed Header

        HEADER STYLE

            ''                                          Classic Header style if no class is added
            'page-header-modern'                        Modern Header style
            'page-header-inverse'                       Dark themed Header (works only with classic Header style)
            'page-header-glass'                         Light themed Header with transparency by default
                                                        (absolute position, perfect for light images underneath - solid light background on scroll if the Header is also set as fixed)
            'page-header-glass page-header-inverse'     Dark themed Header with transparency by default
                                                        (absolute position, perfect for dark images underneath - solid dark background on scroll if the Header is also set as fixed)

        MAIN CONTENT LAYOUT

            ''                                          Full width Main Content if no class is added
            'main-content-boxed'                        Full width Main Content with a specific maximum width (screen width > 1200px)
            'main-content-narrow'                       Full width Main Content with a percentage width (screen width > 1200px)
        -->
    <div id="page-container" class="sidebar-o side-scroll page-header-modern main-content-boxed">
        <!-- Side Overlay-->

        <!-- END Side Overlay -->

        <!-- Sidebar -->
        <!--
                Helper classes

                Adding .sidebar-mini-hide to an element will make it invisible (opacity: 0) when the sidebar is in mini mode
                Adding .sidebar-mini-show to an element will make it visible (opacity: 1) when the sidebar is in mini mode
                    If you would like to disable the transition, just add the .sidebar-mini-notrans along with one of the previous 2 classes

                Adding .sidebar-mini-hidden to an element will hide it when the sidebar is in mini mode
                Adding .sidebar-mini-visible to an element will show it only when the sidebar is in mini mode
                    - use .sidebar-mini-visible-b if you would like to be a block when visible (display: block)
            -->
        @include('includes/headerAndSidebar')
        <div class="col-12 col-xl-12" style="display: contents;">
            {{-- <h2 class="text-center">Welcome to <span style="font-family: "Times New Roman";">STERLING</span> Transformers</h2> --}}
            <h2 class="text-center"><img src="{{ config('app.url') }}/assets/img/sterling-logo.png" alt=""> PSPCL Spot Billing User Panel</h2>
        </div>
        <!-- Main Container -->
        <main id="main-container">
            <!-- Page Content -->
            <div class="content">
                <div class="row gutters-tiny invisible" data-toggle="appear">
                    <!-- Row #1 -->

                    @php
                    // $imp = [];
                    //   $above10 = DB::select("SELECT masterKey,CONVERT(SUBSTRING_INDEX(connectedLoadInKW,'-',-1),UNSIGNED INTEGER) AS num FROM bills where connectedLoadInKW > 10;");
                    //   foreach ($above10 as $key) {
                    //     $blmkey = $key->masterKey;
                    //     $imp[] = $blmkey;
                      // }
                      // $result = "'" . implode ( "', '", $imp ) . "'";
                        // $neImp = implode(',', $imp);
                        // $update = DB::update("update billeds set soapStatus = 'F' where blMasterKey in($result)");
                        // echo $result;
                    @endphp

                    <div class="col-6 col-xl-4">
                            <a class="block block-link-pop text-right bg-primary" href="javascript:void(0)">
                                <div class="block-content block-content-full clearfix border-black-op-b border-3x">
                                    {{-- <div class="float-left mt-10 d-none d-sm-block">
                                        <i class="si si-bar-chart fa-3x text-primary-light"></i>
                                    </div> --}}
                                    @php
                                      // $deleteDuplicates = DB::delete("delete t1 from sap_outputs140720180100 t1 inner join sap_outputs140720180100 t2 where t1.id > t2.id and t1.MR_DOCNumber = t2.MR_DOCNumber");
                                      // $selectAllDups = DB::select("select id, blmasterKey, count(blmasterKey) from billeds240720181100AM group by blmasterKey having count(blmasterKey) > 1");
                                      // // return $selectAllDups;
                                      // foreach ($selectAllDups as $dups) {
                                      //   $selectmr = DB::select("SELECT * FROM billeds where blmasterKey = '$dups->blmasterKey'");
                                      //   return $selectmr;
                                      //   $i = 0;
                                      //   foreach ($selectmr as $mr) {
                                      //     if ($i == 0) {
                                      //           // first
                                      //           $delId= $mr->id;
                                      //       } else {
                                      //         echo "string";
                                      //           // last
                                      //           return $mr;
                                      //           $mr = $mr->blmasterKey;
                                      //           return $mr;
                                      //           $delete = DB::delete("delete from billeds WHERE blmasterKey = '$mr' and id > '$delId'");
                                      //       }
                                      //       $i++;
                                      //   }
                                      //   $i = 0;
                                      //   // echo "$dups->mr_docnumber";
                                      // }
                                      // $removeZero = DB::select("SELECT * FROM sap_outputs140720180100 WHERE currentNoteFromMeterReader = '0'");
                                      // foreach ($removeZero as $zeros) {
                                      //   $zero = $zeros->MR_DOCNumber;
                                      //   $updateInput = DB::update("update sap_inputs set bill_status = 'N' where mr_docnumber = '$zero'");

                                        // echo $zeros->MR_DOCNumber;
                                      // }
                                      $newData = DB::select("select count(*) newData from sap_server");
                                      $totalRecords = $newData[0]->newData;
                                    @endphp

                                    <div class="font-size-h3 font-w600 js-count-to-enabled" data-toggle="countTo" data-speed="1000" data-to="{{$totalRecords}}">{{ ($totalRecords) }}</div>
                                    <div class="font-size-sm font-w600 text-uppercase text-muted">DATA RECEIVED THROUGH SAP API</div>
                                </div>
                            </a>
                        </div>

                    <div class="col-6 col-xl-4">
                            <a class="block block-link-pop text-right bg-corporate" href="javascript:void(0)">
                                <div class="block-content block-content-full clearfix border-black-op-b border-3x">
                                    {{-- <div class="float-left mt-10 d-none d-sm-block">
                                        <i class="si si-fire fa-3x text-corporate-light"></i>
                                    </div> --}}
                                    @php
                                    $newData = DB::select("select count(*) transferedData from sap_server where transfer_status = 'Y'");
                                    $transferedRecords = $newData[0]->transferedData;
                                    @endphp
                                    <div class="font-size-h3 font-w600 text-white js-count-to-enabled" data-toggle="countTo" data-speed="1000" data-to="{{$transferedRecords}}">{{ ($transferedRecords) }}</div>
                                    <div class="font-size-sm font-w600 text-uppercase text-white-op">TRANSFERED DATA FOR BILLING</div>
                                </div>
                            </a>
                        </div>

                    <div class="col-6 col-xl-4">
                            <a class="block block-link-pop text-right bg-earth" href="javascript:void(0)">
                                <div class="block-content block-content-full clearfix border-black-op-b border-3x">

                                    <div class="font-size-h3 font-w600 text-white"><span data-toggle="countTo" data-speed="1000" data-to="{{$totalRecords - $transferedRecords}}" class="js-count-to-enabled">{{ ($totalRecords - $transferedRecords) }}</span></div>
                                    <div class="font-size-sm font-w600 text-uppercase text-white-op">BALANCE</div>
                                </div>
                            </a>
                        </div>
                        <div class="col-12">
                          <div class="row">

                            <div class="col-md-4">
                              <h1 class="font-w400">SAP <small></small></h1>
                            </div>
                            <div class="col-md-4">
                              <h1 class="font-w400">Non SAP <small></small></h1>
                            </div>
                            <div class="col-md-4">
                              <h1 class="font-w400">Meter <small>Readers</small></h1>
                            </div>
                          </div>
                        </div>
                    <div class="col-xl-4 d-flex align-items-stretch">

                            <div class="block block-transparent bg-gd-default d-flex align-items-center w-100">
                                <div class="block-content block-content-full">
                                    <div class="py-15 px-20 clearfix border-black-op-b">
                                        {{-- <div class="float-right mt-15 d-none d-sm-block">
                                            <i class="si si-book-open fa-2x text-success"></i>
                                        </div> --}}
                                        @php
                                          $newData = DB::select("select count(*) totalRecords from sap_inputs");
                                          $totalRecords = $newData[0]->totalRecords;
                                        @endphp
                                        <div class="font-size-h3 font-w600 text-success-light js-count-to-enabled" data-toggle="countTo" data-speed="1000" data-to="{{$totalRecords}}">{{$totalRecords}}</div>
                                        <div class="font-size-sm font-w600 text-uppercase text-success-light">Total Records</div>
                                    </div>
                                    <div class="py-15 px-20 clearfix border-black-op-b">
                                        {{-- <div class="float-right mt-15 d-none d-sm-block">
                                            <i class="si si-wallet fa-2x text-danger"></i>
                                        </div> --}}
                                        @php
                                          $newData = DB::select("select count(*) totalBilled from sap_outputs");
                                          $totalBilled = $newData[0]->totalBilled;
                                        @endphp
                                        <div class="font-size-h3 font-w600 text-danger-light"><span data-toggle="countTo" data-speed="1000" data-to="{{$totalBilled}}">{{$totalBilled}}</span></div>
                                        <div class="font-size-sm font-w600 text-uppercase text-danger-light">Billed Records</div>
                                    </div>
                                    <div class="py-15 px-20 clearfix border-black-op-b">
                                        {{-- <div class="float-right mt-15 d-none d-sm-block">
                                            <i class="si si-envelope-open fa-2x text-warning"></i>
                                        </div> --}}
                                        <div class="font-size-h3 font-w600 text-warning-light js-count-to-enabled" data-toggle="countTo" data-speed="1000" data-to="38">{{$totalRecords - $totalBilled}}</div>
                                        <div class="font-size-sm font-w600 text-uppercase text-warning-light">Balance</div>
                                    </div>
                                    <div class="py-15 px-20 clearfix border-black-op-b">
                                        {{-- <div class="float-right mt-15 d-none d-sm-block">
                                            <i class="si si-users fa-2x text-info"></i>
                                        </div> --}}
                                        @php
                                          $newData = DB::select("select count(*) totalTransfered from sap_outputs where soapStatus = 'S'");
                                          $totalTransfered = $newData[0]->totalTransfered;
                                        @endphp
                                        <div class="font-size-h3 font-w600 text-info-light js-count-to-enabled" data-toggle="countTo" data-speed="1000" data-to="{{$totalTransfered}}">{{$totalTransfered}}</div>
                                        <div class="font-size-sm font-w600 text-uppercase text-info-light">Records Uploaded TO PSPCL</div>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="col-xl-4 d-flex align-items-stretch">

                                <div class="block block-transparent bg-gray-dark d-flex align-items-center w-100">
                                    <div class="block-content block-content-full">
                                        <div class="py-15 px-20 clearfix border-black-op-b">
                                            {{-- <div class="float-right mt-15 d-none d-sm-block">
                                                <i class="si si-book-open fa-2x text-success"></i>
                                            </div> --}}
                                            @php
                                              $newData = DB::select("select count(*) totalRecords from bills");
                                              $totalRecords = $newData[0]->totalRecords;
                                            @endphp
                                            <div class="font-size-h3 font-w600 text-success js-count-to-enabled" data-toggle="countTo" data-speed="1000" data-to="{{$totalRecords}}">{{$totalRecords}}</div>
                                            <div class="font-size-sm font-w600 text-uppercase text-success-light">Total Records</div>
                                        </div>
                                        <div class="py-15 px-20 clearfix border-black-op-b">
                                            {{-- <div class="float-right mt-15 d-none d-sm-block">
                                                <i class="si si-wallet fa-2x text-danger"></i>
                                            </div> --}}
                                            @php
                                              $newData = DB::select("select count(*) totalBilled from billeds");
                                              $totalBilled = $newData[0]->totalBilled;
                                            @endphp
                                            <div class="font-size-h3 font-w600 text-danger"><span data-toggle="countTo" data-speed="1000" data-to="{{$totalBilled}}">{{$totalBilled}}</span></div>
                                            <div class="font-size-sm font-w600 text-uppercase text-danger-light">Billed Records</div>
                                        </div>
                                        <div class="py-15 px-20 clearfix border-black-op-b">
                                            {{-- <div class="float-right mt-15 d-none d-sm-block">
                                                <i class="si si-envelope-open fa-2x text-warning"></i>
                                            </div> --}}
                                            <div class="font-size-h3 font-w600 text-warning js-count-to-enabled" data-toggle="countTo" data-speed="1000" data-to="38">{{$totalRecords - $totalBilled}}</div>
                                            <div class="font-size-sm font-w600 text-uppercase text-warning-light">Balance</div>
                                        </div>
                                        <div class="py-15 px-20 clearfix border-black-op-b">
                                            {{-- <div class="float-right mt-15 d-none d-sm-block">
                                                <i class="si si-users fa-2x text-info"></i>
                                            </div> --}}
                                            @php
                                              $newData = DB::select("select count(*) totalTransfered from billeds where soapStatus = 'S'");
                                              $totalTransfered = $newData[0]->totalTransfered;
                                            @endphp
                                            <div class="font-size-h3 font-w600 text-info js-count-to-enabled" data-toggle="countTo" data-speed="1000" data-to="{{$totalTransfered}}">{{$totalTransfered}}</div>
                                            <div class="font-size-sm font-w600 text-uppercase text-info-light">Recerds Uploaded TO PSPCL</div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-4 d-flex align-items-stretch">

                                    <div class="block block-transparent bg-corporate d-flex align-items-center w-100">
                                        <div class="block-content block-content-full">
                                            <div class="py-15 px-20 clearfix border-black-op-b">

                                                @php
                                                  $newData = DB::select("select count(*) totalSapUsers from users where userGroup = 'sap'");
                                                  $totalSapUsers = $newData[0]->totalSapUsers;
                                                @endphp
                                                <div class="font-size-h3 font-w600 text-success js-count-to-enabled" data-toggle="countTo" data-speed="1000" data-to="{{$totalSapUsers}}">{{$totalSapUsers}}</div>
                                                <div class="font-size-sm font-w600 text-uppercase text-success-light">Total Sap Users</div>
                                            </div>
                                            <div class="py-15 px-20 clearfix border-black-op-b">

                                                @php
                                                  $date = Date('d-m-Y');
                                                  $newData = DB::select("select count(*) totalSapUsers from user_logs where userId > 20000 and loginDate = '$date'");
                                                  $totalSapUsers = $newData[0]->totalSapUsers;
                                                @endphp
                                                <div class="font-size-h3 font-w600 text-danger-light"><span data-toggle="countTo" data-speed="1000" data-to="{{$totalSapUsers}}">{{$totalSapUsers}}</span></div>
                                                <div class="font-size-sm font-w600 text-uppercase text-danger-light">SAP Users Logged In</div>
                                            </div>


                                                <div class="py-15 px-20 clearfix border-black-op-b">

                                                    @php
                                                      $newData = DB::select("select count(*) totalSapUsers from users where userGroup = 'non sap'");
                                                      $totalSapUsers = $newData[0]->totalSapUsers;
                                                    @endphp
                                                    <div class="font-size-h3 font-w600 text-success js-count-to-enabled" data-toggle="countTo" data-speed="1000" data-to="{{$totalSapUsers}}">{{$totalSapUsers}}</div>
                                                    <div class="font-size-sm font-w600 text-uppercase text-success-light">Total Non SAP Users</div>
                                                </div>
                                                <div class="py-15 px-20 clearfix border-black-op-b">

                                                    @php
                                                    $date = Date('d-m-Y');
                                                    $newData = DB::select("select count(*) totalSapUsers from user_logs where userId < 20000 and loginDate = '$date'");
                                                    $totalSapUsers = $newData[0]->totalSapUsers;
                                                    @endphp
                                                    <div class="font-size-h3 font-w600 text-danger-light"><span data-toggle="countTo" data-speed="1000" data-to="{{$totalSapUsers}}">{{$totalSapUsers}}</span></div>
                                                    <div class="font-size-sm font-w600 text-uppercase text-danger-light">Non SAP users Logged In</div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                            <a class="block block-link-shadow overflow-hidden" href="javascript:void(0)">
                                <div class="block-content block-content-full">
                                    <div class="text-center">
                                        <h3>Today's Billing</h3>
                                    </div>
                                    <div class="row py-20">
                                        <div class="col-6 text-right border-r">
                                            <div class="js-appear-enabled animated fadeInLeft" data-toggle="appear" data-class="animated fadeInLeft">
                                              @php
                                              $date = Date('d-m-Y');
                                                $sapTodayBilled = DB::select("SELECT count(*) todayBilledSAP FROM sap_outputs where currentMeterReadingDate = '$date'");
                                                $todayBilledSAP = $sapTodayBilled[0]->todayBilledSAP;
                                              @endphp
                                                <div class="font-size-h3 font-w600 text-info">{{$todayBilledSAP}}</div>
                                                <div class="font-size-sm font-w600 text-uppercase text-muted">SAP</div>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                          @php
                                          $date = Date('d/m/Y');
                                            $nonSAPTodayBilled = DB::select("SELECT count(*) todayBilledNonSAP FROM billeds where blPresentReadingDate = '$date'");
                                            $todayBilledNonSAP = $nonSAPTodayBilled[0]->todayBilledNonSAP;
                                          @endphp
                                            <div class="js-appear-enabled animated fadeInRight" data-toggle="appear" data-class="animated fadeInRight">
                                                <div class="font-size-h3 font-w600 text-success">{{$todayBilledNonSAP}}</div>
                                                <div class="font-size-sm font-w600 text-uppercase text-muted">NON SAP</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>


                    {{-- Search Default Users  --}}
                    @php

                    // Admin
                      $admin = User::where('email', 'admin')->get();
                      // dd($users);
                      if (!$admin->count()) {
                        echo "Admin Created - ";
                        $newUser = new User();
                        $newUser->name = 'admin';
                        $newUser->email = 'admin';
                        $newUser->emailId = 'md_sterling@yahoo.com';
                        $newUser->fatherName = 'father name';
                        $newUser->address = 'address';
                        $newUser->aadhaarNo = '';
                        $newUser->aadhaarPhoto = '';
                        $newUser->mobile = '9848444432';
                        $newUser->password = Hash::make('admin@999');
                        $newUser->save();
                      }

                      // Super User
                      $admin = User::where('email', 'superuser')->get();
                      // dd($users);
                      if (!$admin->count()) {
                        echo "Super User Created - ";
                        $newUser = new User();
                        $newUser->name = 'super user';
                        $newUser->email = 'superuser';
                        $newUser->emailId = 'superuser@sterlingtechenergy.com';
                        $newUser->fatherName = 'father name';
                        $newUser->address = 'address';
                        $newUser->aadhaarNo = '';
                        $newUser->aadhaarPhoto = '';
                        $newUser->mobile = '0000000000';
                        $newUser->password = Hash::make('suser@2277');
                        $newUser->save();
                      }

                      // Test User
                      $admin = User::where('email', 'testuser')->get();
                      // dd($users);
                      if (!$admin->count()) {
                        echo "Test User Created";
                        $newUser = new User();
                        $newUser->name = 'test user';
                        $newUser->email = 'testuser';
                        $newUser->emailId = 'testuser@sterlingtechenergy.com';
                        $newUser->fatherName = 'father name';
                        $newUser->address = 'address';
                        $newUser->aadhaarNo = '';
                        $newUser->aadhaarPhoto = '';
                        $newUser->mobile = '0000000000';
                        $newUser->password = Hash::make('user@12345');
                        $newUser->save();
                      }
                    @endphp

                    <!-- END Row #1 -->
                </div>

            </div>
            <!-- END Page Content -->
        </main>
        <!-- END Main Container -->

        <!-- Footer -->
        @include('includes/footer')
        <!-- END Footer -->
    </div>
    <!-- END Page Container -->

    <!-- Codebase Core JS -->
    <script src="{{ config('app.url') }}/assets/js/core/jquery.min.js"></script>
    <script src="{{ config('app.url') }}/assets/js/core/bootstrap.bundle.min.js"></script>
    <script src="{{ config('app.url') }}/assets/js/core/jquery.slimscroll.min.js"></script>
    <script src="{{ config('app.url') }}/assets/js/core/jquery.scrollLock.min.js"></script>
    <script src="{{ config('app.url') }}/assets/js/core/jquery.appear.min.js"></script>
    <script src="{{ config('app.url') }}/assets/js/core/jquery.countTo.min.js"></script>
    <script src="{{ config('app.url') }}/assets/js/core/js.cookie.min.js"></script>
    <script src="{{ config('app.url') }}/assets/js/codebase.js"></script>

    <!-- Page JS Plugins -->
    <script src="{{ config('app.url') }}/assets/js/plugins/chartjs/Chart.bundle.min.js"></script>

    <!-- Page JS Code -->
    <script src="{{ config('app.url') }}/assets/js/pages/be_pages_dashboard.js"></script>
</body>

</html>

{{-- @extends('layouts.app') @section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                    @endif You are logged in!
                </div>
            </div>
        </div>
    </div>
</div>
@endsection --}}
