<footer id="page-footer" class="opacity-0">
    <div class="content py-20 font-size-xs clearfix">
        {{-- <div class="float-right">
            Crafted with <i class="fa fa-heart text-pulse"></i> & Care by <a class="font-w600" href="http://vjsoft.org" target="_blank">vJsOFT</a>
        </div> --}}
        <div class="float-left">
            <a class="font-w600" href="#" target="_blank">Sterling Transformers</a> &copy; <span class="js-year-copy">2018</span>
        </div>
    </div>
</footer>
