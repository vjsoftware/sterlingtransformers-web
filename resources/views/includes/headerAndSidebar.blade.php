
            <nav id="sidebar">
                <!-- Sidebar Scroll Container -->
                <div id="sidebar-scroll">
                    <!-- Sidebar Content -->
                    <div class="sidebar-content">
                        <!-- Side Header -->
                        <div class="content-header content-header-fullrow px-15">
                            <!-- Mini Mode -->
                            <div class="content-header-section sidebar-mini-visible-b">
                                <!-- Logo -->
                                <span class="content-header-item font-w700 font-size-xl float-left animated fadeIn">
                                    <span class="text-dual-primary-dark">c</span><span class="text-primary">b</span>
                                </span>
                                <!-- END Logo -->
                            </div>
                            <!-- END Mini Mode -->

                            <!-- Normal Mode -->
                            <div class="content-header-section text-center align-parent sidebar-mini-hidden">
                                <!-- Close Sidebar, Visible only on mobile screens -->
                                <!-- Layout API, functionality initialized in Codebase() -> uiApiLayout() -->
                                <button type="button" class="btn btn-circle btn-dual-secondary d-lg-none align-v-r" data-toggle="layout" data-action="sidebar_close">
                                    <i class="fa fa-times text-danger"></i>
                                </button>
                                <!-- END Close Sidebar -->

                                <!-- Logo -->
                                <div class="content-header-item">
                                    <a class="link-effect font-w700" href="{{ config('app.url') }}">
                                        {{-- <i class="si si-fire text-primary"></i> --}}
                                        <img src="{{ config('app.url') }}/assets/img/sterling-logo.png" style="width: 25px;" alt="">
                                        <span class="text-dual-primary-dark">STERLING Transformers</span>
                                    </a>
                                </div>
                                <!-- END Logo -->
                            </div>
                            <!-- END Normal Mode -->
                        </div>
                        <!-- END Side Header -->

                        <!-- Side User -->
                        <div class="content-side content-side-full content-side-user px-10 align-parent">
                            <!-- Visible only in mini mode -->
                            <div class="sidebar-mini-visible-b align-v animated fadeIn">
                                <img class="img-avatar img-avatar32" src="assets/img/avatars/avatar15.jpg" alt="">
                            </div>
                            <!-- END Visible only in mini mode -->

                            <!-- Visible only in normal mode -->
                            <div class="sidebar-mini-hidden-b text-center">
                                <a class="img-link" href="/profile">
                                    <img class="img-avatar" src="assets/img/avatars/avatar15.jpg" alt="">
                                </a>
                                <ul class="list-inline mt-10">
                                    <li class="list-inline-item">
                                        <a class="link-effect text-dual-primary-dark font-size-xs font-w600 text-uppercase" href="be_pages_generic_profile.html">{{ Auth::user()->name }}</a>
                                    </li>
                                    <li class="list-inline-item">
                                        <!-- Layout API, functionality initialized in Codebase() -> uiApiLayout() -->
                                        <a class="link-effect text-dual-primary-dark" data-toggle="layout" data-action="sidebar_style_inverse_toggle" href="javascript:void(0)">
                                            <i class="si si-drop"></i>
                                        </a>
                                    </li>
                                    <li class="list-inline-item">
                                        <a class="link-effect text-dual-primary-dark" href="{{ config('app.url') }}/logout">
                                            <i class="si si-logout"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <!-- END Visible only in normal mode -->
                        </div>
                        <!-- END Side User -->

                        <!-- Side Navigation -->
                        <div class="content-side content-side-full">
                            <ul class="nav-main">
                                <li>
                                    <a class="active" href="{{ config('app.url') }}"><i class="si si-cup"></i><span class="sidebar-mini-hide">Dashboard</span></a>
                                </li>
                                {{-- <li class="nav-main-heading"><span class="sidebar-mini-visible">UI</span><span class="sidebar-mini-hidden">Bill</span></li> --}}
                                <li>
                                    <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-puzzle"></i><span class="sidebar-mini-hide">Users</span></a>
                                    <ul>
                                      @if (Auth::user()->name == 'admin' || Auth::user()->name == 'main')
                                        <li>
                                            <a class="" href="{{ config('app.url') }}/day-close-sap"><span class="sidebar-mini-hide">Day Close Sap</span></a>
                                        </li>
                                        <li>
                                            <a class="" href="{{ config('app.url') }}/day-close-non-sap"><span class="sidebar-mini-hide">Day Close Non Sap</span></a>
                                        </li>
                                        <li>
                                            <a class="" href="{{ config('app.url') }}/data-reset"><span class="sidebar-mini-hide">Data Reset</span></a>
                                        </li>
                                        <li>
                                            <a class="" href="{{ config('app.url') }}/mobile-reset"><span class="sidebar-mini-hide">Mobile Reset</span></a>
                                        </li>
                                        <li>
                                            <a class="" href="{{ config('app.url') }}/password-reset"><span class="sidebar-mini-hide">Password Reset</span></a>
                                        </li>
                                      @endif
                                      <li>
                                          <a class="" href="{{ config('app.url') }}/meter-readers"><span class="sidebar-mini-hide">Meter Reader's List Non SAP</span></a>
                                      </li>
                                      <li>
                                          <a class="" href="{{ config('app.url') }}/meter-readers-sap"><span class="sidebar-mini-hide">Meter Reader's List SAP</span></a>
                                      </li>
                                      <li>
                                          <a class="" href="{{ config('app.url') }}/sbaregister-sap"><span class="sidebar-mini-hide">Register Meter Reader SAP</span></a>
                                      </li>
                                      <li>
                                          <a class="" href="{{ config('app.url') }}/sbaregister-non-sap"><span class="sidebar-mini-hide">Register Meter Reader non SAP</span></a>
                                      </li>
                                      <li>
                                          <a class="" href="{{ config('app.url') }}/register"><span class="sidebar-mini-hide">Register Sterling User</span></a>
                                      </li>
                                      <li>
                                          <a class="" href="{{ config('app.url') }}/register-pspcl"><span class="sidebar-mini-hide">Register Pspcl User</span></a>
                                      </li>
                                    </ul>
                                </li>
                                <li>
                                    <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-puzzle"></i><span class="sidebar-mini-hide">Billing</span></a>
                                    <ul>
                                      <li>
                                          <a class="" href="{{ config('app.url') }}/soapt"><span class="sidebar-mini-hide">SAP Data Transfer PSPCL</span></a>
                                      </li>
                                      <li>
                                          <a class="" href="{{ config('app.url') }}/soapnt"><span class="sidebar-mini-hide">Non SAP Data Transfer PSPCL Server</span></a>
                                      </li>
                                      <li>
                                          <a class="" href="{{ config('app.url') }}/upload"><span class="sidebar-mini-hide">Import Bill</span></a>
                                      </li>
                                      {{-- <li>
                                          <a class="" href="{{ config('app.url') }}/bill"><span class="sidebar-mini-hide">Demo Bill</span></a>
                                      </li> --}}
                                      <li>
                                          <a class="" href="{{ config('app.url') }}/bind"><span class="sidebar-mini-hide">Bind Meter Reader Non SAP</span></a>
                                      </li>
                                      <li>
                                          <a class="" href="{{ config('app.url') }}/bind-sap"><span class="sidebar-mini-hide">Bind Meter Reader SAP</span></a>
                                      </li>
                                      <li>
                                          <a class="" href="{{ config('app.url') }}/bind-ledger"><span class="sidebar-mini-hide">Bind Complete Ledger Non SAP</span></a>
                                      </li>
                                      <li>
                                          <a class="" href="{{ config('app.url') }}/bind-ledger-sap"><span class="sidebar-mini-hide">Bind Complete Ledger SAP</span></a>
                                      </li>
                                      <li>
                                          <a class="" href="{{ config('app.url') }}/view-billing-data"><span class="sidebar-mini-hide">View Billing Data</span></a>
                                      </li>

                                      <li>
                                          <a class="" href="{{ config('app.url') }}/exportOutputFileSpotBill"><span class="sidebar-mini-hide">Billing Data Export NIELIT</span></a>
                                      </li>
                                      <li>
                                          <a class="" href="{{ config('app.url') }}/exportOutputFile"><span class="sidebar-mini-hide">Cash Collection Export</span></a>
                                      </li>
                                      <li>
                                          <a class="" href="{{ config('app.url') }}/exportOutputFileds"><span class="sidebar-mini-hide">Export DS Format</span></a>
                                      </li>
                                      <li>
                                          <a class="" href="{{ config('app.url') }}/gtexport"><span class="sidebar-mini-hide">Export GT Format</span></a>
                                      </li>
                                      <li>
                                          <a href="{{ config('app.url') }}/pending-bills"><span class="sidebar-mini-hide">View Pending Bills</span></a>
                                      </li>
                                      <li>
                                          <a href="{{ config('app.url') }}/sap-server-data-transfer"><span class="sidebar-mini-hide">Sap Server Data</span></a>
                                      </li>
                                    </ul>
                                </li>
                                <li>
                                    <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-puzzle"></i><span class="sidebar-mini-hide">Reports</span></a>
                                    <ul>
                                      <li>
                                          <a class="" href="{{ config('app.url') }}/percentage-report-sap"><span class="sidebar-mini-hide">Percentage Report SAP</span></a>
                                      </li>
                                      <li>
                                          <a class="" href="{{ config('app.url') }}/percentage-report-non-sap"><span class="sidebar-mini-hide">Percentage Report NON SAP</span></a>
                                      </li>
                                      <li>
                                          <a class="" href="{{ config('app.url') }}/sap-details-required"><span class="sidebar-mini-hide">Exec Report</span></a>
                                      </li>
                                    @if (Auth::user()->name == 'main' || Auth::user()->name == 'admin')
                                      <li>
                                          <a class="" href="{{ config('app.url') }}/salary-report-sap-form-view"><span class="sidebar-mini-hide">Salary Report SAP</span></a>
                                      </li>
                                      <li>
                                          <a class="" href="{{ config('app.url') }}/salary-report-non-sap-form-view"><span class="sidebar-mini-hide">Salary Report NON SAP</span></a>
                                      </li>
                                    @endif
                                    @if (Auth::user()->name == 'admin' || Auth::user()->name == 'main' || Auth::user()->id == '125')
                                      <li>
                                          <a class="" href="{{ config('app.url') }}/sap-bill-delete"><span class="sidebar-mini-hide">Delete SAP</span></a>
                                      </li>
                                      <li>
                                          <a class="" href="{{ config('app.url') }}/non-sap-bill-delete"><span class="sidebar-mini-hide">Delete Non SAP</span></a>
                                      </li>
                                    @endif
                                      <li>
                                          <a class="" href="{{ config('app.url') }}/schedule-details-sap"><span class="sidebar-mini-hide">Schedule Details SAP</span></a>
                                      </li>
                                      <li>
                                          <a class="" href="{{ config('app.url') }}/schedule-details-non-sap"><span class="sidebar-mini-hide">Schedule Details NON SAP</span></a>
                                      </li>
                                      <li>
                                          <a class="" href="{{ config('app.url') }}/pending-bills-non-sap"><span class="sidebar-mini-hide">Pending Bills Non SAP</span></a>
                                      </li>
                                      <li>
                                          <a class="" href="{{ config('app.url') }}/pending-bills-sap"><span class="sidebar-mini-hide">Pending Bills SAP</span></a>
                                      </li>
                                      <li>
                                          <a class="" href="{{ config('app.url') }}/billing-summary-non-sap"><span class="sidebar-mini-hide">Billing Summary Non SAP</span></a>
                                      </li>
                                      <li>
                                          <a class="" href="{{ config('app.url') }}/billing-summary"><span class="sidebar-mini-hide">Billing Summary SAP</span></a>
                                      </li>
                                      <li>
                                          <a class="" href="{{ config('app.url') }}/ledger-preformance-report"><span class="sidebar-mini-hide">Ledger Wise Summary SAP</span></a>
                                      </li>
                                      <li>
                                          <a class="" href="{{ config('app.url') }}/ledger-preformance-report-non-sap"><span class="sidebar-mini-hide">Ledger Wise Summary Non SAP</span></a>
                                      </li>
                                      <li>
                                          <a class="" href="{{ config('app.url') }}/meter-reader-preformance-report"><span class="sidebar-mini-hide">Meter Reader Performance Report</span></a>
                                      </li>
                                      <li>
                                          <a class="" href="{{ config('app.url') }}/meter-reader-preformance-report-non-sap"><span class="sidebar-mini-hide">Meter Reader Performance Report NON SAP</span></a>
                                      </li>
                                      <li>
                                          <a class="" href="{{ config('app.url') }}/consumer-enquiry-form"><span class="sidebar-mini-hide">Consumer Enquiry Form</span></a>
                                      </li>
                                      <li>
                                          <a class="" href="{{ config('app.url') }}/track-meter-reader"><span class="sidebar-mini-hide">Track Meter Reader</span></a>
                                      </li>
                                      <li>
                                          <a class="" href="{{ config('app.url') }}/mini-ledger-new"><span class="sidebar-mini-hide">Mini Ledger SAP</span></a>
                                      </li>
                                      <li>
                                          <a class="" href="{{ config('app.url') }}/mini-ledger-non-sap-new"><span class="sidebar-mini-hide">Mini Ledger Non SAP</span></a>
                                      </li>
                                      {{-- <li>
                                          <a class="" href="{{ config('app.url') }}/exceptionalReports"><span class="sidebar-mini-hide">Exception Reports</span></a>
                                      </li> --}}
                                      {{-- <li>
                                          <a class="" href="{{ config('app.url') }}/summaryReports"><span class="sidebar-mini-hide">Summary Reports</span></a>
                                      </li>
                                      <li>
                                          <a class="" href="{{ config('app.url') }}/categoryWiseReports"><span class="sidebar-mini-hide">Category wise Reports</span></a>
                                      </li>
                                      <li>
                                          <a class="" href="{{ config('app.url') }}/divisionWisePerfomanceReports"><span class="sidebar-mini-hide">Division wise Perfomance</span></a>
                                      </li>
                                      <li>
                                          <a class="" href="{{ config('app.url') }}/multipleBillsSameLocation"><span class="sidebar-mini-hide">Multiple Bills</span></a>
                                      </li>
                                      <li>
                                          <a class="" href="{{ config('app.url') }}/viewArrears"><span class="sidebar-mini-hide">View Arrears</span></a>
                                      </li>
                                      <li>
                                        <a class="" href="{{ config('app.url') }}/meter-reader-performance"><span class="sidebar-mini-hide">Meter Reader Performance</span></a>
                                      </li> --}}
                                    </ul>
                                </li>
                                <li>
                                    <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-puzzle"></i><span class="sidebar-mini-hide">Data Entry</span></a>
                                    <ul>
                                      <li>
                                        <a class="" href="{{ config('app.url') }}/batchDataForm"><span class="sidebar-mini-hide">Advice - 11</span></a>
                                      </li>
                                      <li>
                                        <a class="" href="{{ config('app.url') }}/changeOfTariff"><span class="sidebar-mini-hide">Advice - 71</span></a>
                                      </li>
                                      <li>
                                        <a class="" href="{{ config('app.url') }}/previousReadingCorrection"><span class="sidebar-mini-hide">Advice - 72</span></a>
                                      </li>
                                      <li>
                                        <a class="" href="{{ config('app.url') }}/correctionOfMeterParticulars"><span class="sidebar-mini-hide">Advice - 73</span></a>
                                      </li>
                                      <li>
                                        <a class="" href="{{ config('app.url') }}/adviceForDisconnection"><span class="sidebar-mini-hide">Advice - 74</span></a>
                                      </li>
                                      <li>
                                        <a class="" href="{{ config('app.url') }}/correctionOfName"><span class="sidebar-mini-hide">Advice - 75</span></a>
                                      </li>
                                      <li>
                                        <a class="" href="{{ config('app.url') }}/sundryCharges"><span class="sidebar-mini-hide">Advice - 76</span></a>
                                      </li>
                                      <li>
                                        <a class="" href="{{ config('app.url') }}/changeOfMeter"><span class="sidebar-mini-hide">Advice - 79</span></a>
                                      </li>
                                      <li>
                                        <a class="" href="{{ config('app.url') }}/sundryAllowance"><span class="sidebar-mini-hide">Advice - 86</span></a>
                                      </li>
                                      <li>
                                        <a class="" href="{{ config('app.url') }}/changeInLedgerMaster"><span class="sidebar-mini-hide">Advice - 87</span></a>
                                      </li>
                                      <li>
                                        <a class="" href="{{ config('app.url') }}/changeOfAcd"><span class="sidebar-mini-hide">Advice - 80</span></a>
                                      </li>
                                      <li>
                                        <a class="" href="{{ config('app.url') }}/adviceForPsting"><span class="sidebar-mini-hide">Advice - 53</span></a>
                                      </li>
                                      <li>
                                        <a class="" href="{{ config('app.url') }}/advice63"><span class="sidebar-mini-hide">Advice - 63</span></a>
                                      </li>
                                      <li>
                                        <a class="" href="{{ config('app.url') }}/adviceForCharging"><span class="sidebar-mini-hide">Advice - 96</span></a>
                                      </li>
                                    </ul>
                                </li>
                                <li>
                                    <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-puzzle"></i><span class="sidebar-mini-hide">Data Entry Reports</span></a>
                                    <ul>
                                      <li>
                                        <a class="" href="{{ config('app.url') }}/advice-11-report"><span class="sidebar-mini-hide">Advice - 11</span></a>
                                      </li>
                                      <li>
                                        <a class="" href="{{ config('app.url') }}/changeOfTariffEdit"><span class="sidebar-mini-hide">Advice - 71</span></a>
                                      </li>
                                      <li>
                                        <a class="" href="{{ config('app.url') }}/previousReadingCorrectionEdit"><span class="sidebar-mini-hide">Advice - 72</span></a>
                                      </li>
                                      <li>
                                        <a class="" href="{{ config('app.url') }}/correctionOfMeterParticularsEdit"><span class="sidebar-mini-hide">Advice - 73</span></a>
                                      </li>
                                      <li>
                                        <a class="" href="{{ config('app.url') }}/adviceForDisconnectionEdit"><span class="sidebar-mini-hide">Advice - 74</span></a>
                                      </li>
                                      <li>
                                        <a class="" href="{{ config('app.url') }}/correctionOfNameEdit"><span class="sidebar-mini-hide">Advice - 75</span></a>
                                      </li>
                                      <li>
                                        <a class="" href="{{ config('app.url') }}/sundryChargesEdit"><span class="sidebar-mini-hide">Advice - 76</span></a>
                                      </li>
                                      <li>
                                        <a class="" href="{{ config('app.url') }}/changeOfMeterEdit"><span class="sidebar-mini-hide">Advice - 79</span></a>
                                      </li>
                                    <li>
                                      <a class="" href="{{ config('app.url') }}/changeOfAcdEdit"><span class="sidebar-mini-hide">Advice - 80</span></a>
                                    </li>
                                    <li>
                                      <a class="" href="{{ config('app.url') }}/sundryAllowanceEdit"><span class="sidebar-mini-hide">Advice - 86</span></a>
                                    </li>
                                    <li>
                                      <a class="" href="{{ config('app.url') }}/changeInLedgerMastersEdit"><span class="sidebar-mini-hide">Advice - 87</span></a>
                                    </li>
                                    <li>
                                      <a class="" href="{{ config('app.url') }}/advice96-edit"><span class="sidebar-mini-hide">Advice - 96</span></a>
                                    </li>
                                    <li>
                                      <a class="" href="{{ config('app.url') }}/advice53-edit"><span class="sidebar-mini-hide">Advice - 53</span></a>
                                    </li>
                                    <li>
                                      <a class="" href="{{ config('app.url') }}/advice63-edit"><span class="sidebar-mini-hide">Advice - 63</span></a>
                                    </li>


                                      {{-- <li>
                                        <a class="" href="{{ config('app.url') }}/correctionOfMeterParticulars"><span class="sidebar-mini-hide">Advice - 73</span></a>
                                      </li>
                                      <li>
                                        <a class="" href="{{ config('app.url') }}/adviceForDisconnection"><span class="sidebar-mini-hide">Advice - 74</span></a>
                                      </li>
                                      <li>
                                        <a class="" href="{{ config('app.url') }}/correctionOfName"><span class="sidebar-mini-hide">Advice - 75</span></a>
                                      </li>
                                      <li>
                                        <a class="" href="{{ config('app.url') }}/sundryCharges"><span class="sidebar-mini-hide">Advice - 76</span></a>
                                      </li>



                                      <li>
                                        <a class="" href="{{ config('app.url') }}/changeOfAcd"><span class="sidebar-mini-hide">Advice - 80</span></a>
                                      </li> --}}

                                    </ul>
                                </li>
                                {{-- <li>
                                    <a class="" href="{{ config('app.url') }}/upload"><i class="si si-layers"></i><span class="sidebar-mini-hide">Import Bill</span></a>
                                </li>
                                <li>
                                    <a class="" href="{{ config('app.url') }}/bill"><i class="si si-layers"></i><span class="sidebar-mini-hide">Demo Bill</span></a>
                                </li> --}}
                            </ul>
                        </div>
                        <!-- END Side Navigation -->
                    </div>
                    <!-- Sidebar Content -->
                </div>
                <!-- END Sidebar Scroll Container -->
            </nav>
            <!-- END Sidebar -->

            <!-- Header -->
            <header id="page-header">
                <!-- Header Content -->
                <div class="content-header">
                    <!-- Left Section -->
                    <div class="content-header-section">
                        <!-- Toggle Sidebar -->
                        <!-- Layout API, functionality initialized in Codebase() -> uiApiLayout() -->
                        <button type="button" class="btn btn-circle btn-dual-secondary" data-toggle="layout" data-action="sidebar_toggle">
                            <i class="fa fa-navicon"></i>
                        </button>
                        <!-- END Toggle Sidebar -->

                        <!-- Open Search Section -->
                        <!-- Layout API, functionality initialized in Codebase() -> uiApiLayout() -->
                        <button type="button" class="btn btn-circle btn-dual-secondary" data-toggle="layout" data-action="header_search_on">
                            {{-- <i class="fa fa-search"></i> --}}
                        </button>
                        <!-- END Open Search Section -->

                        <!-- Layout Options (used just for demonstration) -->
                        <!-- Layout API, functionality initialized in Codebase() -> uiApiLayout() -->

                        <!-- END Layout Options -->

                        <!-- Color Themes (used just for demonstration) -->
                        <!-- Themes functionality initialized in Codebase() -> uiHandleTheme() -->

                        <!-- END Color Themes -->
                    </div>
                    <!-- END Left Section -->

                    <!-- Right Section -->
                    <div class="content-header-section">
                        <!-- User Dropdown -->
                        <div class="btn-group" role="group">
                            <button type="button" class="btn btn-rounded btn-dual-secondary" id="page-header-user-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                {{ Auth::user()->name }}<i class="fa fa-angle-down ml-5"></i>
                            </button>
                            <div class="dropdown-menu dropdown-menu-right min-width-150" aria-labelledby="page-header-user-dropdown">
                                <a class="dropdown-item" href="/profile">
                                    <i class="si si-user mr-5"></i> Profile
                                </a>

                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href onclick="event.preventDefault();
                             document.getElementById('logout-form').submit();"class="clearfix">
                           <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                              </form>
                                    <i class="si si-logout mr-5"></i> Sign Out
                                </a>
                            </div>
                        </div>
                        <!-- END User Dropdown -->

                        <!-- Toggle Side Overlay -->
                        <!-- Layout API, functionality initialized in Codebase() -> uiApiLayout() -->
                        {{-- <button type="button" class="btn btn-circle btn-dual-secondary" data-toggle="layout" data-action="side_overlay_toggle">
                            <i class="fa fa-tasks"></i>
                        </button> --}}
                        <!-- END Toggle Side Overlay -->
                    </div>
                    <!-- END Right Section -->
                </div>
                <!-- END Header Content -->

                <!-- Header Search -->
                <div id="page-header-search" class="overlay-header">
                    <div class="content-header content-header-fullrow">
                        <form action="be_pages_generic_search.html" method="post">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <!-- Close Search Section -->
                                    <!-- Layout API, functionality initialized in Codebase() -> uiApiLayout() -->
                                    <button type="button" class="btn btn-secondary" data-toggle="layout" data-action="header_search_off">
                                        <i class="fa fa-times"></i>
                                    </button>
                                    <!-- END Close Search Section -->
                                </div>
                                <input type="text" class="form-control" placeholder="Search or hit ESC.." id="page-header-search-input" name="page-header-search-input">
                                <div class="input-group-append">
                                    <button type="submit" class="btn btn-secondary">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- END Header Search -->

                <!-- Header Loader -->
                <!-- Please check out the Activity page under Elements category to see examples of showing/hiding it -->
                <div id="page-header-loader" class="overlay-header bg-primary">
                    <div class="content-header content-header-fullrow text-center">
                        <div class="content-header-item">
                            <i class="fa fa-sun-o fa-spin text-white"></i>
                        </div>
                    </div>
                </div>
                <!-- END Header Loader -->
            </header>
            <!-- END Header -->
