@php
  use App\User;
  use App\Bill;
  use App\sapInput;
@endphp
<!doctype html>
<!--[if lte IE 9]>     <html lang="en" class="no-focus lt-ie10 lt-ie10-msg"> <![endif]-->
<!--[if gt IE 9]><!--> <html lang="en" class="no-focus"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
        @php
          $date = Date('d-m-Y');
          $time = Date('H-i-s');
        @endphp
        @if (isset($data['sub']))
          @php
            $sub = $data['sub'];
            $bg = $data['bg'];
            $bc = $data['bc'];
          @endphp
        @else
          @php
          $sub = '';
          $bg = '';
          $bc = '';
          @endphp
        @endif
        <title>Ledger WISE Billing Summary {{ $sub }} - {{ $bg }} - {{ $bc }} - Date: {{ $date }} - Time: {{ $time }}</title>

        <meta name="description" content="Codebase - Bootstrap 4 Admin Template &amp; UI Framework created by pixelcave and published on Themeforest">
        <meta name="author" content="pixelcave">
        <meta name="robots" content="noindex, nofollow">
        <meta name="csrf-token" content="{{ csrf_token() }}" />

        <!-- Open Graph Meta -->
        <meta property="og:title" content="Codebase - Bootstrap 4 Admin Template &amp; UI Framework">
        <meta property="og:site_name" content="Codebase">
        <meta property="og:description" content="Codebase - Bootstrap 4 Admin Template &amp; UI Framework created by pixelcave and published on Themeforest">
        <meta property="og:type" content="website">
        <meta property="og:url" content="">
        <meta property="og:image" content="">

        <!-- Icons -->
        <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
        <link rel="shortcut icon" href="{{ config('app.url') }}/assets/img/favicons/favicon.png">
        <link rel="icon" type="image/png" sizes="192x192" href="{{ config('app.url') }}/assets/img/favicons/favicon-192x192.png">
        <link rel="apple-touch-icon" sizes="180x180" href="{{ config('app.url') }}/assets/img/favicons/apple-touch-icon-180x180.png">
        <!-- END Icons -->

        <!-- Stylesheets -->
        <!-- Page JS Plugins CSS -->
        <link rel="stylesheet" href="{{ config('app.url') }}/assets/js/plugins/datatables/dataTables.bootstrap4.min.css">

        <!-- Codebase framework -->
        <link rel="stylesheet" id="css-main" href="{{ config('app.url') }}/assets/css/codebase.min.css">

        <!-- Stylesheets -->
        <link rel="stylesheet" href="{{ config('app.url') }}/assets/js/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css"> {{--
        <link rel="stylesheet" href="assets/js/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css"> --}}
        <link rel="stylesheet" href="{{ config('app.url') }}/assets/js/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css">
        {{-- <link rel="stylesheet" href="{{ config('app.url') }}/assets/js/plugins/select2/select2.min.css">
        <link rel="stylesheet" href="{{ config('app.url') }}/assets/js/plugins/select2/select2-bootstrap.min.css">
        <link rel="stylesheet" href="{{ config('app.url') }}/assets/js/plugins/jquery-tags-input/jquery.tagsinput.min.css">
        <link rel="stylesheet" href="{{ config('app.url') }}/assets/js/plugins/jquery-auto-complete/jquery.auto-complete.min.css">
        <link rel="stylesheet" href="{{ config('app.url') }}/assets/js/plugins/ion-rangeslider/css/ion.rangeSlider.min.css">
        <link rel="stylesheet" href="{{ config('app.url') }}/assets/js/plugins/ion-rangeslider/css/ion.rangeSlider.skinHTML5.min.css">
        <link rel="stylesheet" href="{{ config('app.url') }}/assets/js/plugins/dropzonejs/min/dropzone.min.css"> --}}
        <!-- Codebase framework -->
        <link rel="stylesheet" id="css-main" href="{{ config('app.url') }}/assets/css/codebase.min.css">

        <!-- You can include a specific file from css/themes/ folder to alter the default color theme of the template. eg: -->
        <!-- <link rel="stylesheet" id="css-theme" href="assets/css/themes/flat.min.css"> -->
        <!-- END Stylesheets -->
    </head>
    <body>
        <!-- Page Container -->
        <!--
            Available classes for #page-container:

        GENERIC

            'enable-cookies'                            Remembers active color theme between pages (when set through color theme helper Codebase() -> uiHandleTheme())

        SIDEBAR & SIDE OVERLAY

            'sidebar-r'                                 Right Sidebar and left Side Overlay (default is left Sidebar and right Side Overlay)
            'sidebar-mini'                              Mini hoverable Sidebar (screen width > 991px)
            'sidebar-o'                                 Visible Sidebar by default (screen width > 991px)
            'sidebar-o-xs'                              Visible Sidebar by default (screen width < 992px)
            'sidebar-inverse'                           Dark themed sidebar

            'side-overlay-hover'                        Hoverable Side Overlay (screen width > 991px)
            'side-overlay-o'                            Visible Side Overlay by default

            'side-scroll'                               Enables custom scrolling on Sidebar and Side Overlay instead of native scrolling (screen width > 991px)

        HEADER

            ''                                          Static Header if no class is added
            'page-header-fixed'                         Fixed Header

        HEADER STYLE

            ''                                          Classic Header style if no class is added
            'page-header-modern'                        Modern Header style
            'page-header-inverse'                       Dark themed Header (works only with classic Header style)
            'page-header-glass'                         Light themed Header with transparency by default
                                                        (absolute position, perfect for light images underneath - solid light background on scroll if the Header is also set as fixed)
            'page-header-glass page-header-inverse'     Dark themed Header with transparency by default
                                                        (absolute position, perfect for dark images underneath - solid dark background on scroll if the Header is also set as fixed)

        MAIN CONTENT LAYOUT

            ''                                          Full width Main Content if no class is added
            'main-content-boxed'                        Full width Main Content with a specific maximum width (screen width > 1200px)
            'main-content-narrow'                       Full width Main Content with a percentage width (screen width > 1200px)
        -->
        <div id="page-container" class="sidebar-o side-scroll page-header-modern main-content-boxed">
            <!-- Side Overlay-->
            @include('includes/headerAndSidebar')
            <!-- END Header -->

            <!-- Main Container -->
            <main id="main-container">
                <!-- Page Content -->
                <div class="content">
                    <h2 class="content-heading">Ledger Wise Billing Summary NON SAP</h2>

                    <!-- Dynamic Table Full -->
                    <div class="block">
                        {{-- <div class="block-header block-header-default">
                            <h3 class="block-title">Dynamic Table <small></small></h3>
                        </div> --}}
                        <div class="block-content block-content-full">
                            <!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/be_tables_datatables.js -->

                              <form class="" action="{{ route('ledgerPerformanceReportNonSAP') }}" method="post" id="">
                                @csrf
                                <div class="form-group row">
                                      <div class="col-md-3">
                                        <label for="subDivisionCode">Sub Division Code <span class="text-danger">*</span> </label>
                                          <select class="form-control" id="subDivisionCode" name="subDivisionCode" onchange="getGroupCycleAjaxNonSap(this)">
                                            {{-- Sub Division Code's --}}
                                            <option value="">Select</option>
                                            @php
                                              // $sub = sapInput::groupBy('subDivisionCode');
                                              // $sub = sapInput::select('subDivisionCode')->groupBy('subDivisionCode')->take(20)->get();
                                              $sub = DB::select("SELECT distinct subDivisionCode FROM bills group by subDivisionCode");
                                              // $subDivision = '3213';
                                              // $billingsGroups = DB::select("select distinct billingGroup from sap_inputs WHERE subDivisionCode in ('$subDivision') group by billingGroup");
                                              // $billingsCycles = DB::select("select distinct billingCycle from sap_inputs WHERE subDivisionCode in ('$subDivision') group by billingCycle");
                                              // $ledgerCodes = DB::select("SELECT distinct MRU FROM sap_inputs group by MRU");
                                              // dd($sub);
                                            @endphp
                                            @foreach ($sub as $subDivisionCode)
                                              @if (isset($data) && count($data['sub']) > 0)
                                                <option value="{{ $subDivisionCode->subDivisionCode }}" @if ($subDivisionCode->subDivisionCode == $data['sub']) selected @endif>{{ $subDivisionCode->subDivisionCode }}</option>
                                            @else
                                              <option value="{{ $subDivisionCode->subDivisionCode }}" >{{ $subDivisionCode->subDivisionCode }}</option>
                                            @endif
                                            @endforeach
                                          </select>
                                          {{-- <input type="text" class="form-control" id="subDivisionCode" name="subDivisionCode" placeholder="Sub Division Code" required> --}}
                                          {{-- <div class="form-text text-muted">Further info about this input</div> --}}
                                          @if ($errors->has('subDivisionCode'))
                                       <span class="help-block">
                                       <strong class="text-danger">{{ $errors->first('subDivisionCode') }}</strong>
                                        </span>
                                          @endif
                                      </div>
                                      <div class="col-md-3">
                                        <label for="billingGroup">Billing Group <span class="text-danger">*</span> </label>
                                        <select class="form-control" name="billingGroup" id="billingGroup" required onchange="getGroupCycleAjaxNonSapBc(this)">
                                          <option value="">Select</option>
                                          @if (isset($data) && count($data['sub']) > 0)
                                            @php
                                              $sub = $data['sub'];
                                              $bg = $data['bg'];
                                              $bc = $data['bc'];
                                              $meterReaders = DB::select("select distinct billingGroup from bills where subDivisionCode = '$sub'");
                                            @endphp
                                            @foreach ($meterReaders as $meterReader)
                                              <option value="{{ $meterReader->billingGroup }}" @if ($meterReader->billingGroup == $data['bg']) selected @endif>{{ $meterReader->billingGroup }}</option>
                                            @endforeach
                                          @endif
                                        </select>
                                          {{-- <div class="form-text text-muted">Further info about this input</div> --}}
                                          @if ($errors->has('billingGroup'))
                                       <span class="help-block">
                                       <strong class="text-danger">{{ $errors->first('billingGroup') }}</strong>
                                        </span>
                                          @endif
                                      </div>
                                      <div class="col-md-2">
                                        <label for="billingCycle">Billing Cycle <span class="text-danger">*</span></label>
                                        <select class="form-control" name="billingCycle" id="billingCycle" required onchange="getmeterReaders(this)">
                                          <option value="">Select</option>
                                          @if (isset($data) && count($data['sub']) > 0)
                                            @php
                                              $sub = $data['sub'];
                                              $bg = $data['bg'];
                                              $bc = $data['bc'];
                                              $meterReaders = DB::select("select distinct billingCycle from bills where subDivisionCode = '$sub'");
                                            @endphp
                                            @foreach ($meterReaders as $meterReader)
                                              <option value="{{ $meterReader->billingCycle }}" @if ($meterReader->billingCycle == $data['bc']) selected @endif>{{ $meterReader->billingCycle }}</option>
                                            @endforeach
                                          @endif
                                        </select>
                                          {{-- <div class="form-text text-muted">Further info about this input</div> --}}
                                          @if ($errors->has('billingCycle'))
                                       <span class="help-block">
                                       <strong class="text-danger">{{ $errors->first('billingCycle') }}</strong>
                                        </span>
                                          @endif
                                      </div>
                                      {{-- <div class="col-md-3">
                                        <label for="meterReader">Meter Reader <span class="text-danger">*</span></label>
                                        <select class="form-control" name="meterReader" required id="meterReaders">
                                          <option value="">Select</option>
                                          @if (isset($data) && count($data['sub']) > 0)
                                            @php
                                              $sub = $data['sub'];
                                              $bg = $data['bg'];
                                              $bc = $data['bc'];
                                              $from = $data['from'];
                                              $to = $data['to'];
                                              $meterReaders = DB::select("select distinct meterReader from billeds where blSubDivisionCode = '$sub' and blBillingGroup = '$bg' and blBillingCycle = '$bc' and (blPresentReadingDate between '$from' and '$to')");
                                            @endphp
                                            @foreach ($meterReaders as $meterReader)
                                              <option value="{{ $meterReader->meterReader }}" @if ($meterReader->meterReader == $data['reader']) selected @endif>{{ $meterReader->meterReader }}</option>
                                            @endforeach
                                          @endif
                                        </select>
                                        @if ($errors->has('meterReader'))
                                     <span class="help-block">
                                     <strong class="text-danger">{{ $errors->first('meterReader') }}</strong>
                                      </span>
                                        @endif
                                      </div> --}}
                                      <div class="col-md-1">
                                        <label for="accountNoL">. </label>
                                          <button type="submit" class="btn btn-alt-primary">Submit</button>
                                      </div>


                                  </div>

                              </form>

                            @if (isset($data) && $data['data'] != '')
                              <table class="table-bordered table-responsive table-striped table-vcenter js-dataTable-full">
                                  <thead>
                                      <tr>
                                          <th>S.No</th>
                                          <th>Sub</th>
                                          {{-- <th>Name</th> --}}
                                          <th>BG</th>
                                          <th>BC</th>
                                          <th>Ledger</th>
                                          <th>Tot Cons</th>
                                          <th>Bld Cons</th>
                                          <th>O</th>
                                          <th>A</th>
                                          <th>E</th>
                                          <th>C</th>
                                          <th>W</th>
                                          <th>B</th>
                                          <th>H</th>
                                          <th>T</th>
                                          <th>X</th>
                                          <th>L</th>
                                          <th>N</th>
                                          <th>I</th>
                                          <th>D</th>
                                          <th>M</th>
                                          <th>R</th>
                                          <th>F</th>
                                          <th>G</th>
                                          <th>S</th>
                                          {{-- <th>P</th> --}}
                                          <th>Bal</th>
                                      </tr>
                                  </thead>
                                  <tbody>

                                      @php
                                      $report = $data['data'];
                                        $sno = 1;
                                      @endphp
                                      @foreach ($report as $record)
                                        <tr>
                                            <td class="">{{ $sno++ }}</td>
                                            <td class="">{{ $data['sub'] }}</td>
                                            {{-- @php
                                              $subCode = $data['sub'];
                                              $getSubName = DB::select("SELECT subDivisionName from sap_inputs WHERE subDivisionCode = $subCode limit 1");
                                            @endphp
                                            <td class="">{{ $getSubName[0]->subDivisionName }}</td> --}}
                                            <td class="">{{ $data['bg'] }}</td>
                                            <td class="">{{ $data['bc'] }}</td>
                                            <td class="">{{ $record->reader }}</td>
                                            @php
                                            $sub = $data['sub'];
                                            $bg = $data['bg'];
                                            $bc = $data['bc'];
                                            $MRU = $record->reader;
                                              $totalConnections = DB::select("select count(*) totalconnections from bills where subDivisionCode = '$sub' and billingGroup ='$bg' and billingCycle = '$bc' and SUBSTRING(masterKey, 4, 4) = '$MRU'");
                                            @endphp
                                            <td>{{ $totalConnections[0]->totalconnections }}</td>
                                            @php
                                              $totalBilled = DB::select("select count(*) totalbilled from billeds where blsubDivisionCode = '$sub' and blBillingGroup ='$bg' and blBillingCycle = '$bc' and SUBSTRING(blMasterKey, 4, 4) = '$MRU'");
                                            @endphp
                                            <td>{{ $totalBilled[0]->totalbilled }}</td>
                                            @php
                                              $balance = $totalConnections[0]->totalconnections - $totalBilled[0]->totalbilled;
                                              $ocount = DB::select("select count(*) ocount from billeds where blsubDivisionCode = '$sub' and blBillingGroup ='$bg' and blBillingCycle = '$bc' and blPresentStatus = 'O' and SUBSTRING(blMasterKey, 4, 4) = '$MRU'");
                                            @endphp
                                            <td>{{ $ocount[0]->ocount }}</td>
                                            @php
                                              $acount = DB::select("select count(*) acount from billeds where blSubDivisionCode = '$sub' and blBillingGroup ='$bg' and blBillingCycle = '$bc' and blPresentStatus = 'A' and SUBSTRING(blMasterKey, 4, 4) = '$MRU'");
                                            @endphp
                                            <td>{{ $acount[0]->acount }}</td>
                                            @php
                                              $ecount = DB::select("select count(*) ecount from billeds where blSubDivisionCode = '$sub' and blBillingGroup ='$bg' and blBillingCycle = '$bc' and blPresentStatus = 'E' and SUBSTRING(blMasterKey, 4, 4) = '$MRU'");
                                            @endphp
                                            <td>{{ $ecount[0]->ecount }}</td>
                                            @php
                                              $ccount = DB::select("select count(*) ccount from billeds where blSubDivisionCode = '$sub' and blBillingGroup ='$bg' and blBillingCycle = '$bc' and blPresentStatus = 'C' and SUBSTRING(blMasterKey, 4, 4) = '$MRU'");
                                            @endphp
                                            <td>{{ $ccount[0]->ccount }}</td>
                                            @php
                                              $wcount = DB::select("select count(*) wcount from billeds where blSubDivisionCode = '$sub' and blBillingGroup ='$bg' and blBillingCycle = '$bc' and blPresentStatus = 'W' and SUBSTRING(blMasterKey, 4, 4) = '$MRU'");
                                            @endphp
                                            <td>{{ $wcount[0]->wcount }}</td>
                                            @php
                                              $bcount = DB::select("select count(*) bcount from billeds where blSubDivisionCode = '$sub' and blBillingGroup ='$bg' and blBillingCycle = '$bc' and blPresentStatus = 'B' and SUBSTRING(blMasterKey, 4, 4) = '$MRU'");
                                            @endphp
                                            <td>{{ $bcount[0]->bcount }}</td>
                                            @php
                                              $hcount = DB::select("select count(*) hcount from billeds where blSubDivisionCode = '$sub' and blBillingGroup ='$bg' and blBillingCycle = '$bc' and blPresentStatus = 'H' and SUBSTRING(blMasterKey, 4, 4) = '$MRU'");
                                            @endphp
                                            <td>{{ $hcount[0]->hcount }}</td>
                                            @php
                                              $tcount = DB::select("select count(*) tcount from billeds where blSubDivisionCode = '$sub' and blBillingGroup ='$bg' and blBillingCycle = '$bc' and blPresentStatus = 'T' and SUBSTRING(blMasterKey, 4, 4) = '$MRU'");
                                            @endphp
                                            <td>{{ $tcount[0]->tcount }}</td>
                                            @php
                                              $xcount = DB::select("select count(*) xcount from billeds where blSubDivisionCode = '$sub' and blBillingGroup ='$bg' and blBillingCycle = '$bc' and blPresentStatus = 'X' and SUBSTRING(blMasterKey, 4, 4) = '$MRU'");
                                            @endphp
                                            <td>{{ $xcount[0]->xcount }}</td>
                                            @php
                                              $lcount = DB::select("select count(*) lcount from billeds where blSubDivisionCode = '$sub' and blBillingGroup ='$bg' and blBillingCycle = '$bc' and blPresentStatus = 'L' and SUBSTRING(blMasterKey, 4, 4) = '$MRU'");
                                            @endphp
                                            <td>{{ $lcount[0]->lcount }}</td>
                                            @php
                                              $ncount = DB::select("select count(*) ncount from billeds where blSubDivisionCode = '$sub' and blBillingGroup ='$bg' and blBillingCycle = '$bc' and blPresentStatus = 'N' and SUBSTRING(blMasterKey, 4, 4) = '$MRU'");
                                            @endphp
                                            <td>{{ $ncount[0]->ncount }}</td>
                                            @php
                                              $icount = DB::select("select count(*) icount from billeds where blSubDivisionCode = '$sub' and blBillingGroup ='$bg' and blBillingCycle = '$bc' and blPresentStatus = 'I' and SUBSTRING(blMasterKey, 4, 4) = '$MRU'");
                                            @endphp
                                            <td>{{ $icount[0]->icount }}</td>
                                            @php
                                              $dcount = DB::select("select count(*) dcount from billeds where blSubDivisionCode = '$sub' and blBillingGroup ='$bg' and blBillingCycle = '$bc' and blPresentStatus = 'D' and SUBSTRING(blMasterKey, 4, 4) = '$MRU'");
                                            @endphp
                                            <td>{{ $dcount[0]->dcount }}</td>
                                            @php
                                              $mcount = DB::select("select count(*) mcount from billeds where blSubDivisionCode = '$sub' and blBillingGroup ='$bg' and blBillingCycle = '$bc' and blPresentStatus = 'M' and SUBSTRING(blMasterKey, 4, 4) = '$MRU'");
                                            @endphp
                                            <td>{{ $mcount[0]->mcount }}</td>
                                            @php
                                              $rcount = DB::select("select count(*) rcount from billeds where blSubDivisionCode = '$sub' and blBillingGroup ='$bg' and blBillingCycle = '$bc' and blPresentStatus = 'R' and SUBSTRING(blMasterKey, 4, 4) = '$MRU'");
                                            @endphp
                                            <td>{{ $rcount[0]->rcount }}</td>
                                            @php
                                              $fcount = DB::select("select count(*) fcount from billeds where blSubDivisionCode = '$sub' and blBillingGroup ='$bg' and blBillingCycle = '$bc' and blPresentStatus = 'F' and SUBSTRING(blMasterKey, 4, 4) = '$MRU'");
                                            @endphp
                                            <td>{{ $fcount[0]->fcount }}</td>
                                            @php
                                              $gcount = DB::select("select count(*) gcount from billeds where blSubDivisionCode = '$sub' and blBillingGroup ='$bg' and blBillingCycle = '$bc' and blPresentStatus = 'G' and SUBSTRING(blMasterKey, 4, 4) = '$MRU'");
                                            @endphp
                                            <td>{{ $gcount[0]->gcount }}</td>
                                            @php
                                              $scount = DB::select("select count(*) scount from billeds where blSubDivisionCode = '$sub' and blBillingGroup ='$bg' and blBillingCycle = '$bc' and blPresentStatus = 'S' and SUBSTRING(blMasterKey, 4, 4) = '$MRU'");
                                            @endphp
                                            <td>{{ $scount[0]->scount }}</td>
                                            {{-- @php
                                              $pcount = DB::select("select count(*) pcount from billeds where blSubDivisionCode = '$sub' and blBillingGroup ='$bg' and blBillingCycle = '$bc' and blPresentStatus = 'P' and SUBSTRING(blMasterKey, 4, 4) = '$MRU'");
                                            @endphp
                                            <td>{{ $pcount[0]->pcount }}</td> --}}
                                            <td>{{ $balance }}</td>
                                        </tr>

                                      @endforeach

                                  </tbody>
                              </table>
                            @endif
                        </div>
                    </div>
                    <!-- END Dynamic Table Full -->
                </div>
                <!-- END Page Content -->
            </main>
            <!-- END Main Container -->

            <!-- Footer -->
            @include('includes/footer')
            <!-- END Footer -->
        </div>
        <!-- END Page Container -->

        <!-- Codebase Core JS -->
        <script src="{{ config('app.url') }}/assets/js/core/jquery.min.js"></script>
        <script src="{{ config('app.url') }}/assets/js/core/bootstrap.bundle.min.js"></script>
        <script src="{{ config('app.url') }}/assets/js/core/jquery.slimscroll.min.js"></script>
        <script src="{{ config('app.url') }}/assets/js/core/jquery.scrollLock.min.js"></script>
        <script src="{{ config('app.url') }}/assets/js/core/jquery.appear.min.js"></script>
        <script src="{{ config('app.url') }}/assets/js/core/jquery.countTo.min.js"></script>
        <script src="{{ config('app.url') }}/assets/js/core/js.cookie.min.js"></script>
        <script src="{{ config('app.url') }}/assets/js/codebase.js"></script>

        <!-- Page JS Plugins -->
        {{-- <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script> --}}
        <script src="{{ config('app.url') }}/assets/js/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="{{ config('app.url') }}/assets/js/plugins/datatables/dataTables.bootstrap4.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>

        <!-- Page JS Code -->
        <script src="{{ config('app.url') }}/assets/js/pages/be_tables_datatables.js"></script>
        <script src="{{ config('app.url') }}/assets/js/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
        {{-- <script src="{{ config('app.url') }}/assets/js/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script> --}}
        {{-- <script src="{{ config('app.url') }}/assets/js/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js"></script> --}}
        {{-- <script src="{{ config('app.url') }}/assets/js/plugins/select2/select2.full.min.js"></script> --}}
        <script src="{{ config('app.url') }}/assets/js/plugins/jquery-tags-input/jquery.tagsinput.min.js"></script>
        <script src="{{ config('app.url') }}/assets/js/plugins/jquery-auto-complete/jquery.auto-complete.min.js"></script>
        {{-- <script src="{{ config('app.url') }}/assets/js/plugins/masked-inputs/jquery.maskedinput.min.js"></script> --}}
        {{-- <script src="{{ config('app.url') }}/assets/js/plugins/ion-rangeslider/js/ion.rangeSlider.min.js"></script> --}}
        {{-- <script src="{{ config('app.url') }}/assets/js/plugins/dropzonejs/min/dropzone.min.js"></script> --}}

        <!-- Page JS Code -->
        <script src="{{ config('app.url') }}/assets/js/pages/be_forms_plugins.js"></script>

        <script>
            jQuery(function() {
                // Init page helpers (BS Datepicker + BS Colorpicker + BS Maxlength + Select2 + Masked Input + Range Sliders + Tags Inputs plugins)
                Codebase.helpers(['datepicker']);
            });
        </script>
        <script type="text/javascript">
        function getGroupCycleAjaxNonSap(e) {

          $('#billingGroup')
              .empty()
              .append('<option value="">Select</option>')
          ;
          $('#billingCycle')
              .empty()
              .append('<option value="">Select</option>')
          ;
          $('#ledgerCode')
              .empty()
              .append('<option value="">Select</option>')
          ;

          // console.log(e.value);
          var subDivisionCode = e.value;
          $.ajax({
            headers: {
               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
           },
          type: "POST",
          url: '/api/getGroupCycleAjaxNonSap',
          data: {subDivisionCode: subDivisionCode},
            success: function( data ) {
                console.log(data);
                $.each(data.billingGroup, function(key, value) {
                     $('#billingGroup')
                         .append($("<option></option>")
                                    .attr("value",value.billingGroup)
                                    .text(value.billingGroup));
                });
                // $.each(data.billingCycle, function(key, value) {
                //   // console.log(value.billingCycle);
                //      $('#billingCycle')
                //          .append($("<option></option>")
                //                     .attr("value",value.billingCycle)
                //                     .text(value.billingCycle));
                // });
                // $.each(data.ledgerCodes, function(key, value) {
                //   // console.log(value);
                //      $('#ledgerCode')
                //          .append($("<option></option>")
                //                     .attr("value",value.ledgerCode)
                //                     .text(value.ledgerCode));
                // });
                // $("#ajaxResponse").append("<div>"+msg+"</div>");
            }
          });
        }

        function getGroupCycleAjaxNonSapBc(e) {

          // console.log(e.value);
          $('#billingCycle')
              .empty()
              .append('<option value="">Select</option>')
          ;
          $('#ledgerCode')
              .empty()
              .append('<option value="">Select</option>')
          ;

          // console.log(e.value);
          var subDivisionCode = $('#subDivisionCode').val();
          var billingGroup = e.value;
          $.ajax({
            headers: {
               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
           },
          type: "POST",
          url: '/api/getGroupCycleAjaxNonSapBc',
          data: {subDivisionCode: subDivisionCode, billingGroup: billingGroup},
            success: function( data ) {
                console.log(data);
                $.each(data.billingCycle, function(key, value) {
                  // console.log(value.billingCycle);
                     $('#billingCycle')
                         .append($("<option></option>")
                                    .attr("value",value.billingCycle)
                                    .text(value.billingCycle));
                });
                // $("#ajaxResponse").append("<div>"+msg+"</div>");
            }
          });
        }

          function getmeterReaders(e) {

            $('#meterReaders')
                .empty()
                .append('<option value="">Select</option>')
            ;

            // console.log(e.value);
            var subDivisionCode = $('#subDivisionCode').val();
            var billingGroup = $('#billingGroup').val();
            var billingCycle = $('#billingCycle').val();
            var from = $('#from').val();
            var to = $('#to').val();
            console.log(subDivisionCode);
            console.log(from);
            console.log(to);
            $.ajax({
              headers: {
                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
             },
            type: "POST",
            url: '/api/get-meter-reader-sub-bg-bc-non-sap',
            data: {subDivisionCode: subDivisionCode, billingGroup: billingGroup, billingCycle: billingCycle, from: from, to: to},
              success: function( data ) {
                  console.log(data);
                  $.each(data.meterReaders, function(key, value) {
                       $('#meterReaders')
                           .append($("<option></option>")
                                      .attr("value",value.meterReader)
                                      .text(value.meterReader));
                  });
              }
            });
          }



        </script>
    </body>
</html>
