@php
  use App\User;
  use App\Bill;
  use App\sapInput;
@endphp
<!doctype html>
<!--[if lte IE 9]>     <html lang="en" class="no-focus lt-ie10 lt-ie10-msg"> <![endif]-->
<!--[if gt IE 9]><!--> <html lang="en" class="no-focus"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">

        <title>Mini Ledger NON SAP | STERLING Transformers</title>

        <meta name="description" content="Codebase - Bootstrap 4 Admin Template &amp; UI Framework created by pixelcave and published on Themeforest">
        <meta name="author" content="pixelcave">
        <meta name="robots" content="noindex, nofollow">
        <meta name="csrf-token" content="{{ csrf_token() }}" />

        <!-- Open Graph Meta -->
        <meta property="og:title" content="Codebase - Bootstrap 4 Admin Template &amp; UI Framework">
        <meta property="og:site_name" content="Codebase">
        <meta property="og:description" content="Codebase - Bootstrap 4 Admin Template &amp; UI Framework created by pixelcave and published on Themeforest">
        <meta property="og:type" content="website">
        <meta property="og:url" content="">
        <meta property="og:image" content="">

        <!-- Icons -->
        <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
        <link rel="shortcut icon" href="{{ config('app.url') }}/assets/img/favicons/favicon.png">
        <link rel="icon" type="image/png" sizes="192x192" href="{{ config('app.url') }}/assets/img/favicons/favicon-192x192.png">
        <link rel="apple-touch-icon" sizes="180x180" href="{{ config('app.url') }}/assets/img/favicons/apple-touch-icon-180x180.png">
        <!-- END Icons -->

        <!-- Stylesheets -->
        <!-- Page JS Plugins CSS -->
        <link rel="stylesheet" href="{{ config('app.url') }}/assets/js/plugins/datatables/dataTables.bootstrap4.min.css">

        <!-- Codebase framework -->
        <link rel="stylesheet" id="css-main" href="{{ config('app.url') }}/assets/css/codebase.min.css">

        <!-- Stylesheets -->
        <link rel="stylesheet" href="{{ config('app.url') }}/assets/js/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css"> {{--
        <link rel="stylesheet" href="assets/js/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css"> --}}
        <link rel="stylesheet" href="{{ config('app.url') }}/assets/js/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css">
        {{-- <link rel="stylesheet" href="{{ config('app.url') }}/assets/js/plugins/select2/select2.min.css">
        <link rel="stylesheet" href="{{ config('app.url') }}/assets/js/plugins/select2/select2-bootstrap.min.css">
        <link rel="stylesheet" href="{{ config('app.url') }}/assets/js/plugins/jquery-tags-input/jquery.tagsinput.min.css">
        <link rel="stylesheet" href="{{ config('app.url') }}/assets/js/plugins/jquery-auto-complete/jquery.auto-complete.min.css">
        <link rel="stylesheet" href="{{ config('app.url') }}/assets/js/plugins/ion-rangeslider/css/ion.rangeSlider.min.css">
        <link rel="stylesheet" href="{{ config('app.url') }}/assets/js/plugins/ion-rangeslider/css/ion.rangeSlider.skinHTML5.min.css">
        <link rel="stylesheet" href="{{ config('app.url') }}/assets/js/plugins/dropzonejs/min/dropzone.min.css"> --}}
        <!-- Codebase framework -->
        <link rel="stylesheet" id="css-main" href="{{ config('app.url') }}/assets/css/codebase.min.css">

        <!-- You can include a specific file from css/themes/ folder to alter the default color theme of the template. eg: -->
        <!-- <link rel="stylesheet" id="css-theme" href="assets/css/themes/flat.min.css"> -->
        <!-- END Stylesheets -->
    </head>
    <body>
        <!-- Page Container -->
        <!--
            Available classes for #page-container:

        GENERIC

            'enable-cookies'                            Remembers active color theme between pages (when set through color theme helper Codebase() -> uiHandleTheme())

        SIDEBAR & SIDE OVERLAY

            'sidebar-r'                                 Right Sidebar and left Side Overlay (default is left Sidebar and right Side Overlay)
            'sidebar-mini'                              Mini hoverable Sidebar (screen width > 991px)
            'sidebar-o'                                 Visible Sidebar by default (screen width > 991px)
            'sidebar-o-xs'                              Visible Sidebar by default (screen width < 992px)
            'sidebar-inverse'                           Dark themed sidebar

            'side-overlay-hover'                        Hoverable Side Overlay (screen width > 991px)
            'side-overlay-o'                            Visible Side Overlay by default

            'side-scroll'                               Enables custom scrolling on Sidebar and Side Overlay instead of native scrolling (screen width > 991px)

        HEADER

            ''                                          Static Header if no class is added
            'page-header-fixed'                         Fixed Header

        HEADER STYLE

            ''                                          Classic Header style if no class is added
            'page-header-modern'                        Modern Header style
            'page-header-inverse'                       Dark themed Header (works only with classic Header style)
            'page-header-glass'                         Light themed Header with transparency by default
                                                        (absolute position, perfect for light images underneath - solid light background on scroll if the Header is also set as fixed)
            'page-header-glass page-header-inverse'     Dark themed Header with transparency by default
                                                        (absolute position, perfect for dark images underneath - solid dark background on scroll if the Header is also set as fixed)

        MAIN CONTENT LAYOUT

            ''                                          Full width Main Content if no class is added
            'main-content-boxed'                        Full width Main Content with a specific maximum width (screen width > 1200px)
            'main-content-narrow'                       Full width Main Content with a percentage width (screen width > 1200px)
        -->
        <div id="page-container" class="sidebar-o side-scroll page-header-modern main-content-boxed">
            <!-- Side Overlay-->
            @include('includes/headerAndSidebar')
            <!-- END Header -->

            <!-- Main Container -->
            <main id="main-container">
                <!-- Page Content -->
                <div class="content">
                    <h2 class="content-heading">Mini Ledger Non SAP</h2>

                    <!-- Dynamic Table Full -->
                    <div class="block">
                        {{-- <div class="block-header block-header-default">
                            <h3 class="block-title">Dynamic Table <small></small></h3>
                        </div> --}}
                        <div class="block-content block-content-full">
                            <!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/be_tables_datatables.js -->
                              <form class="" action="{{ route('miniLedgerNonSapReport') }}" method="post" id="bindLedgerSap">
                                @csrf
                                <div class="form-group row">
                                  <div class="col-md-3">
                                    <label for="subDivisionCode">Select Month <span class="text-danger">*</span> </label>
                                    <select class="form-control" id="table" name="table" onchange="getdivisions(this)">
                                      <option value="">Select</option>
                                      @php
                                        $tables = DB::SELECT("SELECT table_name FROM information_schema.tables WHERE table_name like 'billeds%'");
                                      @endphp
                                      @foreach ($tables as $table)
                                        @if (isset($data) && count($data['table']) > 0)
                                          <option value="{{ $table->table_name }}" @if ($data['table'] == $table->table_name)
                                            selected
                                          @endif>{{ $table->table_name }}</option>
                                        @else
                                          <option value="{{ $table->table_name }}">{{ $table->table_name }}</option>
                                        @endif
                                      @endforeach
                                    </select>
                                  </div>
                                      <div class="col-md-3">
                                        <label for="subDivisionCode">Sub Division Code <span class="text-danger">*</span> </label>
                                          <select class="form-control" id="subDivisionCode" name="subDivisionCode" onchange="getgroup(this)">
                                            {{-- Sub Division Code's --}}
                                            <option value="">Select</option>
                                            @php
                                              // $sub = sapInput::groupBy('subDivisionCode');
                                              // $sub = sapInput::select('subDivisionCode')->groupBy('subDivisionCode')->take(20)->get();

                                              // $subDivision = '3213';
                                              // $billingsGroups = DB::select("select distinct billingGroup from sap_inputs WHERE subDivisionCode in ('$subDivision') group by billingGroup");
                                              // $billingsCycles = DB::select("select distinct billingCycle from sap_inputs WHERE subDivisionCode in ('$subDivision') group by billingCycle");
                                              // $ledgerCodes = DB::select("SELECT distinct MRU FROM sap_inputs group by MRU");
                                              // dd($sub);
                                            @endphp
                                              @if (isset($data) && count($data['sub']) > 0)
                                                @php
                                                $table = $data['table'];
                                                $sub = DB::select("SELECT distinct blSubDivisionCode subDivisionCode FROM $table group by blSubDivisionCode");
                                                @endphp
                                                @foreach ($sub as $subDivisionCode)
                                                <option value="{{ $subDivisionCode->subDivisionCode }}" @if ($subDivisionCode->subDivisionCode == $data['sub']) selected @endif>{{ $subDivisionCode->subDivisionCode }}</option>
                                                @endforeach
                                            @endif
                                          </select>
                                          {{-- <input type="text" class="form-control" id="subDivisionCode" name="subDivisionCode" placeholder="Sub Division Code" required> --}}
                                          {{-- <div class="form-text text-muted">Further info about this input</div> --}}
                                          @if ($errors->has('subDivisionCode'))
                                       <span class="help-block">
                                       <strong class="text-danger">{{ $errors->first('subDivisionCode') }}</strong>
                                        </span>
                                          @endif
                                      </div>
                                      <div class="col-md-3">
                                        <label for="billingGroup">Billing Group <span class="text-danger">*</span> </label>
                                        <select class="form-control" name="billingGroup" id="billingGroup" required onchange="getCycle(this)">
                                          <option value="">Select</option>
                                          @if (isset($data) && count($data['sub']) > 0)
                                            @php
                                            $table = $data['table'];
                                              $sub = $data['sub'];
                                              $bg = $data['bg'];
                                              $bc = $data['bc'];
                                              $meterReaders = DB::select("select distinct blBillingGroup billingGroup from $table where blSubDivisionCode = '$sub'");
                                            @endphp
                                            @foreach ($meterReaders as $meterReader)
                                              <option value="{{ $meterReader->billingGroup }}" @if ($meterReader->billingGroup == $data['bg']) selected @endif>{{ $meterReader->billingGroup }}</option>
                                            @endforeach
                                          @endif
                                        </select>
                                          {{-- <div class="form-text text-muted">Further info about this input</div> --}}
                                          @if ($errors->has('billingGroup'))
                                       <span class="help-block">
                                       <strong class="text-danger">{{ $errors->first('billingGroup') }}</strong>
                                        </span>
                                          @endif
                                      </div>
                                      <div class="col-md-2">
                                        <label for="billingCycle">Billing Cycle <span class="text-danger">*</span></label>
                                        <select class="form-control" name="billingCycle" id="billingCycle" required onchange="getmeterReaders(this)">
                                          <option value="">Select</option>
                                          @if (isset($data) && count($data['sub']) > 0)
                                            @php
                                              $table = $data['table'];
                                              $sub = $data['sub'];
                                              $bg = $data['bg'];
                                              $bc = $data['bc'];
                                              // $reader = $data['reader'];
                                              $meterReaders = DB::select("select distinct blBillingCycle billingCycle from $table where blSubDivisionCode = '$sub' and blBillingGroup = '$bg'");
                                            @endphp
                                            @foreach ($meterReaders as $meterReader)
                                              <option value="{{ $meterReader->billingCycle }}" @if ($meterReader->billingCycle == $data['bc']) selected @endif>{{ $meterReader->billingCycle }}</option>
                                            @endforeach
                                          @endif
                                        </select>
                                          {{-- <div class="form-text text-muted">Further info about this input</div> --}}
                                          @if ($errors->has('billingCycle'))
                                       <span class="help-block">
                                       <strong class="text-danger">{{ $errors->first('billingCycle') }}</strong>
                                        </span>
                                          @endif
                                      </div>
                                      {{-- <div class="col-md-3">
                                        <label for="meterReader">Meter Reader <span class="text-danger">*</span></label>
                                        <select class="form-control" name="meterReader" required id="meterReaders">
                                          <option value="">Select</option>
                                          @if (isset($data) && count($data['sub']) > 0)
                                            @php
                                              $sub = $data['sub'];
                                              $bg = $data['bg'];
                                              $bc = $data['bc'];
                                              $from = $data['from'];
                                              $to = $data['to'];
                                              $meterReaders = DB::select("select distinct meterReader from billeds where blSubDivisionCode = '$sub' and blBillingGroup = '$bg' and blBillingCycle = '$bc' and (blPresentReadingDate between '$from' and '$to')");
                                            @endphp
                                            @foreach ($meterReaders as $meterReader)
                                              <option value="{{ $meterReader->meterReader }}" @if ($meterReader->meterReader == $data['reader']) selected @endif>{{ $meterReader->meterReader }}</option>
                                            @endforeach
                                          @endif
                                        </select>
                                        @if ($errors->has('meterReader'))
                                     <span class="help-block">
                                     <strong class="text-danger">{{ $errors->first('meterReader') }}</strong>
                                      </span>
                                        @endif
                                      </div> --}}
                                      <div class="col-md-1">
                                        <label for="accountNoL">. </label>
                                          <button type="submit" class="btn btn-alt-primary">Submit</button>
                                      </div>


                                  </div>

                              </form>

                            @if (isset($data) && $data['data'] != '')
                              <table class="table-bordered js-dataTable-full">
                                  <thead>
                                      <tr>
                                          <td>S.No</td>
                                          <td>MasterKey</td>
                                          <td>Name</td>
                                          <td>address</td>
                                          <td>mobile</td>
                                          <td>email</td>
                                          <td>Meter No</td>
                                          <td>Previous Code</td>
                                          <td>Previous Reading</td>
                                          <td>Present Reading</td>
                                          <td>Present Status</td>
                                          <td>Present Reading Date</td>
                                          <td>Units Consumed</td>
                                          <td>Days</td>
                                          <td>Current SOP</td>
                                          <td>Current ED</td>
                                          <td>Current OCTRAI</td>
                                          <td>Current IDF</td>
                                          <td>Current Cow Cess</td>
                                          <td>Cur water Sewage Crg</td>
                                          <td>Net SOP</td>
                                          <td>Net ED</td>
                                          <td>Net Octroi</td>
                                          <td>Current Arrears</td>
                                          <td>Due Date Cash</td>
                                          <td>Due Date Cheque</td>
                                          <td>Amount After Due</td>
                                          <td>Surcharge</td>
                                          <td>Amount Before Due</td>
                                          <td>Meter Reader ID</td>
                                          <td>Meter Reader</td>
                                          <td>Bill Date</td>
                                          <td>Total IDF</td>
                                          <td>Total Cow Cess</td>
                                          <td>Tot Water Sewarage Charges</td>
                                      </tr>
                                  </thead>
                                  <tbody>

                                      @php
                                      $report = $data['data'];
                                        $sno = 1;
                                      @endphp
                                      @foreach ($report as $record)
                                        <tr>
                                          @php
                                          $table = str_replace('billeds', 'bills', $data['table']);
                                          // dd($table);
                                            $bill = DB::select("SELECT * FROM $table WHERE masterKey = '$record->blMasterKey'");
                                          @endphp
                                            <td class="">{{ $sno++ }}</td>
                                            <td class="">{{ $record->blMasterKey }}</td>
                                            <td class="">{{ $record->blConsumerName }}</td>
                                            <td class="">{{ $bill[0]->consumerAddress }}</td>
                                            <td class="">{{ $record->blMobile }}</td>
                                            <td class="">{{ $record->blMobile }}</td>
                                            <td class="">{{ $bill[0]->meterNo }}</td>
                                            <td class="">{{ $bill[0]->perviousCode }}</td> {{-- Verify --}}
                                            <td class="">{{ $bill[0]->perviousReading }}</td> {{-- Verify --}}
                                            <td class="">{{ $record->blPresentReading }}</td>
                                            <td class="">{{ $record->blPresentStatus }}</td>
                                            <td class="">{{ $record->blPresentReadingDate }}</td>
                                            <td class="">{{ $record->blUnitsConsumedNew }}</td>
                                            <td class="">{{ $record->blUnitsConsumedNew }}</td>
                                            <td class="">{{ $record->blCurrentSop }}</td>
                                            <td class="">{{ $record->blCurrentEd }}</td>
                                            <td class="">{{ $record->blCurrentOctroi }}</td>
                                            <td class="">{{ $record->blCurrentIDFWithSign }}</td> {{-- Verify --}}
                                            <td class="">{{ $record->blCurrentCowCessWithSign }}</td>
                                            <td class="">{{ $record->blCurrentWaterSewageChargeWithSign }}</td>
                                            <td class="">{{ $record->blNetSop }}</td>
                                            <td class="">{{ $record->blNetEd }}</td>
                                            <td class="">{{ $record->blNetOctroi }}</td>
                                            <td class=""></td> {{-- Verify --}}
                                            <td class="">{{ $record->blDueDateByCash }}</td>
                                            <td class="">{{ $record->blDueDateByCheque }}</td>
                                            <td class="">{{ $record->blAmountAfterDue }}</td>
                                            <td class="">{{ $record->blSurCharge }}</td>
                                            <td class="">{{ $record->blAmountBeforeDue }}</td>
                                            <td class="">{{ $record->meterReader }}</td>
                                            <td class="">{{ $record->meterReader }}</td>
                                            <td class="">{{ $record->blPresentReadingDate }}</td>
                                            <td class="">{{ $record->bltotalIDFWithSign }}</td>
                                            <td class="">{{ $record->blTotalCowCessWithSign }}</td>
                                            <td class="">{{ $record->blTotalCurrentWaterSewarageChargeWithSign }}</td>
                                        </tr>

                                      @endforeach

                                  </tbody>
                              </table>
                            @endif
                        </div>
                    </div>
                    <!-- END Dynamic Table Full -->
                </div>
                <!-- END Page Content -->
            </main>
            <!-- END Main Container -->

            <!-- Footer -->
            @include('includes/footer')
            <!-- END Footer -->
        </div>
        <!-- END Page Container -->

        <!-- Codebase Core JS -->
        <script src="{{ config('app.url') }}/assets/js/core/jquery.min.js"></script>
        <script src="{{ config('app.url') }}/assets/js/core/bootstrap.bundle.min.js"></script>
        <script src="{{ config('app.url') }}/assets/js/core/jquery.slimscroll.min.js"></script>
        <script src="{{ config('app.url') }}/assets/js/core/jquery.scrollLock.min.js"></script>
        <script src="{{ config('app.url') }}/assets/js/core/jquery.appear.min.js"></script>
        <script src="{{ config('app.url') }}/assets/js/core/jquery.countTo.min.js"></script>
        <script src="{{ config('app.url') }}/assets/js/core/js.cookie.min.js"></script>
        <script src="{{ config('app.url') }}/assets/js/codebase.js"></script>

        <!-- Page JS Plugins -->
        {{-- <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script> --}}
        <script src="{{ config('app.url') }}/assets/js/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="{{ config('app.url') }}/assets/js/plugins/datatables/dataTables.bootstrap4.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>

        <!-- Page JS Code -->
        <script src="{{ config('app.url') }}/assets/js/pages/be_tables_datatables.js"></script>
        <script src="{{ config('app.url') }}/assets/js/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
        {{-- <script src="{{ config('app.url') }}/assets/js/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script> --}}
        {{-- <script src="{{ config('app.url') }}/assets/js/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js"></script> --}}
        {{-- <script src="{{ config('app.url') }}/assets/js/plugins/select2/select2.full.min.js"></script> --}}
        <script src="{{ config('app.url') }}/assets/js/plugins/jquery-tags-input/jquery.tagsinput.min.js"></script>
        <script src="{{ config('app.url') }}/assets/js/plugins/jquery-auto-complete/jquery.auto-complete.min.js"></script>
        {{-- <script src="{{ config('app.url') }}/assets/js/plugins/masked-inputs/jquery.maskedinput.min.js"></script> --}}
        {{-- <script src="{{ config('app.url') }}/assets/js/plugins/ion-rangeslider/js/ion.rangeSlider.min.js"></script> --}}
        {{-- <script src="{{ config('app.url') }}/assets/js/plugins/dropzonejs/min/dropzone.min.js"></script> --}}

        <!-- Page JS Code -->
        <script src="{{ config('app.url') }}/assets/js/pages/be_forms_plugins.js"></script>

        <script>
            jQuery(function() {
                // Init page helpers (BS Datepicker + BS Colorpicker + BS Maxlength + Select2 + Masked Input + Range Sliders + Tags Inputs plugins)
                Codebase.helpers(['datepicker']);
            });
        </script>

        {{-- <script type="text/javascript">
        function getGroupCycleAjaxNonSap(e) {

          $('#billingGroup')
              .empty()
              .append('<option value="">Select</option>')
          ;
          $('#billingCycle')
              .empty()
              .append('<option value="">Select</option>')
          ;
          $('#ledgerCode')
              .empty()
              .append('<option value="">Select</option>')
          ;

          // console.log(e.value);
          var subDivisionCode = e.value;
          $.ajax({
            headers: {
               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
           },
          type: "POST",
          url: '/api/getGroupCycleAjaxNonSap',
          data: {subDivisionCode: subDivisionCode},
            success: function( data ) {
                console.log(data);
                $.each(data.billingGroup, function(key, value) {
                     $('#billingGroup')
                         .append($("<option></option>")
                                    .attr("value",value.billingGroup)
                                    .text(value.billingGroup));
                });
                // $.each(data.billingCycle, function(key, value) {
                //   // console.log(value.billingCycle);
                //      $('#billingCycle')
                //          .append($("<option></option>")
                //                     .attr("value",value.billingCycle)
                //                     .text(value.billingCycle));
                // });
                // $.each(data.ledgerCodes, function(key, value) {
                //   // console.log(value);
                //      $('#ledgerCode')
                //          .append($("<option></option>")
                //                     .attr("value",value.ledgerCode)
                //                     .text(value.ledgerCode));
                // });
                // $("#ajaxResponse").append("<div>"+msg+"</div>");
            }
          });
        }

        function getGroupCycleAjaxNonSapBc(e) {

          // console.log(e.value);
          $('#billingCycle')
              .empty()
              .append('<option value="">Select</option>')
          ;
          $('#ledgerCode')
              .empty()
              .append('<option value="">Select</option>')
          ;

          // console.log(e.value);
          var subDivisionCode = $('#subDivisionCode').val();
          var billingGroup = e.value;
          $.ajax({
            headers: {
               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
           },
          type: "POST",
          url: '/api/getGroupCycleAjaxNonSapBc',
          data: {subDivisionCode: subDivisionCode, billingGroup: billingGroup},
            success: function( data ) {
                console.log(data);
                $.each(data.billingCycle, function(key, value) {
                  // console.log(value.billingCycle);
                     $('#billingCycle')
                         .append($("<option></option>")
                                    .attr("value",value.billingCycle)
                                    .text(value.billingCycle));
                });
                // $("#ajaxResponse").append("<div>"+msg+"</div>");
            }
          });
        }

          function getmeterReaders(e) {

            $('#meterReaders')
                .empty()
                .append('<option value="">Select</option>')
            ;

            // console.log(e.value);
            var subDivisionCode = $('#subDivisionCode').val();
            var billingGroup = $('#billingGroup').val();
            var billingCycle = $('#billingCycle').val();
            var from = $('#from').val();
            var to = $('#to').val();
            console.log(subDivisionCode);
            console.log(from);
            console.log(to);
            $.ajax({
              headers: {
                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
             },
            type: "POST",
            url: '/api/get-meter-reader-sub-bg-bc-non-sap',
            data: {subDivisionCode: subDivisionCode, billingGroup: billingGroup, billingCycle: billingCycle, from: from, to: to},
              success: function( data ) {
                  console.log(data);
                  $.each(data.meterReaders, function(key, value) {
                       $('#meterReaders')
                           .append($("<option></option>")
                                      .attr("value",value.meterReader)
                                      .text(value.meterReader));
                  });
              }
            });
          }



        </script> --}}
        <script type="text/javascript">
          function getdivisions(e) {

            $('#subDivisionCode')
                .empty()
                .append('<option value="">Select</option>')
            ;
            $('#billingGroup')
                .empty()
                .append('<option value="">Select</option>')
            ;
            $('#billingCycle')
                .empty()
                .append('<option value="">Select</option>')
            ;

            var table = e.value;
            $.ajax({
              headers: {
                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
             },
            type: "POST",
            url: '/api/getNonSapDivisinTable',
            data: {table: table},
              success: function( data ) {
                  // console.log(data);
                  $.each(data.division, function(key, value) {
                    // console.log(value.ledgerCode);
                       $('#subDivisionCode')
                           .append($("<option></option>")
                                      .attr("value",value.division)
                                      .text(value.division));
                  });
                  // $("#ajaxResponse").append("<div>"+msg+"</div>");
              }
            });
          }

          function getgroup(e) {
            $('#billingGroup')
                .empty()
                .append('<option value="">Select</option>')
            ;
            $('#billingCycle')
                .empty()
                .append('<option value="">Select</option>')
            ;
            var division = e.value;
            var table = $('#table').val();
            $.ajax({
              headers: {
                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
             },
            type: "POST",
            url: '/api/getNonSapGroupTable',
            data: {table: table, division: division},
              success: function( data ) {
                  // console.log(data);
                  $.each(data.group, function(key, value) {
                    // console.log(value.ledgerCode);
                       $('#billingGroup')
                           .append($("<option></option>")
                                      .attr("value",value.billingGroup)
                                      .text(value.billingGroup));
                  });
                  // $("#ajaxResponse").append("<div>"+msg+"</div>");
              }
            });
          }
          function getCycle(e) {
            $('#billingCycle')
                .empty()
                .append('<option value="">Select</option>')
            ;
            var division = $('#subDivisionCode').val();
            var group = e.value;
            var table = $('#table').val();
            $.ajax({
              headers: {
                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
             },
            type: "POST",
            url: '/api/getNonSapCycleTable',
            data: {table: table, division: division, group: group},
              success: function( data ) {
                  // console.log(data);
                  $.each(data.cycle, function(key, value) {
                    // console.log(value.ledgerCode);
                       $('#billingCycle')
                           .append($("<option></option>")
                                      .attr("value",value.billingCycle)
                                      .text(value.billingCycle));
                  });
                  // $("#ajaxResponse").append("<div>"+msg+"</div>");
              }
            });
          }
        </script>
    </body>
</html>
