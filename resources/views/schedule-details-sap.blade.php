<!doctype html>
<!--[if lte IE 9]>     <html lang="en" class="no-focus lt-ie10 lt-ie10-msg"> <![endif]-->
<!--[if gt IE 9]><!--> <html lang="en" class="no-focus"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">

        <title>Schedule Details SAP| STERLING Transformers</title>

        <meta name="description" content="Codebase - Bootstrap 4 Admin Template &amp; UI Framework created by pixelcave and published on Themeforest">
        <meta name="author" content="pixelcave">
        <meta name="robots" content="noindex, nofollow">
        <meta name="csrf-token" content="{{ csrf_token() }}" />

        <!-- Open Graph Meta -->
        <meta property="og:title" content="Codebase - Bootstrap 4 Admin Template &amp; UI Framework">
        <meta property="og:site_name" content="Codebase">
        <meta property="og:description" content="Codebase - Bootstrap 4 Admin Template &amp; UI Framework created by pixelcave and published on Themeforest">
        <meta property="og:type" content="website">
        <meta property="og:url" content="">
        <meta property="og:image" content="">

        <!-- Icons -->
        <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
        <link rel="shortcut icon" href="{{ config('app.url') }}/assets/img/favicons/favicon.png">
        <link rel="icon" type="image/png" sizes="192x192" href="{{ config('app.url') }}/assets/img/favicons/favicon-192x192.png">
        <link rel="apple-touch-icon" sizes="180x180" href="{{ config('app.url') }}/assets/img/favicons/apple-touch-icon-180x180.png">
        <!-- END Icons -->

        <!-- Stylesheets -->
        <!-- Page JS Plugins CSS -->
        <link rel="stylesheet" href="{{ config('app.url') }}/assets/js/plugins/datatables/dataTables.bootstrap4.min.css">

        <!-- Codebase framework -->
        <link rel="stylesheet" id="css-main" href="{{ config('app.url') }}/assets/css/codebase.css">

        <!-- You can include a specific file from css/themes/ folder to alter the default color theme of the template. eg: -->
        <!-- <link rel="stylesheet" id="css-theme" href="assets/css/themes/flat.min.css"> -->
        <!-- END Stylesheets -->
    </head>
    <body>
        <!-- Page Container -->
        <!--
            Available classes for #page-container:

        GENERIC

            'enable-cookies'                            Remembers active color theme between pages (when set through color theme helper Codebase() -> uiHandleTheme())

        SIDEBAR & SIDE OVERLAY

            'sidebar-r'                                 Right Sidebar and left Side Overlay (default is left Sidebar and right Side Overlay)
            'sidebar-mini'                              Mini hoverable Sidebar (screen width > 991px)
            'sidebar-o'                                 Visible Sidebar by default (screen width > 991px)
            'sidebar-o-xs'                              Visible Sidebar by default (screen width < 992px)
            'sidebar-inverse'                           Dark themed sidebar

            'side-overlay-hover'                        Hoverable Side Overlay (screen width > 991px)
            'side-overlay-o'                            Visible Side Overlay by default

            'side-scroll'                               Enables custom scrolling on Sidebar and Side Overlay instead of native scrolling (screen width > 991px)

        HEADER

            ''                                          Static Header if no class is added
            'page-header-fixed'                         Fixed Header

        HEADER STYLE

            ''                                          Classic Header style if no class is added
            'page-header-modern'                        Modern Header style
            'page-header-inverse'                       Dark themed Header (works only with classic Header style)
            'page-header-glass'                         Light themed Header with transparency by default
                                                        (absolute position, perfect for light images underneath - solid light background on scroll if the Header is also set as fixed)
            'page-header-glass page-header-inverse'     Dark themed Header with transparency by default
                                                        (absolute position, perfect for dark images underneath - solid dark background on scroll if the Header is also set as fixed)

        MAIN CONTENT LAYOUT

            ''                                          Full width Main Content if no class is added
            'main-content-boxed'                        Full width Main Content with a specific maximum width (screen width > 1200px)
            'main-content-narrow'                       Full width Main Content with a percentage width (screen width > 1200px)
        -->
        <div id="page-container" class="sidebar-o side-scroll page-header-modern main-content-boxed">
            <!-- Side Overlay-->
            @include('includes/headerAndSidebar')
            <!-- END Header -->

            <!-- Main Container -->
            <main id="main-container">
                <!-- Page Content -->
                <div class="content">
                    <h2 class="content-heading">Schedule Details ( SAP )</h2>

                    <!-- Dynamic Table Full -->
                    <div class="block">
                        {{-- <div class="block-header block-header-default">
                            <h3 class="block-title">Dynamic Table <small></small></h3>
                        </div> --}}
                        <div class="block-content block-content-full">
                            <!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/be_tables_datatables.js -->
                            <table class="table table-bordered table-striped js-dataTable-full">
                                <thead>
                                    <tr>
                                        <th>S.No</th>
                                        <th>sub</th>
                                        <th>bg</th>
                                        <th>bc</th>
                                        <th>ledgerCode</th>
                                        <th>Tot Cons</th>
                                        <th>Reader</th>
                                        <th>Dwn Sts</th>
                                        <th>Date</th>
                                        <th>Time</th>
                                        <th>Scheduled By</th>
                                        <th>View</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    @php
                                    $schedules = DB::select("SELECT * FROM SAP_SBA_schedules");
                                      $sno = 1;
                                    @endphp
                                    @foreach ($schedules as $sba)
                                      <tr>
                                          <td class="" style="padding: 0px;">{{ $sno++ }}</td>
                                          <td class="" style="padding: 0px;">{{ $sba->subDivisionCode }}</td>
                                          <td class="" style="padding: 0px;">{{ $sba->billingGroup }}</td>
                                          <td class="font-w600" style="padding: 0px;">{{ $sba->billingCycle }}</td>
                                          <td class="font-w600" style="padding: 0px;">{{ $sba->LedgerCode }}</td>
                                          <td class="font-w600" style="padding: 0px;">{{ $sba->TotalCons }}</td>
                                          <td class="font-w600" style="padding: 0px;">{{ $sba->meterReader }}</td>
                                          <td class="font-w600" style="padding: 0px;">{{ $sba->DataDownloadFlag }}</td>
                                          <td class="font-w600" style="padding: 0px;">{{ $sba->DownloadedDate }}</td>
                                          <td class="font-w600" style="padding: 0px;">{{ $sba->DownloadedTime }}</td>
                                          <td class="font-w600" style="padding: 0px;">{{ $sba->ScheduledUser }}</td>
                                          <td class="font-w600" style="padding: 0px;"><a href="{{ config('app.url') }}/schedule-details-sap-info/{{ $sba->meterReader }}/{{ $sba->LedgerCode }}">View</a></td>
                                          <td class="font-w600" style="padding: 0px;">
                                            &nbsp;&nbsp;&nbsp;
                                            <i class="si si-refresh" style="cursor: pointer" onclick="scheduleReset({{ $sba->id }})"></i>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <i class="fa fa-recycle" style="cursor: pointer" onclick="scheduleDelete({{ $sba->id }})"></i>
                                            {{-- <span class="text-warning" style="cursor: pointer" onclick="scheduleReset({{ $sba->id }})">Reset</span> --}}
                                            {{-- <form class="" action="" method="post" > --}}
                                              {{-- <button type="button" name="reset" class="btn btn-danger btn-sm" value="{{ $sba->id }}" onclick="scheduleReset(this)">Reset</button> --}}
                                            {{-- </form> --}}
                                          </td>
                                      </tr>
                                    @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- END Dynamic Table Full -->
                </div>
                <!-- END Page Content -->
            </main>
            <!-- END Main Container -->

            <!-- Footer -->
            @include('includes/footer')
            <!-- END Footer -->
        </div>
        <!-- END Page Container -->

        <!-- Codebase Core JS -->
        <script src="{{ config('app.url') }}/assets/js/core/jquery.min.js"></script>
        <script src="{{ config('app.url') }}/assets/js/core/bootstrap.bundle.min.js"></script>
        <script src="{{ config('app.url') }}/assets/js/core/jquery.slimscroll.min.js"></script>
        <script src="{{ config('app.url') }}/assets/js/core/jquery.scrollLock.min.js"></script>
        <script src="{{ config('app.url') }}/assets/js/core/jquery.appear.min.js"></script>
        <script src="{{ config('app.url') }}/assets/js/core/jquery.countTo.min.js"></script>
        <script src="{{ config('app.url') }}/assets/js/core/js.cookie.min.js"></script>
        <script src="{{ config('app.url') }}/assets/js/codebase.js"></script>

        <!-- Page JS Plugins -->
        <script src="{{ config('app.url') }}/assets/js/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="{{ config('app.url') }}/assets/js/plugins/datatables/dataTables.bootstrap4.min.js"></script>

        <!-- Page JS Code -->
        <script src="{{ config('app.url') }}/assets/js/pages/be_tables_datatables.js"></script>
        <script>
          $(document).ready(function(){
              $('[data-toggle="tooltip"]').tooltip();
          });
        </script>
        <script type="text/javascript">
          function scheduleReset(e) {
            var txt;
                var r = confirm("You sure you wanna reset the data!");
                if (r == true) {
                    txt = "You pressed OK!";
                    var id = e;
                    $.ajax({
                      headers: {
                         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                     },
                    type: "POST",
                    url: '/api/schedule-reset-sap-count',
                    data: {id: id},
                      success: function( data ) {
                        // console.log(data);
                        if (data > 0) {
                          // var txt;
                          var press = confirm(data + " receords found!");
                          if (press == true) {
                            $.ajax({
                              headers: {
                                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                             },
                            type: "POST",
                            url: '/api/schedule-reset-sap',
                            data: {id: id},
                              success: function( data ) {
                                  alert('Schedule Reset Success');
                              }
                            });
                          } else {
                              txt = "You pressed Cancel!";
                          }
                        } else {
                          alert('no records to reset')
                        }
                      }
                    });
                    // console.log(e.value);
                } else {
                    txt = "You pressed Cancel!";
                }
                // document.getElementById("demo").innerHTML = txt;
          }
        </script>
        <script type="text/javascript">
          function scheduleDelete(e) {
            var txt;
                var r = confirm("You sure you wanna remove data!");
                if (r == true) {
                    txt = "You pressed OK!";
                    var id = e;
                    $.ajax({
                      headers: {
                         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                     },
                    type: "POST",
                    url: '/api/schedule-reset-sap-count',
                    data: {id: id},
                      success: function( data ) {
                        // console.log(data);
                        if (data > 0) {
                          // var txt;
                          var press = confirm(data + " receords found!");
                          if (press == true) {
                            $.ajax({
                              headers: {
                                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                             },
                            type: "POST",
                            url: '/api/schedule-remove-sap',
                            data: {id: id},
                              success: function( data ) {
                                  alert('Schedule Removed Success');
                              }
                            });
                          } else {
                              txt = "You pressed Cancel!";
                          }
                        } else {
                          alert('no records to remove')
                        }
                      }
                    });
                    // console.log(e.value);
                } else {
                    txt = "You pressed Cancel!";
                }
                // document.getElementById("demo").innerHTML = txt;
          }
        </script>
    </body>
</html>
