@php
  use App\sapInput;
@endphp
@php
    echo "<?xml version='1.0' ?>";
    echo "<markers>";
@endphp


@foreach ($result as $marker)
  @php
    $userDetails = sapInput::where('mr_docnumber', $marker->MR_DOCNumber)->get();
  @endphp
  @php
  $accountNumber = $userDetails[0]->contractAcNumber;
  $cunsumerName = $userDetails[0]->consumerName;
  $address = $userDetails[0]->address;
  $meterNo = $userDetails[0]->meterMnfSerialNumber;
  $bg = $userDetails[0]->billingGroup;
  $bc = $userDetails[0]->billingCycle;
    // echo "<marker id='$marker->id'/>";
    echo "<marker id='$marker->id' name='$accountNumber' address='$cunsumerName' meterno='$meterNo' bg='$bg' bc='$bc' lat='$marker->blLat' lng='$marker->blLong' type='R'/>";
  @endphp

@endforeach
@php
    echo "</markers>";
@endphp
