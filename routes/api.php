<?php
ini_set('soap.wsdl_cache_enabled',0);
ini_set('soap.wsdl_cache_ttl',0);
// use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\User;
use App\Bill;
use App\Billed;
use App\sapInput;
use App\sapOutput;
use App\resetRequest;
use App\sapVersion;
use App\nonSapVersion;
use App\userLog;
use App\changeOfTariff;
use App\CorrectionOfMeterParticular;
use App\SundryCharge;

use Artisaninweb\SoapWrapper\SoapWrapper;
use App\Soap\Request\GetConversionAmount;
use App\Soap\Response\GetConversionAmountResponse;
use Illuminate\Support\Facades\Storage;

// use Carbon;
// use \Hash;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
function clean($string) {
   $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.

   return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
}

function vle($value = '', $count)
{
  $countMain = $count;
  $count = $count - 1;
  if ($value > 0) {
    $abs = abs($value);
    $abs = sprintf("%.2f", $abs);
    $name = '+'.addZeros($abs, $count);
  } elseif ($value == 0) {
    $abs = 0;
    $abs = sprintf("%.2f", $abs);
    $name = '+'.addZeros($abs, $count);
  } else {
    $abs = abs($value);
    $abs = sprintf("%.2f", $abs);
    // $abs = number_format($abs, 2);
    // return $abs = number_format($abs, 2);
    $name = '-'.addZeros($abs, $count);
    // $blNetSop = $abs;
  }
  return $name;
}

function vlen($value = '', $count)
{
  $countMain = $count;
  $count = $count - 1;
  if ($value > 0) {
    $name = '+'.addZeros((int) $value, $count);
  } elseif ($value == 0) {
    $name = '+'.addZeros('', $count);
  } else {
    $abs = abs((int) $value);
    // $abs = number_format($abs, 2);
    $name = '-'.addZeros($abs, $count);
    // $blNetSop = $abs;
  }
  return $name;
}

function addZerosSign($value='', $length)
{
  $count = strlen($value);
  if ($value >= 1 || $value == 0 || $value == 0.00) {
    $sign = "+";
  } else {
    $sign = "-";
  }
  $numberOfZeros = $length - $count;
  $zeros = '';
  for ($i=1; $i < $numberOfZeros; $i++) {
    $zeros .= 0;
  }

  $newValue = $sign.$zeros.$value;
  return $newValue;
}


function addZerosSignN($value='', $length)
{
  $count = strlen($value);
  if ($value >= 1 || $value == 0 || $value == 0.00) {
    $sign = "+";
  } else {
    $sign = "-";
  }
  $numberOfZeros = $length - $count;
  $zeros = '';
  for ($i=0; $i < $numberOfZeros; $i++) {
    $zeros .= 0;
  }

  $newValue = $sign.$zeros.$value;
  return $newValue;
}

function addZeros($value='', $length)
{
  $count = strlen($value);
  $numberOfZeros = $length - $count;
  $zeros = '';
  for ($i=0; $i < $numberOfZeros; $i++) {
    $zeros .= 0;
  }

  $newValue = $zeros.$value;
  return $newValue;
}

function addZerosBack($value='', $length)
{
  $count = strlen($value);
  $numberOfZeros = $length - $count;
  $zeros = '';
  for ($i=0; $i < $numberOfZeros; $i++) {
    $zeros .= 0;
  }

  $newValue = $value.$zeros;
  return $newValue;
}

function addSpace($value='', $length)
{

  $count = strlen(trim($value));
  $numberOfZeros = $length - $count;
  $zeros = '';
  for ($i=0; $i < $numberOfZeros; $i++) {
    $zeros .= ' ';
  }

  $newValue = $value.$zeros;
  $newValue = substr($newValue, 0, $length);
  return $newValue;
}

function addZerosFloat($value = '', $count)
{
  $countMain = $count;
  $count = $count - 2;
  if ($value > 0) {
    $abs = sprintf("%.2f", $value);
    $name = addZeros($abs, $countMain);
    // $name = addZeros($value, $count);
  } elseif ($value == 0) {
    $abs = 0;
    $abs = sprintf("%.2f", $abs);
    $name = addZeros($abs, $countMain);
  } else {
    $abs = abs($value);
    $abs = sprintf("%.2f", $abs);
    // $abs = number_format($abs, 2);
    // return $abs = number_format($abs, 2);
    $name = addZeros($abs, $countMain);
    // $blNetSop = $abs;
  }
  return $name;
}

function addSpaceFront($value='', $length)
{
  $count = strlen($value);
  $numberOfZeros = $length - $count;
  $zeros = '';
  for ($i=0; $i < $numberOfZeros; $i++) {
    $zeros .= ' ';
  }

  $newValue = $zeros.$value;
  return $newValue;
}

Route::get('/login', function (Request $request) {
  return $request->user();
});

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware('auth:api')->get('/testr', function (Request $request) {
    return 'testr';
});


Route::middleware('auth:api')->get('/datanew', function (Request $request) {
  // return $request->user();
  $userDetails = $request->user();
  // return $userDetails->id;

    $bills = DB::select("select * from bills where find_in_set('$userDetails->email', meterReader) and bill_status = 'N'");


                // ->where('status', 1)->get();
  $received_at = Date('Y-m-d H:i:s');
  // $billsUpdate = Bill::where('meterReader', $userDetails->id)->update(['status' => 2]);
  // return $bills;
  if (count($bills) > 1) {
    $bill = $bills;
  } else {
    $bill = [];
  }
    return $bill;
});

Route::middleware('auth:api')->get('/sapdatanew', function (Request $request) {
  $userDetails = $request->user();

  $bills = DB::select("select * from sap_inputs where find_in_set('$userDetails->email', meterReader) and bill_status = 'N'");

  if (count($bills) >= 1) {
    $bill = $bills;
  } else {
    $bill = [];
  }
    return $bill;
});

Route::middleware('auth:api')->get('/sapdata', function (Request $request) {

  $userDetails = $request->user();
  $bill = [];
  $userId = $userDetails['email'];
  // return $userId['email'];
  // $schedules = DB::select("SELECT count(*) FROM sap_sba_schedule_info WHERE downloadflag = 'N' AND meterreader = '$userDetails->email'");
    $sno = 1;
    // return 'lll';
    // foreach ($schedules as $sc) {
        // $bills = DB::select("select * from bills where subDivisionCode = '$sc->subDivisionCode' and billingGroup = '$sc->billingGroup' and billingCycle = '$sc->billingCycle' and SUBSTRING(masterKey, 4, 4) = '$sc->LedgerCode' and bill_status = 'N' AND Billing_closed_flag = 'N'");
        // $bills = DB::select("select * from sap_inputs where id = $sc->mainid and subDivisionCode = '$sc->subdivcode' and billingGroup = '$sc->bg' and billingCycle = '$sc->cy' and MRU = '$sc->ledgercode' and bill_status = 'N'");
        $bills = DB::select("select * from sap_inputs right join sap_sba_schedule_info on sap_sba_schedule_info.mainid = sap_inputs.id and sap_sba_schedule_info.meterReader = '$userId' and sap_sba_schedule_info.downloadflag = 'N'
                              where sap_inputs.bill_status ='N'");
        // return $bills;
        // if (!empty($bills)) {
        //   $newAr = new ArrayIterator($bills);
        //   array_push($bill, ...$newAr);
        // }
    // }
  if (count($bills)) {
    $bill = $bills;
  } else {
    $bill = [];
  }
  // return count($bill);
    return $bill;
});

Route::middleware('auth:api')->get('/data', function (Request $request) {

  $userDetails = $request->user();
  $bill = [];
  $schedules = DB::select("SELECT * FROM nonsap_sba_schedule_info WHERE downloadflag = 'N' AND meterreader = '$userDetails->email'");
  if (count($schedules)) {
    $sno = 1;
    foreach ($schedules as $sc) {
        // $bills = DB::select("select * from bills where subDivisionCode = '$sc->subDivisionCode' and billingGroup = '$sc->billingGroup' and billingCycle = '$sc->billingCycle' and SUBSTRING(masterKey, 4, 4) = '$sc->LedgerCode' and bill_status = 'N' AND Billing_closed_flag = 'N'");
        $bills = DB::select("select * from bills where id = $sc->mainid and subDivisionCode = '$sc->subdivcode' and billingGroup = '$sc->bg' and billingCycle = '$sc->cy' and SUBSTRING(masterKey, 4, 4) = '$sc->ledgercode' and bill_status = 'N'");
        // return $bills;
        if (!empty($bills)) {
          $newAr = new ArrayIterator($bills);
          array_push($bill, ...$newAr);
        }
    }
  } else {
    $bill = [];
  }
  // return count($bill);
    return $bill;
});

Route::middleware('auth:api')->post('/bill-check', function (Request $request) {
  $userDetails = $request->user();
  $masterkey = $request->masterKey;
  $bills = DB::select("select id from billeds where blMasterKey = '$masterkey'");
  if (count($bills) >= 1) {
    $message['status'] = 'Y';
  } else {
    $message['status'] = 'N';
  }
    return $message;
});

Route::middleware('auth:api')->post('/bill-check-api', function (Request $request) {
  $userDetails = $request->user();
  $masterkey = $request->masterKey;
  $bills = DB::select("select id from sap_outputs where contractAcNumber = '$masterkey'");
  if (count($bills) >= 1) {
    $message['status'] = 'Y';
  } else {
    $message['status'] = 'N';
  }
    return $message;
});

Route::middleware('auth:api')->post('/sap-data', function (Request $request) {
  $date = Date('d-m-Y');
  $time = Date('H-i-s');
  $txtString = "";
  $rest_json = file_get_contents("php://input");
  $_POST = json_decode($rest_json, true);
  $json_string = json_encode($_POST);
  // Storage::put('file.txt', "$json_string");
  foreach ($_POST as $key) {
    $txtString .= $key . "!";
  }
  $newString = "$txtString";
  // return $string;
  DB::enableQueryLog();
  $userDetails = $request->user();
  $eemail = $userDetails->email;
  // Storage::put('file.txt', "$eemail");
  $logInsert = DB::insert("insert into sap_app_access_logs
  (contractAcNumber, MR_DOCNumber, subDivisonCode, billingGroup, billingCycle, mru, billDate, billTime, units, consdata, date, time, sbaid)
  values ('$request->contractAcNumber', '$request->mr_docnumber', '$request->subDivisonCode', '$request->billingGroup', '$request->billingCycle', '$request->MRU', '$request->currentMeterReadingDate', '$request->currentMeterReadingTime', '$request->currentMeterReadingKWH', '$json_string', '$date', '$time', '$eemail')");
  $text = DB::getQueryLog()[0]['query'];
  $read = sapInput::where('contractAcNumber', $request->contractAcNumber)->take(1)->get();

  $readStatus = $read[0]->bill_status;
  // return $readStatus;
  $mr_docnumber = $read[0]->mr_docnumber;
  // return $mr_docnumber;
  if ($readStatus == "N") {
    // return $readStatus;
    $userDetails = $request->user();
    // return $userDetails->id;
    $sapOutput = new sapOutput();
    $sapOutput->subDivisonCode = $request->subDivisonCode;
    $sapOutput->MRU = $request->MRU;
    $sapOutput->connectedPoleNINNumber = $request->connectedPoleNINNumber;
    $sapOutput->neighbourMeterNo = $request->neighbourMeterNo;
    $sapOutput->streetName = $request->streetName;
    $sapOutput->installation = $request->installation;
    $sapOutput->MR_DOCNumber = $mr_docnumber;
    // return $mr_docnumber;
    $sapOutput->scheduledMRDate = $request->scheduledMRDate;
    $sapOutput->SAPMeterNumber = $request->SAPMeterNumber;
    $sapOutput->manufacturerSRNo = $request->manufacturerSRNo;
    $sapOutput->manufacturerName = $request->manufacturerName;
    $sapOutput->contractAcNumber = $request->contractAcNumber;
    $sapOutput->consumptionKWH = $request->consumptionKWH;
    $sapOutput->consumptionKVAH = $request->consumptionKVAH;
    $sapOutput->consumptionKVA = $request->consumptionKVA;
    $sapOutput->currentMeterReadingKWH = $request->currentMeterReadingKWH;
    $sapOutput->currentMeterReadingKVA = $request->currentMeterReadingKVA;
    $sapOutput->currentMeterReadingKVAH = $request->currentMeterReadingKVAH;
    $sapOutput->currentMeterReadingDate = $request->currentMeterReadingDate;
    $sapOutput->currentMeterReadingTime = $request->currentMeterReadingTime;
    $sapOutput->currentNoteFromMeterReader = $request->currentNoteFromMeterReader;
    $sapOutput->previousMeterReadingKWH = $request->previousMeterReadingKWH;
    $sapOutput->previousMeterReadingKVA = $request->previousMeterReadingKVA;
    $sapOutput->previousMeterReadingKVAH = $request->previousMeterReadingKVAH;
    $sapOutput->previousMeterReadingDate = $request->previousMeterReadingDate;
    $sapOutput->previousMeterReadingTime = $request->previousMeterReadingTime;
    $sapOutput->previoustNoteFromMeterReader = $request->previoustNoteFromMeterReader;
    $sapOutput->octraiFlag = $request->octraiFlag;
    $sapOutput->SOP = $request->SOP;
    $sapOutput->ED = $request->ED;
    $sapOutput->OCTRAI = $request->OCTRAI;
    $sapOutput->DSSF = $request->DSSF;
    $sapOutput->surchargeLevied = $request->surchargeLevied;
    $sapOutput->serviceRent = $request->serviceRent;
    $sapOutput->meterRent = $request->meterRent;
    $sapOutput->serviceCharge = $request->serviceCharge;
    $sapOutput->monthlyMinimumCharges = $request->monthlyMinimumCharges;
    $sapOutput->powerFactorSurcharges = $request->powerFactorSurcharges;
    $sapOutput->powerFactorIncentive = $request->powerFactorIncentive;
    $sapOutput->demandCharges = $request->demandCharges;
    $sapOutput->waterCharges = $request->waterCharges;
    $sapOutput->voltageCharges = $request->voltageCharges;
    $sapOutput->peakLoadExemptionCharges = $request->peakLoadExemptionCharges;
    $sapOutput->sundryCharges = $request->sundryCharges;
    $sapOutput->miscellaneousCharges = $request->miscellaneousCharges;
    $sapOutput->fuelAdjustment = $request->fuelAdjustment;
    $sapOutput->billNumber = $request->billNumber;
    $sapOutput->noOfDaysBilledFor = $request->noOfDaysBilledFor;
    $sapOutput->billCycle = $request->billCycle;
    $sapOutput->billDate = $request->billDate;
    $sapOutput->dueDate = $request->dueDate;
    $sapOutput->billType = $request->billType;
    $sapOutput->paymantAmount = $request->paymantAmount;
    $sapOutput->paymantMode = $request->paymantMode;
    $sapOutput->checkNo = $request->checkNo;
    $sapOutput->bankName = $request->bankName;
    $sapOutput->paymentID = $request->paymentID;
    $sapOutput->IFSCcode = $request->IFSCcode;
    $sapOutput->MICRcode = $request->MICRcode;
    $sapOutput->paymentDate = $request->paymentDate;
    $sapOutput->paymentRemark = $request->paymentRemark;
    $sapOutput->totBillAmount = $request->totBillAmount;
    $sapOutput->SBMnumber = substr($userDetails->imeiNumber, 7, 8);
    $sapOutput->meterReaderName = $request->meterReaderName;
    $sapOutput->inHouseOutsourcedSBM = $request->inHouseOutsourcedSBM;
    $sapOutput->transformerCode = $request->transformerCode;
    $sapOutput->mcbRent = $request->mcbRent;
    $sapOutput->LPSC = $request->LPSC;
    $sapOutput->totalAmountAfterDueDate = $request->totalAmountAfterDueDate;
    $sapOutput->totalOfNetSopNetEdNetOctrai = $request->totalOfNetSopNetEdNetOctrai;
    $sapOutput->subDivisionName = $request->subDivisionName;
    $sapOutput->consumerLegacyNumber = $request->consumerLegacyNumber;
    $sapOutput->consumerName = $request->consumerName;
    $sapOutput->houseNumber = $request->houseNumber;
    $sapOutput->address = $request->address;
    $sapOutput->category = $request->category;
    $sapOutput->subCategory = $request->subCategory;
    $sapOutput->billingCycle = $request->billingCycle;
    $sapOutput->billingGroup = $request->billingGroup;
    $sapOutput->phaseCode = $request->phaseCode;
    $sapOutput->sanctionedLoadKW = $request->sanctionedLoadKW;
    $sapOutput->totalOldMeterConsumptionUnitsKWH = $request->totalOldMeterConsumptionUnitsKWH;
    $sapOutput->totalOldMeterConsumptionUnitsKVAH = $request->totalOldMeterConsumptionUnitsKVAH;
    $sapOutput->newMeterInitialReadingKWH = $request->newMeterInitialReadingKWH;
    $sapOutput->newMeterInitialReadingKVAH = $request->newMeterInitialReadingKVAH;
    $sapOutput->sundryChargesSOP = $request->sundryChargesSOP;
    $sapOutput->sundryChargesED = $request->sundryChargesED;
    $sapOutput->sundryChargesOCTRAI = $request->sundryChargesOCTRAI;
    $sapOutput->sundryAllowancesSOP = $request->sundryAllowancesSOP;
    $sapOutput->sundryAllowancesED = $request->sundryAllowancesED;
    $sapOutput->sundryAllowancesOCTRAI = $request->sundryAllowancesOCTRAI;
    $sapOutput->otherCharges = $request->otherCharges;
    $sapOutput->roundAdjustmentAmount = $request->roundAdjustmentAmount;
    $sapOutput->adjustmentAmountSOP = $request->adjustmentAmountSOP;
    $sapOutput->adjustmentAmountED = $request->adjustmentAmountED;
    $sapOutput->adjustmentAmountOCTRAI = $request->adjustmentAmountOCTRAI;
    $sapOutput->provisionalAdjustmentAmountSOP = $request->provisionalAdjustmentAmountSOP;
    $sapOutput->provisionalAdjustmentAmountED = $request->provisionalAdjustmentAmountED;
    $sapOutput->provisionalAdjustmentAmountOCTRAI = $request->provisionalAdjustmentAmountOCTRAI;
    $sapOutput->arrearSOPCurrentYear = $request->arrearSOPCurrentYear;
    $sapOutput->arrearEDCurrentYear = $request->arrearEDCurrentYear;
    $sapOutput->arrearOCTRAICurrentYear = $request->arrearOCTRAICurrentYear;
    $sapOutput->arrearSOPPreviousYear = $request->arrearSOPPreviousYear;
    $sapOutput->arrearEDPreviousYear = $request->arrearEDPreviousYear;
    $sapOutput->arrearOCTRAIPreviousYear = $request->arrearOCTRAIPreviousYear;
    $sapOutput->advanceConsumptionDeposit = $request->advanceConsumptionDeposit;
    $sapOutput->complaintCenterPhoneNumber = $request->complaintCenterPhoneNumber;
    $sapOutput->meterSecurityAmount = $request->meterSecurityAmount;
    $sapOutput->nearestCashCounter = $request->nearestCashCounter;
    $sapOutput->multiplicationFactor = $request->multiplicationFactor;
    $sapOutput->overallMf = $request->overallMf;
    $sapOutput->contractedDemandKVA = $request->contractedDemandKVA;
    $sapOutput->miscExpensesDetails = $request->miscExpensesDetails;
    $sapOutput->previousKWHCycle1 = $request->previousKWHCycle1;
    $sapOutput->previousKWHCycle2 = $request->previousKWHCycle2;
    $sapOutput->previousKWHCycle3 = $request->previousKWHCycle3;
    $sapOutput->previousKWHCycle4 = $request->previousKWHCycle4;
    $sapOutput->previousKWHCycle5 = $request->previousKWHCycle5;
    $sapOutput->previousKWHCycle6 = $request->previousKWHCycle6;
    $sapOutput->roundingPresent = $request->roundingPresent;
    $sapOutput->estimationType = $request->estimationType;
    $sapOutput->ml = $request->ml;
    $sapOutput->mtFlag = $request->mtFlag;
    $sapOutput->feederCode = $request->feederCode;
    $sapOutput->scWsdAmountWithHeld = $request->scWsdAmountWithHeld;
    $sapOutput->fixedCharges = $request->fixedCharges;
    $sapOutput->mt = $request->mt;
    $sapOutput->previousFyCons = $request->previousFyCons;
    $sapOutput->meterReadingHigh = $request->meterReadingHigh;
    $sapOutput->meterReadingNote = $request->meterReadingNote;
    $sapOutput->amountOfCowCess = $request->amountOfCowCess;
    $sapOutput->DuedateCheq = $request->DuedateCheq;
    $sapOutput->InfradevCess = $request->InfradevCess;
    $sapOutput->TotalInfraDevCess = $request->TotalInfraDevCess;
    $sapOutput->meterReader = $userDetails->email;
    $sapOutput->blLat = $request->blLat;
    $sapOutput->blLong = $request->blLong;
    $sapOutput->meterRemark = $request->meterRemark;
    $sapOutput->mdiRemark = $request->mdiRemark;
    $sapOutput->meterLocation = $request->meterLocation;
    $sapOutput->meterType = $request->meterType;
    $sapOutput->meterPhase = $request->meterPhase;
    $sapOutput->billNo = $request->billNo;
    $sapOutput->appVersion = $request->appVersion;
    $sapOutput->readingType = $request->readingType;
    if ($request->image != '' || $request->image != null) {
      // $base64 = $request->image;
      // list($baseType, $image) = explode(';', $base64);
      // list(, $image) = explode(',', $image);
      $image = base64_decode($request->image);
      $imageName = rand(11111111, 99999999). '.png';
      $year = Date('Y').'/';
      $type = 'SAP/';
      $bgbc = $request->billingGroup.'_'.$request->billingCycle;
      $imgPath = $year.'/'.$type.'/'.$bgbc;
      $imgName = $request->contractAcNumber.'_'.$bgbc.'_'.$imageName;
      $p = Storage::disk('s3')->put($imgPath . $imgName, $image, 'public');
      $sapOutput->imageLocation = $imgPath.$imgName;
    }
    $sapOutput->save();

    $sapOutputId = $sapOutput->id;
    $contractAcNoToUpdate = $sapOutput->contractAcNumber;
    $meterReaderToUpdate = $sapOutput->meterReader;

    $billStatusCheck = sapInput::where('contractAcNumber', $sapOutput->contractAcNumber)->get();
    $billStatusUpdate = sapInput::find($billStatusCheck->first()->id);
    $billStatusUpdate->bill_status = 'Y';
    $billStatusUpdate->save();
    // return $billStatusUpdate;
    if ($sapOutput) {
      $result['aws_status'] = "Y";

      $updateSchedule = DB::update("update sap_sba_schedule_info set bld_flag = 'Y' WHERE masterkey = '$contractAcNoToUpdate' and meterreader = '$meterReaderToUpdate'");
    } else {
      $result['aws_status'] = "N";
    }



  $scheduledMRDate = Carbon\Carbon::createFromFormat('d/m/Y', $sapOutput['scheduledMRDate'])->format('Y-m-d');
  $scheduledMRTime = 'T'.'00:00:00';
  $scheduledMRDate = $scheduledMRDate.$scheduledMRTime;
  $currentMeterReadingDate = Carbon\Carbon::createFromFormat('d-m-Y', $sapOutput['currentMeterReadingDate'])->format('Y-m-d');
  $currentMeterReadingTime = 'T'.'00:00:00';
  $currentMeterReadingDate = $currentMeterReadingDate.$currentMeterReadingTime;
  $previousMeterReadingDate = Carbon\Carbon::createFromFormat('d/m/Y', $sapOutput['previousMeterReadingDate'])->format('Y-m-d');
  $previousMeterReadingTime = 'T'.'00:00:00';
  $previousMeterReadingDate = $previousMeterReadingDate.$previousMeterReadingTime;
  $billDate = Carbon\Carbon::createFromFormat('d/m/Y', $sapOutput['billDate'])->format('Y-m-d');
  $billTime = 'T'.'00:00:00';
  $billDate = $billDate.$billTime;
  $dueDate = Carbon\Carbon::createFromFormat('d-m-Y', $sapOutput['dueDate'])->format('Y-m-d');
  $dueTime = 'T'.'00:00:00';
  $dueDate = $dueDate.$dueTime;
  $billDate2 = Carbon\Carbon::createFromFormat('d-m-Y', $sapOutput['dueDate'])->format('Y-m-d');
  $billTime2 = 'T'.'00:00:00';
  $billDate2 = $billDate2.$billTime2;
  $dueDate2 = Carbon\Carbon::createFromFormat('d-m-Y', $sapOutput['DuedateCheq'])->format('Y-m-d');
  $dueTime2 = 'T'.'00:00:00';
  $dueDate2 = $dueDate2.$dueTime2;
  // $currentMeterReadingTime = Carbon\Carbon::createFromFormat('H:i:s', $sapOutput['currentMeterReadingTime'])->format('His');
  $currentMeterReadingTime = Carbon\Carbon::createFromFormat('H:i:s', $sapOutput['currentMeterReadingTime'])->format('His');
  // try {
  //     // $client = new SoapClient ( "some.aspx?WSDL" );
  //     // $client = new SoapClient ( "some.aspx?WSDL" );
  //     $client = new SoapClient("https://billingdatareceiver.pspcl.in/SAP_SBM_DATA.ASMX?wsdl");
  //     // return $sapOutput['scheduledMRDate'];
  //     // $scheduledMRDate = 2018-06-11T21:32:52;
  //     if ($sapOutput['ml'] == '-' || $sapOutput['ml'] == '' || $sapOutput['ml'] == null) {
  //       $sapOutput['ml'] = 0;
  //     }
  //     if ($sapOutput['mtFlag'] == '-' || $sapOutput['mtFlag'] == '' || $sapOutput['mtFlag'] == null) {
  //       $sapOutput['mtFlag'] = 0;
  //     }
  //     if ($sapOutput['mobileNo'] == null || $sapOutput['mobileNo'] == '' || $sapOutput['mobileNo'] == '-') {
  //       $sapOutput['mobileNo'] = 0;
  //     }
  //     if ($sapOutput['feederCode'] == '-' || $sapOutput['feederCode'] == '' || $sapOutput['feederCode'] == null) {
  //       $sapOutput['feederCode'] = 0;
  //     }
  //     if ($sapOutput['fixedCharges'] == '' || $sapOutput['fixedCharges'] == null || $sapOutput['fixedCharges'] == '-') {
  //       $sapOutput['fixedCharges'] = 0;
  //     }
  //     if ($sapOutput['mt'] == '' || $sapOutput['mt'] == null || $sapOutput['mt'] == '-') {
  //       $sapOutput['mt'] = 0;
  //     }
  //     if ($sapOutput['previousFyCons'] == '-' || $sapOutput['previousFyCons'] == '' || $sapOutput['previousFyCons'] == null) {
  //       $sapOutput['previousFyCons'] = 0;
  //     }
  //     if ($sapOutput['meterReadingHigh'] == '-' || $sapOutput['meterReadingHigh'] == '' || $sapOutput['meterReadingHigh'] == null) {
  //       $sapOutput['meterReadingHigh'] = 0;
  //     }
  //     if ($sapOutput['meterReadingNote'] == '-' || $sapOutput['meterReadingNote'] == '' || $sapOutput['meterReadingNote'] == null) {
  //       $sapOutput['meterReadingNote'] = 0;
  //     }
  //     if ($sapOutput['amountOfCowCess'] == '-' || $sapOutput['amountOfCowCess'] == '' || $sapOutput['amountOfCowCess'] == null) {
  //       $sapOutput['amountOfCowCess'] = 0;
  //     }
  //
  //     $octroi = $sapOutput['OCTRAI'];
  //     $params = array(
  //       'reqArr' => [
  //         'SAP_RequestParams' => [
  //           'SUB_DIVISION_CODE' => $sapOutput['subDivisonCode'],
  //           'MRU' => $sapOutput['MRU'],
  //           'CONNECTED_POLE_INI_NUMBER' => $sapOutput['connectedPoleNINNumber'],
  //           'NEIGHBOR_METER_NO' => $sapOutput['neighbourMeterNo'],
  //           'STREET_NAME' => $sapOutput['streetName'],
  //           'INSTALLATION' => $sapOutput['installation'],
  //           'MR_DOC_NO' => $sapOutput['MR_DOCNumber'],
  //           'SCHEDULED_MRDATE' => $scheduledMRDate,
  //           'METER_NUMBER' => $sapOutput['SAPMeterNumber'],
  //           'MANUFACTURER_SR_NO' => $sapOutput['manufacturerSRNo'],
  //           'MANUFACTURER_NAME' => $sapOutput['manufacturerName'],
  //           'CONTRACT_ACCOUNT_NUMBER' => $sapOutput['contractAcNumber'],
  //           'CONSUMPTION_KWH' => $sapOutput['consumptionKWH'],
  //           'CONSUMPTION_KWAH' => $sapOutput['consumptionKVAH'],
  //           'CONSUMPTION_KVA' => $sapOutput['consumptionKVA'],
  //           'CUR_METER_READING_KWH' => $sapOutput['currentMeterReadingKWH'],
  //           'CUR_METER_READING_KVA' => $sapOutput['currentMeterReadingKVA'],
  //           'CUR_METER_READING_KVAH' => $sapOutput['currentMeterReadingKVAH'],
  //           'CUR_METER_READING_DATE' => $currentMeterReadingDate,
  //           'CUR_METER_READING_TIME' => $currentMeterReadingTime,
  //           'CUR_METER_READER_NOTE' => $sapOutput['currentNoteFromMeterReader'],
  //           'PRV_METER_READING_KWH' => $sapOutput['previousMeterReadingKWH'],
  //           'PRV_METER_READING_KVA' => $sapOutput['previousMeterReadingKVA'],
  //           'PRV_METER_READING_KWAH' => $sapOutput['previousMeterReadingKVAH'],
  //           'PRV_METER_READING_DATE' => $previousMeterReadingDate,
  //           'PRV_METER_READING_TIME' => $sapOutput['previousMeterReadingTime'],
  //           'PRV_METER_READER_NOTE' => $sapOutput['previoustNoteFromMeterReader'],
  //           'OCTROI_FLAG' => $sapOutput['octraiFlag'],
  //           'SOP' => $sapOutput['SOP'],
  //           'ED' => $sapOutput['ED'],
  //           'OCTROI' => $octroi,
  //           'DSSF' => $sapOutput['TotalInfraDevCess'],
  //           'SURCHARGE_LEIVED' => $sapOutput['surchargeLevied'],
  //           'SERVICE_RENT' => $sapOutput['serviceRent'],
  //           'METER_RENT' => $sapOutput['meterRent'],
  //           'SERVICE_CHARGE' => $sapOutput['serviceCharge'],
  //           'MONTHLY_MIN_CHARGES' => $sapOutput['monthlyMinimumCharges'],
  //           'PF_SURCHARGE' => $sapOutput['powerFactorSurcharges'],
  //           'PF_INCENTIVE' => $sapOutput['powerFactorIncentive'],
  //           'DEMAND_CHARGES' => $sapOutput['demandCharges'],
  //           'FIXEDCHARGES' => $sapOutput['fixedCharges'],
  //           'VOLTAGE_SURCHARGE' => $sapOutput['voltageCharges'],
  //           'PEAKLOAD_EXEMPTION_CHARGES' => $sapOutput['peakLoadExemptionCharges'],
  //           'SUNDRY_CHARGES' => $sapOutput['sundryCharges'],
  //           'MISCELLANEOUS_CHARGES' => $sapOutput['miscellaneousCharges'],
  //           'FUEL_ADJUSTMENT' => $sapOutput['fuelAdjustment'],
  //           'BILL_NUMBER' => $sapOutput['billNumber'],
  //           'NO_OF_DAYS_BILLED' => $sapOutput['noOfDaysBilledFor'],
  //           'BILL_CYCLE' => $sapOutput['billCycle'],
  //           'BILL_DATE' => $billDate2,
  //           'DUE_DATE' => $dueDate2,
  //           'BILL_TYPE' => $sapOutput['billType'],
  //           'PAYMENT_AMOUNT' => $sapOutput['paymantAmount'],
  //           'PAYMENT_MODE' => $sapOutput['paymantMode'],
  //           'CHECK_NO' => $sapOutput['checkNo'],
  //           'BANK_NAME' => $sapOutput['bankName'],
  //           'PAYMENT_ID' => $sapOutput['paymentID'],
  //           'IFSC_CODE' => $sapOutput['IFSCcode'],
  //           'MICRCODE' => $sapOutput['MICRcode'],
  //           'PAYMENT_DATE' => $sapOutput['paymentDate'],
  //           'PAYMENT_REMARK' => '#'.$sapOutput['ml'].'#'.$sapOutput['mtFlag'].'#'.$sapOutput['mobileNo'].'#'.$sapOutput['feederCode'].'#'.$sapOutput['scWsdAmountWithHeld'].'#'.$sapOutput['fixedCharges'].'#'.$sapOutput['mt'].'#'.$sapOutput['previousFyCons'].'#'.$sapOutput['meterReadingHigh'].'#'.$sapOutput['meterReadingNote'].'#'.$sapOutput['amountOfCowCess'],
  //           'TOT_BILLAMOUNT' => $sapOutput['totBillAmount'],
  //           'SBM_NUMBER' => $sapOutput['SBMnumber'],
  //           'METER_READER_NAME' => $sapOutput['meterReaderName'],
  //           'INHOUSE_OUTSOURCED_SBM' => $sapOutput['inHouseOutsourcedSBM'],
  //           'TRANSFROMERCODE' => $sapOutput['transformerCode'],
  //           'MCB_RENT' => $sapOutput['mcbRent'],
  //           'LPSC' => $sapOutput['LPSC'],
  //           'TOT_AMT_DUE_DATE' => $sapOutput['totalAmountAfterDueDate'],
  //           'TOT_SOP_ED_OCT' => $sapOutput['totalOfNetSopNetEdNetOctrai'],
  //           'SUBDIVISIONNAME' => $sapOutput['subDivisionName'],
  //           'CONSUMERLEGACYNUMBER' => $sapOutput['consumerLegacyNumber'],
  //           'CONSUMERNAME' => $sapOutput['consumerName'],
  //           'HOUSENUMBER' => $sapOutput['houseNumber'],
  //           'ADDRESS' => $sapOutput['address'],
  //           'CATEGORY' => $sapOutput['category'],
  //           'SUBCATEGORY' => $sapOutput['subCategory'],
  //           'BILLINGCYCLE' => $sapOutput['billingCycle'],
  //           'BILLINGGROUP' => $sapOutput['billingGroup'],
  //           'PHASECODE' => $sapOutput['phaseCode'],
  //           'SANCTIONEDLOAD_KW' => $sapOutput['sanctionedLoadKW'],
  //           'TOTALOLDMETER_CONS_UNIT_KWH' => $sapOutput['totalOldMeterConsumptionUnitsKWH'],
  //           'TOTALOLDMETER_CONS_UNIT_KVAH' => $sapOutput['totalOldMeterConsumptionUnitsKVAH'],
  //           'NEWMETERINITIALREADING_KWH' => $sapOutput['newMeterInitialReadingKWH'],
  //           'NEWMETERINITIALREADING_KVAH' => $sapOutput['newMeterInitialReadingKVAH'],
  //           'SUNDRYCHRS_SOP' => $sapOutput['sundryChargesSOP'],
  //           'SUNDRYCHRS_ED' => $sapOutput['sundryChargesED'],
  //           'SUNDRYCHRS_OCTRAI' => $sapOutput['sundryChargesOCTRAI'],
  //           'SUNDRYALLOWANCES_SOP' => $sapOutput['sundryAllowancesSOP'],
  //           'SUNDRYALLOWANCES_ED' => $sapOutput['sundryAllowancesED'],
  //           'SUNDRYALLOWANCES_OCTRAI' => $sapOutput['sundryAllowancesOCTRAI'],
  //           'OTHERCHRS' => $sapOutput['otherCharges'],
  //           'ROUNDADJUSTMENTAMNT' => $sapOutput['roundAdjustmentAmount'],
  //           'ADJUSTMENTAMT_SOP' => $sapOutput['adjustmentAmountSOP'],
  //           'ADJUSTMENTAMT_ED' => $sapOutput['adjustmentAmountED'],
  //           'ADJUSTMENTAMT_OCTRAI' => $sapOutput['adjustmentAmountOCTRAI'],
  //           'PROVISIONALADJUSTMENTAMT_SOP' => $sapOutput['provisionalAdjustmentAmountSOP'],
  //           'PROVISIONALADJUSTMENTAMT_ED' => $sapOutput['provisionalAdjustmentAmountED'],
  //           'PROVADJUSTMENTAMT_OCTRAI' => $sapOutput['provisionalAdjustmentAmountOCTRAI'],
  //           'ARREARSOPCURRENTYEAR' => $sapOutput['arrearSOPCurrentYear'],
  //           'ARREAREDCURRENTYEAR' => $sapOutput['arrearEDCurrentYear'],
  //           'ARREAROCTRAICURRENTYEAR' => $sapOutput['arrearOCTRAICurrentYear'],
  //           'ARREARSOPPREVIOUSYEAR' => $sapOutput['arrearSOPPreviousYear'],
  //           'ARREAREDPREVIOUSYEAR' => $sapOutput['arrearEDPreviousYear'],
  //           'ARREAROCTRAIPREVIOUSYEAR' => $sapOutput['arrearOCTRAIPreviousYear'],
  //           'ADVANCECONS_DEPOSIT' => $sapOutput['advanceConsumptionDeposit'],
  //           'COMPLAINTCENTERPHONENUMBER' => $sapOutput['complaintCenterPhoneNumber'],
  //           'METERSECURITYAMT' => $sapOutput['meterSecurityAmount'],
  //           'NEARESTCASHCOUNTER' => $sapOutput['nearestCashCounter'],
  //           'MULTIPLICATIONFACTOR' => $sapOutput['multiplicationFactor'],
  //           'OVERALLMF' => $sapOutput['overallMf'],
  //           'CONTRACTEDLOAD_KVA' => $sapOutput['contractedDemandKVA'],
  //           'MISCEXPENSESDETAILS' => $sapOutput['miscExpensesDetails'],
  //           'PREVIOUSKWHCYCLE_1' => (int) $sapOutput['previousKWHCycle1'],
  //           'PREVIOUSKWHCYCLE_2' => (int) $sapOutput['previousKWHCycle2'],
  //           'PREVIOUSKWHCYCLE_3' => (int) $sapOutput['previousKWHCycle3'],
  //           'PREVIOUSKWHCYCLE_4' => (int) $sapOutput['previousKWHCycle4'],
  //           'PREVIOUSKWHCYCLE_5' => (int) $sapOutput['previousKWHCycle5'],
  //           'PREVIOUSKWHCYCLE_6' => (int) $sapOutput['previousKWHCycle6'],
  //           'ROUNDING_PRESENT' => $sapOutput['roundingPresent'],
  //           'ESTIMATION_TYPE' => $sapOutput['estimationType'],
  //           'RECTYPE' => "1",
  //           'USERID' => "STRTECH",
  //           'PASSCODE' => "STR@1PSPCL",
  //           'VID' => "1",
  //           ]
  //         ]
  //
  //         );
  //
  //     $resultt = $client->PushSAP_Data($params);
  //   //   return $result;
  //
  //     $result['soap'] = $resultt;
  //     // return 'ppppp';
  //     // return dd($result['soap']->PushSAP_DataResult->SAP_ResponseParams);
  //     $updateSoap = sapOutput::find($sapOutputId);
  //     $updateSoap->soapStatus = $result['soap']->PushSAP_DataResult->SAP_ResponseParams->STATUS;
  //     $updateSoap->soapMessage = $result['soap']->PushSAP_DataResult->SAP_ResponseParams->MSG;
  //     $updateSoap->soapCount = $updateSoap->soapCount + 1;
  //     $updateSoap->save();
  //     // return $updateSoap;
  //     return $result;
  //
  //
  // } catch ( SoapFault $fault ) {
  //   // echo "error";
  //     // trigger_error ( "SOAP Fault: (faultcode: {$fault->faultcode}, faultstring: {$fault->faultstring})", E_USER_ERROR );
  //   $result['aws_status'] = "Y";
  // }
} else {

  $result['aws_status'] = "Y";
}
  return $result;
});

// Route::get('/sap-soap',  'preDataController@show');

Route::middleware('auth:api')->post('/data', function (Request $request) {
  // $read = Billed::where('blMasterKey', $request->blMasterKey)->take(1)->get();
  $read = DB::select("SELECT count(*) readCount FROM billeds where blMasterKey = '$request->blMasterKey'");
  $userDetails = $request->user();
  // return $userDetails->email;
  // return $read->count();

  $readStatus = $read[0]->readCount;
  // $mr_docnumber = $read[0]->mr_docnumber;
  // if ($readStatus == "N") {
  if ($readStatus <= 0) {
    // $read = Bill::find($request->id);
    // $readStatus = $read->status;

    $userDetails = $request->user();
    // return $userDetails->id;

      $newBill = new Billed();
      $newBill->blMeterReader = $userDetails->id;
      $newBill->blSubDivisionCode = substr($request->blMasterKey, 0, 3);
      $newBill->blMasterKey = $request->blMasterKey;
      $newBill->blConsumerName = $request->blConsumerName;
      $newBill->blTariffType = $request->blTariffType;
      $newBill->blBillingGroup = $request->blBillingGroup;
      $newBill->blBillingCycle = $request->blBillingCycle;
      $newBill->blCurrentMeterRent = $request->blCurrentMeterRent;
      $newBill->blCurrentServiceRent = $request->blCurrentServiceRent;
      $newBill->blPresentReading = $request->blPresentReading;
      $newBill->blPresentReadingDate = $request->blPresentReadingDate;
      $readDateFormat = DateTime::createFromFormat('d/m/Y', $request->blPresentReadingDate);
      $newBill->blPresentReadingDateFormated = $readDateFormat->format('Y-m-d');
      $newBill->blPresentStatus = $request->blPresentStatus;
      $newBill->blUnitsConsumed = $request->blUnitsConsumed;
      $newBill->blDays = $request->blDays;
      $newBill->blMMCFlag = $request->blMMCFlag;
      $newBill->blCurrentSop = $request->blCurrentSop;
      $newBill->blCurrentEd = $request->blCurrentEd;
      $newBill->blCurrentOctroi = $request->blCurrentOctroi;
      $newBill->blNetSop = $request->blNetSop;
      $newBill->blNetEd = $request->blNetEd;
      $newBill->blNetOctroi = $request->blNetOctroi;
      $newBill->blCurrentRoundingAmt = $request->blCurrentRoundingAmt;
      $newBill->blAmountBeforeDue = $request->blAmountBeforeDue;
      $newBill->blSurCharge = $request->blSurCharge;
      $newBill->blDueDateByCash = $request->blDueDateByCash;
      $newBill->blDueDateByCheque = $request->blDueDateByCheque;
      $newBill->blAmountAfterDue = $request->blAmountAfterDue;
      $newBill->blTime = $request->blTime;
      $newBill->blConcessionalUnits = $request->blConcessionalUnits;
      $newBill->blCurrentIDFWithSign = $request->blCurrentIDFWithSign;
      $newBill->blCurrentCowCessWithSign = $request->blCurrentCowCessWithSign;
      if($request->blCurrentWaterSewageChargeWithSign != '' || $request->blCurrentWaterSewageChargeWithSign != null) {
        $newBill->blCurrentWaterSewageChargeWithSign = $request->blCurrentWaterSewageChargeWithSign;
      } else {
          $newBill->blCurrentWaterSewageChargeWithSign = 0;
      }

      $newBill->bltotalIDFWithSign = $request->bltotalIDFWithSign;
      $newBill->blTotalCowCessWithSign = $request->blTotalCowCessWithSign;
      if($request->blTotalCurrentWaterSewarageChargeWithSign != '' || $request->blTotalCurrentWaterSewarageChargeWithSign != null) {
        $newBill->blTotalCurrentWaterSewarageChargeWithSign = $request->blTotalCurrentWaterSewarageChargeWithSign;
      } else {
         $newBill->blTotalCurrentWaterSewarageChargeWithSign = 0;
      }

      $newBill->blCurrentFCAChargesWithSign = $request->blCurrentFCAChargesWithSign;
      $newBill->blCurrentFixedChargesWithSign = $request->blCurrentFixedChargesWithSign;
      $newBill->blLat = $request->blLat;
      $newBill->blLong = $request->blLong;
      $newBill->blEmail = $request->blEmail;
      $newBill->blMobile = $request->blMobile;
      $newBill->blRemark = $request->blRemarks;
      $newBill->meterReader = $userDetails->email;
      $newBill->meterRemark = $request->meterRemark;
      $newBill->mdiRemark = $request->mdiRemark;
      $newBill->meterLocation = $request->meterLocation;
      $newBill->meterType = $request->meterType;
      $newBill->meterPhase = $request->meterPhase;
      $newBill->blUnitsConsumedNew = $request->blUnitsConsumedNew;
      $newBill->blUnitsConsumedOld = $request->blUnitsConsumedOld;
      $newBill->billNo = $request->billNo;
      $newBill->appVersion = $request->appVersion;
      $newBill->blpreRoundamnt = $request->blpreRoundamnt;
      $newBill->blProvadjamnt = $request->blProvadjamnt;
      $newBill->blprevyrarrear = $request->blprevyrarrear;
      $newBill->blcurryrarrear = $request->blcurryrarrear;
      $newBill->blsundrycharges = $request->blsundrycharges;
      $newBill->blallowance = $request->blallowance;
      $newBill->blconcessionalec = $request->blconcessionalec;
      $newBill->blconcessnEcBe = $request->blconcessnEcBe;
      $newBill->blothercharges = $request->blothercharges;
      $newBill->blvoltSurcORrebate = $request->blvoltSurcORrebate;
      $newBill->blsancloadkw = $request->blsancloadkw;
      $newBill->blpreviosreading = $request->blpreviosreading;
      $newBill->blprevmtrstatus = $request->blprevmtrstatus;
      $newBill->blpreviosreadingdate = $request->blpreviosreadingdate;
      $newBill->save();
      $newBillId = $newBill->id;
      $masterKeyToUpdate = $newBill->blMasterKey;
      $meterReaderToUpdate = $newBill->meterReader;
      // return $newBillId;
      // $billDetails = Bill::find($newBill->id);
      // return $billDetails['consumerAddress'];

      $billStatusCheck = Bill::where('masterKey', $newBill->blMasterKey)->get();
      // return $billStatusCheck;
      $billStatusUpdate = Bill::find($billStatusCheck->first()->id);
      $billStatusUpdate->bill_status = 'Y';
      $billStatusUpdate->save();

      // $billStatusCheck = sapInput::where('contractAcNumber', $sapOutput->contractAcNumber)->get();
      // $billStatusUpdate = sapInput::find($billStatusCheck->first()->id);
      // $billStatusUpdate->bill_status = 'Y';
      // $billStatusUpdate->save();

      if ($newBill) {
          $result['aws_status'] = "Y";

          $updateSchedule = DB::update("update nonsap_sba_schedule_info set bld_flag = 'Y' WHERE masterkey = '$masterKeyToUpdate' and meterreader = '$meterReaderToUpdate'");
      } else {
        $result['aws_status'] = "N";
      }
      // return $newBillId;
      $billDetails = Bill::where('masterKey', $newBill->blMasterKey)->take(1)->get();

      $searchId = $billDetails[0]->id;
      $billDetails = Bill::find($searchId);
      $fnYearY = Date('Y');
      // return $fnYearY;
      $fnYear = Date('y')+1;
      $fnYear = $fnYearY.'-'.$fnYear;
      // return $billDetails['subDivisionName'];
      // if($billDetails['connectedLoadInKW'] < 10){
      //
      //   // return $fnYear;
      //   try {
      //     // $client = new SoapClient ( "some.aspx?WSDL" );
      //     $client = new SoapClient("https://billingdatareceiver.pspcl.in/NONSAP_SBM_DATA.ASMX?wsdl");
      //
      //     $blPresentReadingDate = Carbon\Carbon::createFromFormat('d/m/Y', $newBill['blPresentReadingDate'])->format('Y-m-d');
      //     $blPresentReadingTime = 'T'.'00:00:00';
      //     $time = $newBill['blTime'];
      //     $timeS1 = substr($time, 0, 2);
      //     $timeS2 = substr($time, 2, 2);
      //     $timeS3 = substr($time, 4, 2);
      //     $blTime = $timeS1.':'.$timeS2.':'.$timeS3;
      //     $blPresentReadingDate = $blPresentReadingDate.'T'.$blTime;
      //
      //     $blDueDateByCash = Carbon\Carbon::createFromFormat('d-m-Y', $newBill['blDueDateByCash'])->format('Y-m-d');
      //     $blDueTimeByCash = 'T'.'00:00:00';
      //     $blDueDateByCash = $blDueDateByCash.$blDueTimeByCash;
      //
      //     $blDueDateByCheque = Carbon\Carbon::createFromFormat('d-m-Y', $newBill['blDueDateByCheque'])->format('Y-m-d');
      //     $blDueTimeByCheque = 'T'.'00:00:00';
      //     $blDueDateByCheque = $blDueDateByCheque.$blDueTimeByCheque;
      //
      //     $blcurrentReadingDate = Carbon\Carbon::createFromFormat('d/m/Y', $newBill['blPresentReadingDate'])->format('d-m-Y');
      //     $blpreviousReadingDate = Carbon\Carbon::createFromFormat('d/m/Y', $billDetails['previousReadingDate'])->format('d-m-Y');
      //     $billNoDate = Carbon\Carbon::createFromFormat('d/m/Y', $newBill['blPresentReadingDate'])->format('dmy');
      //     // return $billNoDate;
      //     $billNo = $newBill['billNo'];
      //     $adjAmt = 0;
      //     if (
      //       $billDetails['perviousCode'] == 'L' || $billDetails['perviousCode'] == 'N' || $billDetails['perviousCode'] == "I"
      //       && $newBill['blPresentStatus'] == 'O' || $newBill['blPresentStatus'] == 'C' || $newBill['blPresentStatus'] == 'H' ||
      //       $newBill['blPresentStatus'] == 'X' || $newBill['blPresentStatus'] == 'T' ||
      //       $newBill['blPresentStatus'] == 'W' || $newBill['blPresentStatus'] == 'A'
      //     ) {
      //       $adjAmt = $billDetails['averageAdjustmentSOP'] + $billDetails['averageAdjustmentED'] + $billDetails['averageAdjustmentOctroi'] + $billDetails['avgAdjustmentIDF'] + $billDetails['avgAdjustmentCowCess'] + $billDetails['avgAdjustmentWaterSewarageChargesCode'];
      //     } else {
      //       $adjAmt = 0;
      //     }
      //     // $adjAmt = $billDetails['averageAdjustmentSOP'] + $billDetails['averageAdjustmentED'] + $billDetails['averageAdjustmentOctroi'] + $billDetails['avgAdjustmentIDF'] + $billDetails['avgAdjustmentCowCess'] + $billDetails['avgAdjustmentWaterSewarageChargesCode'];
      //     $SUNDARYCHARGES = $billDetails['sundarySOP'] + $billDetails['sundaryED'] + $billDetails['sundaryOctroi'] + $billDetails['sundryIDF'] + $billDetails['sundryCowCess'] + $billDetails['sundryWaterSewarageChargesCode'];
      //     $SUNDARYALLOWANCE = $billDetails['allowanceSOP'] + $billDetails['allowanceED'] + $billDetails['allowanceOctroi'] + $billDetails['allowanceIDF'] + $billDetails['allowanceCowCess'] + $billDetails['allowanceWaterSewarageChargesCode'];
      //     // return $billNo;
      //     // Tariff type Converted to 3 digits as per telephonic conversation with it team on 24-07-2018 4-20 Pm
      //     $tariffType = '';
      //     if ($newBill->blTariffType == 1) {
      //       $tariffType = "DS";
      //     } elseif ($newBill->blTariffType == 3) {
      //       $tariffType = "BE";
      //     } elseif ($newBill->blTariffType == 5) {
      //       $tariffType = "WSD";
      //     } elseif ($newBill->blTariffType == 6) {
      //       $tariffType = "BPL";
      //     } elseif ($newBill->blTariffType == 0) {
      //       $tariffType = "BO";
      //     } elseif ($newBill->blTariffType == 2) {
      //       $tariffType = "NRS";
      //     } elseif ($newBill->blTariffType == 4) {
      //       $tariffType = "RLY";
      //     } elseif ($newBill->blTariffType == 8) {
      //       $tariffType = "CGO";
      //     }
      //     $params = array(
      //       'reqArr' => [
      //         'NonSAP_RequestParams_Below10kw' => [
      //           'ACCOUNTNO' => $newBill['blMasterKey'],
      //           'NAME' => $newBill['blConsumerName'],
      //           'ADDRESS' => $billDetails['consumerAddress'], // From Bills Table
      //           'ISSUEDATE' => $blPresentReadingDate,
      //           'DUEDATECASH' => $blDueDateByCash, // Need to format
      //           'DUEDATECHEQUE' => $blDueDateByCheque, // Need to format
      //           'VILLCITYNAME' => "",
      //           'SUBDIVNAME' => $billDetails['subDivisionName'], // Form Bills Table
      //           'BILLNO' => $billNo, // Need to Generate
      //           'CURRENTREADINGDATE' => $blcurrentReadingDate,
      //           'PREVREADINGDATE' => $blpreviousReadingDate, // Frm bills Table
      //           'BILLPERIOD' => $newBill['blDays'],
      //           'TARIFFTYPE' => $tariffType,
      //           'CONNECTEDLOAD' => $billDetails['connectedLoadInKW'], // Verify
      //           'PREVBILLSTATUS' => '', //$billDetails['perviousCode'], // From bills Table
      //           'METERNO' => $billDetails['meterNo'], // From bills Table
      //           'SECURITYAMT' => $billDetails['securityDeposit'], // From bills Table
      //           'BILLSTATUS' => $newBill['blPresentStatus'],
      //           'BILLCYCLE' => $newBill['blBillingCycle'],
      //           'BILLGROUP' => $newBill['blBillingGroup'],
      //           'METERREADINGCURRENT' => $newBill['blPresentReading'], // Verify
      //           'METERREADINGPREV' => $billDetails['perviousReading'], // From bills Table "Verify"
      //           'LINECTRATIO' => $billDetails['lineCtRatio'], // From Bills Table
      //           'METERCTRATIO' => $billDetails['meterCtRatio'], // From Bills Table
      //           'METERMULTIPLIER' => $billDetails['meterMultiplier'], // From Bills Table
      //           'OVERMETERMULTIPLIER' => $billDetails['overallMultiplingFactor'], // From Bills Table
      //           'METERCODE' => $newBill['blPresentStatus'], // Verify
      //           'UNITSCONSUMEDNEW' => $newBill['blUnitsConsumedNew'],
      //           'UNITSCONSUMEDOLD' => $newBill['blUnitsConsumedOld'],
      //           'TOTALUNITSCONSUMED' => $newBill['blUnitsConsumed'],
      //           'CURRENTSOP' => $newBill['blCurrentSop'],
      //           'CURRENTED' => $newBill['blCurrentEd'],
      //           'OCTROI' => $newBill['blCurrentOctroi'],
      //           'METERRENT' => $newBill['blCurrentMeterRent'],
      //           'SERVICERENT' => $newBill['blCurrentServiceRent'],
      //           'ADJAMT' => $adjAmt, // From Bills Table
      //           'ADJPERIOD' => $billDetails['periodOfAdjustmentDetail'], // From Bills TAble
      //           'ADJREASON' => $billDetails['reason'], // From Bills TAble
      //           'CONSUNITS' => $newBill['blConcessionalUnits'], // Verify
      //           'FIXEDCHARGES' => $newBill['blCurrentFixedChargesWithSign'], // Verify
      //           'FUELCOST' => $newBill['blCurrentFCAChargesWithSign'], // Verify
      //           'VOLTCLASSCHARGES' => '0', // $billDetails['othersvoltageSurchargeAmpunt'], // From Bills Table
      //           'PREVTOTALARR' => $billDetails['previousArrear'], // From Bills Table
      //           'CURRENTTOTALARREAR' => $billDetails['currentArrear'], // From Bills Table
      //           'OTHERCHARGES' => $billDetails['others'], // Verify
      //           'SUNDARYCHARGES' => $SUNDARYCHARGES, // Verify // Add field in Database
      //           'SUNDARYALLOWANCE' => $SUNDARYALLOWANCE, // Verify
      //           'CURRENTROUNDEDAMT' => $newBill['blCurrentRoundingAmt'],
      //           'PREVROUNDEDAMT' => $billDetails['previousRoundingAmount'], // From Bills Table
      //           'TOTALNETSOP' => $newBill['blNetSop'],
      //           'TOTALNETED' => $newBill['blNetEd'],
      //           'TOTALNETOCTROI' => $newBill['blNetOctroi'],
      //           'TOTALAMT' => $newBill['blAmountBeforeDue'],
      //           'TOTALSURCHARGE' => $newBill['blSurCharge'],
      //           'TOTALAMTGROSS' => $newBill['blAmountAfterDue'],
      //           'BILLYEAR' => Date('Y'),
      //           'SUNDARYMESSAGE' => $billDetails['sundaryChargeDetail'], // From Bills Table
      //           'SUNDARYDTFROM' => "", // Verify
      //           'SUNDARYDTTO' => "", // Verify
      //           'LINE1' => "", // Verify
      //           'LINE2' => "", // Verify
      //           'PREVCYCLECONSUMPTION1' => (int) substr($billDetails['lastSixMonthConsumption'], 0, 6), // Calculate "Verify"
      //           'PREVCYCLECONSUMPTION2' => (int) substr($billDetails['lastSixMonthConsumption'], 6, 6), // Calculate "Verify"
      //           'PREVCYCLECONSUMPTION3' => (int) substr($billDetails['lastSixMonthConsumption'], 12, 6), // Calculate "Verify"
      //           'PREVCYCLECONSUMPTION4' => (int) substr($billDetails['lastSixMonthConsumption'], 18, 6), // Calculate "Verify"
      //           'PREVCYCLECONSUMPTION5' => (int) substr($billDetails['lastSixMonthConsumption'], 24, 6), // Calculate "Verify"
      //           'PREVCYCLECONSUMPTION6' => (int) substr($billDetails['lastSixMonthConsumption'], 30, 6), // Calculate "Verify"
      //           'AMTPAID' => "0",
      //           'AMTDATE' => "",
      //           'WEBCCCR' => "", // Verify
      //           'FINANCIALYEAR' => $fnYear,  // 2018-19
      //           'SPOTREC' => "S", // Verify
      //           'CURRIDF' => $newBill['blCurrentIDFWithSign'], // Verify
      //           'CCOWCESS' => $newBill['blCurrentCowCessWithSign'], // Verify
      //           'CWATCESS' => $newBill['blCurrentWaterSewageChargeWithSign'],
      //           'TOTIDF' => $newBill['bltotalIDFWithSign'],
      //           'TCOWCESS' => $newBill['blTotalCowCessWithSign'],
      //           'TWATCESS' => $newBill['blTotalCurrentWaterSewarageChargeWithSign'],
      //           'EXCOL' => "1",
      //           'USERID' => "STRTECH",
      //           'PASSCODE' => "STR@1PSPCL",
      //           'VID' => "1"
      //           ]
      //         ]
      //
      //         );
      //
      //     $resultt = $client->Push_NonSAP_Below10kw_Data($params);
      //   //   dd($result);
      //     // $result = json_encode($result);
      //     //
      //     // return $result;
      //
      //     $result['soap'] = $resultt;
      //     // return 'ppppp';
      //     // return dd($result['soap']->Push_NonSAP_Below10kw_DataResult->NonSAP_ResponseParams);
      //     $updateSoap = Billed::find($newBill['id']);
      //     $updateSoap->soapStatus = $result['soap']->Push_NonSAP_Below10kw_DataResult->NonSAP_ResponseParams->STATUS;
      //     $updateSoap->soapMessage = $result['soap']->Push_NonSAP_Below10kw_DataResult->NonSAP_ResponseParams->MSG;
      //     $updateSoap->soapCount = $updateSoap->soapCount + 1;
      //     $updateSoap->save();
      //     // return $updateSoap;
      //     // return $result;
      //
      //   //   $xml = simplexml_load_string($result->NonSAP_ResponseParams);
      //   //   $res = $xml->STATUS;
      //   //   return $xml;
      //   //   echo "<pre>" ;
      //   //   foreach ($xml as $key => $value)
      //   //   {
      //   //     $key . " = " .$value . PHP_EOL;
      //   //       // foreach($value as $ekey => $eValue)
      //   //       // {
      //   //       //     print($ekey . " = " . $eValue . PHP_EOL);
      //
      //   //       // }
      //   //   }
      //
      // } catch ( SoapFault $fault ) {
      //   // echo "error";
      //   return $result;
      //     trigger_error ( "SOAP Fault: (faultcode: {$fault->faultcode}, faultstring: {$fault->faultstring})", E_USER_ERROR );
      // }
      // } else {
      //   // return dd($newBill->blPresentReadingDate);
      //     try {
      //     // $client = new SoapClient ( "some.aspx?WSDL" );
      //     $client = new SoapClient("https://billingdatareceiver.pspcl.in/NONSAP_SBM_DATA.ASMX?wsdl");
      //     $blPresentReadingDate = Carbon\Carbon::createFromFormat('d/m/Y', $newBill['blPresentReadingDate'])->format('Y-m-d');
      //     $blPresentReadingTime = 'T'.'00:00:00';
      //     $time = $newBill['blTime'];
      //     $timeS1 = substr($time, 0, 2);
      //     $timeS2 = substr($time, 2, 2);
      //     $timeS3 = substr($time, 4, 2);
      //     $blTime = $timeS1.':'.$timeS2.':'.$timeS3;
      //     $blPresentReadingDate = $blPresentReadingDate.'T'.$blTime;
      //
      //     $blDueDateByCash = Carbon\Carbon::createFromFormat('d-m-Y', $newBill['blDueDateByCash'])->format('Y-m-d');
      //     $blDueTimeByCash = 'T'.'00:00:00';
      //     $blDueDateByCash = $blDueDateByCash.$blDueTimeByCash;
      //
      //     $blDueDateByCheque = Carbon\Carbon::createFromFormat('d-m-Y', $newBill['blDueDateByCheque'])->format('Y-m-d');
      //     $blDueTimeByCheque = 'T'.'00:00:00';
      //     $blDueDateByCheque = $blDueDateByCheque.$blDueTimeByCheque;
      //
      //     $blcurrentReadingDate = Carbon\Carbon::createFromFormat('d/m/Y', $newBill['blPresentReadingDate'])->format('d-m-Y');
      //     $blpreviousReadingDate = Carbon\Carbon::createFromFormat('d/m/Y', $billDetails['previousReadingDate'])->format('d-m-Y');
      //     $billNoDate = Carbon\Carbon::createFromFormat('d/m/Y', $newBill['blPresentReadingDate'])->format('dmy');
      //     // return $billNoDate;
      //     $billNo = $newBill['billNo'];
      //     $adjAmt = 0;
      //     if (
      //       $billDetails['perviousCode'] == 'L' || $billDetails['perviousCode'] == 'N' || $billDetails['perviousCode'] == "I"
      //       && $newBill['blPresentStatus'] == 'O' || $newBill['blPresentStatus'] == 'C' || $newBill['blPresentStatus'] == 'H' ||
      //       $newBill['blPresentStatus'] == 'X' || $newBill['blPresentStatus'] == 'T' ||
      //       $newBill['blPresentStatus'] == 'W' || $newBill['blPresentStatus'] == 'A'
      //     ) {
      //       $adjAmt = $billDetails['averageAdjustmentSOP'] + $billDetails['averageAdjustmentED'] + $billDetails['averageAdjustmentOctroi'] + $billDetails['avgAdjustmentIDF'] + $billDetails['avgAdjustmentCowCess'] + $billDetails['avgAdjustmentWaterSewarageChargesCode'];
      //     } else {
      //       $adjAmt = 0;
      //     }
      //     // $adjAmt = $billDetails['averageAdjustmentSOP'] + $billDetails['averageAdjustmentED'] + $billDetails['averageAdjustmentOctroi'] + $billDetails['avgAdjustmentIDF'] + $billDetails['avgAdjustmentCowCess'] + $billDetails['avgAdjustmentWaterSewarageChargesCode'];
      //     $SUNDARYCHARGES = $billDetails['sundarySOP'] + $billDetails['sundaryED'] + $billDetails['sundaryOctroi'] + $billDetails['sundryIDF'] + $billDetails['sundryCowCess'] + $billDetails['sundryWaterSewarageChargesCode'];
      //     $SUNDARYALLOWANCE = $billDetails['allowanceSOP'] + $billDetails['allowanceED'] + $billDetails['allowanceOctroi'] + $billDetails['allowanceIDF'] + $billDetails['allowanceCowCess'] + $billDetails['allowanceWaterSewarageChargesCode'];
      //
      //     // Tariff type Converted to 3 digits as per telephonic conversation with it team on 24-07-2018 4-20 Pm
      //     $tariffType = '';
      //     if ($newBill->blTariffType == 1) {
      //       $tariffType = "DS";
      //     } elseif ($newBill->blTariffType == 3) {
      //       $tariffType = "BE";
      //     } elseif ($newBill->blTariffType == 5) {
      //       $tariffType = "WSD";
      //     } elseif ($newBill->blTariffType == 6) {
      //       $tariffType = "BPL";
      //     } elseif ($newBill->blTariffType == 0) {
      //       $tariffType = "BO";
      //     } elseif ($newBill->blTariffType == 2) {
      //       $tariffType = "NRS";
      //     } elseif ($newBill->blTariffType == 4) {
      //       $tariffType = "RLY";
      //     } elseif ($newBill->blTariffType == 8) {
      //       $tariffType = "CGO";
      //     }
      //     $params = array(
      //       'reqArr' => [
      //         'NonSAP_RequestParams_Above10kw' => [
      //           'ACCOUNTNO' => $newBill['blMasterKey'],
      //           'NAME' => $newBill['blConsumerName'],
      //           'ADDRESS' => $billDetails['consumerAddress'], // From Bills Table
      //           'ISSUEDATE' => $blPresentReadingDate,
      //           'DUEDATECASH' => $blDueDateByCash, // Need to format
      //           'DUEDATECHEQUE' => $blDueDateByCheque, // Need to format
      //           'VILLCITYNAME' => "",
      //           'SUBDIVNAME' => $billDetails['subDivisionName'], // Form Bills Table
      //           'BILLNO' => $billNo, // Need to Generate
      //           'CURRENTREADINGDATE' => $blcurrentReadingDate,
      //           'PREVREADINGDATE' => $blpreviousReadingDate, // Frm bills Table
      //           'BILLPERIOD' => $newBill['blDays'],
      //           'TARRIFTYPE' => $tariffType,
      //           'CONNECTEDLOAD' => $billDetails['connectedLoadInKW'], // Verify
      //           'PREVBILLSTATUS' => '', //$billDetails['perviousCode'], // From bills Table
      //           'METERNO' => $billDetails['meterNo'], // From bills Table
      //           'SECURITYAMT' => $billDetails['securityDeposit'], // From bills Table
      //           'METERSTATUS' => $newBill['blPresentStatus'],
      //           'BILLCYCLE' => $newBill['blBillingCycle'],
      //           'BILLGROUP' => $newBill['blBillingGroup'],
      //           'METERREADINGCURRENT' => $newBill['blPresentReading'], // Verify
      //           'METERREADINGPREV' => $billDetails['perviousReading'], // From bills Table "Verify"
      //           'LINECTRATIO' => $billDetails['lineCtRatio'], // From Bills Table
      //           'METERCTRATIO' => $billDetails['meterCtRatio'], // From Bills Table
      //           'METERMULTIPLIER' => $billDetails['meterMultiplier'], // From Bills Table
      //           'OVERMETERMULTIPLIER' => $billDetails['overallMultiplingFactor'], // From Bills Table
      //           'METERCODE' => $newBill['blPresentStatus'], // Verify
      //           'UNITSCONSUMEDNEW' => $newBill['blUnitsConsumedNew'],
      //           'UNITSCONSUMEDOLD' => $newBill['blUnitsConsumedOld'],
      //           'TOTALUNITSCONSUMED' => $newBill['blUnitsConsumed'],
      //           'CURRENTSOP' => $newBill['blCurrentSop'],
      //           'CURRENTED' => $newBill['blCurrentEd'],
      //           'CURRENTOCTROI' => $newBill['blCurrentOctroi'],
      //           'METERRENT' => $newBill['blCurrentMeterRent'],
      //           'SERVICERENT' => $newBill['blCurrentServiceRent'],
      //           'TOTALADJAMT' => $adjAmt, // From Bills Table
      //           'TOTALADJPERIOD' => $billDetails['periodOfAdjustmentDetail'], // From Bills TAble
      //           'TOTALADJREASON' => $billDetails['reason'], // From Bills TAble
      //           'CONCLUNITS' => $newBill['blConcessionalUnits'], // Verify
      //           'FIXEDCHARGES' => $newBill['blCurrentFixedChargesWithSign'], // Verify
      //           'FUELCOSTCHARGES' => $newBill['blCurrentFCAChargesWithSign'], // Verify
      //           'VOLTAGECLASSCHARGES' => '0', // From Bills Table
      //           'PREVTOTALARREARS' => $billDetails['previousArrear'], // From Bills Table
      //           'CURRENTTOTALARREARS' => $billDetails['currentArrear'], // From Bills Table
      //           'OTHERCHARGES' => $billDetails['others'], // Verify
      //           'SUNDARYCHARGES' => $SUNDARYCHARGES, // Verify // Add field in Database
      //           'SUNDARYALLOWANCES' => $SUNDARYALLOWANCE, // Verify
      //           'CURRENTROUNDINGAMT' => $newBill['blCurrentRoundingAmt'],
      //           'PREVROUNDAMT' => $billDetails['previousRoundingAmount'], // From Bills Table
      //           'TOTALNETSOP' => $newBill['blNetSop'],
      //           'TOTALNETED' => $newBill['blNetEd'],
      //           'TOTALNETOCTROI' => $newBill['blNetOctroi'],
      //           'TOTALAMT' => $newBill['blAmountBeforeDue'],
      //           'TOTALSURCHARGE' => $newBill['blSurCharge'],
      //           'TOTALAMTGROSS' => $newBill['blAmountAfterDue'],
      //           'BILLYEAR' => Date('Y'),
      //           'SUNDARYMESSAGE' => $billDetails['sundaryChargeDetail'], // From Bills Table
      //           'SUNDARYDATEFROM' => "", // Verify
      //           'SUNDARYDATETO' => "", // Verify
      //           'LINE1' => "", // Verify
      //           'LINE2' => "", // Verify
      //           'TOTALSURCHARGES' => "", // Verify
      //           'TOTALGROSS' => "", // Verify
      //           'DUEDATECASH2' => "", // Verify
      //           'TOTALSURCHARGE2' => "", // Verify
      //           'TOTALGROSS2' => "", // Verify
      //           'CONSUMPTIONCYCLE1' => (int) substr($billDetails['lastSixMonthConsumption'], 0, 6), // Calculate "Verify"
      //           'CONSUMPTIONCYCLE2' => (int) substr($billDetails['lastSixMonthConsumption'], 6, 6), // Calculate "Verify"
      //           'CONSUMPTIONCYCLE3' => (int) substr($billDetails['lastSixMonthConsumption'], 12, 6), // Calculate "Verify"
      //           'CONSUMPTIONCYCLE4' => (int) substr($billDetails['lastSixMonthConsumption'], 18, 6), // Calculate "Verify"
      //           'CONSUMPTIONCYCLE5' => (int) substr($billDetails['lastSixMonthConsumption'], 24, 6), // Calculate "Verify"
      //           'CONSUMPTIONCYCLE6' => (int) substr($billDetails['lastSixMonthConsumption'], 30, 6), // Calculate "Verify"
      //           'AMTPAID' => "0",
      //           'AMTDATE' => "",
      //           'WEBCCCR' => "", // Verify
      //           'FINYEAR' => $fnYear,  // 2018-19
      //           'CURRIDF' => $newBill['blCurrentIDFWithSign'], // Verify
      //           'CCOWCESS' => $newBill['blCurrentCowCessWithSign'], // Verify
      //           'CWATCESS' => $newBill['blCurrentWaterSewageChargeWithSign'],
      //           'TOTIDF' => $newBill['bltotalIDFWithSign'],
      //           'TCOWCESS' => $newBill['blTotalCowCessWithSign'],
      //           'TWATCESS' => $newBill['blTotalCurrentWaterSewarageChargeWithSign'],
      //           'EXCOL' => "1",
      //           'USERID' => "STRTECH",
      //           'PASSCODE' => "STR@1PSPCL",
      //           'VID' => "1"
      //           ]
      //         ]
      //
      //         );
      //
      //     $resultt = $client->Push_NonSAP_Above10kw_Data($params);
      //   //   dd($result);
      //   $result['soap'] = $resultt;
      //   // return 'ppppp';
      //   // return dd($result['soap']->Push_NonSAP_Above10kw_DataResult->NonSAP_ResponseParams);
      //   $updateSoap = Billed::find($newBill['id']);
      //   $updateSoap->soapStatus = $result['soap']->Push_NonSAP_Above10kw_DataResult->NonSAP_ResponseParams->STATUS;
      //   $updateSoap->soapMessage = $result['soap']->Push_NonSAP_Above10kw_DataResult->NonSAP_ResponseParams->MSG;
      //   $updateSoap->soapCount = $updateSoap->soapCount + 1;
      //   $updateSoap->save();
      //   // return $updateSoap;
      //   // return $result;
      //
      //   //   $xml = simplexml_load_string($result->NonSAP_ResponseParams);
      //   //   $res = $xml->STATUS;
      //   //   return $xml;
      //   //   echo "<pre>" ;
      //   //   foreach ($xml as $key => $value)
      //   //   {
      //   //     $key . " = " .$value . PHP_EOL;
      //   //       // foreach($value as $ekey => $eValue)
      //   //       // {
      //   //       //     print($ekey . " = " . $eValue . PHP_EOL);
      //
      //   //       // }
      //   //   }
      //
      // } catch ( SoapFault $fault ) {
      //   // echo "error";
      //   return $result;
      //     trigger_error ( "SOAP Fault: (faultcode: {$fault->faultcode}, faultstring: {$fault->faultstring})", E_USER_ERROR );
      // }
      // }

  } else {

        $result['aws_status'] = "Y";

  }

  return $result;
});

Route::middleware('auth:api')->post('/day-close', function (Request $request) {

  $userDetails = $request->user();
  $closeDate = Date('d-m-Y');
  $closeTime = Date('H:i:s');
  $lat = $request->lat;
  $long = $request->long;

  $log = userLog::where('user', $userDetails->id)->orderBy('id', 'DESC')->take(1)
                  ->update([
                    'dayCloseDate' => $closeDate,
                    'dayCloseTime' => $closeTime,
                    'dayCloseLat' => $lat,
                    'dayCloseLong' => $long,
                    'status' => "C",
                  ]);
  // return $log;
  // $log->dayCloseDate = $closeDate;
  // $log->dayCloseTime = $closeTime;
  // $log->dayCloseLat = $lat;
  // $log->dayCloseLong = $long;
  // $log->status = "C";
  // $log->update();

  if ($log) {
    $message['status'] = "Y";
  } else {
    $message['status'] = "N";
  }

  return $message;

});

Route::middleware('auth:api')->post('/in-complete-data', function (Request $request) {
  $userDetails = $request->user();
  $userId = $userDetails->email.',';
  $implode = explode(",", $request->ids);
  if($userDetails->userGroup == "non sap") {
    // $billUpdate = Bill::whereIn('id', $implode)->update(['status' => 0, 'meterReader' => 0]);
    $billUpdate = DB::update("UPDATE bills
                              SET received_by = REPLACE(received_by, '$userId', ''), meterReader = REPLACE(meterReader, '$userId', '')
                              WHERE find_in_set('$userDetails->email', received_by)");
  } else {
    // $billUpdate = sapInput::whereIn('id', $implode)->update(['status' => 0, 'meterReader' => 0]);
    $billUpdate = DB::update("UPDATE sap_inputs
                              SET received_by = REPLACE(received_by, '$userId', ''), meterReader = REPLACE(meterReader, '$userId', '')
                              WHERE find_in_set('$userDetails->email', received_by)");
  }

    if ($billUpdate) {
      $message['status'] = 'success';
    } else {
      $message['status'] = 'error';
    }

    return $message;

});

Route::middleware('auth:api')->post('/received-data-non-sap-new', function (Request $request) {
  $userDetails = $request->user();
  $received_at = Date('Y-m-d H:i:s');
    $implode = $request->ids;
    // return $implode;
    // $billUpdate = Bill::whereIn('id', $implode)->update(['received_by' => $userDetails->email, 'received_at' => $received_at]);
    $getMeterReaders = DB::select("SELECT id, received_by FROM bills where id in ($implode)");
    // return $getMeterReaders;
    foreach ($getMeterReaders as $updateReader) {
      $newEmail = $userDetails->email.',';
      // $billUpdate = DB::update("UPDATE bills SET received_by = CONCAT('$updateReader', '$userDetails->email'), where id = '$updateReader->id' and not find_in_set('$userDetails->email', meterReader) <> 0");
      $billUpdate = DB::update("UPDATE bills SET received_by = CONCAT('$updateReader->received_by', '$newEmail') where id = $updateReader->id");
    }
    // return $billUpdate;
    // return $getMeterReaders[0]->meterReader;
    // $meterReader = $getMeterReaders[0]->meterReader;
    //  $searchBills = DB::update("UPDATE bills SET meterReader = CONCAT('$meterReader', '$meterReaderNew'), assigned_by = '$assigned_by', assigned_at = '$assigned_at', a_count = a_count + 1, binding_status = 'Y' where SUBSTRING(masterKey, 1, 3) = '$request->subDivisionCode' and billingGroup = '$request->billingGroup' and billingCycle = '$request->billingCycle' and SUBSTRING(masterKey, 4, 4) = '$request->ledgerCode' and not find_in_set('$meterReaderFound', meterReader) <> 0");

    if ($billUpdate) {
      $message['status'] = 'success';
    } else {
      $message['status'] = 'error';
    }

    return $message;

});

Route::middleware('auth:api')->post('/received-data-sap-new', function (Request $request) {
  // $userDetails = $request->user();
  // $received_at = Date('Y-m-d H:i:s');
  //   $implode = explode(",", $request->ids);
  //   $billUpdate = sapInput::whereIn('id', $implode)->update(['received_by' => $userDetails->email, 'received_at' => $received_at]);

    $userDetails = $request->user();
    $received_at = Date('Y-m-d H:i:s');
      $implode = $request->ids;
      $getMeterReaders = DB::select("SELECT id, received_by FROM sap_inputs where id in ($implode)");
      foreach ($getMeterReaders as $updateReader) {
        $newEmail = $userDetails->email.',';
        $billUpdate = DB::update("UPDATE sap_inputs SET received_by = CONCAT('$updateReader->received_by', '$newEmail') where id = $updateReader->id");
      }

    if ($billUpdate) {
      $message['status'] = 'success';
    } else {
      $message['status'] = 'error';
    }

    return $message;

});

Route::middleware('auth:api')->post('/received-data-sap', function (Request $request) {

    $userDetails = $request->user();
    $received_at = Date('Y-m-d H:i:s');
      // $schedules = DB::select("SELECT * FROM SAP_SBA_schedules WHERE DataDownloadFlag = 'N' AND meterReader = '$userDetails->email'");
      // $assignedCoount = 0;
      // foreach ($schedules as $cnt) {
      //   $assignedCoount += $cnt->TotalCons;
      // }
      // $assignedCoount = $assignedCoount;
      $implode = $request->ids;
      // return $implode;
      // $explode = explode(',', $implode);
      // $receivedCoount = count($explode);
        $date = Date('d-m-Y');
        $time = Date('H:s:i');
        $updateRecords = DB::update("UPDATE sap_sba_schedule_info set downloadflag = 'Y', dwndate = '$date', dwntime = '$time' WHERE id in ($implode) and meterreader = '$userDetails->email'");
        // return $updateRecords;
        if ($updateRecords) {
          $schedulesUpdate = DB::select("UPDATE SAP_SBA_schedules set DataDownloadFlag = 'Y', DownloadedDate = '$date', DownloadedTime = '$time' WHERE DataDownloadFlag = 'N' AND meterReader = '$userDetails->email'");
          $message['status'] = 'success';
        } else {
          $message['status'] = 'error';
        }



    return $message;

});

Route::middleware('auth:api')->post('/received-data-non-sap', function (Request $request) {

    $userDetails = $request->user();
    $received_at = Date('Y-m-d H:i:s');
      $schedules = DB::select("SELECT * FROM NONSAP_SBA_schedules WHERE DataDownloadFlag = 'N' AND meterReader = '$userDetails->email'");
      $assignedCoount = 0;
      foreach ($schedules as $cnt) {
        $assignedCoount += $cnt->TotalCons;
      }
      $assignedCoount = $assignedCoount;
      $implode = $request->ids;

      $explode = explode(',', $implode);
      $receivedCoount = count($explode);
        $date = Date('d-m-Y');
        $time = Date('H:s:i');
        $updateRecords = DB::update("UPDATE nonsap_sba_schedule_info set downloadflag = 'Y', dwndate = '$date', dwntime = '$time' WHERE mainid in ($implode) and meterreader = '$userDetails->email'");

        if ($updateRecords) {
          $schedulesUpdate = DB::select("UPDATE NONSAP_SBA_schedules set DataDownloadFlag = 'Y', DownloadedDate = '$date', DownloadedTime = '$time' WHERE DataDownloadFlag = 'N' AND meterReader = '$userDetails->email'");
          $message['status'] = 'success';
        } else {
          $message['status'] = 'error';
        }



    return $message;

});

Route::middleware('auth:api')->post('/change-password', function (Request $request) {

    $userDetails = $request->user();

    if (Hash::check($request->password, $userDetails->password)) {
        $updateUser = User::find($userDetails->id)->update(['password' => Hash::make($request->newPassword)]);
        if ($updateUser) {
      $message['status'] = 'success';
    } else {
      $message['status'] = 'error';
    }
    } else {
        $message['status'] = 'wrong_credientials';
    }


      $newPassword = $request->password;



    return $message;

});

Route::middleware('auth:api')->post('/forget-password', function (Request $request) {

    $userDetails = $request->user();

      $newRequest = new resetRequest();
      $newRequest->imei = $request->imei;
      $newRequest->save();

    if ($newRequest != '') {
      $message['status'] = 'success';
    } else {
      $message['status'] = 'error';
    }

    return $message;

});

// Version Check
// Sap
Route::middleware('auth:api')->post('/version', function (Request $request) {


      $app = $request->app;
      $v = $request->v;
      if ($app == "sap") {
        $versionCheck = sapVersion::all()->sortByDesc("id")->take(1);
        if ($versionCheck->first()->version == $v) {
          $message['status'] = "n";
        } else {
          $message['status'] = "y";
        }
      } elseif ($app == "non-sap") {
        $versionCheck = nonSapVersion::all()->sortByDesc("id")->take(1);
        if ($versionCheck->first()->version == $v) {
          $message['status'] = "N";
        } else {
          $message['status'] = "Y";
        }
      }

    return $message;

});

Route::middleware('auth:api')->post('/getGroupCycleAjaxSap', function (Request $request) {

      $subDivisionCode = $request->subDivisionCode;
      $billingsGroups = DB::select("select distinct billingGroup from sap_inputs WHERE subDivisionCode in ('$subDivisionCode') group by billingGroup");
      $billingsCycles = DB::select("select distinct billingCycle from sap_inputs WHERE subDivisionCode in ('$subDivisionCode') group by billingCycle");
      $ledgerCodes = DB::select("SELECT distinct MRU FROM sap_inputs WHERE subDivisionCode in ('$subDivisionCode') group by MRU");
      $data = [
        'billingGroup' => $billingsGroups
      ];
      return $data;

});

// SAP Get Billing Cycles
Route::middleware('auth:api')->post('/getGroupCycleAjaxSapBc', function (Request $request) {

      $subDivisionCode = $request->subDivisionCode;
      $billingGroup = $request->billingGroup;
      // $billingsGroups = DB::select("select distinct billingGroup from sap_inputs WHERE subDivisionCode in ('$subDivisionCode') group by billingGroup");
      $billingsCycles = DB::select("select distinct billingCycle from sap_inputs WHERE subDivisionCode in ('$subDivisionCode') AND billingGroup in ($billingGroup) group by billingCycle");
      // $ledgerCodes = DB::select("SELECT distinct MRU FROM sap_inputs WHERE subDivisionCode in ('$subDivisionCode') group by MRU");
      $data = [
        'billingCycle' => $billingsCycles
      ];
      return $data;

});

// Get Ledger Code
Route::middleware('auth:api')->post('/getGroupCycleAjaxSapLc', function (Request $request) {

      $subDivisionCode = $request->subDivisionCode;
      $billingGroup = $request->billingGroup;
      $billingCycle = $request->billingCycle;
      // $billingsGroups = DB::select("select distinct billingGroup from sap_inputs WHERE subDivisionCode in ('$subDivisionCode') group by billingGroup");
      // $billingsCycles = DB::select("select distinct billingCycle from sap_inputs WHERE subDivisionCode in ('$subDivisionCode') AND billingGroup in ($billingGroup) group by billingCycle");
      // $ledgerCodes = DB::select("SELECT distinct MRU FROM sap_inputs WHERE subDivisionCode in ('$subDivisionCode') AND billingGroup in ($billingGroup) AND billingCycle in ($billingCycle) AND bindingStatus = 'N' group by MRU");
      $ledgerCodes = DB::select("SELECT distinct MRU FROM sap_inputs WHERE subDivisionCode in ('$subDivisionCode') AND billingGroup in ($billingGroup) AND billingCycle in ($billingCycle) group by MRU");
      $data = [
        'ledgerCode' => $ledgerCodes
      ];
      return $data;

});

// Get Ledger Code For Normal Binding
Route::middleware('auth:api')->post('/getGroupCycleAjaxSapLcNb', function (Request $request) {

      $subDivisionCode = $request->subDivisionCode;
      $billingGroup = $request->billingGroup;
      $billingCycle = $request->billingCycle;
      // $billingsGroups = DB::select("select distinct billingGroup from sap_inputs WHERE subDivisionCode in ('$subDivisionCode') group by billingGroup");
      // $billingsCycles = DB::select("select count(*) count from sap_inputs WHERE subDivisionCode in ('$subDivisionCode') AND billingGroup in ($billingGroup) and billingCycle in ($billingCycle) and MRU in ()");
      $ledgerCodes = DB::select("SELECT distinct MRU FROM sap_inputs WHERE subDivisionCode in ('$subDivisionCode') AND billingGroup in ($billingGroup) AND billingCycle in ($billingCycle) group by MRU");
      $data = [
        'ledgerCode' => $ledgerCodes
      ];
      return $data;

});

// Get Account Number Min And Max Code For Normal Binding
Route::middleware('auth:api')->post('/getGroupCycleAjaxSapAc', function (Request $request) {

      $ledgerCode = $request->ledgerCode;
      // $billingsGroups = DB::select("select distinct billingGroup from sap_inputs WHERE subDivisionCode in ('$subDivisionCode') group by billingGroup");
      // $billingsCycles = DB::select("select distinct billingCycle from sap_inputs WHERE subDivisionCode in ('$subDivisionCode') AND billingGroup in ($billingGroup) group by billingCycle");
      $accountNumbers = DB::select("select min(sap_inputs.contractAcNumber) minAc, max(sap_inputs.contractAcNumber) maxAc from sap_inputs where mru = '$ledgerCode' AND bindingStatus = 'N'");
      $data = [
        'accountNumbers' => $accountNumbers
      ];
      return $data;

});

// Non SAP
Route::middleware('auth:api')->post('/getGroupCycleAjaxNonSap', function (Request $request) {



      $subDivisionCode = $request->subDivisionCode;
      $billingGroups = DB::select("select distinct billingGroup from bills WHERE SUBSTRING(masterKey, 1, 3) in ('$subDivisionCode') group by billingGroup");
      $billingCycles = DB::select("select distinct billingCycle from bills WHERE SUBSTRING(masterKey, 1, 3) in ('$subDivisionCode') group by billingCycle");
      $ledgerCodes = DB::select("SELECT SUBSTRING(masterKey, 4, 4) ledgerCode FROM bills WHERE SUBSTRING(masterKey, 1, 3) in ('$subDivisionCode') group by SUBSTRING(masterKey, 4, 4)");
      // $billingsGroups = DB::select("select distinct billingGroup from bills WHERE subDivisionCode in ('$subDivisionCode') group by billingGroup");
      // $billingsCycles = DB::select("select distinct billingCycle from bills WHERE subDivisionCode in ('$subDivisionCode') group by billingCycle");
      // $ledgerCodes = DB::select("SELECT distinct MRU FROM sap_inputs WHERE subDivisionCode in ('$subDivisionCode') group by MRU");
      $data = [
        'billingGroup' => $billingGroups
        // 'billingCycle' => $billingCycles,
        // 'ledgerCodes' => $ledgerCodes
      ];
      return $data;

});

// Gt billingGroup Ajax
Route::middleware('auth:api')->post('/getGtBillingGroupAjax', function (Request $request) {

      $subDivisionCode = $request->subDivisionCode;
      $billingGroups = DB::select("select distinct billingGroup from bills WHERE SUBSTRING(masterKey, 1, 3) in ('$subDivisionCode') and billingGroup = 8 group by billingGroup");
      $data = [
        'billingGroup' => $billingGroups
      ];
      return $data;

});
// Bc
Route::middleware('auth:api')->post('/getGroupCycleAjaxNonSapBc', function (Request $request) {

      $subDivisionCode = $request->subDivisionCode;
      $billingGroup = $request->billingGroup;
      // $billingGroups = DB::select("select distinct billingGroup from bills WHERE SUBSTRING(masterKey, 1, 3) in ('$subDivisionCode') group by billingGroup");
      $billingCycles = DB::select("select distinct billingCycle from bills WHERE SUBSTRING(masterKey, 1, 3) in ('$subDivisionCode') and billingGroup in ('$billingGroup') group by billingCycle");
      // $ledgerCodes = DB::select("SELECT SUBSTRING(masterKey, 4, 4) ledgerCode FROM bills WHERE SUBSTRING(masterKey, 1, 3) in ('$subDivisionCode') group by SUBSTRING(masterKey, 4, 4)");
      // $billingsGroups = DB::select("select distinct billingGroup from bills WHERE subDivisionCode in ('$subDivisionCode') group by billingGroup");
      // $billingsCycles = DB::select("select distinct billingCycle from bills WHERE subDivisionCode in ('$subDivisionCode') group by billingCycle");
      // $ledgerCodes = DB::select("SELECT distinct MRU FROM sap_inputs WHERE subDivisionCode in ('$subDivisionCode') group by MRU");
      $data = [
        // 'billingGroup' => $billingGroups,
        'billingCycle' => $billingCycles
        // 'ledgerCodes' => $ledgerCodes
      ];
      return $data;

});
// Lc
Route::middleware('auth:api')->post('/getGroupCycleAjaxNonSapLc', function (Request $request) {

      $subDivisionCode = $request->subDivisionCode;
      $billingGroup = $request->billingGroup;
      $billingCycle = $request->billingCycle;
      // $billingGroups = DB::select("select distinct billingGroup from bills WHERE SUBSTRING(masterKey, 1, 3) in ('$subDivisionCode') group by billingGroup");
      // $billingCycles = DB::select("select distinct billingCycle from bills WHERE SUBSTRING(masterKey, 1, 3) in ('$subDivisionCode') and billingGroup in ('$billingGroup') group by billingCycle");
      $ledgerCodes = DB::select("SELECT SUBSTRING(masterKey, 4, 4) ledgerCode FROM bills WHERE SUBSTRING(masterKey, 1, 3) in ('$subDivisionCode') and billingGroup in ('$billingGroup') and billingCycle in ('$billingCycle') group by SUBSTRING(masterKey, 4, 4)");
      // $billingsGroups = DB::select("select distinct billingGroup from bills WHERE subDivisionCode in ('$subDivisionCode') group by billingGroup");
      // $billingsCycles = DB::select("select distinct billingCycle from bills WHERE subDivisionCode in ('$subDivisionCode') group by billingCycle");
      // $ledgerCodes = DB::select("SELECT distinct MRU FROM sap_inputs WHERE subDivisionCode in ('$subDivisionCode') group by MRU");
      $data = [
        'ledgerCodes' => $ledgerCodes
      ];
      return $data;

});

// Get Account Number Min And Max Code For Normal Binding
Route::middleware('auth:api')->post('/getGroupCycleAjaxNonSapAc', function (Request $request) {

      $ledgerCode = $request->ledgerCode;
      // $billingsGroups = DB::select("select distinct billingGroup from sap_inputs WHERE subDivisionCode in ('$subDivisionCode') group by billingGroup");
      // $billingsCycles = DB::select("select distinct billingCycle from sap_inputs WHERE subDivisionCode in ('$subDivisionCode') AND billingGroup in ($billingGroup) group by billingCycle");
      $accountNumbers = DB::select("select min(bills.accountNumber) minAc, max(bills.accountNumber) maxAc from bills where substr(masterKey, 4, 4) = '$ledgerCode'");
      $data = [
        'accountNumbers' => $accountNumbers
      ];
      return $data;

});

// Bind Complete Ledger SAP
Route::middleware('auth:api')->post('/bindLedgerSap', function (Request $request) {
  $userDetails = $request->user();
  $data  = [];
//   $log = new Bind();
//   $log->meterReader = $request->meterReader;
//   $log->accountNoG = '';
//   $log->accountNoL = '';
//   $log->subDivisionCode = $request->subDivisionCode;
//   $log->billingCycle = $request->billingCycle;
//   $log->billingGroup = $request->billingGroup;
//   $log->ledgerCode = $request->ledgerCode;
//   $log->user_id = $userDetails->id;
//   $log->save();

  $assigned_by = $userDetails->id;
  $assigned_at = Date('Y-m-d H:i:s');

  $meterReader = User::find($request->meterReader);

  $meterReaderFound = $meterReader->email;
//   return $meterReaderFound;

//   if ($log) {
    // return $request;
    $meterReaderNew = $meterReaderFound . ',';
    $getMeterReaders = sapInput::where('mru', $request->ledgerCode)
                                ->where('billingGroup', $request->billingGroup)
                                ->where('billingCycle', $request->billingCycle)
                                ->where('subDivisionCode', $request->subDivisionCode)->get();
                                // return $getMeterReaders;
    $meterReader = $getMeterReaders->first()->meterReader;
    // return $meterReaderFound;
    // $searchBills = DB::update("UPDATE sap_inputs SET meterReader = '$meterReader->email', assigned_by = '$assigned_by', assigned_at = '$assigned_at', bindingStatus = 'Y', a_count = a_count + 1 where mru = '$request->ledgerCode' and billingGroup = '$request->billingGroup' and billingCycle = '$request->billingCycle' and subDivisionCode = '$request->subDivisionCode' AND bindingStatus = 'N'");
    $preCheck = DB::select("select count(*) count from sap_inputs where mru = '$request->ledgerCode' and billingGroup = '$request->billingGroup' and billingCycle = '$request->billingCycle' and subDivisionCode = '$request->subDivisionCode' and find_in_set('$meterReaderFound', meterReader) <> 0 and bill_status = 'N'");
    // return $preCheck;
    if ($preCheck[0]->count >= 1) {
      $data['status'] = 'A';
    } else {
      $searchBills = DB::update("UPDATE sap_inputs SET meterReader = CONCAT('$meterReader', '$meterReaderNew'), assigned_by = '$assigned_by', assigned_at = '$assigned_at', bindingStatus = 'Y', a_count = a_count + 1 where mru = '$request->ledgerCode' and billingGroup = '$request->billingGroup' and billingCycle = '$request->billingCycle' and subDivisionCode = '$request->subDivisionCode' and not find_in_set('$meterReaderFound', meterReader) <> 0 and bill_status = 'N'");
      if ($searchBills) {
        $data['status'] = 'Y';
      } else {
        $data['status'] = 'N';
      }
    }

    // return $searchBills;
    // $affectedRows = DB::select("SELECT id FROM sap_inputs where mru = '$request->ledgerCode' and billingGroup = '$request->billingGroup' and billingCycle = '$request->billingCycle' and subDivisionCode = '$request->subDivisionCode'");

    // return $searchBills->result();
//   }

  // return $searchBills->success();



  return $data;

});

// Bind Complete Ledger SAP New
Route::middleware('auth:api')->post('/bindLedgerSapNew', function (Request $request) {
  // return 'pppp';
  $userDetails = $request->user();
  $data  = [];

  $ScheduledUser = $userDetails->name;
  $ScheduleDate = Date('d-m-Y');
  $Scheduletime = Date('H:i:s');

  $meterReader = User::find($request->meterReader);

  $meterReaderFound = $meterReader->email;
//   return $meterReaderFound;
$sub = $request->subDivisionCode;
$bg = $request->billingGroup;
$bc = $request->billingCycle;
$lc = $request->ledgerCode;
$getMeterReaders = DB::select("SELECT meterReader from SAP_SBA_schedules WHERE subDivisionCode = '$sub' and billingGroup = '$bg' and billingCycle = '$bc' and LedgerCode = '$lc' and meterReader = '$meterReaderFound'");
if (count($getMeterReaders)) {
  $data['status'] = 'A';
} else {
  $bindData = DB::insert("INSERT INTO SAP_SBA_schedules
    (TotalCons, LedgerCode, meterReader, subDivisionCode, billingGroup, billingCycle, ScheduleDate, Scheduletime, ScheduledUser)
    VALUES ((select count(*) totalConnections from sap_inputs WHERE subDivisionCode = '$sub' and billingGroup = '$bg' and billingCycle = '$bc' and MRU = '$lc' and bill_status = 'N'),
    '$lc', '$meterReaderFound', '$sub', '$bg', '$bc', '$ScheduleDate', '$Scheduletime', '$ScheduledUser')");
    if ($bindData) {
        $insertNew = DB::insert("INSERT INTO sap_sba_schedule_info
                                (mainid, masterkey, subdivcode, bg, cy, ledgercode, meterreader) (SELECT id, contractAcNumber, subDivisionCode, billingGroup, billingCycle, MRU, $meterReaderFound
                                from sap_inputs WHERE subDivisionCode = '$sub' and billingGroup = '$bg' and billingCycle = '$bc' and MRU = '$lc' and bill_status = 'N')");
      $data['status'] = 'Y';
    } else {
      $data['status'] = 'N';
    }
}
return $data;

});

// Bind Complete Ledger Non SAP New
Route::middleware('auth:api')->post('/bindLedgerNew', function (Request $request) {
  // return 'pppp';
  $userDetails = $request->user();
  $data  = [];

  $ScheduledUser = $userDetails->name;
  $ScheduleDate = Date('d-m-Y');
  $Scheduletime = Date('H:i:s');

  $meterReader = User::find($request->meterReader);

  $meterReaderFound = $meterReader->email;
//   return $meterReaderFound;
$sub = $request->subDivisionCode;
$bg = $request->billingGroup;
$bc = $request->billingCycle;
$lc = $request->ledgerCode;
$getMeterReaders = DB::select("SELECT meterReader from NONSAP_SBA_schedules WHERE subDivisionCode = '$sub' and billingGroup = '$bg' and billingCycle = '$bc' and LedgerCode = '$lc' and meterReader = '$meterReaderFound'");
if (count($getMeterReaders)) {
  $data['status'] = 'A';
} else {
  $totalCons = DB::select("SELECT count(*) totalConnections from bills WHERE subDivisionCode = '$sub' and billingGroup = '$bg' and billingCycle = '$bc' and SUBSTRING(masterKey, 4, 4) = '$lc' and bill_status = 'N'");
  $totalCons = $totalCons[0]->totalConnections;

  $bindData = DB::insert("INSERT INTO NONSAP_SBA_schedules
    (TotalCons, LedgerCode, meterReader, subDivisionCode, billingGroup, billingCycle, ScheduleDate, Scheduletime, ScheduledUser)
    VALUES ((select count(*) totalConnections from bills WHERE subDivisionCode = '$sub' and billingGroup = '$bg' and billingCycle = '$bc' and SUBSTRING(masterKey, 4, 4) = '$lc' and bill_status = 'N'),
    '$lc', '$meterReaderFound', '$sub', '$bg', '$bc', '$ScheduleDate', '$Scheduletime', '$ScheduledUser')");
    if ($bindData) {
        $insertNew = DB::insert("INSERT INTO nonsap_sba_schedule_info
                                (mainid, masterkey, subdivcode, bg, cy, ledgercode, meterreader) (SELECT id, masterKey, subDivisionCode, billingGroup, billingCycle, SUBSTRING(masterKey, 4, 4), $meterReaderFound
                                from bills WHERE subDivisionCode = '$sub' and billingGroup = '$bg' and billingCycle = '$bc' and SUBSTRING(masterKey, 4, 4) = '$lc' and bill_status = 'N')");
      $data['status'] = 'Y';
    } else {
      $data['status'] = 'N';
    }
}
return $data;

});

// Bind Complete Ledger NON SAP
Route::middleware('auth:api')->post('/bindLedgerNonSap', function (Request $request) {
  // return 'oooo';
  // return $request;
  $userDetails = $request->user();
  $data  = [];

  $assigned_by = $userDetails->id;
  $assigned_at = Date('Y-m-d H:i:s');

  $meterReader = User::find($request->meterReader);

  $meterReaderFound = $meterReader->email;
  // return $meterReaderFound;

    $meterReaderNew = $meterReaderFound . ',';
    // return $meterReaderNew;
    // $getMeterReaders = Bill::where('mru', $request->ledgerCode)
    //                             ->where('billingGroup', $request->billingGroup)
    //                             ->where('billingCycle', $request->billingCycle)
    //                             ->where('subDivisionCode', $request->subDivisionCode)->get();
    DB::enableQueryLog();

    $getMeterReaders = DB::select("
                                    select * from bills where subDivisionCode = '$request->subDivisionCode'
                                    and billingGroup = $request->billingGroup and billingCycle = $request->billingCycle
                                    and SUBSTRING(masterKey, 4, 4) = '$request->ledgerCode'
                                    ");
                                    // return $getMeterReaders;
    // $getMeterReaders = Bill::where(SUBSTRING(masterKey, 4, 4), $request->ledgerCode)->get()
                                // ->where('billingGroup', $request->billingGroup)
                                // ->where('billingCycle', $request->billingCycle)
                                // ->where('subDivisionCode', $request->subDivisionCode)->get();
    $meterReader = $getMeterReaders[0]->meterReader;
    // return $meterReader;
    // $searchBills = DB::update("UPDATE sap_inputs SET meterReader = '$meterReader->email', assigned_by = '$assigned_by', assigned_at = '$assigned_at', bindingStatus = 'Y', a_count = a_count + 1 where mru = '$request->ledgerCode' and billingGroup = '$request->billingGroup' and billingCycle = '$request->billingCycle' and subDivisionCode = '$request->subDivisionCode' AND bindingStatus = 'N'");
    $preCheck = DB::select("select count(*) count from bills where SUBSTRING(masterKey, 4, 4) = '$request->ledgerCode' and billingGroup = '$request->billingGroup' and billingCycle = '$request->billingCycle' and subDivisionCode = '$request->subDivisionCode' and find_in_set('$meterReaderFound', meterReader) <> 0 and bill_status = 'N'");
    // return $preCheck;
    if ($preCheck[0]->count >= 1) {
      $data['status'] = 'A';
    } else {
      // return $meterReaderFound;
      $searchBills = DB::update("UPDATE bills SET meterReader = CONCAT('$meterReader', '$meterReaderNew'), assigned_by = '$assigned_by', assigned_at = '$assigned_at', binding_status = 'Y', a_count = a_count + 1 where SUBSTRING(masterKey, 4, 4) = '$request->ledgerCode' and billingGroup = '$request->billingGroup' and billingCycle = '$request->billingCycle' and subDivisionCode = '$request->subDivisionCode' and not find_in_set('$meterReaderFound', meterReader) <> 0 and bill_status = 'N'");
      // return $searchBills;
      if ($searchBills) {
        $data['status'] = 'Y';
      } else {
        $data['status'] = 'N';
      }
    }

    // return $searchBills;
    // $affectedRows = DB::select("SELECT id FROM sap_inputs where mru = '$request->ledgerCode' and billingGroup = '$request->billingGroup' and billingCycle = '$request->billingCycle' and subDivisionCode = '$request->subDivisionCode'");

    // return $searchBills->result();
//   }

  // return $searchBills->success();



  return $data;

});

// Get Meter Reader Based on Sub Division Code
Route::middleware('auth:api')->post('/get-meter-reader-sub', function (Request $request) {
  $userDetails = $request->user();
  $subDivisionCode = $request->subDivisionCode;
  $from = $request->from;
  $to = $request->to;
  $meterReaders = DB::select("select distinct meterReader from sap_outputs where subDivisonCode = '$subDivisionCode' and (currentMeterReadingDate between '$from' and '$to')");
  $data = [
    'meterReaders' => $meterReaders
  ];

  return $data;

});

// Get Meter Reader Based on Sub Division Code, billingGroup, billingCycle SAP
Route::middleware('auth:api')->post('/get-meter-reader-sub-bg-bc', function (Request $request) {
  $userDetails = $request->user();
  $subDivisionCode = $request->subDivisionCode;
  $billingGroup = $request->billingGroup;
  $billingCycle = $request->billingCycle;
  $from = $request->from;
  $to = $request->to;
  // return $request;
  $meterReaders = DB::select("select distinct meterReader from sap_outputs where subDivisonCode = '$subDivisionCode' and billingGroup = '$billingGroup' and billingCycle = '$billingCycle' and (currentMeterReadingDate between '$from' and '$to')");
  $data = [
    'meterReaders' => $meterReaders
  ];

  return $data;

});

// Get Meter Reader Based on Sub Division Code, billingGroup, billingCycle Non SAP
Route::middleware('auth:api')->post('/get-meter-reader-sub-bg-bc-non-sap', function (Request $request) {
  $userDetails = $request->user();
  $subDivisionCode = $request->subDivisionCode;
  $billingGroup = $request->billingGroup;
  $billingCycle = $request->billingCycle;
  $from = $request->from;
  $to = $request->to;
  // return $request;
  $meterReaders = DB::select("select distinct meterReader from billeds where blSubDivisionCode = '$subDivisionCode' and blBillingGroup = '$billingGroup' and blBillingCycle = '$billingCycle' and (blPresentReadingDate between '$from' and '$to')");
  $data = [
    'meterReaders' => $meterReaders
  ];

  return $data;

});

// Get Sub Division Based On Date
// SAP
Route::middleware('auth:api')->post('/getSubOnDate', function (Request $request) {
  $userDetails = $request->user();
  $date = $request->date;
  // return $request;
  $meterReaders = DB::select("select distinct subDivisonCode from sap_outputs where currentMeterReadingDate = '$date' and soapStatus is null or soapStatus = 'F' or soapStatus = '' order by subDivisonCode ASC");
  $data = [
    'subDivisonCode' => $meterReaders
  ];

  return $data;

});

// Non SAP
Route::middleware('auth:api')->post('/getNonSapSubOnDate', function (Request $request) {
  $userDetails = $request->user();
  $date = $request->date;
  // return $request;
  $meterReaders = DB::select("select distinct blsubDivisionCode from billeds where blPresentReadingDate = '$date' and soapStatus is null or soapStatus = 'F' or soapStatus = '' order by blsubDivisionCode ASC");
  $data = [
    'subDivisonCode' => $meterReaders
  ];

  return $data;

});

// Export Cash Report
Route::middleware('auth:api')->post('/export-cash-report', function (Request $request) {
  // $userDetails = $request->user();
  // $subDivisionCode = $request->subDivisionCode;
  // $billingGroup = $request->billingGroup;
  // $billingCycle = $request->billingCycle;
  // $from = $request->from;
  // $to = $request->to;
  // return $request;
  $search = Billed::where('blSubDivisionCode', $request->subDivisionCode)
                    ->where('blBillingGroup', $request->billingGroup)
                    ->where('blBillingCycle', $request->billingCycle)
                    ->whereBetween('blPresentReadingDate', [$request->from, $request->to])
                    ->where('cashReport', 'N')->get();
  // return $search->count();
  if ($search->count() > 0) {
    $data = [];
    $data['message'] = 'Y';
    return $data;
  } else {
    $data = [];
    $data['message'] = 'A';
    return $data;
  }

});

// Data reset SAP
Route::middleware('auth:api')->post('/schedule-reset-sap-count', function (Request $request) {
  // $userDetails = $request->user();
  $id = $request->id;
  $details = DB::select("SELECT * FROM `SAP_SBA_schedules` WHERE `id` = '$id'");
  $sub = $details[0]->subDivisionCode;
  $bg = $details[0]->billingGroup;
  $bc = $details[0]->billingCycle;
  $lc = $details[0]->LedgerCode;
  $reader = $details[0]->meterReader;
  // return $reader;
  $resetSelect = DB::select("SELECT * FROM `sap_sba_schedule_info` WHERE `subdivcode` = '$sub' and `bg` = '$bg' and `cy` = '$bc' and `ledgercode` = '$lc' and `meterreader` = '$reader' and `bld_flag` = 'N'");
  return count($resetSelect);

});

Route::middleware('auth:api')->post('/schedule-reset-sap', function (Request $request) {
  // $userDetails = $request->user();
  $id = $request->id;
  $details = DB::select("SELECT * FROM `SAP_SBA_schedules` WHERE `id` = '$id'");
  $sub = $details[0]->subDivisionCode;
  $bg = $details[0]->billingGroup;
  $bc = $details[0]->billingCycle;
  $lc = $details[0]->LedgerCode;
  $reader = $details[0]->meterReader;
  // return $reader;
  $resetSelect = DB::select("UPDATE `sap_sba_schedule_info` SET downloadflag = 'N' WHERE `subdivcode` = '$sub' and `bg` = '$bg' and `cy` = '$bc' and `ledgercode` = '$lc' and `meterreader` = '$reader' and `bld_flag` = 'N'");
  return $resetSelect;

});

// Non SAP
Route::middleware('auth:api')->post('/schedule-reset-non-sap-count', function (Request $request) {
  // $userDetails = $request->user();
  $id = $request->id;
  $details = DB::select("SELECT * FROM `NONSAP_SBA_schedules` WHERE `id` = '$id'");
  $sub = $details[0]->subDivisionCode;
  $bg = $details[0]->billingGroup;
  $bc = $details[0]->billingCycle;
  $lc = $details[0]->LedgerCode;
  $reader = $details[0]->meterReader;
  // return $reader;
  $resetSelect = DB::select("SELECT * FROM `nonsap_sba_schedule_info` WHERE `subdivcode` = '$sub' and `bg` = '$bg' and `cy` = '$bc' and `ledgercode` = '$lc' and `meterreader` = '$reader' and `bld_flag` = 'N'");
  return count($resetSelect);

});

Route::middleware('auth:api')->post('/schedule-reset-non-sap', function (Request $request) {
  // $userDetails = $request->user();
  $id = $request->id;
  $details = DB::select("SELECT * FROM `NONSAP_SBA_schedules` WHERE `id` = '$id'");
  $sub = $details[0]->subDivisionCode;
  $bg = $details[0]->billingGroup;
  $bc = $details[0]->billingCycle;
  $lc = $details[0]->LedgerCode;
  $reader = $details[0]->meterReader;
  // return $reader;
  $resetSelect = DB::select("UPDATE `nonsap_sba_schedule_info` SET downloadflag = 'N' WHERE `subdivcode` = '$sub' and `bg` = '$bg' and `cy` = '$bc' and `ledgercode` = '$lc' and `meterreader` = '$reader' and `bld_flag` = 'N'");
  // $resetSelect = DB::select("SELECT * FROM `nonsap_sba_schedule_info` WHERE `subdivcode` = '$sub' and `bg` = '$bg' and `cy` = '$bc' and `ledgercode` = '$lc' and `meterreader` = '$reader' and `bld_flag` = 'N'");
  return $resetSelect;

});

// Remove Schedule Data
// Sap
Route::middleware('auth:api')->post('/schedule-remove-sap', function (Request $request) {
  // $userDetails = $request->user();
  $id = $request->id;
  $details = DB::select("SELECT * FROM `SAP_SBA_schedules` WHERE `id` = '$id'");
  $sub = $details[0]->subDivisionCode;
  $bg = $details[0]->billingGroup;
  $bc = $details[0]->billingCycle;
  $lc = $details[0]->LedgerCode;
  $reader = $details[0]->meterReader;
  // return $reader;
  $removeSchedule = DB::delete("delete from `SAP_SBA_schedules` WHERE `id` = '$id'");
  $removeSelect = DB::delete("delete from `sap_sba_schedule_info` WHERE `subdivcode` = '$sub' and `bg` = '$bg' and `cy` = '$bc' and `ledgercode` = '$lc' and `meterreader` = '$reader' and `bld_flag` = 'N'");
  return $resetSelect;

});

// Non SAP
Route::middleware('auth:api')->post('/schedule-remove-non-sap', function (Request $request) {
  // $userDetails = $request->user();
  $id = $request->id;
  $details = DB::select("SELECT * FROM `NONSAP_SBA_schedules` WHERE `id` = '$id'");
  $sub = $details[0]->subDivisionCode;
  $bg = $details[0]->billingGroup;
  $bc = $details[0]->billingCycle;
  $lc = $details[0]->LedgerCode;
  $reader = $details[0]->meterReader;
  // return $reader;
  $removeSchedule = DB::delete("delete from `NONSAP_SBA_schedules` WHERE `id` = '$id'");
  $removeSelect = DB::delete("delete from `nonsap_sba_schedule_info` WHERE `subdivcode` = '$sub' and `bg` = '$bg' and `cy` = '$bc' and `ledgercode` = '$lc' and `meterreader` = '$reader' and `bld_flag` = 'N'");
  // $resetSelect = DB::select("SELECT * FROM `nonsap_sba_schedule_info` WHERE `subdivcode` = '$sub' and `bg` = '$bg' and `cy` = '$bc' and `ledgercode` = '$lc' and `meterreader` = '$reader' and `bld_flag` = 'N'");
  return $resetSelect;

});

// Delete Bill SAP
Route::post('/delete-sap-bill', function (Request $request) {
  $userDetails = $request->user();
  $dTime = Date('H:i:s');
  $dDate = Date('d-m:Y');
  $mr_docnumber = $request->mr_docnumber;
  // return $mr_docnumber;
  $details = DB::select("SELECT * FROM `sap_outputs` WHERE `MR_DOCNumber` = '$mr_docnumber'");
  // return $details;
  // return $details[0]->subDivisionCode;
  $id = $details[0]->id;
  $sbCode = $details[0]->subDivisonCode;
  $MRU = $details[0]->MRU;
  $connectedPoleNINNumber = $details[0]->connectedPoleNINNumber;
  // return $details[0]->neighbourMeterNo;
  $neighbourMeterNo = $details[0]->neighbourMeterNo;
  $streetName = $details[0]->streetName;
  $installation = $details[0]->installation;
  $MR_DOCNumber = $details[0]->MR_DOCNumber;
  $scheduledMRDate = $details[0]->scheduledMRDate;
  $SAPMeterNumber = $details[0]->SAPMeterNumber;
  $manufacturerSRNo = $details[0]->manufacturerSRNo;
  $manufacturerName = $details[0]->manufacturerName;
  $contractAcNumber = $details[0]->contractAcNumber;
  $consumptionKWH = $details[0]->consumptionKWH;
  $consumptionKVAH = $details[0]->consumptionKVAH;
  $consumptionKVA = $details[0]->consumptionKVA;
  $currentMeterReadingKWH = $details[0]->currentMeterReadingKWH;
  $currentMeterReadingKVA = $details[0]->currentMeterReadingKVA;
  $currentMeterReadingKVAH = $details[0]->currentMeterReadingKVAH;
  $currentMeterReadingDate = $details[0]->currentMeterReadingDate;
  $currentMeterReadingTime = $details[0]->currentMeterReadingTime;
  $currentNoteFromMeterReader = $details[0]->currentNoteFromMeterReader;
  $previousMeterReadingKWH = $details[0]->previousMeterReadingKWH;
  $previousMeterReadingKVA = $details[0]->previousMeterReadingKVA;
  $previousMeterReadingKVAH = $details[0]->previousMeterReadingKVAH;
  $previousMeterReadingDate = $details[0]->previousMeterReadingDate;
  $previousMeterReadingTime = $details[0]->previousMeterReadingTime;
  $previoustNoteFromMeterReader = $details[0]->previoustNoteFromMeterReader;
  $octraiFlag = $details[0]->octraiFlag;
  $SOP = $details[0]->SOP;
  $ED = $details[0]->ED;
  $OCTRAI = $details[0]->OCTRAI;
  $DSSF = $details[0]->DSSF;
  $surchargeLevied = $details[0]->surchargeLevied;
  $serviceRent = $details[0]->serviceRent;
  $meterRent = $details[0]->meterRent;
  $serviceCharge = $details[0]->serviceCharge;
  $monthlyMinimumCharges = $details[0]->monthlyMinimumCharges;
  $powerFactorSurcharges = $details[0]->powerFactorSurcharges;
  $powerFactorIncentive = $details[0]->powerFactorIncentive;
  $demandCharges = $details[0]->demandCharges;
  $waterCharges = $details[0]->waterCharges;
  $voltageCharges = $details[0]->voltageCharges;
  $peakLoadExemptionCharges  = $details[0]->peakLoadExemptionCharges;
  $sundryCharges = $details[0]->sundryCharges;
  $miscellaneousCharges = $details[0]->miscellaneousCharges;
  $fuelAdjustment = $details[0]->fuelAdjustment;
  $billNumber = $details[0]->billNumber;
  $noOfDaysBilledFor = $details[0]->noOfDaysBilledFor;
  $billCycle = $details[0]->billCycle;
  $billDate = $details[0]->billDate;
  $dueDate = $details[0]->dueDate;
  $billType = $details[0]->billType;
  $paymantAmount = $details[0]->paymantAmount;
  $paymantMode = $details[0]->paymantMode;
  $checkNo = $details[0]->checkNo;
  $bankName = $details[0]->bankName;
  $paymentID = $details[0]->paymentID;
  $IFSCcode = $details[0]->IFSCcode;
  $MICRcode = $details[0]->MICRcode;
  $paymentDate = $details[0]->paymentDate;
  $paymentRemark = $details[0]->paymentRemark;
  $totBillAmount = $details[0]->totBillAmount;
  $SBMnumber = $details[0]->SBMnumber;
  $meterReaderName = $details[0]->meterReaderName;
  $inHouseOutsourcedSBM = $details[0]->inHouseOutsourcedSBM;
  $transformerCode = $details[0]->transformerCode;
  $mcbRent = $details[0]->mcbRent;
  $LPSC = $details[0]->LPSC;
  $totalAmountAfterDueDate = $details[0]->totalAmountAfterDueDate;
  $totalOfNetSopNetEdNetOctrai = $details[0]->totalOfNetSopNetEdNetOctrai;
  $subDivisionName = $details[0]->subDivisionName;
  $consumerLegacyNumber = $details[0]->consumerLegacyNumber;
  $consumerName = $details[0]->consumerName;
  $houseNumber = $details[0]->houseNumber;
  $address = $details[0]->address;
  $category = $details[0]->category;
  $subCategory = $details[0]->subCategory;
  $billingCycle = $details[0]->billingCycle;
  $billingGroup = $details[0]->billingGroup;
  $phaseCode = $details[0]->phaseCode;
  $sanctionedLoadKW = $details[0]->sanctionedLoadKW;
  $totalOldMeterConsumptionUnitsKWH = $details[0]->totalOldMeterConsumptionUnitsKWH;
  $totalOldMeterConsumptionUnitsKVAH = $details[0]->totalOldMeterConsumptionUnitsKVAH;
  $newMeterInitialReadingKWH = $details[0]->newMeterInitialReadingKWH;
  $newMeterInitialReadingKVAH = $details[0]->newMeterInitialReadingKVAH;
  $sundryChargesSOP = $details[0]->sundryChargesSOP;
  $sundryChargesED = $details[0]->sundryChargesED;
  $sundryChargesOCTRAI = $details[0]->sundryChargesOCTRAI;
  $sundryAllowancesSOP = $details[0]->sundryAllowancesSOP;
  $sundryAllowancesED = $details[0]->sundryAllowancesED;
  $sundryAllowancesOCTRAI = $details[0]->sundryAllowancesOCTRAI;
  $otherCharges = $details[0]->otherCharges;
  $roundAdjustmentAmount = $details[0]->roundAdjustmentAmount;
  $adjustmentAmountSOP = $details[0]->adjustmentAmountSOP;
  $adjustmentAmountED = $details[0]->adjustmentAmountED;
  $adjustmentAmountOCTRAI = $details[0]->adjustmentAmountOCTRAI;
  $provisionalAdjustmentAmountSOP = $details[0]->provisionalAdjustmentAmountSOP;
  $provisionalAdjustmentAmountED = $details[0]->provisionalAdjustmentAmountED;
  $provisionalAdjustmentAmountOCTRAI = $details[0]->provisionalAdjustmentAmountOCTRAI;
  $arrearSOPCurrentYear = $details[0]->arrearSOPCurrentYear;
  $arrearEDCurrentYear = $details[0]->arrearEDCurrentYear;
  $arrearOCTRAICurrentYear = $details[0]->arrearOCTRAICurrentYear;
  $arrearSOPPreviousYear = $details[0]->arrearSOPPreviousYear;
  $arrearEDPreviousYear = $details[0]->arrearEDPreviousYear;
  $arrearOCTRAIPreviousYear = $details[0]->arrearOCTRAIPreviousYear;
  $advanceConsumptionDeposit = $details[0]->advanceConsumptionDeposit;
  $complaintCenterPhoneNumber = $details[0]->complaintCenterPhoneNumber;
  $meterSecurityAmount = $details[0]->meterSecurityAmount;
  $nearestCashCounter = $details[0]->nearestCashCounter;
  $multiplicationFactor = $details[0]->multiplicationFactor;
  $overallMf = $details[0]->overallMf;
  $contractedDemandKVA = $details[0]->contractedDemandKVA;
  $miscExpensesDetails = $details[0]->miscExpensesDetails;
  $previousKWHCycle1 = $details[0]->previousKWHCycle1;
  $previousKWHCycle2 = $details[0]->previousKWHCycle2;
  $previousKWHCycle3 = $details[0]->previousKWHCycle3;
  $previousKWHCycle4 = $details[0]->previousKWHCycle4;
  $previousKWHCycle5 = $details[0]->previousKWHCycle5;
  $previousKWHCycle6 = $details[0]->previousKWHCycle6;
  $roundingPresent = $details[0]->roundingPresent;
  $estimationType = $details[0]->estimationType;
  $ml = $details[0]->ml;
  $mtFlag = $details[0]->mtFlag;
  $feederCode = $details[0]->feederCode;
  $scWsdAmountWithHeld = $details[0]->scWsdAmountWithHeld;
  $fixedCharges = $details[0]->fixedCharges;
  $mt = $details[0]->mt;
  $previousFyCons = $details[0]->previousFyCons;
  $meterReadingHigh = $details[0]->meterReadingHigh;
  $meterReadingNote = $details[0]->meterReadingNote;
  $amountOfCowCess = $details[0]->amountOfCowCess;
  $DuedateCheq = $details[0]->DuedateCheq;
  $InfradevCess = $details[0]->InfradevCess;
  $TotalInfraDevCess = $details[0]->TotalInfraDevCess;
  $meterReader = $details[0]->meterReader;
  $blLat = $details[0]->blLat;
  $blLong = $details[0]->blLong;
  $meterRemark = $details[0]->meterRemark;
  $mdiRemark = $details[0]->mdiRemark;
  $meterLocation = $details[0]->meterLocation;
  $meterType = $details[0]->meterType;
  $meterPhase = $details[0]->meterPhase;
  $billNo = $details[0]->billNo;
  $appVersion = $details[0]->appVersion;

  $backUp = DB::insert("INSERT INTO sap_deleted_bills (
    id,
    subDivisonCode,
    MRU,
    connectedPoleNINNumber,
    neighbourMeterNo,
    streetName,
    installation,
    MR_DOCNumber,
    scheduledMRDate,
    SAPMeterNumber,
    manufacturerSRNo,
    manufacturerName,
    contractAcNumber,
    consumptionKWH,
    consumptionKVAH,
    consumptionKVA,
    currentMeterReadingKWH,
    currentMeterReadingKVA,
    currentMeterReadingKVAH,
    currentMeterReadingDate,
    currentMeterReadingTime,
    currentNoteFromMeterReader,
    previousMeterReadingKWH,
    previousMeterReadingKVA,
    previousMeterReadingKVAH,
    previousMeterReadingDate,
    previousMeterReadingTime,
    previoustNoteFromMeterReader,
    octraiFlag,
    SOP,
    ED,
    OCTRAI,
    DSSF,
    surchargeLevied,
    serviceRent,
    meterRent,
    serviceCharge,
    monthlyMinimumCharges,
    powerFactorSurcharges,
    powerFactorIncentive,
    demandCharges,
    waterCharges,
    voltageCharges,
    peakLoadExemptionCharges,
    sundryCharges,
    miscellaneousCharges,
    fuelAdjustment,
    billNumber,
    noOfDaysBilledFor,
    billCycle,
    billDate,
    dueDate,
    billType,
    paymantAmount,
    paymantMode,
    checkNo,
    bankName,
    paymentID,
    IFSCcode,
    MICRcode,
    paymentDate,
    paymentRemark,
    totBillAmount,
    SBMnumber,
    meterReaderName,
    inHouseOutsourcedSBM,
    transformerCode,
    mcbRent,
    LPSC,
    totalAmountAfterDueDate,
    totalOfNetSopNetEdNetOctrai,
    subDivisionName,
    consumerLegacyNumber,
    consumerName,
    houseNumber,
    address,
    category,
    subCategory,
    billingCycle,
    billingGroup,
    phaseCode,
    sanctionedLoadKW,
    totalOldMeterConsumptionUnitsKWH,
    totalOldMeterConsumptionUnitsKVAH,
    newMeterInitialReadingKWH,
    newMeterInitialReadingKVAH,
    sundryChargesSOP,
    sundryChargesED,
    sundryChargesOCTRAI,
    sundryAllowancesSOP,
    sundryAllowancesED,
    sundryAllowancesOCTRAI,
    otherCharges,
    roundAdjustmentAmount,
    adjustmentAmountSOP,
    adjustmentAmountED,
    adjustmentAmountOCTRAI,
    provisionalAdjustmentAmountSOP,
    provisionalAdjustmentAmountED,
    provisionalAdjustmentAmountOCTRAI,
    arrearSOPCurrentYear,
    arrearEDCurrentYear,
    arrearOCTRAICurrentYear,
    arrearSOPPreviousYear,
    arrearEDPreviousYear,
    arrearOCTRAIPreviousYear,
    advanceConsumptionDeposit,
    complaintCenterPhoneNumber,
    meterSecurityAmount,
    nearestCashCounter,
    multiplicationFactor,
    overallMf,
    contractedDemandKVA,
    miscExpensesDetails,
    previousKWHCycle1,
    previousKWHCycle2,
    previousKWHCycle3,
    previousKWHCycle4,
    previousKWHCycle5,
    previousKWHCycle6,
    roundingPresent,
    estimationType,
    ml,
    mtFlag,
    feederCode,
    scWsdAmountWithHeld,
    fixedCharges,
    mt,
    previousFyCons,
    meterReadingHigh,
    meterReadingNote,
    amountOfCowCess,
    DuedateCheq,
    InfradevCess,
    TotalInfraDevCess,
    meterReader,
    blLat,
    blLong,
    meterRemark,
    mdiRemark,
    meterLocation,
    meterType,
    meterPhase,
    billNo,
    appVersion,
    delete_time,
    delete_date,
    Reason_for_delete,
    deleted_userid
  ) VALUES (
    '$id',
    '$sbCode',
    '$MRU',
    '$connectedPoleNINNumber',
    '$neighbourMeterNo',
    '$streetName',
    '$installation',
    '$MR_DOCNumber',
    '$scheduledMRDate',
    '$SAPMeterNumber',
    '$manufacturerSRNo',
    '$manufacturerName',
    '$contractAcNumber',
    '$consumptionKWH',
    '$consumptionKVAH',
    '$consumptionKVA',
    '$currentMeterReadingKWH',
    '$currentMeterReadingKVA',
    '$currentMeterReadingKVAH',
    '$currentMeterReadingDate',
    '$currentMeterReadingTime',
    '$currentNoteFromMeterReader',
    '$previousMeterReadingKWH',
    '$previousMeterReadingKVA',
    '$previousMeterReadingKVAH',
    '$previousMeterReadingDate',
    '$previousMeterReadingTime',
    '$previoustNoteFromMeterReader',
    '$octraiFlag',
    '$SOP',
    '$ED',
    '$OCTRAI',
    '$DSSF',
    '$surchargeLevied',
    '$serviceRent',
    '$meterRent',
    '$serviceCharge',
    '$monthlyMinimumCharges',
    '$powerFactorSurcharges',
    '$powerFactorIncentive',
    '$demandCharges',
    '$waterCharges',
    '$voltageCharges',
    '$peakLoadExemptionCharges',
    '$sundryCharges',
    '$miscellaneousCharges',
    '$fuelAdjustment',
    '$billNumber',
    '$noOfDaysBilledFor',
    '$billCycle',
    '$billDate',
    '$dueDate',
    '$billType',
    '$paymantAmount',
    '$paymantMode',
    '$checkNo',
    '$bankName',
    '$paymentID',
    '$IFSCcode',
    '$MICRcode',
    '$paymentDate',
    '$paymentRemark',
    '$totBillAmount',
    '$SBMnumber',
    '$meterReaderName',
    '$inHouseOutsourcedSBM',
    '$transformerCode',
    '$mcbRent',
    '$LPSC',
    '$totalAmountAfterDueDate',
    '$totalOfNetSopNetEdNetOctrai',
    '$subDivisionName',
    '$consumerLegacyNumber',
    '$consumerName',
    '$houseNumber',
    '$address',
    '$category',
    '$subCategory',
    '$billingCycle',
    '$billingGroup',
    '$phaseCode',
    '$sanctionedLoadKW',
    '$totalOldMeterConsumptionUnitsKWH',
    '$totalOldMeterConsumptionUnitsKVAH',
    '$newMeterInitialReadingKWH',
    '$newMeterInitialReadingKVAH',
    '$sundryChargesSOP',
    '$sundryChargesED',
    '$sundryChargesOCTRAI',
    '$sundryAllowancesSOP',
    '$sundryAllowancesED',
    '$sundryAllowancesOCTRAI',
    '$otherCharges',
    '$roundAdjustmentAmount',
    '$adjustmentAmountSOP',
    '$adjustmentAmountED',
    '$adjustmentAmountOCTRAI',
    '$provisionalAdjustmentAmountSOP',
    '$provisionalAdjustmentAmountED',
    '$provisionalAdjustmentAmountOCTRAI',
    '$arrearSOPCurrentYear',
    '$arrearEDCurrentYear',
    '$arrearOCTRAICurrentYear',
    '$arrearSOPPreviousYear',
    '$arrearEDPreviousYear',
    '$arrearOCTRAIPreviousYear',
    '$advanceConsumptionDeposit',
    '$complaintCenterPhoneNumber',
    '$meterSecurityAmount',
    '$nearestCashCounter',
    '$multiplicationFactor',
    '$overallMf',
    '$contractedDemandKVA',
    '$miscExpensesDetails',
    '$previousKWHCycle1',
    '$previousKWHCycle2',
    '$previousKWHCycle3',
    '$previousKWHCycle4',
    '$previousKWHCycle5',
    '$previousKWHCycle6',
    '$roundingPresent',
    '$estimationType',
    '$ml',
    '$mtFlag',
    '$feederCode',
    '$scWsdAmountWithHeld',
    '$fixedCharges',
    '$mt',
    '$previousFyCons',
    '$meterReadingHigh',
    '$meterReadingNote',
    '$amountOfCowCess',
    '$DuedateCheq',
    '$InfradevCess',
    '$TotalInfraDevCess',
    '$meterReader',
    '$blLat',
    '$blLong',
    '$meterRemark',
    '$mdiRemark',
    '$meterLocation',
    '$meterType',
    '$meterPhase',
    '$billNo',
    '$appVersion',
    '$dTime',
    '$dDate',
    'Wrong Reading',
    ''
  )");
  $removeBillStatus = DB::update("update sap_inputs set bill_status = 'N' where mr_docnumber = '$MR_DOCNumber'");
  $updateScheduleStatus = DB::update("update sap_sba_schedule_info set downloadflag = 'N', bld_flag = 'N' where mainid = '$id'");
  $deleteBillFromSapOutputs = DB::delete("delete from `sap_outputs` WHERE `MR_DOCNumber` = '$MR_DOCNumber'");
  return $deleteBillFromSapOutputs;
  // $sub = $details[0]->subDivisionCode;
  // $bg = $details[0]->billingGroup;
  // $bc = $details[0]->billingCycle;
  // $lc = $details[0]->LedgerCode;
  // $reader = $details[0]->meterReader;
  // return $reader;
  // $removeSelect = DB::delete("delete from `nonsap_sba_schedule_info` WHERE `subdivcode` = '$sub' and `bg` = '$bg' and `cy` = '$bc' and `ledgercode` = '$lc' and `meterreader` = '$reader' and `bld_flag` = 'N'");
  // return $resetSelect;

  // $to = $request->to;
});

// Non SAP
Route::post('/delete-non-sap-bill', function (Request $request) {
  // return $request;
  $userDetails = $request->user();
  $dTime = Date('H:i:s');
  $dDate = Date('d/m/Y');
  $mr_docnumber = $request->mr_docnumber;
  $reason = $request->reason;
  // return $mr_docnumber;
  $details = DB::select("SELECT * FROM `billeds` WHERE `blMasterKey` = '$mr_docnumber'");
  // return $details;
  // return $details[0]->blSubDivisionCode;
  // return $userDetails;
  $id = $details[0]->id;
  $backUp = DB::insert("insert into nonsap_billeds_deleted select *,'1234','$reason','$dDate','$dTime' from billeds where blmasterkey = '$mr_docnumber'");
  $removeBillStatus = DB::update("update bills set bill_status = 'N' where masterKey = '$mr_docnumber'");
  $updateScheduleStatus = DB::update("update sap_sba_schedule_info set downloadflag = 'N', bld_flag = 'N' where mainid = '$id'");
  $deleteBillFromSapOutputs = DB::delete("delete from `billeds` WHERE `blMasterKey` = '$mr_docnumber'");
  // return $updateScheduleStatus;
  return 'success';
  // $sub = $details[0]->subDivisionCode;
  // $bg = $details[0]->billingGroup;
  // $bc = $details[0]->billingCycle;
  // $lc = $details[0]->LedgerCode;
  // $reader = $details[0]->meterReader;
  // return $reader;
  // $removeSelect = DB::delete("delete from `nonsap_sba_schedule_info` WHERE `subdivcode` = '$sub' and `bg` = '$bg' and `cy` = '$bc' and `ledgercode` = '$lc' and `meterreader` = '$reader' and `bld_flag` = 'N'");
  // return $resetSelect;

  // $to = $request->to;
});

// Get Account Number Min And Max Code For Normal Binding
Route::get('/track-meter-reader.xml', function (Request $request) {
$subDivision = $request->sub;
$meterReader = $request->reader;
$from = $request->from;
// $meterReaderFind = User::find($meterReader);
$meterReader = $request->reader;
$result = DB::select("SELECT * FROM sap_outputs WHERE (currentMeterReadingDate BETWEEN '$from' and '$to') and subDivisonCode = '$subDivision' and meterReader = '$meterReader' and blLat != '0.0' order by id");
// return $result;
// echo "string";
// $content = View::make('track-api')->with('somevar', $)
// return Response::make('content', '200')->header('Content-Type', 'text/xml');
// header("Content-type: text/xml");
return response()->view('track-api', compact('result'))->header('Content-Type', 'text/xml')->header('Access-Control-Allow-Origin', '*');

      // return $data;

});


// Get Account Number Min And Max Code For Normal Binding
Route::get('/track-meter-reader.xml', function (Request $request) {
$subDivision = $request->sub;
$meterReader = $request->reader;
$from = $request->from;
$to = $request->to;
// $meterReaderFind = User::find($meterReader);
$meterReader = $request->reader;
$result = DB::select("SELECT * FROM sap_outputs WHERE (currentMeterReadingDate BETWEEN '$from' and '$to') and subDivisonCode = '$subDivision' and meterReader = '$meterReader' and blLat != '0.0' order by id");
// return $result;
// echo "string";
// $content = View::make('track-api')->with('somevar', $)
// return Response::make('content', '200')->header('Content-Type', 'text/xml');
// header("Content-type: text/xml");
return response()->view('track-api', compact('result'))->header('Content-Type', 'text/xml')->header('Access-Control-Allow-Origin', '*');

      // return $data;

});

// data entry
Route::post('/tariffChangeAjax', function(Request $request)
{
  // return $request;
  // validate($request,[
  //   'ledgerGroup' => 'required',
  //   ]);
  //
    $data = SundryCharge::create($request->except('_token'));
    // return $data;

    if ($data) {
      $status = [
        'status' => 'success'
      ];
    } else {
      $status = [
        'status' => 'error'
      ];
    }

    return $status;

});

Route::get('/genetaredText.txt', function(Request $request)
{
  $bills =  Billed::where('blSubDivisionCode', $request->sub)
                    ->where('blBillingGroup', $request->bg)
                    ->where('blBillingCycle', $request->bc)->get();

  // return addSpace('pppp', 12);
  $txt = '';
  foreach ($bills as $bill) {
    $details = Bill::where('masterkey', $bill->blMasterKey)->first();
    $time = $bill->created_at->format('h:i');
    $cashDate = Carbon\Carbon::createFromFormat('d-m-Y', $bill->blDueDateByCash)->format('d/m/Y');
    $chequeDate = Carbon\Carbon::createFromFormat('d-m-Y', $bill->blDueDateByCheque)->format('d/m/Y');
    $txt .= addSpace($bill->blMasterKey, 12);
    $txt .= addSpace($bill->blConsumerName, 25);
    $txt .= addSpace($details->consumerAddress, 34);
    // echo "$details->consumerAddress";
    $txt .= addZeros($bill->blTariffType, 1);
    $txt .= addZeros($bill->blBillingGroup, 1);
    $txt .= addZeros($bill->blBillingCycle, 2);
    $txt .= addZeros((int) $bill->blCurrentMeterRent, 6);
    $txt .= addZeros((int) $bill->blCurrentServiceRent, 6);
    $txt .= addZeros((int) $bill->blPresentReading, 8);
    $txt .= addSpace($bill->blPresentReadingDate, 10);
    $txt .= addSpace(strtoupper($bill->blPresentStatus), 1);
    $txt .= addZeros((int) $bill->blUnitsConsumed, 8);
    $txt .= addZeros($bill->blDays, 5);
    $txt .= addZeros($bill->blMMCFlag, 3);
    $txt .= addZerosFloat($bill->blCurrentSop, 10);
    $txt .= addZerosFloat($bill->blCurrentEd, 10);
    $txt .= addZerosFloat($bill->blCurrentOctroi, 10);
    if ($bill->blNetSop > 0) {
      $blNetSop = '+'.addZeros($bill->blNetSop, 9);
    } elseif ($bill->blNetSop == 0) {
      $blNetSop = addZeros('', 10);
    } else {
      $abs = abs($bill->blNetSop);
      $abs = sprintf("%.2f", $abs);
      $blNetSop = '-'.addZeros($abs, 9);
    }
    if ($bill->blNetEd > 0) {
      $blNetEd = '+'.addZeros($bill->blNetEd, 9);
    } elseif ($bill->blNetEd == 0) {
      $blNetEd = addZeros('', 10);
    } else {
      $abs = abs($bill->blNetEd);
      $abs = sprintf("%.2f", $abs);
      $blNetEd = '-'.addZeros($abs, 9);
    }
    if ($bill->blNetOctroi > 0) {
      $blNetOctroi = '+'.addZeros($bill->blNetOctroi, 9);
    } elseif ($bill->blNetOctroi == 0) {
      $blNetOctroi = addZeros('', 10);
    } else {
      $abs = abs($bill->blNetOctroi);
      $abs = sprintf("%.2f", $abs);
      $blNetOctroi = '-'.addZeros($abs, 9);
    }

    $txt .= vle($bill->blNetSop, 10);
    $txt .= vle($bill->blNetEd, 10);
    $txt .= vle($bill->blNetOctroi, 10);
    $txt .= addSpaceFront($bill->blCurrentRoundingAmt, 2);
    $txt .= vle($bill->blAmountBeforeDue, 10);
    // $txt .= addZerosSign($bill->blAmountBeforeDue, 10);
    $txt .= addZerosSign((int)$bill->blSurCharge, 7);
    $txt .= addSpace($cashDate, 10);
    $txt .= addSpace($chequeDate, 10);
    $txt .= vlen($bill->blAmountAfterDue, 7);
    $txt .= addSpace($time, 5);
    $txt .= addSpace('', 1);
    $txt .= addSpace('', 6);
    $txt .= addSpace('', 6);
    $txt .= addZeros(substr($bill->blTime, 0, 5), 5);
    $txt .= addZeros('', 5);
    $txt .= addZeros(substr($bill->meterReader, 1, 4), 4);
    $txt .= addSpace('', 10);
    $txt .= addZeros('', 7);
    $txt .= addZeros('', 7);
    $txt .= addZeros(substr((int) $bill->blConcessionalUnits, 0, 4), 4);
    $txt .= addSpace('', 4);
    $txt .= addZerosBack($bill->appVersion, 4);
    $txt .= addZeros('', 1);
    $txt .= addZeros('', 1);
    $txt .= vlen($bill->blCurrentIDFWithSign, 9);
    $txt .= vlen($bill->blCurrentCowCessWithSign, 9);
    $txt .= vlen($bill->blTotalCurrentWaterSewarageChargeWithSign, 9);
    $txt .= vlen($bill->bltotalIDFWithSign, 9);
    $txt .= vlen($bill->blTotalCowCessWithSign, 9);
    $txt .= vlen($bill->blTotalCurrentWaterSewarageChargeWithSign, 9);
    $txt .= vlen($bill->blCurrentFCAChargesWithSign, 9);
    $txt .= vlen($bill->blCurrentFixedChargesWithSign, 9);
    $txt .= "\r\n";
  }
  $sub = $request->sub;
  $bg = $request->bg;
  $bc = $request->bc;
  $tmp = '';
  if ($bg == 8) {
    $tmp = 'GT';
  } else {
    $tmp = 'DS';
  }

  // return $sub;

  // Update Billing_closed_flag
  $updateBilleds = DB::update("update billeds set Billing_closed_flag = 'Y' WHERE blSubDivisionCode = '$sub' and blBillingGroup = '$bg' and blBillingCycle = '$bc'");
  $updateBills = DB::update("update bills set Billing_closed_flag = 'Y' WHERE subDivisionCode = '$sub' and billingGroup = '$bg' and billingCycle = '$bc'");
  $fileName = "upload$sub$bc$bg$tmp.txt";
  // $textFile = response($txt);
  $textFile = response($txt)
            ->withHeaders([
                'Content-Type' => 'text/plain',
                // 'Cache-Control' => 'no-store, no-cache',
                // 'Content-Disposition' => "attachment; filename=upload$sub$bc$bg$tmp.txt",
            ])->getContent();
  Storage::disk('s3')->put('/toEncrypt/'.$fileName, (string) $textFile, 'public');
  $textUrl = "https://pspcl.s3.ap-south-1.amazonaws.com/toEncrypt/$fileName";
  // return $textLink;
  $ch = curl_init();

  curl_setopt($ch, CURLOPT_URL,"http://i1moodle.cloudapp.net/decrypt/api/encrypturl");
  curl_setopt($ch, CURLOPT_POST, 1);
  curl_setopt($ch, CURLOPT_POSTFIELDS,
              "url=$textUrl");

  // in real life you should use something like:
  // curl_setopt($ch, CURLOPT_POSTFIELDS,
  //          http_build_query(array('postvar1' => 'value1')));

  // receive server response ...
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

  $server_output = curl_exec ($ch);

  curl_close ($ch);
  $encFile = str_replace('"', '', $server_output);
  // $fileNameEnc = "upload$sub$bc$bg$tmp.enc";
  // return exec("wget $encFile -O $fileNameEnc");
  // file_put_contents("enctest.enc", fopen($encFile, 'r'));
  return $encFile;
  // further processing ....
  // if ($server_output == "OK") { ... } else { ... }
  return $server_output;
  // echo $bills;

});

// Route::get('/my_first_api', function () {
//   return 'po';
// });
Route::post('/image', function(Request $request)
{
  // return $request;
  // $base64 = $request['image'];
  // list($baseType, $image) = explode(';', $base64);
  // list(, $image) = explode(',', $image);
  $image = base64_decode($request['image']);
  $imageName = rand(11111111, 99999999). '.png';
  $year = Date('Y').'/';
  $p = Storage::disk('s3')->put('tttt' . $imageName, $image, 'public');
  return $p;
  // validate($request,[
  //   'ledgerGroup' => 'required',
  //   ]);
  //
    // $data = SundryCharge::create($request->except('_token'));
    // return $data;

    if ($data) {
      $status = [
        'status' => 'success'
      ];
    } else {
      $status = [
        'status' => 'error'
      ];
    }

    return $status;

});
Route::post('/sap_missing_cons', function(Request $request)
{
  $meterno = $request['meterno'];
  $metermake = $request['metermake'];
  $meterreading = $request['meterreading'];
  $houseno = $request['houseno'];
  $streetname = $request['streetname'];
  $areaname = $request['areaname'];
  $ledgercode = $request['ledgercode'];
  $mobileno = $request['mobileno'];
  $remarks = $request['remarks'];
  $meterreader = $request['meterreader'];
  $lat = $request['lat'];
  $lang = $request['lang'];
  $indate = $request['indate'];
  $updateddate = $request['updateddate'];
  $updatedtime = $request['updatedtime'];
  // $futureuse1 = $request['futureuse1'];

  $insertNew = DB::INSERT("INSERT INTO sap_missing_cons
    (meterno, metermake, meterreading, houseno, streetname, areaname, ledgercode, mobileno, remarks, meterreader, lat, lang, indate, updateddate, updatedtime)
    VALUES
    ('$meterno', '$metermake', '$meterreading', '$houseno', '$streetname', '$areaname', '$ledgercode', '$mobileno', '$remarks', '$meterreader', '$lat', '$lang', '$indate', '$updateddate', '$updatedtime')
    ");
    if ($insertNew) {
      $status = [
        'status' => 'success'
      ];
    } else {
      $status = [
        'status' => 'error'
      ];
    }

    return $status;

});
Route::post('/getDivisinTable', function(Request $request)
{
  $table = $request['table'];
  $divisions = DB::SELECT("SELECT substr(blMasterKey, 1, 2) division FROM $table group by substr(blMasterKey, 1, 2)");
  $data['division'] = $divisions;
  return $data;

});
Route::post('/getSapDivisinTable', function(Request $request)
{
  $table = $request['table'];
  $divisions = DB::SELECT("SELECT subDivisonCode division FROM $table group by subDivisonCode");
  $data['division'] = $divisions;
  return $data;

});
Route::post('/getSapGroupTable', function(Request $request)
{
  $table = $request['table'];
  $division = $request['division'];
  $group = DB::SELECT("SELECT billingGroup FROM $table WHERE subDivisonCode = '$division' group by billingGroup");
  $data['group'] = $group;
  return $data;

});
Route::post('/getSapCycleTable', function(Request $request)
{
  $table = $request['table'];
  $division = $request['division'];
  $group = $request['group'];
  $cycle = DB::SELECT("SELECT billingCycle FROM $table WHERE subDivisonCode = '$division' and billingGroup = '$group' group by billingCycle");
  $data['cycle'] = $cycle;
  return $data;

});
Route::post('/getGroupTable', function(Request $request)
{
  $table = $request['table'];
  $division = $request['division'];
  $group = DB::SELECT("SELECT blBillingGroup FROM $table WHERE substr(blMasterKey, 1, 2) = '$division' group by blBillingGroup");
  $data['group'] = $group;
  return $data;

});

// Non SAP
Route::post('/getNonSapDivisinTable', function(Request $request)
{
  $table = $request['table'];
  $divisions = DB::SELECT("SELECT blSubDivisionCode division FROM $table group by blSubDivisionCode");
  $data['division'] = $divisions;
  return $data;

});
Route::post('/getNonSapGroupTable', function(Request $request)
{
  $table = $request['table'];
  $division = $request['division'];
  $group = DB::SELECT("SELECT blBillingGroup billingGroup FROM $table WHERE blSubDivisionCode = '$division' group by blBillingGroup");
  $data['group'] = $group;
  return $data;

});
Route::post('/getNonSapCycleTable', function(Request $request)
{
  $table = $request['table'];
  $division = $request['division'];
  $group = $request['group'];
  $cycle = DB::SELECT("SELECT blBillingCycle billingCycle FROM $table WHERE blSubDivisionCode = '$division' and blBillingGroup = '$group' group by blBillingCycle");
  $data['cycle'] = $cycle;
  return $data;

});

// Day Close SAP
Route::post('/dayCloseSap', function(Request $request)
{
  $deleteId = $request['deleteId'];
  $dayClose = DB::UPDATE("UPDATE user_logs SET `status` = 'C' WHERE `id` = '$deleteId'");
  if ($dayClose) {
    $status = 'Y';
  } else {
    $status = 'N';
  }
  return $status;

});
Route::get('/my_first_api', 'HomeController@my_first_api');
