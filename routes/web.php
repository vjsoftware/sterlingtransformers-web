<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// use FarhanWazir\GoogleMaps\Facades\GMapsFacade;

Route::get('/', 'HomeController@index');
Route::get('/testr', function () {
    return 'test 1 by vJ';
});

Route::get('soap/{key}/server', [
    'as' => 'zoap.server.wsdl',
    'uses' => 'zoapController@server'
]);

Route::post('soap/{key}/server', [
    'as' => 'zoap.server',
    'uses' => '\Viewflex\Zoap\ZoapController@server'
]);

Route::get('/map', 'HomeController@map');

Route::get('/token', function () {
    return Auth::user()->createToken('user');
});

Route::get('/message', function () {
    return view('message');
});

// Soap Live Server Test
Route::get('/soapt', function () {
    return view('soaptnew');
});

Route::post('/soapDataTransfer', 'soapDataController@transfer')
        ->name('soapDataTransfer');

Route::get('/soapnt', function () {
  return view('soapntnew');
});

Route::post('/soapDataTransferNonSAP', 'soapDataController@transfernonSAP')
        ->name('soapDataTransferNonSAP');

// meterRaeder Mobile Reset
Route::get('/mobile-reset', 'BindMeterReaderController@mobileResetView');
Route::post('/mobile-reset', 'BindMeterReaderController@mobileReset')
      ->name('mobileReset');
Route::get('/day-close-sap', 'dayCloseController@sap');
Route::get('/day-close-non-sap', 'dayCloseController@nonSap');

// Schedules
// SAP
Route::get('/schedule-details-sap', 'scheduleReport@sap');
Route::get('/schedule-details-sap-info/{meterReader}/{ledgerCode}', 'scheduleReport@sapInfo');
Route::get('/schedule-details-non-sap', 'scheduleReport@nonsap');
Route::get('/schedule-details-non-sap-info/{meterReader}/{ledgerCode}', 'scheduleReport@nonsapInfo');
// Route::post('/data-reset', 'scheduleReport@dataReset')
//       ->name('dataReset');

// Data Release
Route::get('/data-reset', 'BindMeterReaderController@dataResetView');
Route::post('/data-reset', 'BindMeterReaderController@dataReset')
      ->name('dataReset');

// Consumer Enquiry Form
Route::get('/consumer-enquiry-form', 'ReportsController@consumerEnquiryForm');
Route::post('/consumer-enquiry-form', 'ReportsController@consumerEnquiry')
      ->name('consumerEnquiry');

// Billing Summary
Route::get('/billing-summary', 'ReportsController@billingSummaryView');
Route::post('/billing-summary', 'ReportsController@billingSummary')
      ->name('billingSummary');
Route::get('/billing-summary-non-sap', 'ReportsController@billingSummaryNonSapView');
Route::post('/billing-summary-non-sap', 'ReportsController@billingSummaryNonSap')
      ->name('billingSummaryNonSap');

// Pendin Bills
Route::get('/pending-bills-sap', 'ReportsController@pendingBillsView');
Route::post('/pending-bills-sap', 'ReportsController@pendingBillsSAP')
      ->name('pendingBillsSAP');
Route::get('/pending-bills-non-sap', 'ReportsController@pendingBillsNonSapView');
Route::post('/pending-bills-non-sap', 'ReportsController@pendingBillsNonSAP')
      ->name('pendingBillsNonSAP');
// Route::get('/billing-summary-non-sap', 'ReportsController@billingSummaryNonSap');

// Mini Ledger
Route::get('/mini-ledger-new', 'ReportsController@miniLedgerView');
Route::post('/mini-ledger-new', 'ReportsController@miniLedgerReport')
      ->name('miniLedgerReport');
Route::get('/mini-ledger-non-sap-new', 'ReportsController@miniLedgerNonSapView');
Route::post('/mini-ledger-non-sap-new', 'ReportsController@miniLedgerNonSapReport')
      ->name('miniLedgerNonSapReport');

// Meter Reader Performance Report
Route::get('/meter-reader-preformance-report', 'ReportsController@meterReaderPerformanceReportView');
Route::post('/meter-reader-preformance-report', 'ReportsController@meterReaderPerformanceReport')
      ->name('meterReaderPerformanceReport');
// Ledger Performance Report
// SAP
Route::get('/ledger-preformance-report', 'ReportsController@ledgerPerformanceReportView');
Route::post('/ledger-preformance-report', 'ReportsController@ledgerPerformanceReport')
      ->name('ledgerPerformanceReport');
// Non SAP
Route::get('/ledger-preformance-report-non-sap', 'ReportsController@ledgerPerformanceReportNonSapView');
Route::post('/ledger-preformance-report-non-sap', 'ReportsController@ledgerPerformanceReportNonSap')
      ->name('ledgerPerformanceReportNonSAP');
Route::get('/meter-reader-preformance-report-non-sap', 'ReportsController@meterReaderPerformanceReportNonSapView');
Route::post('/meter-reader-preformance-report-non-sap', 'ReportsController@meterReaderPerformanceNonSapReport')
      ->name('meterReaderPerformanceNonSapReport');
// Route::get('/meter-reader-preformance-report-non-sap', function () {
//     return view('meter-reader-preformance-report-non-sap');
// });

// SAP Details Required
Route::get('/sap-details-required', 'ReportsController@sapDetailsRequiredForm');

// SAP Details Required
Route::get('/percentage-report-sap', 'ReportsController@percentageReportSapForm');
Route::get('/percentage-report-non-sap', 'ReportsController@percentageReportNonSapForm');
Route::post('/percentage-report-sap', 'ReportsController@percentageReportSapFormPost')
      ->name('percentage-report-sap');

// Mr Doc Number Check
Route::get('/mr-check', 'ReportsController@mrCheck');
// Route::post('/consumer-enquiry-form', 'ReportsController@consumerEnquiry')
//       ->name('consumerEnquiry');



// Auth
Auth::routes();

Route::get('/register-pspcl', function () {
    return view('auth/registerpspcl');
});
Route::post('/employeeRegister', 'Auth\EmployeeRegisterController@register')
        ->name('employeeRegister')
        ->middleware('can:upload-bill');

Route::post('/sbaRegister', 'Auth\EmployeeRegisterController@sbaregister')
        ->name('sbaRegister')
        ->middleware('can:upload-bill');

Route::post('/profile', 'Auth\EmployeeRegisterController@changepassword')
        ->name('changepassword')
        ->middleware('can:upload-bill');

Route::get('/password-reset', function () {
    return view('password_reset');
});
Route::post('/profileForce', 'Auth\EmployeeRegisterController@changepasswordForce')
        ->name('changepasswordForce')
        ->middleware('can:upload-bill');

Route::get('/userSuccess', function () {
    return view('userSuccess');
});
Route::get('/sbaSuccess', function () {
    return view('sbaSuccess');
});
Route::get('/passwordSuccess', function () {
    return view('passwordSuccess');
});
Route::get('/passwordfailed', function () {
    return view('passwordFailed');
});

Route::get('/profile', function () {
    return view('profile');
});

Route::get('/sbaregister-sap', function () {
    return view('auth/sbaregister');
});

Route::get('/sbaregister-non-sap', function () {
    return view('auth/sbaregister-non-sap');
});

Route::get('/sba-register-edit/{id}', 'Auth\EmployeeRegisterController@edit');

Route::post('/sba-register-edit', 'Auth\EmployeeRegisterController@sbaRegisterUpdate')
      ->name('sbaRegisterUpdate');





Route::get('/home', 'HomeController@index')->name('home');


// Reports
Route::get('/pending-bills', 'ReportsController@pendingBills')
        ->middleware('can:upload-bill');
Route::post('/pending-bills', 'ReportsController@pendingBillsSearch')
        ->name('pendingBills')
        ->middleware('can:upload-bill');

Route::get('/mini-ledger', 'ReportsController@miniLedger')
        ->middleware('can:upload-bill');
Route::post('/mini-ledger', 'ReportsController@miniLedgerSearch')
        ->name('miniLedger')
        ->middleware('can:upload-bill');

Route::get('/track-meter-reader-non-sap', 'ReportsController@trackMeterReaderNonSAP')
        ->middleware('can:upload-bill');

Route::get('/track-meter-reader', 'ReportsController@trackMeterReader')
        ->middleware('can:upload-bill');

Route::post('/track-meter-reader', 'ReportsController@trackMeterReaderSearch')
        ->name('trackMeterReaderSearch')
        ->middleware('can:upload-bill');

Route::get('/meter-readers', 'ReportsController@meterReaders')
        ->middleware('can:upload-bill');

Route::get('/meter-readers-sap', 'ReportsController@meterReadersSap')
        ->middleware('can:upload-bill');

Route::get('/meter-reader-performance', 'ReportsController@meterReaderPerformance')
        ->middleware('can:upload-bill');

Route::post('/meter-reader-performance', 'ReportsController@meterReaderPerformanceSearch')
        ->name('meterReaderPerformanceSearch')
        ->middleware('can:upload-bill');

Route::get('/exceptionalReports', 'ReportsController@exceptional');
Route::post('/exceptionalReportsPost', 'ReportsController@exceptionalReportsPost')
      ->name('exceptionalReportsPost');

Route::get('/summaryReports', 'ReportsController@summary');

Route::get('/categoryWiseReports', 'ReportsController@category');

Route::get('/divisionWisePerfomanceReports', 'ReportsController@division');

Route::get('/multipleBillsSameLocation', 'ReportsController@multiple');

Route::get('/viewArrears', 'ReportsController@arrears');


Route::get('/view-billing-data', 'BillingController@viewBillingData')
        ->name('viewBillingData')
        ->middleware('can:upload-bill');
Route::get('/view-billing-data-records/{fileId}', 'BillingController@viewBillingDataRecords')
        ->name('viewBillingDataRecords')
        ->middleware('can:upload-bill');
Route::get('/exportOutputFile', 'BillingController@view')
        ->middleware('can:upload-bill');
Route::post('/export', 'BillingController@exportBillingDataRecords')
        ->name('exportBillingDataRecords')
        ->middleware('can:upload-bill');
Route::get('/exportOutputFileSpotBill', 'BillingController@viewSpotBill')
        ->middleware('can:upload-bill');
Route::post('/exportnew', 'BillingController@exportNewBillingDataRecords')
        ->name('exportOutputFileSpotBill')
        ->middleware('can:upload-bill');

// Comma !
Route::get('/exportOutputFileSpotBillComma', 'BillingController@viewSpotBillComma')
        ->middleware('can:upload-bill');
Route::post('/exportnewComma', 'BillingController@exportNewBillingDataRecordsComma')
        ->name('exportOutputFileSpotBillComma')
        ->middleware('can:upload-bill');

Route::get('/exportOutputFileds', 'BillingController@viewDs')
        ->middleware('can:upload-bill');
Route::post('/exportds', 'BillingController@exportDs')
        ->name('exportDs')
        ->middleware('can:upload-bill');
Route::get('/gtexport', 'BillingController@viewGt')
        ->middleware('can:upload-bill');
Route::post('/exportGt', 'BillingController@exportGt')
        ->name('exportGt')
        ->middleware('can:upload-bill');


// Upload
Route::get('/upload', 'HomeController@upload')
        ->middleware('can:upload-bill');
Route::get('/success', 'HomeController@uploadSuccess')
        ->middleware('can:upload-bill');
Route::post('/upload', 'HomeController@uploadStore')
        ->name('uploadStore')
        ->middleware('can:upload-bill');

Route::post('/uploadSap', 'HomeController@sapDataStore')
        ->name('sapDataStore')
        ->middleware('can:upload-bill');

// Bind meter reader Non SAP
Route::get('/bind', 'BindMeterReaderController@index')
        ->name('bindView')
        ->middleware('can:upload-bill');
Route::post('/bind', 'BindMeterReaderController@store')
        ->name('bindPost')
        ->middleware('can:upload-bill');

// Bind meter reader SAP
Route::get('/bind-sap', 'BindMeterReaderController@sapView')
        ->name('bindView')
        ->middleware('can:upload-bill');
Route::post('/bind-sap', 'BindMeterReaderController@storeSap')
        ->name('bindSapPost')
        ->middleware('can:upload-bill');

// Bind Complete Ledger

// Non Sap
Route::get('/bind-ledger-new', 'BindMeterReaderController@ledgerView')
        ->name('bindLedgerView')
        ->middleware('can:upload-bill');
Route::post('/bindLedger', 'BindMeterReaderController@storeLedger')
        ->name('bindPostLedger')
        ->middleware('can:upload-bill');
Route::get('/bind-ledger', 'BindMeterReaderController@ledgerViewNew')
        ->name('bindLedgerViewNew')
        ->middleware('can:upload-bill');
Route::post('/bindLedgerNew', 'BindMeterReaderController@storeLedgerNew')
        ->name('bindPostLedgerNew')
        ->middleware('can:upload-bill');

// Sap
Route::get('/bind-ledger-sap-new', 'BindMeterReaderController@ledgerViewSap')
    ->name('bindLedgerViewSapNew')
    ->middleware('can:upload-bill');
Route::post('/bindLedgerSapNew', 'BindMeterReaderController@storeLedgerSap')
    ->name('bindPostLedgerSapNew')
    ->middleware('can:upload-bill');
Route::get('/bind-ledger-sap', 'BindMeterReaderController@ledgerViewSapNew')
    ->name('bindLedgerViewSapNew')
    ->middleware('can:upload-bill');
Route::post('/bindLedgerSap', 'BindMeterReaderController@storeLedgerSapNew')
    ->name('bindPostLedgerSapNew')
    ->middleware('can:upload-bill');


Route::get('/bind/success', 'BindMeterReaderController@bindSuccess')
        ->name('bindSuccessView')
        ->middleware('can:upload-bill');


// Bill Delete
    // SAP
Route::get('/sap-bill-delete', 'BillController@sapDeleteFormView')
    ->name('sapDeleteFormView')
    ->middleware('can:upload-bill');
Route::post('/sap-bill-delete-submit', 'BillController@consumerEnquirySap')
      ->name('consumerEnquirySap');
    // Non SAP
Route::get('/non-sap-bill-delete', 'BillController@nonsapDeleteFormView')
    ->name('nonsapDeleteFormView')
    ->middleware('can:upload-bill');
Route::post('/non-sap-bill-delete-submit', 'BillController@consumerEnquiryNonSap')
      ->name('consumerEnquiryNonSap');

// Demo Bill
        Route::get('/bill', 'HomeController@bill')
        ->middleware('can:new-bill');
        Route::get('/search/masterkey/{masterkey}/{status}', 'HomeController@searchView')
        ->middleware('can:new-bill');
        Route::get('/search/masterkey/{masterkey}/{status}/{units}', 'HomeController@searchView')
        ->middleware('can:new-bill');
        Route::post('/bill', 'HomeController@billSearch')
        ->name('billSearch')
        ->middleware('can:new-bill');
        Route::post('/units', 'HomeController@units')
        ->name('units')
        ->middleware('can:new-bill');


//Punching
        Route::get('/batchDataForm', 'BatchDataFormController@batch');
        Route::post('/batchDataForm', 'BatchDataFormController@batchData')
              ->name('batchAdd');
        Route::post('/batchDataFormAjax', 'BatchDataFormController@batchDataAjax')
              ->name('batchAjax');
        Route::get('/advice11-report', 'BatchDataFormController@report');
        Route::post('/advice11-report-post', 'BatchDataFormController@reportPost')
              ->name('reportPost');

        Route::get('/changeOfTariff', 'ChangeOfTariffController@tariff');
        Route::post('/changeOfTariff', 'ChangeOfTariffController@tariffChange')
              ->name('tariff');
        Route::post('/changeOfTariffAjax', 'ChangeOfTariffController@tariffChangeAjax')
              ->name('tariffAjax');

        Route::get('/previousReadingCorrection','PreviousReadingCorrectionController@previous');
        Route::post('/previousReadingCorrection','PreviousReadingCorrectionController@previousReading')
              ->name('previousReading');
        Route::post('/previousReadingAjax', 'PreviousReadingCorrectionController@previousReadingAjax')
              ->name('previousReadingAjax');

        Route::get('/correctionOfMeterParticulars', 'CorrectionOfMeterParticularController@particular');
        Route::post('/correctionOfMeterParticulars', 'CorrectionOfMeterParticularController@store')
              ->name('correctionOfMeterParticulars');
        Route::post('/correctionOfMeterAjax', 'CorrectionOfMeterParticularController@particularDataAjax')
              ->name('correctionOfAjax');

        Route::get('/adviceForDisconnection', 'AdviceForDisconnectionController@advice');
        Route::post('/adviceForDisconnection', 'AdviceForDisconnectionController@store')
              ->name('adviceForDisconnection');
        Route::post('/adviceForDisconnectionAjax', 'AdviceForDisconnectionController@storeAjax')
              ->name('adviceForDisconnectionAjax');

        Route::get('/correctionOfName', 'CorrectionOfNameController@name');
        Route::post('/correctionOfName', 'CorrectionOfNameController@store')
              ->name('correctionOfName');
        Route::post('/correctionOfNameAjax', 'CorrectionOfNameController@storeAjax')
              ->name('correctionOfNameAjax');

        Route::get('/sundryCharges', 'SundryChargeController@sundry');
        Route::post('/sundryCharges', 'SundryChargeController@store')
              ->name('sundryCharges');
        Route::post('/sundryChargesAjax', 'SundryChargeController@storeAjax')
              ->name('sundryChargesAjax');

        Route::get('/changeOfMeter', 'ChangeOfMeterController@meter');
        Route::post('/changeOfMeter', 'ChangeOfMeterController@store')
              ->name('changeOfMeter');
        Route::post('/changeOfMeterAjax', 'ChangeOfMeterController@storeAjax')
              ->name('changeOfMeterAjax');

        Route::get('/sundryAllowance', 'SundryAllowanceController@allowance');
        Route::post('/sundryAllowance', 'SundryAllowanceController@store')
             ->name('sundryAllowance');
        Route::post('/sundryAllowanceAjax', 'SundryAllowanceController@storeAjax')
             ->name('sundryAllowanceAjax');

        Route::get('/changeInLedgerMaster', 'ChangeInLedgerMasterController@ledger');
        Route::post('/changeInLedgerMaster', 'ChangeInLedgerMasterController@store')
             ->name('changeInLedgerMaster');
        Route::post('/changeInLedgerMasterAjax', 'ChangeInLedgerMasterController@storeAjax')
             ->name('changeInLedgerMasterAjax');

        Route::get('/changeOfAcd', 'ChangeOfAcdController@acd');
        Route::post('/changeOfAcd', 'ChangeOfAcdController@store')
            ->name('changeOfAcd');
        Route::post('/changeOfAcdAjax', 'ChangeOfAcdController@storeAjax')
            ->name('changeOfAcdAjax');



        Route::get('/advice63', 'TabAdvice63Controller@posting');
        Route::post('/advice63', 'TabAdvice63Controller@store')
              ->name('aDvice63');
        Route::post('/posthDataAjax63', 'TabAdvice63Controller@posthDataAjax')->name('postAjax63');


              Route::get('/adviceForPsting', 'AdviceForPostingController@posting');
              Route::post('/adviceForPsting', 'AdviceForPostingController@store')
                    ->name('adviceForPosting');
              Route::post('/posthDataAjax', 'AdviceForPostingController@posthDataAjax')
                    ->name('postAjax');

        Route::get('/adviceForCharging', 'TabAdvice96Controller@charging');
        Route::post('/adviceForCharging', 'TabAdvice96Controller@store')
              ->name('adviceForCharging');
        Route::post('/chargeDataAjax', 'TabAdvice96Controller@chargeDataAjax')
               ->name('chargeAjax');

        Route::get('/decrypt', 'Hashing@decrypt');

        // Server Data Transfer
        Route::get('/sap-server-data-transfer', 'dataTransferController@sapTransferView');
        Route::post('/sapDataTransfer', 'dataTransferController@sapDataTransfer')
              ->name('sapDataTransfer');

        // Salary Report
        Route::get('/salary-report-sap-form-view', 'salaryController@sapForm');
        Route::post('/salary-report-sap', 'salaryController@sap')->name('sapSalary');
        Route::get('/salary-report-non-sap-form-view', 'salaryController@nonSapForm');
        Route::post('/salary-report-non-sap', 'salaryController@nonSap')->name('nonSapSalary');


        //Advice 11 edit
        Route::get('/advice-11-report', 'BatchDataFormController@batchDataedit');
        Route::post('/advice-11-generate-report', 'BatchDataFormController@batchDataGenerateReport')->name('advice11GenerateReport');
        Route::get('/batchDataFormEditView/{id}', 'BatchDataFormController@editable');
        Route::post('/batchDataFormEditUpdate', 'BatchDataFormController@update')->name("batchDataFormEditUpdate");
        //Advice 11 Delete
        Route::get('/batchDataFormDeleteView/{id}', 'BatchDataFormController@deletable');
        Route::post('/batchDataFormDeleteView', 'BatchDataFormController@delete')->name("batchDataFormEditDelete");


        //Advice 71 edit
        Route::get('/changeOfTariffEdit', 'ChangeOfTariffController@tarrifEdit');
        Route::post('/advice-71-generate-report', 'ChangeOfTariffController@batchDataGenerateReport')->name('advice71GenerateReport');
        Route::get('/changeOfTariffEditView/{id}', 'ChangeOfTariffController@editable');
        Route::post('/changeOfTariffEditUpdate', 'ChangeOfTariffController@update')->name("changeOfTariffEditUpdate");
        //Advice 71 Delete
        Route::get('/changeOfTarriffDeleteView/{id}', 'ChangeOfTariffController@deletable');
        Route::post('/changeOfTarriffDeleteView', 'ChangeOfTariffController@delete')->name("changeOfTariffEditDelete");
        //Adivice 72 edit
        Route::get('/previousReadingCorrectionEdit', 'PreviousReadingCorrectionController@previousrReadingEdit');
        Route::post('/advice-72-generate-report', 'PreviousReadingCorrectionController@batchDataGenerateReport')->name('advice72GenerateReport');
        Route::get('/previousReadingCorrectionEditView/{id}', 'PreviousReadingCorrectionController@editable');
        Route::post('/previousReadingCorrectionEditUpdate', 'PreviousReadingCorrectionController@update')->name("previousReadingUpdate");

        //Advice 72 delete
        Route::get('/previousReadingCorrectionDeleteView/{id}', 'PreviousReadingCorrectionController@deletable');
        Route::post('/previousReadingCorrectionDeleteView', 'PreviousReadingCorrectionController@delete')->name("previousReadingCorrectionDelete");


        //Advice 73 Edit
        Route::get('/correctionOfMeterParticularsEdit', 'CorrectionOfMeterParticularController@correctionOfMeterEdit');
        Route::post('/advice-73-generate-report', 'CorrectionOfMeterParticularController@batchDataGenerateReport')->name('advice73GenerateReport');
        Route::get('/correctionOfMeterParticularsEditView/{id}', 'CorrectionOfMeterParticularController@editable');
        Route::post('/correctionOfMeterParticularsEditUpdate', 'CorrectionOfMeterParticularController@update')->name("correctionOfMeterParticularsEditUpdate");

        //Adivice 73 Delete
        Route::get('/correctionOfMeterParticularsDeleteView/{id}', 'CorrectionOfMeterParticularController@deletable');
        Route::post('/correctionOfMeterParticularsDeleteView', 'CorrectionOfMeterParticularController@delete')->name("correctionOfMeterParticularsDelete");

        //Adive 74 Edit
        Route::get('/adviceForDisconnectionEdit', 'AdviceForDisconnectionController@adviceForDisconnectionEdit');
        Route::post('/advice-74-generate-report', 'AdviceForDisconnectionController@batchDataGenerateReport')->name('advice74GenerateReport');
        Route::get('/adviceForDisconnectionEdittView/{id}', 'AdviceForDisconnectionController@editable');
        Route::post('/adviceForDisconnectionEditUpdate', 'AdviceForDisconnectionController@update')->name("adviceForDisconnectionEditUpdate");
        //Advice 74 delete
        Route::get('/adviceForDisconnectionDeleteView/{id}', 'AdviceForDisconnectionController@deletable');
        Route::post('/adviceForDisconnectionDeleteView', 'AdviceForDisconnectionController@delete')->name("adviceForDisconnectionDelete");

        //Advice 75 Edit
        Route::get('/correctionOfNameEdit', 'CorrectionOfNameController@correctionOfNameEdit');
        Route::post('/advice-75-generate-report', 'CorrectionOfNameController@batchDataGenerateReport')->name('advice75GenerateReport');
        Route::get('/correctionOfNameEditView/{id}', 'CorrectionOfNameController@editable');
        Route::post('/correctionOfNameEditUpdate', 'CorrectionOfNameController@update')->name("correctionOfNameEditUpdate");

        //Advice 75 delete
        Route::get('/correctionOfNameDeleteView/{id}', 'CorrectionOfNameController@deletable');
        Route::post('/correctionOfNameDeleteView', 'CorrectionOfNameController@delete')->name("correctionOfNameDelete");

        //Advice 76 edit
        Route::get('/sundryChargesEdit', 'SundryChargeController@sundryChargesEdit');
        Route::post('/advice-76-generate-report', 'SundryChargeController@batchDataGenerateReport')->name('advice76GenerateReport');
        Route::get('/sundryChargesEditView/{id}', 'SundryChargeController@editable');
        Route::post('/sundryChargesEditUpdate', 'SundryChargeController@update')->name("sundrychargesEditUpdate");

        //Advice 76 Delete
        Route::get('/sundryChargesDeleteView/{id}', 'SundryChargeController@deletable');
        Route::post('/sundryChargesDeleteView', 'SundryChargeController@delete')->name("sundryChargesDelete");

        //Advice 79 Edit
        Route::get('/changeOfMeterEdit', 'ChangeOfMeterController@changeofMeterEdit');
        Route::post('/advice-79-generate-report', 'ChangeOfMeterController@batchDataGenerateReport')->name('advice79GenerateReport');
        Route::get('/changeOfMeterEditView/{id}', 'ChangeOfMeterController@editable');
        Route::post('/changeOfMeterEditUpdate', 'ChangeOfMeterController@update')->name("changeOfMeterEditUpdate");

        //Advice 79 Delete
        Route::get('/changeOfMeterDeleteView/{id}', 'ChangeOfMeterController@deletable');
        Route::post('/changeOfMeterDeleteView', 'ChangeOfMeterController@delete')->name("changeOfMeterDelete");

        //Advice 86 edit
        Route::get('/sundryAllowanceEdit', 'SundryAllowanceController@sundryallowanceEdit');
        Route::post('/advice-86-generate-report', 'SundryAllowanceController@batchDataGenerateReport')->name('advice86GenerateReport');
        Route::get('/sundryAllowanceEditView/{id}', 'SundryAllowanceController@editable');
        Route::post('/sundryAllowanceEditUpdate', 'SundryAllowanceController@update')->name("sundryAllowanceEditUpdate");

        //Advice 86 delete
        Route::get('/sundryAllowanceDeleteView/{id}', 'SundryAllowanceController@deletable');
        Route::post('/sundryAllowanceDeleteView', 'SundryAllowanceController@delete')->name("sundryAllowanceDelete");

        //Advice 87 edit
        Route::get('/changeInLedgerMastersEdit', 'ChangeInLedgerMasterController@changeInLedgerEdit');
        Route::post('/advice-87-generate-report', 'ChangeInLedgerMasterController@batchDataGenerateReport')->name('advice87GenerateReport');
        Route::get('/changeInLedgerMastersEditView/{id}', 'ChangeInLedgerMasterController@editable');
        Route::post('/changeInLedgerMastersEditUpdate', 'ChangeInLedgerMasterController@update')->name("changeInLedgerMastersEditUpdate");

        //Advice 87 Delete
        Route::get('/changeInLedgerMastersDeleteView/{id}', 'ChangeInLedgerMasterController@deletable');
        Route::post('/changeInLedgerMastersDeleteView', 'ChangeInLedgerMasterController@delete')->name("changeInLedgerMastersDelete");

        //Advice 80 edit
        Route::get('/changeOfAcdEdit', 'ChangeOfAcdController@changeOfAcdEdit');
        Route::post('/advice-80-generate-report', 'ChangeOfAcdController@batchDataGenerateReport')->name('advice80GenerateReport');
        Route::get('/changeOfAcdEditView/{id}', 'ChangeOfAcdController@editable');
        Route::post('/changeOfAcdEditUpdate', 'ChangeOfAcdController@update')->name("changeOfAcdEditUpdate");

        //Advice 80 delete
        Route::get('/changeOfAcdDeleteView/{id}', 'ChangeOfAcdController@deletable');
        Route::post('/changeOfAcdDeleteView', 'ChangeOfAcdController@delete')->name("changeOfAcdDelete");

        //Advice 53
        Route::get('/advice53-edit', 'AdviceForPostingController@adviceForPostingEdit');
        Route::post('/advice-53-generate-report', 'AdviceForPostingController@batchDataGenerateReport')->name('advice53GenerateReport');
        Route::get('/advice53-EditView/{id}', 'AdviceForPostingController@editable');
        Route::post('/advice53-EditUpdate', 'AdviceForPostingController@update')->name("advice53EditUpdate");
        //Advice 53 Delete
        Route::get('/advice53-DeleteView/{id}', 'AdviceForPostingController@deletable');
        Route::post('/advice53-DeleteView', 'AdviceForPostingController@delete')->name("advice53Delete");


        //Advice 63
        Route::get('/advice63-edit', 'TabAdvice63Controller@adviceForPostingEdit');
        Route::post('/advice-63-generate-report', 'TabAdvice63Controller@batchDataGenerateReport')->name('advice63GenerateReport');
        Route::get('/advice63-EditView/{id}', 'TabAdvice63Controller@editable');
        Route::post('/advice63-EditUpdate', 'TabAdvice63Controller@update')->name("advice63EditUpdate");

        //Advice 63 Delete
        Route::get('/advice63-DeleteView/{id}', 'TabAdvice63Controller@deletable');
        Route::post('/advice63-DeleteView', 'TabAdvice63Controller@delete')->name("advice63Delete");



        //Advice 96
        Route::get('/advice96-edit', 'TabAdvice96Controller@adviceForChargingEdit');
        Route::post('/advice-96-generate-report', 'TabAdvice96Controller@batchDataGenerateReport')->name('advice96GenerateReport');
        Route::get('/advice96-EditView/{id}', 'TabAdvice96Controller@editable');
        Route::post('/advice96-EditUpdate', 'TabAdvice96Controller@update')->name("advice96EditUpdate");
        //Advice 96 Delete
        Route::get('/advice96-DeleteView/{id}', 'TabAdvice96Controller@deletable');
        Route::post('/advice96-DeleteView', 'TabAdvice96Controller@delete')->name("advice96Delete");
